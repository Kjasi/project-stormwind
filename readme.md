[![Build status](https://ci.appveyor.com/api/projects/status/q2p8ewr9ead3ohdw?svg=true)](https://ci.appveyor.com/project/Kjasi/project-stormwind)

Project Stormwind is a cross-platform program aimed at being able to extract any and all 3D files from World of Warcraft into a form usable by other 3D software, such as Maya, Max, or Blender, for the purposes of Machinima. Project Stormwind also has a Vulkan display system for previewing loaded files.

# World of Warcraft Compatibility
Project Stormwind is built from the ground up to support virtually every version of World of Warcraft, starting with 1.0.0. (We don't include support for Beta files.) When a new version of WoW comes out, Project Stormwind will still support most of the data, unless there has been a massive change in the file format.

# Download
You can download the latest build [from here](https://ci.appveyor.com/project/Kjasi/project-stormwind/build/artifacts). These files will only last 6 months from when it was last built. A more permanent location will come later.

You will likely only need the requirements file once. (There may be future updates that require new files, but for the most part, you will only need it once.)

Project Stormwind will only be released as a 64-bit program. There will not be a 32-bit version released. You can build a 32-bit release from [the source code](#building-from-source) if you wish.

# Getting the files
In the future, Project Stormwind might be able to access WoW's data directly from the game, but for now, you must use 3rd party software to extract the files out of the game.

For Windows users, I recommend using [MPQ Editor](http://www.zezula.net/en/mpq/download.html) for Vanilla through Mists of Pandaria, and [Casc View](http://www.zezula.net/en/casc/main.html) for Warlords of Draenor+.

# Plugin System
Project Stormwind uses a Plugin system to expand it's exporting capabilities. Users may build and share new plugins as they wish. They may also contact me about including their plugin with Project Stormwind. (Not all plugins will be accepted.)

Several plugins come with Project Stormwind:

### Image Exporting
For exporting textures, ADT heightmaps, masks, and other images.

* PNGs. Simple, universally accepted.
* RAW file format. Also included, RAW 16-bit files (r16) for heightmaps.
* TGA. The defacto standard for 8-bit color images in 3D applications.
* TIFF. A more robust format for even the trickiest of image types.

### Model Exporting
Exports both static meshes, and animated meshes.

* FBX.
* OBJ. Globally accepted for static meshes.

### Placement Exporting
These will export location, rotation, & scale for child models of the currently loaded item. Example: With an ADT loaded, WMOs and models will have their places saved. Similarly, all WMOs will have all their doodads placements exported as well, relative to the WMO itself.

* Unreal Engine 4
* Unity
* CSV (Comma-Separated Values)

# Building from Source
Currently, Project Stormwind is only available on Windows. The included project files are for Visual Studio 2017. Once we start cross-platform building, we will include Mac & Linux build solutions.

### Prerequisites
* [Qt SDK](https://www.qt.io/) (The free Open Source version is fine)
* [Vulkan SDK](https://www.khronos.org/vulkan/)

All other prerequisites are included with the project.

### First Build

1. Install the Prerequisites.
1. Make an environmental variable named `QTDIR` and have it point to your Qt compiler's directory. (Example: For Visual Studio 2017 64-bit builds, it can be set to `E:\Qt\Qt5.12.3\5.12.3\msvc2017_64`.) This will save you some time setting up the project to work on your system.
1. Download, or clone the source code to your local hard-drive.
1. Open `Project Stormwind.sln` with Visual Studio 2017.
1. Make sure your configuration is on `Debug` `x64`!
1. Compile!
1. Go to your `Project Stormwind` directory. Then navigate to `Bin\Debug\x64_v141`.
1. Open `Project Stormwind.exe`.
1. Enjoy!

# License
A license has not yet been chosen for Project Stormwind, but it will be open source license.

# FAQ

1. **Q:** What version of WoW does this work with?  
 **A:** Currently, Project Stormwind is fully compatible with extracted files from Vanilla (1.0.0) to Mists of Pandaria (5.x). Warlords of Draenor (6.x) and Legion (7.x) can work, but some files may not be fully functional, since many aspects of the file formats changed with the introduction of CASC. Battle For Azeroth (8.x) support is being worked on...
1. **Q:** Does this program work with WoW Classic?  
 **A:** Not at the moment. Look for 8.x support, as they use the same data storage system. Once 8.x is fully supported, Classic will be supported as well.
1. **Q:** Do I ***HAVE*** to extract the files?  
 **A:** At the moment, yes. Project Stormwind currently does not support reading the game files directly. We are planning for this support in the future.
1. **Q:** Can I export my WoW character as a model?  
 **A:** Yes and no. While Project Stormwind is designed for exporting files from World of Warcraft, we currently don't have any character-customization support added. You can export the base mesh for your character's race, but it won't be your specific character. We will eventually add the ability to design and export characters, but for now I would recommend downloading WoW Model Viewer and exporting your character from that.
1. **Q:** Haven't I seen you from somewhere? Have you done anything else?  
 **A:** I've helped develop WoW Model Viewer for many years, mostly focusing on the exporting. I started Project Stormwind because I wasn't happy with some of the limiting factors of WMV, especially when it came to ADT and WMO files.
1. **Q:** Do you want to replace WoW Model Viewer with Project Stormwind?  
 **A:** I'd be lying if I said that wouldn't be cool. No, I don't want to replace WMV, I want to see both be used equally. WMV will always be a cutting edge character tool for machinimists, while Project Stormwind is aimed more at people doing their own 3D art or animation.
1. **Q:** Is Project Stormwind compatible with WoW Model Viewer?  
 **A:** Once we add character designing to Project Stormwind, we will do our best to make sure you can import and export your characters to & from WoW Model Viewer, but otherwise, there's not a lot to be compatible with. We read the same files, but what we do with it is very different.