#pragma once

enum ADTFileType
{
  ADTFILETYPE_CORE = 0,
  ADTFILETYPE_TEX,
  ADTFILETYPE_OBJ,
  ADTFILETYPE_LOD,

  ADTFILETYPE_MAX
};

enum LiquidType
{
  LIQUID_TYPE_WATER = 0,
  LIQUID_TYPE_OCEAN,
  LIQUID_TYPE_MAGMA,
  LIQUID_TYPE_SLIME
};