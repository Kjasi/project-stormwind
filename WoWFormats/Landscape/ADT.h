#pragma once
#include <Foundation/Classes/ModelClass.h>
#include <Foundation/Classes/ImageData.h>
#include <Foundation/GlobalEnums.h>
#include "MapTile.h"

class ADT : public ModelClass
{
public:
  ADT(QString filename, bool single_mat = false, bool skip_vulkan = false);
  ~ADT();

  double PointHighest = -99999999999999999.0;
  double PointLowest = 99999999999999999.0;

  QString getCoreName() { return coreFilename; }
  bool isLoaded() { return loaded; }
  bool isObjectBuilt() { return objectBuilt; }
  bool isHeightmapBuilt() { return heightmapBuilt; }
  bool isPlacementBuilt() { return placementBuilt; }
  ADTResolution getHeightmapResolution() { return heightmapResolution; };
  QStringList getTextureList();
  QStringList getWMOModelList();
  QStringList getDoodadModelList();

  void setSingleMaterial(bool setting) { singleMaterial = setting; buildModelObject(); }

  void buildModelObject();
  void buildCollisionObject();
  void buildPlacementData();
  void buildHeightMap(ADTResolution adt_resolution, bool use_middle_point);
  void buildMapTileTexture(uint32_t resolution = 4096);		// Most tiles are 256x256. 256*16 = 4096

  ImageData* getHeightmap() {	return heightmap; };
  ImageData* getMapTexture() { return mapTexture; };
  QList<ImageData> getLayerMasks() { return mapTile->layerMasks; }
  QList<ImageData*> getTextures() { return textureImages; }
  ImageData * getLayerMaskRGB() { return mapTile->layerMaskRGB; }

private:
  bool loaded = false;
  bool objectBuilt = false;
  bool heightmapBuilt = false;
  bool placementBuilt = false;
  bool singleMaterial = false;
  bool builtSingleMaterial = false;
  bool skipVulkan = false;

  int ADTTile_X = 0;
  int ADTTile_Y = 0;

  ADTResolution heightmapResolution;

  QString inputFilename;
  QDir fileDirectory;

  QString coreFilename;			// The main ADT file.
  QString wdtFilename;			// The associated WDT file. Usually the same name as the coreFile, but without the coordinates. Contains some info we need...
  QStringList objFilenames;
  QStringList texFilenames;
  QStringList lodFilenames;
  QList<ImageData*> textureImages;

  Vec3D buildOffset = Vec3D();

  MapTile *mapTile = 0;
  ImageData *heightmap = 0;
  ImageData *mapTexture = 0;

  void findFilenames();
  void updateHighLow();
};