#pragma once

struct MHDR_Chunk
{
  enum MHDRFlags {
    mhdr_MFBO = 1,			// contains a MFBO chunk.
    mhdr_northrend = 2,		// is set for some northrend ones.
  };
  uint32_t flags;
  uint32_t offset_MCIN;
  uint32_t offset_MTEX;
  uint32_t offset_MMDX;
  uint32_t offset_MMID;
  uint32_t offset_MWMO;
  uint32_t offset_MWID;
  uint32_t offset_MDDF;
  uint32_t offset_MODF;
  uint32_t offset_MFBO;			// this is only set if flags & mhdr_MFBO.
  uint32_t offset_MH20;
  uint32_t offset_MTXF;

  uint8_t MAMP_Value;
  uint8_t padding[3];
  uint32_t unused[3];

  void read(FILE *file)
  {
    fread(this, sizeof(MHDR_Chunk), 1, file);
  }
  void read(QByteArray *data)
  {
    *this = *reinterpret_cast<MHDR_Chunk*>(data->data());
  }
};