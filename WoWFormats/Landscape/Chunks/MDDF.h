#pragma once
#include <Foundation/Classes/Vectors.h>
#include <qstring.h>
#include <qlist.h>

enum MDDFFlags
{
  MDDF_Biodome = 0x1,		// this sets internal flags to | 0x800 (WDOODADDEF.var0xC).
  MDDF_Shrubbery = 0x2,	// the actual meaning of these is unknown to me. maybe biodome is for really big M2s. 6.0.1.18179 seems not to check  for this flag

  // Legion+
  MDDF_Unk_4 = 0x4,
  MDDF_Unk_8 = 0x8,
  MDDF_Liquid = 0x20,
  MDDF_EntryIsFileDataID = 0x40,	// nameId is a file data id to directly load
  MDDF_Unk_100 = 0x100,
};

struct MDDF_Chunk
{
public:
  struct MDDF_Data
  {
    uint32_t nameID;				// references an entry in the MMID chunk, specifying the model to use. if flag mddf_entry_is_filedata_id is set, a file data id instead, ignoring MMID.
    uint32_t uniqueID;			// this ID should be unique for all ADTs currently loaded. Best, they are unique for the whole map. Blizzard has these unique for the whole game.
    Vec3F position;				// This is relative to a corner of the map. Subtract 17066 from the non vertical values and you should start to see something that makes sense. You'll then likely have to negate one of the non vertical values in whatever coordinate system you're using to finally move it into place.
    Vec3F rotation;				// Degrees. This is not the same coordinate system orientation like the ADT itself! (see history.)
    uint16_t scale;				// 1024 is the default size equaling 1.0f.
    uint16_t flags;				// values from enum MDDFFlags.
  };

  QList<MDDF_Data> *data;

  ~MDDF_Chunk() { delete data; }

  void read(FILE * file, size_t size)
  {
    data = new QList<MDDF_Data>;

    size_t bytes = 0;
    while (bytes < size)
    {
      MDDF_Data a;
      fread(&a, sizeof(MDDF_Data), 1, file);
      data->push_back(a);
      bytes += sizeof(MDDF_Data);
    }
  }

  void read(QByteArray *fileData)
  {
    data = new QList<MDDF_Data>;

    for (uint32_t i = 0; i < fileData->size() / sizeof(MDDF_Data); i++)
    {
      MDDF_Data d = MDDF_Data();
      d = *reinterpret_cast<MDDF_Data*>(fileData->mid((sizeof(MDDF_Data) * i), sizeof(MDDF_Data)).data());
      data->push_back(d);
    }
  }
};
