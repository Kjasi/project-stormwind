#pragma once
#include <qlist.h>

class MCNR_Chunk
{
private:
  struct MCNREntry
  {
    int8_t normal[3];
    Vec3D toVec3D()
    {
      Vec3D v;
      v.x = (double)normal[0] / 127.5;
      v.y = (double)normal[1] / 127.5;
      v.z = (double)normal[2] / 127.5;

      return v;
    }
  };
public:
  QList<double> NormalVectors;

  void read(FILE* file)
  {
    for (uint32_t i = 0; i < 145; i++)
    {
      MCNREntry a;
      fread(&a, sizeof(MCNREntry), 1, file);
      Vec3D v = a.toVec3D();

      // Correct Normals
      v.SwapYZ();
      v.SwapXZ();
      v.InvertZ();
      v.InvertX();

      NormalVectors.push_back(v.x);
      NormalVectors.push_back(v.y);
      NormalVectors.push_back(v.z);

      //LOGFILE.Debug("Normal Vectors for Point %i: %f %f %f", i, v.x, v.y, v.z);
    }
  }

  void read(QByteArray *fileData)
  {
    for (uint32_t i = 0; i < 145; i++)
    {
      MCNREntry a = *reinterpret_cast<MCNREntry*>(fileData->mid(i * sizeof(MCNREntry)).data());
      Vec3D v = a.toVec3D();

      // Correct Normals
      v.SwapYZ();
      v.SwapXZ();
      v.InvertZ();
      v.InvertX();

      NormalVectors.push_back(v.x);
      NormalVectors.push_back(v.y);
      NormalVectors.push_back(v.z);

      //LOGFILE.Debug("Normal Vectors for Point %i: %f %f %f", i, v.x, v.y, v.z);
    }
  }
};