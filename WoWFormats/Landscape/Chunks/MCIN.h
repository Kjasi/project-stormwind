#pragma once
#include <Foundation/Functions.h>
#include <QList>

struct MCIN_Data
{
  uint32_t offset;
  uint32_t size;
  uint32_t flags;
  char pad[4];
};

class MCIN_Chunk
{
public:
  QList<MCIN_Data> *data;

  MCIN_Chunk() { data = new QList<MCIN_Data>(); };
  ~MCIN_Chunk() { delete data; }

  void readAllChunks(FILE *file)
  {
    data = new QList<MCIN_Data>();
    for (uint32_t i = 0; i < 256; i++)
    {
      MCIN_Data d = MCIN_Data();
      fread(&d, sizeof(MCIN_Data), 1, file);
      data->push_back(d);
    }
  }

  void read(QByteArray *fileData)
  {
    data = new QList<MCIN_Data>();
    for (uint32_t i = 0; i < 256; i++)
    {
      MCIN_Data d = MCIN_Data();
      d.offset = getQBANumber<uint32_t>(fileData->mid((sizeof(MCIN_Data) * i) + 0, sizeof(uint32_t)));
      d.size = getQBANumber<uint32_t>(fileData->mid((sizeof(MCIN_Data) * i) + 4, sizeof(uint32_t)));
      d.flags = getQBANumber<uint32_t>(fileData->mid((sizeof(MCIN_Data) * i) + 8, sizeof(uint32_t)));
      for (uint32_t j = 0; j < 4; j++)
      {
        d.pad[j] = fileData->mid((sizeof(MCIN_Data) * i) + 12 + (j * sizeof(char)), sizeof(char)).toShort();
      }
      data->push_back(d);
    }
  }
};