#pragma once
#include <Foundation/Functions.h>

struct MPHD_Chunk
{
  enum MPHD_Flags
  {
    wdt_uses_global_map_obj = 0x0001, // Use global map object definition.
    adt_has_mccv = 0x0002, // >= Wrath adds color: ADT.MCNK.MCCV. with this flag every ADT in the map _must_ have MCCV chunk at least with default values, else only base texture layer is rendered on such ADTs.
    adt_has_big_alpha = 0x0004, // shader = 2. Decides whether to use _env terrain shaders or not: funky and if MCAL has 4096 instead of 2048(?)
    adt_has_doodadrefs_sorted_by_size_cat = 0x0008, // if enabled, the ADT's MCRF(m2 only)/MCRD chunks need to be sorted by size category
    adt_has_mclv = 0x0010, // >= Cata adds second color: ADT.MCNK.MCLV
    adt_has_upside_down_ground = 0x0020, // >= Cata Flips the ground display upside down to create a ceiling
    unk_0x0040 = 0x0040, // >= Mists ??? -- Only found on Firelands2.wdt (but only since MoP) before Legion
    adt_has_height_texturing = 0x0080, // >= Mists shader = 6. Decides whether to influence alpha maps by _h+MTXP: (without with)
    // also changes MCAL size to 4096 for uncompressed entries
    unk_0x0100 = 0x0100, // >= Legion implicitly sets 0x8000
    unk_0x0200 = 0x0200,
    unk_0x0400 = 0x0400,
    unk_0x0800 = 0x0800,
    unk_0x1000 = 0x1000,
    unk_0x2000 = 0x2000,
    unk_0x4000 = 0x4000,
    unk_0x8000 = 0x8000, // >= Legion implicitly set for map ids 0, 1, 571, 870, 1116 (continents). Affects the rendering of _lod.adt

    mask_vertex_buffer_format = adt_has_mccv | adt_has_mclv,                    // CMap::LoadWdt
    mask_render_chunk_something = adt_has_height_texturing | adt_has_big_alpha,   // CMapArea::PrepareRenderChunk, CMapChunk::ProcessIffChunks
  };

  uint32_t flags;
  uint32_t something;
  uint32_t unused[6];

  int getMCalSize() { return (flags & mask_render_chunk_something ? 4096 : 2048); }

  void read(QByteArray *data)
  {
    flags = getQBANumber<uint32_t>(data->mid(0, sizeof(uint32_t)));
    something = getQBANumber<uint32_t>(data->mid(4, sizeof(uint32_t)));

    for (uint32_t i = 0; i < 6; i++)
    {
      unused[i] = getQBANumber<uint32_t>(data->mid(8 + (4 * i), sizeof(uint32_t)));
    }
  }
};