#pragma once
#include <Foundation/Functions.h>

struct LIQVertex
{
  uint16_t Unk1;
  uint16_t Unk2;
  float Height;
};

struct MCLQ_Chunk
{
public:
  QList<LIQVertex> HeightValues;
  QList<int> Flags;
  double LiquidHeightMin = 99999999999999999.0;
  double LiquidHeightMax = -99999999999999999.0;

  void read(FILE *file)
  {
    fpos_t fPos;
    fgetpos(file, &fPos);
    char str[5];
    fread(str, 1, 4, file);
    flipcc(str);
    str[4] = 0;

    if (strncmp(str, "MCSE", 4) == 0)
    {
      int size;
      fread(&size, 4, 1, file);
      if (size == 0)
      {
        LOGFILE.Warn("Empty MCLQ detected. Reading chunk aborted...");
        return;
      }
    }
    fseek(file, fPos, SEEK_SET);

    //LOGFILE.Info("Reading MCLQ Chunk...");
    fread(&LiquidHeightMin, sizeof(float), 1, file);
    fread(&LiquidHeightMax, sizeof(float), 1, file);

    //LOGFILE.Debug("Reading Liquid Height Values...");
    for (int i = 0; i < (9 * 9); i++) {
      LIQVertex a;
      fread(&a, sizeof(LIQVertex), 1, file);
      //LOGFILE.Debug("Liquid Height Values for %i/%i: %f, Unknowns: %i, %i", i, (9 * 9)-1, a.Height, a.Unk1, a.Unk2);
      HeightValues.push_back(a);
    }
    //LOGFILE.Debug("Reading Liquid Flag Values...");
    for (int i = 0; i < (8 * 8); i++) {
      uint8_t a;
      fread(&a, sizeof(uint8_t), 1, file);
      //LOGFILE.Debug("Liquid Flag Values for %i/%i: 0x%02X (%i)", i, ((8 * 8) - 1), a, a);
      Flags.push_back(a);
    }
    //LOGFILE.Info("Finished reading MCLQ Chunk!");
  }

  void read(QByteArray *fileData)
  {

  }
};