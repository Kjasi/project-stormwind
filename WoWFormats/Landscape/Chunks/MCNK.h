#pragma once
#include <Foundation/Classes/Vectors.h>

struct MCNK_Header
{
  struct
  {
    uint32_t has_mcsh : 1;
    uint32_t impass : 1;
    uint32_t lq_river : 1;
    uint32_t lq_ocean : 1;
    uint32_t lq_magma : 1;
    uint32_t lq_slime : 1;
    uint32_t has_mccv : 1;
    uint32_t unknown_0x80 : 1;
    uint32_t : 7;                             // not set in 6.2.0.20338
    uint32_t do_not_fix_alpha_map : 1;        // "fix" alpha maps in MCAL (4 bit alpha maps are 63*63 instead of 64*64). Note that this also means that it *has* to be 4 bit alpha maps, otherwise UnpackAlphaShadowBits will assert.
    uint32_t high_res_holes : 1;              // Since ~5.3 WoW uses full 64-bit to store holes for each tile if this flag is set.
    uint32_t : 15;                            // not set in 6.2.0.20338
  } flags;
  uint32_t IndexX = 0;
  uint32_t IndexY = 0;
  uint32_t nLayers = 1;
  uint32_t nDoodadRefs = 0;
  union
  {
    struct offsets
    {
      uint32_t offsHeight; // MCVT
      uint32_t offsNormal; // MCNR
    } offsetHeightNormal;
    uint8_t Holes_HighRes[8];							// only used with flags.high_res_holes
  };
  uint32_t offsLayer;		// MCLY
  uint32_t offsRefs;		// MCRF
  uint32_t offsAlpha;		// MCAL
  uint32_t sizeAlpha;
  uint32_t offsShadow;		// MCSH
  uint32_t sizeShadow;
  uint32_t areaid;
  uint32_t nMapObjRefs = 0;
  uint16_t Holes_LowRes;
  uint16_t Holes_Align;		// in alpha: padding
  uint16_t ReallyLowQualityTextureingMap[8]; // UINT2[8][8], "predTex", It is used to determine which detail doodads to show. Values are an array of two bit unsigned integers, naming the layer.
  uint64_t noEffectDoodad;    // WoD: may be an explicit MCDD chunk
  uint32_t offsSndEmitters;	// Offset to MCSE (Sound Emitter) chunk
  uint32_t nSndEmitters;		// will be set to 0 in the client if ofsSndEmitters doesn't point to MCSE!
  uint32_t offsLiquid;		// Offset to MCLQ, if present.
  uint32_t sizeLiquid;		// 8 when not used; only read if >8.

  // in alpha: remainder is padding but unused.

  Vec3F Position = Vec3F();	// Actually read in -Y, Z, -X order. (Fixed in read function)
  uint32_t ofsMCCV;			// only with flags.has_mccv, had uint32_t textureId; in ObscuR's structure.
  uint32_t ofsMCLV;			// introduced in Cataclysm
  uint32_t unused;			// currently unused

  void read(QByteArray *data)
  {
    *this = *reinterpret_cast<MCNK_Header*>(data->data());
    Vec3F a;
    a.x = -Position.y;
    a.y = Position.z;
    a.z = -Position.x;
    Position = a;
  }
};