#pragma once
#include <WoWFormats/CommonClasses.h>

enum MODFFlags {
  modf_destroyable = 1,         // set for destroyable buildings like the tower in DeathknightStart. This makes it a server-controllable game object.
  modf_use_lod = 2,             // WoD(?)+: also load _LOD1.WMO for use dependent on distance
  modf_unk_has_scale = 4,       // Legion+: if this flag is set then use scale = scale / 1024, otherwise scale is 1.0
  modf_entry_is_filedata_id = 8 // Legion+: nameId is a file data id to directly load //SMMapObjDef::FLAG_FILEDATAID
};

struct MODF_Chunk
{
public:
  struct MODF_Data
  {
    uint32_t nameID;              // References an entry in the MWMO chunk, specifying the model to use. (Technically points to MWID, which points back to MWMO)
    uint32_t uniqueID;            // this ID should be unique for all ADTs currently loaded. Best, they are unique for the whole map.
    Vec3F position;
    Vec3F rotation;				// degrees.
    CAaBox extents;             // position plus the transformed wmo bounding box. used for defining if they are rendered as well as collision.
    uint16_t flags;               // values from enum MODFFlags.
    uint16_t doodadSet;           // which WMO doodad set is used.
    uint16_t nameSet;             // which WMO name set is used. Used for renaming goldshire inn to northshire inn while using the same model.
    uint16_t scale;               // Legion+: scale, 1024 means 1 (same as MDDF). Padding since 0.5.3 alpha.
  };

  QList<MODF_Data> *data;
  
  void read(QByteArray *fileData)
  {
    data = new QList<MODF_Data>;

    for (uint32_t i = 0; i < fileData->size() / sizeof(MODF_Data); i++)
    {
      MODF_Data d = MODF_Data();
      d = *reinterpret_cast<MODF_Data*>(fileData->mid((sizeof(MODF_Data) * i), sizeof(MODF_Data)).data());

      data->push_back(d);
    }
  }
};