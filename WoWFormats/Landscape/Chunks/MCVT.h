#pragma once
#include <Foundation/Functions.h>

class MCVT_Chunk
{
public:
  QList<double> *HeightData = NULL;

  MCVT_Chunk() {}
  ~MCVT_Chunk() { delete HeightData; }

  void read(FILE *file)
  {
    // LOGFILE.Debug("Height Lines:");
    HeightData = new QList<double>();
    for (size_t x = 0; x < 145; x++) {
      float h = 0.0f;
      size_t result = fread(&h, 1, 4, file);
      if (result != 4) {
        LOGFILE.Error("An error occured reading the height data for line %i.", x);
      }
      else {
        // LOGFILE.Debug("  Line: %d, Value: %f", x, h);
        HeightData->push_back(h);
      }
    }
    // LOGFILE.Debug("Finished Read. HeightData Size: %i", HeightData->count());
  }

  void read(QByteArray *fileData)
  {
    // LOGFILE.Debug("Height Lines:");
    HeightData = new QList<double>();
    for (uint32_t x = 0; x < 145; x++) {
      double h = getQBANumber<double>(fileData->mid(x*(sizeof(float)), sizeof(float)));
      HeightData->push_back(h);
    }
    // LOGFILE.Debug("Finished Read. HeightData Size: %i", HeightData->count());
  }
};