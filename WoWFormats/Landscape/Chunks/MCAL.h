#pragma once
#include "MCLY.h"
#include "MPHD.h"
#include <qbytearray.h>

struct MCAL_RawData
{
  uint8_t rawData[4096];

  void clear()
  {
    for (uint32_t i; i < 4096; i++)
    {
      rawData[i] = 0;
    }
  }
  void clearWhite()
  {
    for (uint32_t i = 0; i < 4096; i++)
    {
      rawData[i] = 255;
    }
  }
};

typedef union Split2048
{
  uint8_t value;
  struct {
    uint8_t first : 4;
    uint8_t second : 4;
  };
} Split2048;

struct MCAL_CompressedData
{
  enum class mode_t
  {
    copy = 0,              // append value[0..count - 1]
    fill = 1,              // append value[0] count times
  };
  uint8_t count : 7;
  uint8_t mode : 1;
  uint8_t *value;
};

struct MCAL_Data
{
  MCAL_RawData alpha;

  void init() {
    alpha.clearWhite();
  }

  // Read uncompressed 4096 values
  int read4096(QByteArray *data)
  {
    init();
    //LOGFILE.Debug("Reading MCAL_Data (4096)...");
    for (uint32_t i = 0; i < 4096; i++)
    {
      alpha.rawData[i] = getQBANumber<uint8_t>(data->mid(i, sizeof(uint8_t)));
    }
    return 4096;
  }

  // Read uncompressed 2048 values
  int read2048(QByteArray *data)
  {
    init();
    //LOGFILE.Debug("Reading MCAL_Data (2048)...");

    for (uint32_t i = 0; i < 2048; ++i)
    {
      Split2048 number;

      auto c = static_cast<uint8_t>(data->operator[](i).operator char());
      number.value = c;

      uint8_t first = number.first * 17;		// 0-F = 0-15 (16 values) 15*17 = 255, the max color value on a 0-255 scale.
      uint8_t second = number.second * 17;

      // LOGFILE.Debug("i: %i, raw: %c, Value: %i, First: %i, Second: %i", i, c, number.value, first, second);
      alpha.rawData[(i * 2) + 0] = first;
      alpha.rawData[(i * 2) + 1] = second;
    }
    return 2048;
  }

  int readCompressed(QByteArray *data)
  {
    init();
    //LOGFILE.Debug("Reading MCAL_Data (Compressed)...");

    uint32_t inOffset = 0;
    uint32_t outOffset = 0;
    while (outOffset < 4096 && inOffset < (uint32_t)data->count())
    {
      uint8_t info = getQBANumber<uint8_t>(data->mid(inOffset, sizeof(uint8_t)));
      ++inOffset;

      auto mode = (uint32_t)(info & 0x80) >> 7; // 0 = copy, 1 = fill
      auto count = (uint32_t)(info & 0x7f); // do mode operation count times
      //LOGFILE.Debug("Mode: %i, Count: %i", mode, count);

      if (mode == 0)
      {
        while (count-- > 0 && outOffset < 4096)
        {
          auto val = getQBANumber<uint8_t>(data->mid(inOffset, sizeof(uint8_t)));
          ++inOffset;
          alpha.rawData[outOffset] = val;
          ++outOffset;
        }
      }
      else // mode == 1
      {
        auto val = getQBANumber<uint8_t>(data->mid(inOffset, sizeof(uint8_t)));
        ++inOffset;
        while (count-- > 0 && outOffset < 4096)
        {
          alpha.rawData[outOffset] = val;
          ++outOffset;
        }
      }
    }

    //LOGFILE.Debug("Compressed Data successfully read.");
    return inOffset;
  }
};

class MCAL_Layer
{
public:
  enum Alpha_Type
  {
    UNCOMPRESSED_2048 = 0,
    UNCOMPRESSED_4096,
    COMPRESSED,

    ALPHATYPEMAX
  };

  MCAL_Data *rawData = nullptr;

  void setAlphaType(Alpha_Type type)
  {
    alphaType = type;
  }
  
  int read(QByteArray *data)
  {
    int bytesRead = 0;
    rawData = new MCAL_Data;
    switch (alphaType)
    {
    case MCAL_Layer::UNCOMPRESSED_2048:
      bytesRead = rawData->read2048(data);
      break;
    case MCAL_Layer::UNCOMPRESSED_4096:
      bytesRead = rawData->read4096(data);
      break;
    case MCAL_Layer::COMPRESSED:
      bytesRead = rawData->readCompressed(data);
      break;
    default:
      LOGFILE.Error("Invalid MCAL compression type. Unable to read data!");
      break;
    }
    return bytesRead;
  }
  int read(QByteArray *data, Alpha_Type alpha_type) {
    setAlphaType(alpha_type);
    return read(data);
  }
private:
  Alpha_Type alphaType = ALPHATYPEMAX;
};

class MCAL_Chunk
{
public:
  QList<MCAL_RawData> dataLayers;

  void read(QByteArray *data, MCNK_Header *header, QList<MCLY_Data>* mcly, MPHD_Chunk *mphd)
  {
    MCAL_RawData layer0;
    layer0.clearWhite();
    dataLayers.push_back(layer0);

    int bytesRead = 0;
    for (uint32_t i = 1; i < header->nLayers; i++)
    {
      MCAL_Layer a;
      if (mcly->at(i).flags.alpha_map_compressed == true)
      {
        int remainer = data->count() - bytesRead;
        bytesRead += a.read(&data->mid(bytesRead, remainer), MCAL_Layer::COMPRESSED);
      }
      else if (mphd->flags & mphd->adt_has_big_alpha || mphd->flags & mphd->adt_has_height_texturing)
      {
        a.read(&data->mid((i - 1) * 4096, 4096), MCAL_Layer::UNCOMPRESSED_4096);
      }
      else {
        a.read(&data->mid((i - 1) * 2048, 2048), MCAL_Layer::UNCOMPRESSED_2048);
      }
      dataLayers.push_back(a.rawData->alpha);
    }
  }
};