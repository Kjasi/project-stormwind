#include <Foundation/Functions.h>
#include <Foundation/Classes.h>
#include <Foundation/Classes/Lerp.h>
#include <ProjectStormwind/Utilities/common.h>
#include <ProjectStormwind/Utilities/buildADTChunk.h>

#include "WoWFormats/Defines.h"
#include "ADT.h"
#include "WDT.h"
#include "Textures/BLP.h"

#include <qdir.h>
#include <qregularexpression.h>
#include <qqueue.h>
#include <qthreadpool.h>

int HoleTab_H[4] = { 0x1111, 0x2222, 0x4444, 0x8888 };
int HoleTab_V[4] = { 0x000F, 0x00F0, 0x0F00, 0xF000 };

bool isHole(size_t holes, size_t h, size_t v) {
  return (holes & HoleTab_H[h] & HoleTab_V[v]) != 0;
}

ADT::ADT(QString filename, bool single_mat, bool skip_vulkan)
  :inputFilename(filename), singleMaterial(single_mat), skipVulkan(skip_vulkan)
{
  LOGFILE.Info("Starting ADT File reading...");
  double time_startf = getCurrentTime();
  LOGFILE.Debug("Start Time: %f", time_startf);

  double findNamesStart = getCurrentTime();
  findFilenames();
  double findNamesEnd = getCurrentTime();

  double checkFilesStart = getCurrentTime();
  // Check the core file
  if (coreFilename.isEmpty())
  {
    LOGFILE.Error("Unable to find core ADT file, as the filename is empty. Aborting read...");
    return;
  }
  LOGFILE.Debug("coreFilename: %s", qPrintable(coreFilename));

  // Check to see if the file exists...
  if (QFile::exists(coreFilename) == false)
  {
    LOGFILE.Error("Unable to find core ADT file. The file does not exist. Aborting read...");
    return;
  }

  // Check the WDT file
  if (wdtFilename.isEmpty())
  {
    LOGFILE.Error("Unable to find WDT file, as the filename is empty. Aborting read...");
    return;
  }
  LOGFILE.Debug("wdtFilename: %s", qPrintable(wdtFilename));

  if (objFilenames.count() > 0)
  {
    for (int i = 0; i < objFilenames.count(); i++)
    {
      LOGFILE.Debug("Object Filename: %s", qPrintable(objFilenames.at(i)));
    }
  }
  if (texFilenames.count() > 0)
  {
    for (int i = 0; i < texFilenames.count(); i++)
    {
      LOGFILE.Debug("Texture Filename: %s", qPrintable(texFilenames.at(i)));
    }
  }
  if (lodFilenames.count() > 0)
  {
    for (int i = 0; i < lodFilenames.count(); i++)
    {
      LOGFILE.Debug("LOD Filename: %s", qPrintable(lodFilenames.at(i)));
    }
  }
  double checkFilesEnd = getCurrentTime();

  double WDTStart = getCurrentTime();
  // Get Header for WDT file
  WDT wdt(wdtFilename);
  double WDTEnd = getCurrentTime();

  double readStart = getCurrentTime();
  // At this point, we definely have some data to go with!
  mapTile = new MapTile(coreFilename, fileDirectory, wdt.getHeader(), objFilenames, texFilenames, lodFilenames);
  mapTile->read();
  double readEnd = getCurrentTime();

  double buildMasksStart = getCurrentTime();
  mapTile->buildLayerMasks();
  double buildMasksEnd = getCurrentTime();
  LOGFILE.Debug("MapTile Layer Masks built in %.3f", (buildMasksEnd - buildMasksStart));

  double loadTexturesStart = getCurrentTime();
  QStringList textureNames = getTextureList();
  for (int i = 0; i < textureNames.count(); i++)
  {
    BLPInfo *blpInfo = ReadBLPFile(WoWDir.absoluteFilePath(textureNames.at(i)));
    textureImages.push_back(blpInfo->getImageData());
  }
  double loadTexturesEnd = getCurrentTime();

  LOGFILE.Debug("Building Map Tile Texture...");
  double BuildMapTileTextureStart = getCurrentTime();
  //buildMapTileTexture();
  double BuildMapTileTextureEnd = getCurrentTime();
  LOGFILE.Debug("Map Tile Texture built in %.3f", (BuildMapTileTextureEnd - BuildMapTileTextureStart));

  LOGFILE.Debug("Building Model Object...");
  double buildStart = getCurrentTime();
  buildModelObject();
  double buildEnd = getCurrentTime();
  LOGFILE.Debug("Model Object Built. Checking validity...");

  if (modelObject == NULL || objectBuilt == false)
  {
    LOGFILE.Error("Error building ADT file.");
    return;
  }
  LOGFILE.Debug("Build Completed in: %.3f", (buildEnd - buildStart));

  double highlowStart = getCurrentTime();
  updateHighLow();
  double highlowEnd = getCurrentTime();

  double time_endf = getCurrentTime();
  LOGFILE.Debug("End Time: %.3f", time_endf);
  LOGFILE.Info("Read ADT file successfully in %.3f seconds. Find Names: %.3f, Check Files: %.3f, WDT: %.3f, Read Time: %.3f, Layer Masks Build Time: %.3f, Texture Load Time: %.3f, Build Maptile Texture Time: %.3f, BuildTime: %.3f, HighLow: %.3f\n", (time_endf-time_startf), (findNamesEnd-findNamesStart), (checkFilesEnd-checkFilesStart), (WDTEnd-WDTStart), (readEnd-readStart), (buildMasksEnd-buildMasksStart), (loadTexturesEnd-loadTexturesStart), (BuildMapTileTextureEnd-BuildMapTileTextureStart), (buildEnd-buildStart), (highlowEnd - highlowStart));
  loaded = true;
}

ADT::~ADT()
{
  delete mapTile;
  delete heightmap;
  delete mapTexture;
}

// Find the all the associated filenames, based on the inputFilename.
void ADT::findFilenames()
{
  if (inputFilename.isEmpty())
  {
    LOGFILE.Error("Invalid or empty input filename. Can not determine other ADT file's names.");
    return;
  }

  fileDirectory.setPath(inputFilename.mid(0, inputFilename.lastIndexOf("/")));
  QString justFilename = inputFilename.mid(inputFilename.lastIndexOf("/")+1);

  QRegularExpression reg("(.+)_(\\d+)_(\\d+)(.*)\\.adt$", QRegularExpression::CaseInsensitiveOption);
  QRegularExpressionMatch match = reg.match(justFilename);

  if (match.hasMatch() == false)
  {
    LOGFILE.Error("Inputted filename did not match any known filename structures for ADT files.");
    return;
  }

  QString rawName = match.captured(1);
  ADTTile_X = match.captured(2).toInt();
  ADTTile_Y = match.captured(3).toInt();

  LOGFILE.Debug("Matched Results: %s, rawName: %s, xTile: %i, yTile: %i, Suffix: %s", qPrintable(match.captured(0)), qPrintable(rawName), ADTTile_X, ADTTile_Y, qPrintable(match.captured(4)));

  {
    QString fname = fileDirectory.absoluteFilePath(QString("%1_%2_%3.adt").arg(rawName).arg(ADTTile_X).arg(ADTTile_Y));
    LOGFILE.Debug("Looking for core file: %s", qPrintable(fname));
    QFile fpo(fname);
    if (fpo.open(QIODevice::ReadOnly))
    {
      fpo.close();
      coreFilename = fileDirectory.absoluteFilePath(QString("%1_%2_%3.adt").arg(rawName).arg(ADTTile_X).arg(ADTTile_Y));
    }
    fpo.deleteLater();
  }
  {
    QFile fpo(fileDirectory.absoluteFilePath(QString("%1.wdt").arg(rawName)));
    if (fpo.open(QIODevice::ReadOnly))
    {
      fpo.close();
      wdtFilename = fileDirectory.absoluteFilePath(QString("%1.wdt").arg(rawName));
    }
    fpo.deleteLater();
  }

  QStringList filters;
  filters << QString("%1_%2_%3_*.adt").arg(rawName).arg(ADTTile_X).arg(ADTTile_Y);

  QStringList listToCheck;
  QStringList files = fileDirectory.entryList(filters, QDir::Files);

  LOGFILE.Debug("Num Files Found: %i", files.count());
  for (uint32_t i = 0; i < (uint32_t)files.count(); i++)
  {
    QString fname = fileDirectory.absoluteFilePath(files.at(i));
    if (listToCheck.contains(fname) == true)
    {
      continue;
    }
    listToCheck.push_back(fname);
  }

  // Check to make sure we can actually open the files...
  for (int i = 0; i < listToCheck.count(); i++)
  {
    QString fn = listToCheck.at(i);

    QFile fpo(fn);
    if (fpo.open(QIODevice::ReadOnly))
    {
      fpo.close();

      if (fn.contains("_obj"))
      {
        objFilenames.push_back(fn);
      }
      else if (fn.contains("_tex"))
      {
        texFilenames.push_back(fn);
      }
      else if (fn.contains("_lod"))
      {
        lodFilenames.push_back(fn);
      }
    }
    fpo.deleteLater();
  }
}

void ADT::updateHighLow()
{
  if (objectBuilt == false)
  {
    buildModelObject();
  }

  PointHighest = modelObject->boundingBox.max.y;
  PointLowest = modelObject->boundingBox.min.y;
}

QStringList ADT::getTextureList()
{
  QStringList list;
  if (mapTile->MTEXChunk->strings->count() > 0)
  {
    list.append(*mapTile->MTEXChunk->strings);
    list.sort();
  }
  else
  {
    QStringList diffuseList;
    QStringList heightList;
    if (mapTile->MDIDChunk != NULL && mapTile->MDIDChunk->Indexs->count() > 0)
    {
      for (int i = 0; i < mapTile->MDIDChunk->Indexs->count(); i++)
      {
        diffuseList.push_back(QString("Diffuse File ID %1").arg(mapTile->MDIDChunk->Indexs->at(i)));
      }
      diffuseList.sort();
      list.append(diffuseList);
    }
    if (mapTile->MHIDChunk != NULL && mapTile->MHIDChunk->Indexs->count() > 0)
    {
      for (int i = 0; i < mapTile->MHIDChunk->Indexs->count(); i++)
      {
        list.push_back(QString("Height File ID %1").arg(mapTile->MHIDChunk->Indexs->at(i)));
      }
      heightList.sort();
      list.append(heightList);
    }
  }
  return list;
}

QStringList ADT::getWMOModelList()
{
  QStringList list;
  if (mapTile->MWMOChunk->strings->count() > 0)
  {
    list.append(*mapTile->MWMOChunk->strings);
  }
  else if (mapTile->MWIDChunk != NULL && mapTile->MWIDChunk->Indexs->count() > 0)
  {
    for (int i = 0; i < mapTile->MWIDChunk->Indexs->count(); i++)
    {
      list.push_back(QString("WMO File ID %1").arg(mapTile->MWIDChunk->Indexs->at(i)));
    }
  }
  list.sort();
  return list;
}

QStringList ADT::getDoodadModelList()
{
  QStringList list;
  if (mapTile->MMDXChunk->strings->count() > 0)
  {
    list.append(*mapTile->MMDXChunk->strings);
  }
  else if (mapTile->MMIDChunk != NULL && mapTile->MMIDChunk->Indexs->count() > 0)
  {
    for (int i = 0; i < mapTile->MMIDChunk->Indexs->count(); i++)
    {
      list.push_back(QString("M2 File ID %1").arg(mapTile->MMIDChunk->Indexs->at(i)));
    }
  }
  list.sort();
  return list;
}

void ADT::buildModelObject()
{
  LOGFILE.Info("Building ADT Model Object...");
  if (modelObject != NULL && objectBuilt == true && builtSingleMaterial == singleMaterial) return;

  if (modelScene == nullptr) modelScene = new ModelScene;
  modelScene->Name = inputFilename.mid(inputFilename.lastIndexOf("/") + 1);

  modelObject = new ModelObject;
  modelObject->objectName = inputFilename.mid(inputFilename.lastIndexOf("/") + 1);
  modelObject->objectName.append("Mesh");
  ModelMeshData *modelData = new ModelMeshData();
  modelData->groupName = "Landscape";
  modelData->meshType = ModelMeshType::STATICMESH;
  modelData->originalFilename = coreFilename;

  QList<Vec3D> blockPointLists[16][16];
  buildOffset = Vec3D(-ADT_BLOCKCENTER, 0, -ADT_BLOCKCENTER);

  LOGFILE.Debug("Building ADT Points...");
  uint64_t pointCounter = 0;
  for (uint32_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (uint32_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      LOGFILE.Debug("Processing Block X: %i, Y: %i", blockX, blockY);
      QList<Vec3D> pointList = QList<Vec3D>();
      LOGFILE.Debug("Point List prepared.");
      double BlockXOffset = blockX * ADT_CHUNKSIZE;
      double BlockYOffset = blockY * ADT_CHUNKSIZE;
      LOGFILE.Debug("Block offsets set.");
      MapChunk *Chunk = mapTile->MapData[blockX][blockY];
      LOGFILE.Debug("Chunk data grabbed.");
      LOGFILE.Debug("Has MCVTChunk: %s", (Chunk->MCVTChunk == NULL ? "false" : "true"));
      LOGFILE.Debug("Has Height Data: %s", (Chunk->MCVTChunk->HeightData == NULL ? "false" : "true"));
      LOGFILE.Debug("Height Data Count: %i", Chunk->MCVTChunk->HeightData->count());
      QList<double> *data = Chunk->MCVTChunk->HeightData;
      LOGFILE.Debug("Data gathered.");
      Vec3D offset = Chunk->Header->Position;// -mapTile->MapData[0][0]->Header->Position;

      LOGFILE.Debug("Default data set. Starting Point generation...");
      uint32_t point = 0;
      for (size_t horz = 0; horz < 17; horz++) {
        for (size_t vert = 0; vert < ((horz % 2) ? 8 : 9); vert++, point++) {
          Vec3D PointPos;
          float pntheight = 0.0f;
          ModelPoint Point;

          if ((uint32_t)data->count() > point)
          {
            pntheight = data->at(point);
            //LOGFILE.Debug("Extracted height: %f", pntheight);
          }
          else {
            LOGFILE.Warn("Unable to find Point Data for point #%i.", point);
          }

          PointPos.x = (vert * ADT_UNITSIZE) + BlockXOffset;
          PointPos.y = pntheight + offset.y;
          PointPos.z = (horz * 0.5f * ADT_UNITSIZE) + BlockYOffset;
          //LOGFILE.Debug("PointPos BlockX %d, BlockY %d: X: %f, Y: %f (C: %f H: %f), Z: %f", blockX, blockY, PointPos.x, PointPos.y, offset.y, pntheight, PointPos.z);
          if (PointPos.z == -0.0f)
            PointPos.z = -PointPos.z;
          if (horz % 2) {
            PointPos.x += ADT_UNITSIZE * 0.5f;
          }
          double multiplier = 16.0;
          if (singleMaterial == true)
            multiplier = 1.0;
          Point.UVPosition.x = Normalize<double>(PointPos.x, 0, ADT_BLOCKSIZE) * multiplier;
          Point.UVPosition.y = Normalize<double>(-PointPos.z, 0, ADT_BLOCKSIZE) * multiplier;

          PointPos *= YARDStoMETERS;
          pointList.push_back(PointPos);

          if (PointPos.y < PointLowest) { PointLowest = PointPos.y; }
          if (PointPos.y > PointHighest) { PointHighest = PointPos.y; }
          modelData->boundingBox.updateMinAndMax(PointPos);

          Point.Position = PointPos;
          Point.NormalVector.x = Chunk->MCNRChunk->NormalVectors.at(3 * point + 0);
          Point.NormalVector.y = Chunk->MCNRChunk->NormalVectors.at(3 * point + 1);
          Point.NormalVector.z = Chunk->MCNRChunk->NormalVectors.at(3 * point + 2);

          modelData->pointList.push_back(Point);
        }
      }
      LOGFILE.Debug("Points generated.");
      blockPointLists[blockX][blockY].append(pointList);
      LOGFILE.Debug("Points assigned to block.");
    }
  }

  LOGFILE.Debug("Building ADT Polygons...");
  for (uint32_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (uint32_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      uint32_t matID = 0;
      if (singleMaterial == false)
      {
        ModelMaterial modelMat;
        QString matNamePrefix = coreFilename.mid(coreFilename.lastIndexOf("/") + 1, (coreFilename.lastIndexOf(".") - (coreFilename.lastIndexOf("/") + 1)));
        modelMat.Name = QString("%3_y%2_x%1").arg(blockX, 2, 10, QChar('0')).arg(blockY, 2, 10, QChar('0')).arg(matNamePrefix);
        modelObject->materialList.push_back(modelMat);
        matID = modelObject->materialList.count() - 1;
      }
      MapChunk Chunk = *mapTile->MapData[blockX][blockY];

      QList<Vec3D> pointList = blockPointLists[blockX][blockY];
      uint32_t blockID = (ADT_CELLSPERCHUNK * blockY) + blockX;
      //LOGFILE.Debug("Block: %d, PointSize: %d", blockID, pointList.count());

      for (int z = 1; z < 9; z++) {
        for (int x = 1; x < 9; x++) {
          // If this section is part of a Hole, don't build these polygons.
          if ((Chunk.holes[(z-1)] >> (x-1) & 1) == 1)
          {
            continue;
          }

          uint32_t xid1 = 17 * (z - 1) + x - 1;
          uint32_t xid2 = 17 * (z - 1) + x;
          uint32_t xid3 = 17 * (z)+x;
          uint32_t xid4 = 17 * (z)+x - 1;
          uint32_t xid5 = 17 * (z)+x - 9;

          //LOGFILE.Debug("Point IDs: TL: %d (%f|%f|%f), TR: %d (%f|%f|%f), BL: %d (%f|%f|%f), BR: %d (%f|%f|%f), M: %d (%f|%f|%f)", xid1, pntb->topLeft.x + ChunkPos.x, pntb->topLeft.y + ChunkPos.y, pntb->topLeft.z + ChunkPos.z, xid2, pntb->topRight.x + ChunkPos.x, pntb->topRight.y + ChunkPos.y, pntb->topRight.z + ChunkPos.z, xid3, pntb->bottomLeft.x + ChunkPos.x, pntb->bottomLeft.y + ChunkPos.y, pntb->bottomLeft.z + ChunkPos.z, xid4, pntb->bottomRight.x + ChunkPos.x, pntb->bottomRight.y + ChunkPos.y, pntb->bottomRight.z + ChunkPos.z, xid5, pntb->middle.x + ChunkPos.x, pntb->middle.y + ChunkPos.y, pntb->middle.z + ChunkPos.z);

          ModelPolygon Poly;
          Poly.materialID = matID;

          Poly.point0 = xid1 + pointCounter;
          Poly.point1 = xid2 + pointCounter;
          Poly.point2 = xid5 + pointCounter;
          modelData->polygonList.push_back(Poly);

          Poly.point0 = xid2 + pointCounter;
          Poly.point1 = xid3 + pointCounter;
          Poly.point2 = xid5 + pointCounter;
          modelData->polygonList.push_back(Poly);

          Poly.point0 = xid3 + pointCounter;
          Poly.point1 = xid4 + pointCounter;
          Poly.point2 = xid5 + pointCounter;
          modelData->polygonList.push_back(Poly);

          Poly.point0 = xid4 + pointCounter;
          Poly.point1 = xid1 + pointCounter;
          Poly.point2 = xid5 + pointCounter;
          modelData->polygonList.push_back(Poly);
        }
      }
      
      pointCounter += 145;
    }
  }

  LOGFILE.Debug("Building ADT Materials...");
  if (singleMaterial == true)
  {
    ModelMaterial modelMat;
    modelMat.Name = "Landscape";
    modelMat.ColorTextureID = modelObject->ImageList.count();
    modelObject->TextureList.push_back("Baked Map Texture");
    modelObject->ImageList.push_back(mapTexture);
    modelObject->materialList.push_back(modelMat);
  }

  ModelMaterial modelMatMasks;
  modelMatMasks.Name = "Landscape Masks";
  modelMatMasks.ColorTextureID = modelObject->ImageList.count();
  modelMatMasks.useAlphaAsSpecular = false;
  modelMatMasks.hasSpecular = false;
  modelObject->TextureList.push_back("Layer Masks");
  modelObject->ImageList.push_back(mapTile->layerMaskRGB);
  modelObject->materialList.push_back(modelMatMasks);

  modelObject->boundingBox.updateMinAndMax(modelData->boundingBox);
  modelObject->MeshList.push_back(modelData);
  objectBuilt = true;
  builtSingleMaterial = singleMaterial;
  modelScene->Objects.push_back(modelObject);

  // Submeshes
  //mapTile->MWMOChunk->strings
  //mapTile->MMDXChunk->strings

  if (skipVulkan == false) {
    double time_startf = getCurrentTime();
    buildVulkanData(buildOffset);
    double time_endf = getCurrentTime();
    LOGFILE.Debug("Build Object Vulkan Time: %.3f", (time_endf - time_startf));
  }
}

void ADT::buildCollisionObject()
{
}

void ADT::buildPlacementData()
{
  LOGFILE.Info("Building ADT Placement Data...");
  placementData = new PlacementData;

  Vec3D ZeroPoint3D = Vec3D(ADT_ZEROPOINT, 0.0, ADT_ZEROPOINT);
  double TileOffsetX = (ADTTile_X - 32) * ADT_BLOCKSIZE;
  double TileOffsetY = (ADTTile_Y - 32) * ADT_BLOCKSIZE;

  // ADT Parent
  PlacementObject pADT;
  pADT.objectType = PLACEMENTOBJECTTYPE_ADT;
  int nameStart = coreFilename.lastIndexOf("/") + 1;
  int nameEnd = coreFilename.lastIndexOf(".");
  pADT.Name = coreFilename.mid(nameStart, nameEnd - nameStart);
  pADT.OriginOffset = Vec3D(0.0);
  pADT.Location = Vec3D(mapTile->MapData[0][0]->Header->Position) * Vec3D(1.0, 0.0, 1.0) * YARDStoMETERS;
  //LOGFILE.Debug("Maptile Origin: %f, %f, %f", pADT.OriginOffset.x, pADT.OriginOffset.y, pADT.OriginOffset.z);
  pADT.Rotation = Quaternion();
  pADT.Scale = Vec3D(1.0);
  
  if (mapTile->MODFChunk == NULL)
  {
    return;
  }

  // WMOs
  for (uint32_t i = 0; i < (uint32_t)mapTile->MODFChunk->data->count(); i++)
  {
    PlacementObject pObj;
    MODF_Chunk::MODF_Data a = mapTile->MODFChunk->data->at(i);
    Vec3D newPos = Vec3D(a.position) - ZeroPoint3D - Vec3D(TileOffsetX, 0.0, TileOffsetY);
    pObj.objectType = PLACEMENTOBJECTTYPE_WMO;
    pObj.OriginOffset = pADT.Location;
    pObj.Location = newPos * YARDStoMETERS;
    //LOGFILE.Debug("WMO Original: %f, %f, %f, Rotated: %f, %f, %f, Location: %f, %f, %f", mapTile->MODFChunk->data->at(i).position.x, mapTile->MODFChunk->data->at(i).position.y, mapTile->MODFChunk->data->at(i).position.z, newPos.x, newPos.y, newPos.z, pObj.Location.x, pObj.Location.y, pObj.Location.z);
    pObj.setRotation(a.rotation);
    pObj.InfoList.push_back(QString("Doodad Set: %1").arg(a.doodadSet));
    if (a.scale > 0)
    {
      pObj.Scale = Vec3D((a.scale / 1024.0)*0.917); // 0.917 seems to be the correct WMO scale in Unreal Engine
    }
    pObj.Name = mapTile->MWMOChunk->strings->at(a.nameID);
    pObj.Name.replace(".WMO", ".wmo", Qt::CaseSensitive);

    pADT.children.push_back(pObj);
  }

  // Doodads
  for (uint32_t i = 0; i < (uint32_t)mapTile->MDDFChunk->data->count(); i++)
  {
    PlacementObject pObj;
    MDDF_Chunk::MDDF_Data a = mapTile->MDDFChunk->data->at(i);
    Vec3D newPos = Vec3D(a.position) - ZeroPoint3D - Vec3D(TileOffsetX, 0.0, TileOffsetY);
    pObj.objectType = PLACEMENTOBJECTTYPE_DOODAD;
    pObj.OriginOffset = pADT.Location;
    pObj.Location = newPos * YARDStoMETERS;
    pObj.setRotation(a.rotation);
    if (a.scale > 0)
    {
      pObj.Scale = Vec3D((a.scale / 1024.0));
    }
    pObj.Name = mapTile->MMDXChunk->strings->at(a.nameID);
    pObj.Name.replace(".M2", ".m2", Qt::CaseSensitive);
    pObj.Name.replace(".mdx", ".m2", Qt::CaseInsensitive);
    pObj.Name.replace(".mdl", ".m2", Qt::CaseInsensitive);

    pADT.children.push_back(pObj);
  }

  placementData->Objects->push_back(pADT);
  placementBuilt = true;
}


void ADT::buildHeightMap(ADTResolution adt_resolution, bool use_middle_point)
{
  if (objectBuilt == false)
  {
    buildModelObject();
  }

  // Build Chunk Point Lists
  QList<Vec3D> blockPointLists[ADT_CELLSPERCHUNK][ADT_CELLSPERCHUNK];
  QList<PointBlock> *pointBlocks = new QList<PointBlock>;

  uint64_t pointCounter = 0;
  for (uint32_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (uint32_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      QList<Vec3D> pointList = QList<Vec3D>();
      float BlockXOffset = blockX * ADT_CHUNKSIZE;
      float BlockYOffset = blockY * ADT_CHUNKSIZE;
      MapChunk Chunk = *mapTile->MapData[blockX][blockY];
      QList<double> *data = Chunk.MCVTChunk->HeightData;
      Vec3D offset = Chunk.Header->Position;

      uint32_t point = 0;
      for (uint32_t horz = 0; horz < 17; horz++) {
        for (uint32_t vert = 0; vert < (uint32_t)((horz % 2) ? 8 : 9); vert++, point++) {
          Vec3D PointPos;
          float pntheight = 0.0f;
          ModelPoint Point;

          if ((uint32_t)data->count() > point)
          {
            pntheight = data->at(point);
            //LOGFILE.Debug("Extracted height: %f", pntheight);
          }
          else {
            LOGFILE.Warn("Unable to find Point Data for point #%i.", point);
          }

          PointPos.x = (vert * ADT_UNITSIZE) + BlockXOffset;
          PointPos.y = pntheight + offset.y;
          PointPos.z = (horz * 0.5f * ADT_UNITSIZE) + BlockYOffset;
          //LOGFILE.Debug("PointPos BlockX %d, BlockY %d: X: %f, Y: %f (C: %f H: %f), Z: %f", blockX, blockY, PointPos.x, PointPos.y, offset.y, pntheight, PointPos.z);
          if (PointPos.z == -0.0f)
            PointPos.z = -PointPos.z;
          if (horz % 2) {
            PointPos.x += ADT_UNITSIZE * 0.5f;
          }
          PointPos *= YARDStoMETERS;
          pointList.push_back(PointPos);

          if (PointPos.y < PointLowest) { PointLowest = PointPos.y; }
          if (PointPos.y > PointHighest) { PointHighest = PointPos.y; }
        }
      }
      blockPointLists[blockX][blockY] = pointList;
    }
  }

  for (uint32_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (uint32_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      QList<Vec3D> pointList = blockPointLists[blockX][blockY];
      int blockID = (16 * blockY) + blockX;
      //LOGFILE.Debug("Block: %d, PointSize: %d", blockID, pointList.count());
      for (int z = 1; z < 9; z++) {
        for (int x = 1; x < 9; x++) {
          PointBlock *pntb = new PointBlock;

          uint32_t xid1 = 17 * (z - 1) + x - 1;
          uint32_t xid2 = 17 * (z - 1) + x;
          uint32_t xid3 = 17 * (z)+x;
          uint32_t xid4 = 17 * (z)+x - 1;
          uint32_t xid5 = 17 * (z)+x - 9;

          // Proper Point Order
          pntb->topLeft = pointList.at(xid1);
          pntb->topRight = pointList.at(xid2);
          pntb->bottomRight = pointList.at(xid3);
          pntb->bottomLeft = pointList.at(xid4);
          pntb->middle = pointList.at(xid5);

          pointBlocks->push_back(*pntb);
          delete pntb;

          //LOGFILE.Debug("Point IDs: TL: %d (%f|%f|%f), TR: %d (%f|%f|%f), BL: %d (%f|%f|%f), BR: %d (%f|%f|%f), M: %d (%f|%f|%f)", xid1, pntb->topLeft.x + ChunkPos.x, pntb->topLeft.y + ChunkPos.y, pntb->topLeft.z + ChunkPos.z, xid2, pntb->topRight.x + ChunkPos.x, pntb->topRight.y + ChunkPos.y, pntb->topRight.z + ChunkPos.z, xid3, pntb->bottomLeft.x + ChunkPos.x, pntb->bottomLeft.y + ChunkPos.y, pntb->bottomLeft.z + ChunkPos.z, xid4, pntb->bottomRight.x + ChunkPos.x, pntb->bottomRight.y + ChunkPos.y, pntb->bottomRight.z + ChunkPos.z, xid5, pntb->middle.x + ChunkPos.x, pntb->middle.y + ChunkPos.y, pntb->middle.z + ChunkPos.z);
        }
      }
      pointCounter += 145;
    }
  }

  // Find and Adjust ADTScale
  double localScale = PointHighest - PointLowest;
  double folderScale = ADTFolderMax - ADTFolderMin;
  ADTScale = Vec3D(1.0, 1.0, (localScale / folderScale));

  // Build Image from height data

  /*

  Default 9 pixels per poly-block. Can be generated from 1 to 1000, but 9 is the minimum with all height data.
  Each Polyblock is a square of polys. In High-Res ADTs, this is each block of 5 polys, (4 edge points, plus center point) Low res: 2 polys. (Just the 4 edges)

  For Each Polyblock:
  Step 1: Lerp each of the sides.
  Step 2: Find edge middle points.
  Step 3: Lerp edge middle points to Center point. (This gives us all the lerps we need to build an image)
  Step 4: Build X "blank" pixels across & down for each polyblock
  Step 5: use Lerps to find middle of pixel, assign to image.

  */

  // Create initial blank image
  // 3 pixels * 8 blocks per chunk * 16 chunks = 384 pixels minimum. 5 pixels across = 640 pixels. Might be better to render near the desired resolution, (by whole pixels) then scale down/up.
  int pixels = 5;
  switch (ADTOutputResolution)
  {
  case ADTResolution::ADTRES_2X:
    pixels = 10;
    break;
  case ADTResolution::ADTRES_4X:
    pixels = 20;
    break;
  case ADTResolution::ADTRES_UE4_253:
  case ADTResolution::ADTRES_CRYLUM_256:
  case ADTResolution::ADTRES_UNITY_257:
    pixels = 2;
    break;
  case ADTResolution::ADTRES_UE4_505:
  case ADTResolution::ADTRES_CRYLUM_512:
  case ADTResolution::ADTRES_UNITY_513:
    pixels = 4;
    break;
  case ADTResolution::ADTRES_UE4_1009:
  case ADTResolution::ADTRES_CRYLUM_1024:
  case ADTResolution::ADTRES_UNITY_1025:
    pixels = 8;
    break;
  case ADTResolution::ADTRES_UE4_2017:
  case ADTResolution::ADTRES_CRYLUM_2048:
  case ADTResolution::ADTRES_UNITY_2049:
    pixels = 16;
    break;
  case ADTResolution::ADTRES_UE4_4033:
  case ADTResolution::ADTRES_CRYLUM_4096:
  case ADTResolution::ADTRES_UNITY_4097:
    pixels = 32;
    break;
  case ADTResolution::ADTRES_025:
  case ADTResolution::ADTRES_05:
  case ADTResolution::ADTRES_ORIGINAL:
  default:
    pixels = 5;
    break;
  }

  heightmap = new ImageData;
  double pixelsmiddle = pixels / 0.5;       // The center of our pixel blocks.
  double divisions = 1.0 / pixels;          // The number of edges for all the pixels.
  double halfpoint = divisions / 2;         // The centerpoint for each pixel.
  int size = (8 * pixels) * ADT_CELLSPERCHUNK;  // Total Image Size
  heightmap->setWidthHeight(size, size);
  heightmap->ColorType = RGBAColorType::Grey;
  for (int i = 0; i < (size * size); i++) {
    heightmap->ColorData->addTo(RGBAColor(0.0, 0.0, 0.0));
    heightmap->ColorDataRaw->addTo(RGBAColor(0.0, 0.0, 0.0));
  }
  
  // Build PointBlocks
  int pbcount = pointBlocks->count();
  //LOGFILE.Info("Num PointBlocks: %d", pbcount);
  if (use_middle_point == true)
  {
    LOGFILE.Debug("Building map with Middle Points.");
  }

  for (uint32_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (uint32_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      int blockXPos = (size / ADT_CELLSPERCHUNK)*blockX;
      int blockYPos = (size / ADT_CELLSPERCHUNK)*blockY;

      int pbcX = blockX * 64;
      int pbcY = (64 * ADT_CELLSPERCHUNK)*blockY;
      int pbcStart = pbcX + pbcY;

      for (int pbc = 0; pbc < 64; pbc++)
      {
        int pbctrue = pbcStart + pbc;
        int pbcClamp = floor(pbc / 8);
        //LOGFILE.Info("Block #%d", pbctrue);
        PointBlock pb = pointBlocks->at(pbctrue);

        lerp<double> TopSide = lerp<double>(pb.topLeft.y, pb.topRight.y);
        lerp<double> BottomSide = lerp<double>(pb.bottomLeft.y, pb.bottomRight.y);
        lerp<double> LeftSide = lerp<double>(pb.topLeft.y, pb.bottomLeft.y);
        lerp<double> RightSide = lerp<double>(pb.topRight.y, pb.bottomRight.y);
        lerp<double> MiddleVert = lerp<double>(TopSide.getPoint(0.5), pb.middle.y, BottomSide.getPoint(0.5));
        lerp<double> MiddleHorz = lerp<double>(LeftSide.getPoint(0.5), pb.middle.y, RightSide.getPoint(0.5));

        for (int y = 0; y < pixels; y++)
        {
          for (int x = 0; x < pixels; x++)
          {

            // Get the middle of each pixel for pointX & pointY
            double pointX = (divisions*x);
            double pointY = (divisions*y);
            double pointXh = pointX + halfpoint;
            double pointYh = pointY + halfpoint;

            int posX = x + blockXPos + ((pbc - (8 * pbcClamp))*pixels);
            int posY = y + blockYPos + (pbcClamp*pixels);

            int pixelID = posX + (size*posY);

            double dataLow = lerp<double>::doBlerp(TopSide, BottomSide, pointXh, pointYh);
            double pixelData = dataLow;
            if (use_middle_point == true)
            {
              double dataMid = lerp<double>::doBlerp(MiddleHorz, MiddleVert, pointXh, pointYh);
              double dataMerge = lerp<double>::doLerp(dataLow, dataMid, dataLow, pointXh);
              pixelData = lerp<double>::doLerp(dataLow, dataMerge, dataLow, pointYh);
            }

            double pixelDataNormalized = Normalize<double>(pixelData, PointLowest, PointHighest);

            //LOGFILE.Debug("Pixel ID %d: PointX: %f, PointY: %f, PixelData: %f (%f/%f) | %f", pixelID, pointX, pointY, pixelData, min, max, pixelDataNormalizedf);

            RGBAColor pixelColor(pixelData, pixelData, pixelData);
            RGBAColor pixelColorNormalized(pixelDataNormalized, pixelDataNormalized, pixelDataNormalized);

            heightmap->ColorData->setColor(pixelID, pixelColorNormalized);
            heightmap->ColorDataRaw->setColor(pixelID, pixelColor);

          }
        }
      }
    }
  }
  heightmapResolution = adt_resolution;

  heightmapBuilt = true;
}

void ADT::buildMapTileTexture(uint32_t resolution)
{
  //return;
  if (resolution < 256)
  {
    LOGFILE.Error("Resolution too small! Can't build Map Tile Texture.");
    return;
  }
  if (resolution > 32768)
  {
    LOGFILE.Error("Resolution too big! Can't build Map Tile Texture.");
    return;
  }
  int id = qRegisterMetaType<ImageData>();
  if (id == QMetaType::UnknownType)
  {
    LOGFILE.Error("ImageData not a valid QMetaType!");
    return;
  }
  LOGFILE.Info("Building Map Tile Texture with a resolution of %i.", resolution);
  double buildStart = getCurrentTime();

  // Basic information
  mapTexture = new ImageData;
  mapTexture->makeBlackTexture(resolution, resolution);
  mapTexture->sourceFilename = "Baked Map Texture";

  int blockSizePixels = (resolution / ADT_CELLSPERCHUNK);
  LOGFILE.Debug("Block Pixel Size: %i", blockSizePixels);

  QQueue<buildMapImageChunk*> jobs;

  // Assume they do, and move on.
  for (size_t blockY = 0; blockY < ADT_CELLSPERCHUNK; blockY++)
  {
    for (size_t blockX = 0; blockX < ADT_CELLSPERCHUNK; blockX++)
    {
      MapChunk chunk = *mapTile->MapData[blockX][blockY];

      buildMapImageChunk *chunkTask = new buildMapImageChunk(chunk, textureImages, blockSizePixels);
      jobs.enqueue(chunkTask);
      chunkTask->setAutoDelete(true);
    }
  }
  LOGFILE.Debug("Chunks queued.");

  constructMapImage* mapImageConstructor = new constructMapImage(resolution);
  LOGFILE.Debug("MapImage created.");

  QThreadPool *threadPool = QThreadPool::globalInstance();
  LOGFILE.Debug("ThreadPool gathered.");
  for (size_t i = 0; i < jobs.count(); i++)
  {
    LOGFILE.Debug("Queueing Job #%i...", i);
    buildMapImageChunk *newTask = jobs.dequeue();
    LOGFILE.Debug("Task Dequeued. Making connection...");
    QObject::connect(newTask, &buildMapImageChunk::chunkComplete, mapImageConstructor, &constructMapImage::chunkComplete, Qt::ConnectionType::AutoConnection);
    LOGFILE.Debug("Starting Task...");
    threadPool->start(newTask);
  }
  LOGFILE.Debug("Queue processing begun...");

  // Wait for all threads to finish
  threadPool->waitForDone();

  LOGFILE.Debug("Setting Data...");
  mapTexture->ColorData->addTo(*mapImageConstructor->getImage()->ColorData);
  mapTexture->ColorDataRaw->addTo(*mapImageConstructor->getImage()->ColorDataRaw);
  LOGFILE.Debug("Map color data successfully transfered.");

  double buildEnd = getCurrentTime();
  LOGFILE.Debug("Map Texture Build completed in %.3f seconds.", (buildEnd - buildStart));

  // int pID = 6;
  // RGBAColor constPix = mapImageConstructor->getImage()->ColorData->at(pID);
  // RGBAColor mapTPix = mapTexture->ColorData->at(pID);
  // LOGFILE.Debug("Data test, src %i: R: %i, G: %i, B: %i, A: %i  |  dst R: %i, G: %i, B: %i, A: %i", pID, constPix.toInts().R, constPix.toInts().G, constPix.toInts().B, constPix.toInts().A, mapTPix.toInts().R, mapTPix.toInts().G, mapTPix.toInts().B, mapTPix.toInts().A);

  delete mapImageConstructor;
  LOGFILE.Debug("mapImageConstructor deleted.");
}
