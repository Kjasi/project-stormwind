#pragma once
#include "Foundation/Classes/ImageData.h"
#include "WoWFormats/Enums.h"
#include "WoWFormats/Defines.h"
#include "Chunks/MCNK.h"

// Sub-Chunks
#include "Chunks/MCVT.h"
#include "Chunks/MCNR.h"
#include "Chunks/MCLQ.h"
#include "Chunks/MCLY.h"
#include "Chunks/MCAL.h"

class MapTile;

class MapChunk
{
public:
  MapTile *Parent;
  size_t ChunkID;
  bool hasHoles = false;
  uint8_t holes[8];

  // Chunks
  MCNK_Header* Header = NULL;
  MCVT_Chunk* MCVTChunk = NULL;
  MCNR_Chunk* MCNRChunk = NULL;
  MCLQ_Chunk* MCLQChunk = NULL;
  MCLY_Chunk* MCLYChunk = NULL;
  MCAL_Chunk* MCALChunk = NULL;

  MapChunk() {};
  ~MapChunk() {};
  MapChunk(MapTile *parent, size_t chunk_id, bool has_MCIN);
  void init(MapTile *parent, size_t chunk_id);
  void destroy();

  void readHeader(QByteArray *fileData);
  void read(QByteArray *fileData, ADTFileType filetype);
  QList<ImageData> *getMaskLayers() { return &maskLayers; };

private:
  bool hasMCIN = false;

  QList<ImageData> maskLayers;
  bool TransformHolesToHighRes(uint16_t holes, uint8_t hiResHoles[8]);

  void determineHoles();
  void dumpChunkHeader();
  void buildAllMaskLayers();
  ImageData buildMaskLayer(int layerID);
};