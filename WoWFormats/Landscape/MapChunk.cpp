#include "MapChunk.h"
#include "MapTile.h"
#include "WoWFormats/Defines.h"

MapChunk::MapChunk(MapTile *parent, size_t chunk_id, bool has_MCIN)
  : Parent(parent), ChunkID(chunk_id), hasMCIN(has_MCIN)
{}

void MapChunk::init(MapTile *parent, size_t chunk_id)
{
  Parent = parent;
  ChunkID = chunk_id;
}

void MapChunk::destroy()
{
  delete Header;
  delete MCVTChunk;
  delete MCNRChunk;
  delete MCLQChunk;
  delete MCLYChunk;
  delete MCALChunk;
}

void MapChunk::readHeader(QByteArray *fileData)
{
  //LOGFILE.Info("Reading Chunk Header...");
  Header = new MCNK_Header();
  Header->read(fileData);
  determineHoles();
  //LOGFILE.Info("Header, Dump Chunk Data");
  //dumpChunkHeader();
  //LOGFILE.Info("Read Header Complete");
}

void MapChunk::read(QByteArray *fileData, ADTFileType filetype)
{
  fpos_t filePos = 0;
  do
  {
    QString chunk = flipcc(fileData->mid(filePos, 4));
    uint32_t size = getQBANumber<uint32_t>(fileData->mid(filePos + 4, 4));

    //LOGFILE.Debug("Checking Chunk: %s, Size: %i", qPrintable(chunk), size);

    if (chunk == "MCVT")			// Height Data
    {
      //LOGFILE.Debug("MCVT chunk found");
      MCVTChunk = new MCVT_Chunk();
      MCVTChunk->read(&fileData->mid(filePos + 8, size));
    }
    else if (chunk == "MCLV") {		// Cata+ Chunk Lighting. These are the result of baking level-designer placed omni lights. In contrast to MCCV does not only color but also lightens up the vertices. (Ambient color)
      //LOGFILE.Info("Found MCLV chunk.");
    }
    else if (chunk == "MCCV") {		// WotLK+ vertex shading
      //LOGFILE.Info("MCCV Chunk Found.");
    }
    else if (chunk == "MCNR")		// Normal Vectors (NOT UV data)
    {
      //LOGFILE.Debug("MCNR chunk found");
      MCNRChunk = new MCNR_Chunk;
      MCNRChunk->read(&fileData->mid(filePos + 8, 0x1C0));	// 0x1C0 is the corrected "size"

      size = 0x1C0;
    }
    else if (chunk == "MCLY") {		// Texture Layer Definitions
      //LOGFILE.Info("Found MCLY chunk.");
      MCLYChunk = new MCLY_Chunk();
      MCLYChunk->read(&fileData->mid(filePos + 8, size));
      if (Header->nLayers != MCLYChunk->getLayers()->count())
      {
        Header->nLayers = MCLYChunk->getLayers()->count();
      }
    }
    else if (chunk == "MCRF") {		// Collision WMO & Doodad List. A list of with MCNK.nDoodadRefs + MCNK.nMapObjRefs indices into the file's MDDF and MODF chunks
      //LOGFILE.Info("MCRF Chunk Found.");
    }
    else if (chunk == "MCRD") {		// Cata+ replacement for MCRF; Doodad edition
      //LOGFILE.Info("MCRD Chunk Found.");
    }
    else if (chunk == "MCRW") {		// Cata+ replacement for MCRF; World Object edition
      //LOGFILE.Info("MCRW Chunk Found.");
    }
    else if (chunk == "MCSH") {		// Shadow map for static shadows on the terrain
      //LOGFILE.Info("MCSH Chunk Found.");
    }
    else if (chunk == "MCAL") {		// Alpha Maps
      //LOGFILE.Info("Found MCAL chunk.");
      MCALChunk = new MCAL_Chunk();
      MCALChunk->read(&fileData->mid(filePos + 8, size), Header, MCLYChunk->getLayers(), Parent->wdtHeader);
      buildAllMaskLayers();
    }
    else if (chunk == "MCLQ")		// Original Liquid Chunk
    {
      //LOGFILE.Info("Found MCLQ chunk.");
      if (Header->sizeLiquid > 8)
      {
        MCLQChunk = new MCLQ_Chunk;
        MCLQChunk->read(&fileData->mid(Header->offsLiquid+8, 804));		// 804 is the actual size

        size = 804;
      }
    }
    else if (chunk == "MCSE") {		// Sound Emmiters
      //LOGFILE.Info("MCSE Chunk Found.");
    }
    else if (chunk == "MCBB") {		// MoP+ blend batches. max 256 per MCNK
      //LOGFILE.Info("MCBB Chunk Found.");
    }
    else if (chunk == "MCMT") {		// Cata+ Name, ShaderID and Enviroment Map Path
      //LOGFILE.Info("MCMT Chunk Found.");
    }
    else if (chunk == "MCDD") {		// Unknown appearance. At least seen in WoD. Doodad disabler.
      //LOGFILE.Info("MCDD Chunk Found.");
    }

    filePos += 8 + size;
  } while (filePos < fileData->size());
}

void MapChunk::determineHoles()
{
  //LOGFILE.Debug("Determining Hole Data...");

  // Hole data
  if (!(Header->flags.high_res_holes))
  {
    if (uint16_t hole = Header->Holes_LowRes)
      if (TransformHolesToHighRes(hole, holes))
        hasHoles = true;
  }
  else
  {
    //LOGFILE.Debug("MemCopy...");
    memcpy(holes, Header->Holes_HighRes, sizeof(uint64_t));
    //LOGFILE.Debug("MemCopy Successful");
    if (*((uint64_t*)holes) != 0)
    {
      hasHoles = true;
      //LOGFILE.Debug("hasHoles: True");
    }
  }
}

bool MapChunk::TransformHolesToHighRes(uint16_t holes, uint8_t hiResHoles[8])
{
  for (int i = 0; i < 8; i++)
  {
    for (int j = 0; j < 8; j++)
    {
      int holeIdxL = (i / 2) * 4 + (j / 2);
      if (((holes >> holeIdxL) & 1) == 1)
        hiResHoles[i] |= (1 << j);
    }
  }
  return *((uint64_t*)hiResHoles) != 0;
}

void MapChunk::dumpChunkHeader()
{
  bool doDump = false;
#if _DEBUG
  doDump = true;
#endif
  // Add over-riding option to dump chunk header

  if (doDump == false) return;

  LOGFILE.Info("Chunk Header:");
  LOGFILE.Info("  Flags:");
  LOGFILE.Info("    has_mcsh: %s", Header->flags.has_mcsh == 0 ? "false" : "true");
  LOGFILE.Info("    impass: %s", Header->flags.impass == 0 ? "false" : "true");
  LOGFILE.Info("    lq_river: %s", Header->flags.lq_river == 0 ? "false" : "true");
  LOGFILE.Info("    lq_ocean: %s", Header->flags.lq_ocean == 0 ? "false" : "true");
  LOGFILE.Info("    lq_magma: %s", Header->flags.lq_magma == 0 ? "false" : "true");
  LOGFILE.Info("    lq_slime: %s", Header->flags.lq_slime == 0 ? "false" : "true");
  LOGFILE.Info("    has_mccv: %s", Header->flags.has_mccv == 0 ? "false" : "true");
  LOGFILE.Info("    unknown_0x80: %s", Header->flags.unknown_0x80 == 0 ? "false" : "true");
  LOGFILE.Info("    do_not_fix_alpha_map: %s", Header->flags.do_not_fix_alpha_map == 0 ? "false" : "true");
  LOGFILE.Info("    high_res_holes: %s", Header->flags.high_res_holes == 0 ? "false" : "true");
  LOGFILE.Info("  Index X: %i", Header->IndexX);
  LOGFILE.Info("  Index Y: %i", Header->IndexY);
  LOGFILE.Info("  nLayers: %i", Header->nLayers);
  LOGFILE.Info("  nDoodadRefs: %i", Header->nDoodadRefs);
  LOGFILE.Info("  offsHeight: %i", Header->offsetHeightNormal.offsHeight);
  LOGFILE.Info("  offsNormal: %i", Header->offsetHeightNormal.offsNormal);
  LOGFILE.Info("  Holes_HighRes[0]: %i", Header->Holes_HighRes[0]);
  LOGFILE.Info("  Holes_HighRes[1]: %i", Header->Holes_HighRes[1]);
  LOGFILE.Info("  Holes_HighRes[2]: %i", Header->Holes_HighRes[2]);
  LOGFILE.Info("  Holes_HighRes[3]: %i", Header->Holes_HighRes[3]);
  LOGFILE.Info("  Holes_HighRes[4]: %i", Header->Holes_HighRes[4]);
  LOGFILE.Info("  Holes_HighRes[5]: %i", Header->Holes_HighRes[5]);
  LOGFILE.Info("  Holes_HighRes[6]: %i", Header->Holes_HighRes[6]);
  LOGFILE.Info("  Holes_HighRes[7]: %i", Header->Holes_HighRes[7]);
  LOGFILE.Info("  offsLayer: %i", Header->offsLayer);
  LOGFILE.Info("  offsRefs: %i", Header->offsRefs);
  LOGFILE.Info("  offsAlpha: %i", Header->offsAlpha);
  LOGFILE.Info("  sizeAlpha: %i", Header->sizeAlpha);
  LOGFILE.Info("  offsShadow: %i", Header->offsShadow);
  LOGFILE.Info("  sizeShadow: %i", Header->sizeShadow);
  LOGFILE.Info("  areaid: %i", Header->areaid);
  LOGFILE.Info("  nMapObjRefs: %i", Header->nMapObjRefs);
  LOGFILE.Info("  Holes_LowRes: %i", Header->Holes_LowRes);
  LOGFILE.Info("  Holes_Align: %i", Header->Holes_Align);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[0]: %i", Header->ReallyLowQualityTextureingMap[0]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[1]: %i", Header->ReallyLowQualityTextureingMap[1]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[2]: %i", Header->ReallyLowQualityTextureingMap[2]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[3]: %i", Header->ReallyLowQualityTextureingMap[3]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[4]: %i", Header->ReallyLowQualityTextureingMap[4]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[5]: %i", Header->ReallyLowQualityTextureingMap[5]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[6]: %i", Header->ReallyLowQualityTextureingMap[6]);
  LOGFILE.Info("  ReallyLowQualityTextureingMap[7]: %i", Header->ReallyLowQualityTextureingMap[7]);
  LOGFILE.Info("  noEffectDoodad: %i", Header->noEffectDoodad);
  LOGFILE.Info("  offsSndEmitters: %i", Header->offsSndEmitters);
  LOGFILE.Info("  nSndEmitters: %i", Header->nSndEmitters);
  LOGFILE.Info("  offsLiquid: %i", Header->offsLiquid);
  LOGFILE.Info("  sizeLiquid: %i", Header->sizeLiquid);
  LOGFILE.Info("  Position: X: %f, Y: %f, Z: %f", Header->Position.x, Header->Position.y, Header->Position.z);
  LOGFILE.Info("  ofsMCCV: %i", Header->ofsMCCV);
  LOGFILE.Info("  ofsMCLV: %i", Header->ofsMCLV);
  LOGFILE.Info("  unused: %i", Header->unused);
  LOGFILE.Write("");
  LOGFILE.Info("Chunk Header Complete!");
}

void MapChunk::buildAllMaskLayers()
{
  //LOGFILE.Info("Building %i Mask Layers...", Header->nLayers);
  maskLayers.clear();

  //if (Header->flags.do_not_fix_alpha_map == true)
  //  LOGFILE.Debug("Do Not Fix Alpha Map");

  for (uint32_t i = 0; i < Header->nLayers; i++)
  {
    maskLayers.push_back(buildMaskLayer(i));
  }
}

ImageData MapChunk::buildMaskLayer(int layerID)
{
  ImageData a = ImageData();
  a.ColorType = RGBAColorType::RGBA;
  //LOGFILE.Debug("Building Mask for Layer ID: %i", layerID);

  a.setWidthHeight(ADT_CHUNK_MASKIMAGESIZE, ADT_CHUNK_MASKIMAGESIZE);

  if (layerID == 0)
  {
    a.makeWhiteTexture(ADT_CHUNK_MASKIMAGESIZE, ADT_CHUNK_MASKIMAGESIZE);
  }
  else {
    for (int i = 0; i < ADT_CHUNK_MASKDATASIZE; i++)
    {
      int y = floor((double)i / (double)ADT_CHUNK_MASKIMAGESIZE);
      int x = i - (y * ADT_CHUNK_MASKIMAGESIZE);
      int xOffset = x;
      int yOffset = y * ADT_CHUNK_MASKIMAGESIZE;

      if (Header->flags.do_not_fix_alpha_map == false)
      {
        if (y == 63)
        {
          yOffset = y * 63;
        }
        if (x == 63)
        {
          xOffset = x - 1;
        }
        if ((y == 63) && (x == 62))
        {
          yOffset = y * 63;
          xOffset = x - 1;
        }
        if ((y == 63) && (x == 63))
        {
          yOffset = y * 63;
          xOffset = x - 2;
        }
      }
      uint8_t value = MCALChunk->dataLayers.at(layerID).rawData[yOffset + xOffset];

      RGBAColor color;
      color.setColors(value, value, value);
      a.ColorData->addTo(color);
      a.ColorDataRaw->addTo(color);
    }
  }
  //LOGFILE.Debug("Build Layer Complete.");

  return a;
}
