#include "MapTile.h"
#include "WoWFormats/Defines.h"

MapTile::MapTile(QString core_filename, QDir file_directory, MPHD_Chunk *wdt_header, QStringList obj_filenames, QStringList tex_filenames, QStringList lod_filenames)
  : coreFilename(core_filename), fileDirectory(file_directory), wdtHeader(wdt_header), objFilenames(obj_filenames), texFilenames(tex_filenames), lodFilenames(lod_filenames)
{
  layerMaskRGB = new ImageData();
  layerMaskScaledRGB = new ImageData();
}

MapTile::~MapTile()
{
  delete Header;
  delete MCINChunk;
  delete MTEXChunk;
  delete MDIDChunk;
  delete MHIDChunk;
  delete MMDXChunk;
  delete MWMOChunk;
  delete MMIDChunk;
  delete MWIDChunk;
  delete MDDFChunk;
  delete MODFChunk;
  delete wdtHeader;

  delete layerMaskRGB;
  delete layerMaskScaledRGB;

  for (int x = 0; x < ADT_CELLSPERCHUNK; x++)
  {
    for (int y = 0; y < ADT_CELLSPERCHUNK; y++)
    {
      MapData[x][y]->destroy();
      delete MapData[x][y];
    }
  }
}

// Global Chunk Reader. Designed to work with 1.0 to 8.1+ ADT files.
void MapTile::read()
{
  LOGFILE.Info("Reading Maptile Core file \"%s\"...", qPrintable(coreFilename));
  if (validateFile(coreFilename, "Maptile Core") == false) { return; }

  // Initialize Map Chunks
  for (int x = 0; x < ADT_CELLSPERCHUNK; x++)
  {
    for (int y = 0; y < ADT_CELLSPERCHUNK; y++)
    {
      MapData[x][y] = new MapChunk();
    }
  }

  // Read core file
  readFile(coreFilename, ADTFileType::ADTFILETYPE_CORE);

  // Read Subfiles
  for (uint32_t i = 0; i < (uint32_t)objFilenames.count(); i++)
  {
    readFile(objFilenames.at(i), ADTFileType::ADTFILETYPE_OBJ);
  }
  for (uint32_t i = 0; i < (uint32_t)texFilenames.count(); i++)
  {
    readFile(texFilenames.at(i), ADTFileType::ADTFILETYPE_TEX);
  }
  for (uint32_t i = 0; i < (uint32_t)lodFilenames.count(); i++)
  {
    readFile(lodFilenames.at(i), ADTFileType::ADTFILETYPE_LOD);
  }
}

void MapTile::readFile(QString filename, ADTFileType filetype)
{
  if (validateFile(filename, "Maptile file") == false) { return; }
  QFile *file = new QFile(filename);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(filename));
    delete file;
    return;
  }
  QByteArray fileData = file->readAll();
  file->close();
  file->deleteLater();
  delete file;

  if (flipcc(fileData.left(4)) != "MVER")
  {
    LOGFILE.Error("File %s is not a compatible World of Warcraft ADT file.", qPrintable(filename));
    return;
  }

  if (filetype == ADTFILETYPE_CORE && fileData.contains(qPrintable(flipcc("MHDR"))) == false)
  {
    LOGFILE.Error("Tried to load a non-core ADT file into the core file class.");
    return;
  }

  LOGFILE.Debug("Reading data from %s...", qPrintable(filename));
  readData(fileData, filetype);
}

void MapTile::readData(QByteArray fileData, ADTFileType filetype)
{
  fpos_t filePos = 12;
  uint32_t chunkCount = 0;
  bool hasMCIN = false;

  do
  {
    QString chunk = flipcc(fileData.mid(filePos, 4));
    uint32_t size = getQBANumber<uint32_t>(fileData.mid(filePos + 4, 4));

    if (chunk != "MCNK")
      LOGFILE.Debug("Checking Chunk: %s, Size: %i", qPrintable(chunk), size);

    if (chunk == "MHDR" && filetype == ADTFILETYPE_CORE)
    {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MHDR chunk.");
      Header = new MHDR_Chunk;
      Header->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MHDR chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MCIN") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MCIN chunk.");
      hasMCIN = true;
      MCINChunk = new MCIN_Chunk;
      MCINChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MCIN chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MWMO") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MWMO chunk.");
      MWMOChunk = new MWMO_Chunk;
      MWMOChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MWMO chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MMDX") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MMDX chunk.");
      MMDXChunk = new MMDX_Chunk;
      MMDXChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MMDX chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MMID") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MMID chunk.");
      MMIDChunk = new MMID_Chunk;
      MMIDChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MMID chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MWID") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MWID chunk.");
      MWIDChunk = new MWID_Chunk;
      MWIDChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MWID chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MDDF") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MDDF chunk.");
      MDDFChunk = new MDDF_Chunk;
      MDDFChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MDDF chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MODF") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MODF chunk.");
      MODFChunk = new MODF_Chunk;
      MODFChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MODF chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MTEX") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MTEX chunk.");
      MTEXChunk = new MTEX_Chunk();
      MTEXChunk->read(&fileData.mid(filePos + 8, size));
      LOGFILE.Debug("Num Textures: %i", MTEXChunk->strings->count());

      for (uint32_t i = 0; i < (uint32_t)MTEXChunk->strings->count(); i++)
      {
        QString a = MTEXChunk->strings->at(i);
        LOGFILE.Debug("Texture #%i: %s", i, qPrintable(a));
      }
      double End = getCurrentTime();
      LOGFILE.Debug("MTEX chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MDID") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MDID chunk.");
      MDIDChunk = new MDID_Chunk;
      MDIDChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MDID chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MHID") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MHID chunk.");
      MHIDChunk = new MHID_Chunk;
      MHIDChunk->read(&fileData.mid(filePos + 8, size));
      double End = getCurrentTime();
      LOGFILE.Debug("MHID chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MH20") {
      double Start = getCurrentTime();
      LOGFILE.Debug("Found MH20 chunk.");
      double End = getCurrentTime();
      //LOGFILE.Debug("MH20 chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MCNK") {		// Cata+: Header only found in root file. Data found in non-root files.
      double Start = getCurrentTime();
      QByteArray chunkData = fileData.mid(filePos + 8, size);
      if (filetype == ADTFILETYPE_CORE)
      {
        MapChunk *chunk = new MapChunk(this, chunkCount, hasMCIN);
        chunk->readHeader(&chunkData.mid(0, sizeof(MCNK_Header)));
        //LOGFILE.Debug("Header Data: X: %i, Y: %i, nDoodadRefs: %i", chunk->Header->IndexX, chunk->Header->IndexY, chunk->Header->nDoodadRefs);
        chunk->read(&chunkData.mid(sizeof(MCNK_Header)), filetype);
        MapData[chunk->Header->IndexX][chunk->Header->IndexY] = chunk;
        memcpy(MapHoles[chunk->Header->IndexX][chunk->Header->IndexY], chunk->holes, sizeof(uint64_t));
      }
      else {
        uint32_t y = floor(chunkCount / ADT_CELLSPERCHUNK);
        uint32_t x = chunkCount - (y * ADT_CELLSPERCHUNK);
        MapChunk *chunk = MapData[x][y];
        chunk->read(&chunkData, filetype);
      }
      chunkCount++;
      double End = getCurrentTime();
      //LOGFILE.Debug("MCNK chunk read complete.");
      //LOGFILE.Debug("MCNK chunk read in %.3f seconds.", (End - Start));
    }
    else if (chunk == "MFBO") {		// BC+ A bounding box for flying.
      LOGFILE.Debug("Found MFBO chunk.");
    }
    else if (chunk == "MTXF") {		// WotLK+ Array of flags for entries in MTEX
      LOGFILE.Debug("Found MTXF chunk.");
    }
    else if (chunk == "MTXP") {		// MoP+ Texture Parameters
      LOGFILE.Debug("Found MTXF chunk.");
    }
    else if (chunk == "MBMH") {		// MoP+ blend mesh header
      LOGFILE.Debug("Found MBMH chunk.");
    }
    else if (chunk == "MBBB") {		// MoP+ blend mesh bounding boxes
      LOGFILE.Debug("Found MBBB chunk.");
    }
    else if (chunk == "MBNV") {		// MoP+ blend mesh vertices
      LOGFILE.Debug("Found MBNV chunk.");
    }
    else if (chunk == "MBMI") {		// MoP+ blend mesh indices
      LOGFILE.Debug("Found MBMH chunk.");
    }
    else if (chunk == "MAMP") {		// Cata+ Some sort of Texture Size? texture_size = 64 / (2^mamp_value). either defined here or in MHDR.mamp_value. 
      LOGFILE.Debug("Found MAMP chunk.");
    }

    filePos += 8 + size;
  } while (filePos < fileData.size());
}

// Build a composite mask image for each layer. Also build the RGB mask layer for displaying.
void MapTile::buildLayerMasks()
{
  LOGFILE.Debug("Building Layer Masks");
  layerMasks.clear();
  if (layerMaskRGB == NULL)
    layerMaskRGB = new ImageData();
  layerMaskRGB->setWidthHeight(ADT_TILE_MASKIMAGESIZE, ADT_TILE_MASKIMAGESIZE); // 16*64 = 1024

  uint32_t numLayers = 0;	// Number of masks.
  for (int x = 0; x < ADT_CELLSPERCHUNK; x++)
  {
    for (int y = 0; y < ADT_CELLSPERCHUNK; y++)
    {
      if (MapData[x][y]->Header->nLayers > numLayers)
        numLayers = MapData[x][y]->Header->nLayers;
    }
  }

  LOGFILE.Debug("Num Layers gathered (%i)", numLayers);

  // Build baseline masks.
  for (uint32_t i = 0; i < numLayers; i++)
  {
    layerMasks.push_back(ImageData::generateBlackTexture(1024, 1024));
    layerMaskRGB->makeBlackTexture(1024, 1024);
  }

  LOGFILE.Debug("Masks set to Black");

  for (int x = 0; x < ADT_CELLSPERCHUNK; x++)
  {
    //LOGFILE.Debug("X: %i", x);
    for (int y = 0; y < ADT_CELLSPERCHUNK; y++)
    {
      //LOGFILE.Debug("X: %i, Y: %i", x, y);
      QList<ImageData> *masks = MapData[x][y]->getMaskLayers();
      if (masks->count() <= 0) continue;

      int offsetX = ADT_CHUNK_MASKIMAGESIZE * x;
      int offsetY = ADT_CHUNK_MASKIMAGESIZE * y;

      //LOGFILE.Debug("Mask Count: %i", masks.count());
      for (int i = 0; i < masks->count(); i++)
      {
        ImageData src = masks->at(i);
        ImageData dst = layerMasks.at(i);

        bool blockResult = dst.setBlockData(&src, offsetX, offsetY, COLORID_RGBONLY);
        if (blockResult == false)
        {
          LOGFILE.Error("Unable to set block info for mask %i, block X: %i, Block Y: %i", i, x, y);
          continue;
        }
        // LOGFILE.Debug("Dst Color: %f %f %f", dst->ColorData->at(0).R, dst->ColorData->at(0).G, dst->ColorData->at(0).B);
        if (i == 1)
        {
          layerMaskRGB->setRedChannel(&src, offsetX, offsetY);
        }
        else if (i == 2)
        {
          layerMaskRGB->setGreenChannel(&src, offsetX, offsetY);
        }
        else if (i == 3)
        {
          layerMaskRGB->setBlueChannel(&src, offsetX, offsetY);
        }
      }
    }
  }

  layerMaskScaledRGB = layerMaskRGB;
  layerMaskScaledRGB->resizeImage(4096, 4096);
}