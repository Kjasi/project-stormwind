#pragma once
#include <Foundation/Classes/ModelClass.h>

#include "Chunks/MPHD.h"
#include "Chunks/MAIN.h"
#include "Chunks/MWMO.h"
#include "Chunks/MODF.h"

class WDT
{
public:
  WDT(QString filename = QString());
  ~WDT();

  MPHD_Chunk* getHeader() { return MPHDChunk; };

private:

  MPHD_Chunk *MPHDChunk = NULL;


};