#pragma once
#include "MapChunk.h"
#include "WoWFormats/Enums.h"

// Chunks
#include "Chunks/MPHD.h"
#include "Chunks/MHDR.h"
#include "Chunks/MCIN.h"
#include "Chunks/MWMO.h"
#include "Chunks/MODF.h"
#include "Chunks/MTEX.h"
#include "Chunks/MDID.h"
#include "Chunks/MHID.h"
#include "Chunks/MMDX.h"
#include "Chunks/MMID.h"
#include "Chunks/MWID.h"
#include "Chunks/MDDF.h"
#include "Chunks/MODF.h"

#include <qdir.h>

class MapTile
{
public:
  MapTile(QString core_filename, QDir file_directory, MPHD_Chunk *wdt_header, QStringList obj_filenames = QStringList(), QStringList tex_filenames = QStringList(), QStringList lod_filenames = QStringList());
  ~MapTile();

  void read();
  void buildLayerMasks();

  MHDR_Chunk* Header = NULL;
  MCIN_Chunk* MCINChunk = NULL;
  MTEX_Chunk* MTEXChunk = NULL;
  MDID_Chunk* MDIDChunk = NULL;
  MHID_Chunk* MHIDChunk = NULL;
  MMDX_Chunk* MMDXChunk = NULL;
  MWMO_Chunk* MWMOChunk = NULL;
  MMID_Chunk* MMIDChunk = NULL;
  MWID_Chunk* MWIDChunk = NULL;
  MDDF_Chunk* MDDFChunk = NULL;
  MODF_Chunk* MODFChunk = NULL;

  MapChunk* MapData[ADT_CELLSPERCHUNK][ADT_CELLSPERCHUNK];

  QList<ImageData> layerMasks;
  ImageData *layerMaskRGB = 0;
  ImageData *layerMaskScaledRGB = 0;

private:
  friend MapChunk;
  QDir fileDirectory;
  QString coreFilename;
  QStringList objFilenames;	// The Object files, used since Cata
  QStringList texFilenames;	// The Texture files, used since Cata
  QStringList lodFilenames;	// The LOD files, used since Legion. Only need 1 for now, but why take the chance?
  MPHD_Chunk *wdtHeader;
  uint8_t MapHoles[ADT_CELLSPERCHUNK][ADT_CELLSPERCHUNK][8];

  void readFile(QString filename, ADTFileType filetype);
  void readData(QByteArray fileData, ADTFileType filetype);
};