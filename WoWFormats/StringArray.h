#pragma once
#include <Foundation/Functions.h>
#include <qfile.h>

struct StringArray
{
  QByteArray rawStrings;
  QStringList *strings;

  void read(FILE* file, uint32_t size)
  {
    strings = new QStringList;
    fpos_t fp;
    fgetpos(file, &fp);
    QFile qf;
    qf.open(file, QIODevice::OpenModeFlag::ReadOnly);
    rawStrings = qf.readAll().mid(fp, size);
    qf.close();
    fseek(file, fp, SEEK_SET);
    ReadStringsIntoList(strings, file, size);
  }
  void read(QByteArray *data)
  {
    strings = new QStringList;
    rawStrings = *data;
    ReadStringsIntoList(strings, data, 0, data->size());
  }
  QString getStringFromIndex(int index)
  {
    int indexEnd = rawStrings.indexOf('\0', index);
    QString r = rawStrings.mid(index, indexEnd);

    return r;
  }
};