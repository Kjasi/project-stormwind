#pragma once
#include <Foundation/Functions.h>

template <class T>
struct IndexArray
{
  QList<T> *Indexs;

  void read(FILE* file, size_t size, int byteSize = 4)
  {
    Indexs = new QList<T>;
    
    size_t bytes = 0;
    while (bytes < size) {
      T val;
      fread(&val, byteSize, 1, file);

      Indexs->push_back(val);

      bytes += byteSize;
    }
  }

  void read(QByteArray *fileData)
  {
    Indexs = new QList<T>;

    for (uint32_t i = 0; i < fileData->size() / sizeof(T); i++)
    {
      T d = T();
      d = getQBANumber<T>(fileData->mid((sizeof(T) * i), sizeof(T)).data());
      Indexs->push_back(d);
    }
  }

};