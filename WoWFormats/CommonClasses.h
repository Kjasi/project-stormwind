#pragma once
#include <Foundation/Classes/Vectors.h>
#include <Foundation/Classes/RGBAColor.h>

class CRange
{
  float min;
  float max;
};

class CUiRange
{
  uint32_t min;
  uint32_t max;
};

struct CAaBox
{
  Vec3F min;
  Vec3F max;
};

struct CArgba
{
  unsigned char r;
  unsigned char g;
  unsigned char b;
  unsigned char a;

  RGBAColor toRGBAColor()
  {
    return RGBAColor((int)r, (int)g, (int)b, (int)a);
  }
};

struct CAbgra
{
  unsigned char b;
  unsigned char g;
  unsigned char r;
  unsigned char a;

  RGBAColor toRGBAColor()
  {
    return RGBAColor((int)r, (int)g, (int)b, (int)a);
  }
};