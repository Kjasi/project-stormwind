#pragma once
#include <QtCore/QString>

// SubChunks
#include "Chunks/MOGP.h"
#include "Chunks/MOPY.h"
#include "Chunks/MOVI.h"
#include "Chunks/MOVT.h"
#include "Chunks/MONR.h"
#include "Chunks/MOTV.h"
#include "Chunks/MOLV.h"
#include "Chunks/MOIN.h"
#include "Chunks/MODR.h"
#include "Chunks/MOLR.h"

class WMORoot;

class WMOGroup
{
  struct PolyData
  {
    uint64_t pointID0, pointID1, pointID2;
    PolyData() { pointID0 = pointID1 = pointID2 = 0; }
  };
public:
  QString groupFilename;
  QString groupName;

  WMOGroup(WMORoot *root, QString group_filename);

  void read();

  QList<Vec3D> PointData;
  QList<Vec3D> NormalData;
  QList<PolyData> PolygonData;
  QList<Vec2D> PointUVData;
  QList<RGBAColor> VertColorData;
  QList<uint8_t> MaterialIDs;
  QList<uint16_t> DoodadIDs;
  QList<uint16_t> LightIDs;
  QList<MOPY_Chunk::Polyflags> MaterialFlags;

private:
  WMORoot *rootFile;

  // Raw Subchunks go here
  MOGP_Header *header = NULL;
  MOVI_Chunk *MOVIChunk = NULL;
  MOVT_Chunk *MOVTChunk = NULL;
  MONR_Chunk *MONRChunk = NULL;
  MOTV_Chunk *MOTVChunk = NULL;
  MOPY_Chunk *MOPYChunk = NULL;
  MODR_Chunk *MODRChunk = NULL;
  MOLR_Chunk *MOLRChunk = NULL;

  void getGroupName();
  void processChunks();
};