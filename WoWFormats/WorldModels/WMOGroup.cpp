#include "WMOGroup.h"
#include "WMORoot.h"
#include <qfile.h>

WMOGroup::WMOGroup(WMORoot *root, QString group_filename)
  :rootFile(root), groupFilename(group_filename)
{
}

void WMOGroup::read()
{
  LOGFILE.Info("Reading WMO Group file \"%s\"...", qPrintable(groupFilename));
  if (validateFile(groupFilename, "WMO Group") == false){	return;	}

  //LOGFILE.Debug("WMO Group file is a valid file. Checking to see if it's really a WMO...");

  QFile *file = new QFile(groupFilename);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(groupFilename));
    return;
  }
  QByteArray fileData = file->readAll();
  file->close();
  file->deleteLater();
  if (flipcc(fileData.left(4)) != "MVER")
  {
    LOGFILE.Error("File %s is not a compatible World of Warcraft WMO file.", qPrintable(groupFilename));
    return;
  }

  int lastSlash = groupFilename.lastIndexOf("/");

  if (fileData.contains(qPrintable(flipcc("MOHD"))) == true)
  {
    LOGFILE.Error("Tried to load a Root WMO file into the Group file class.");
    return;
  }
  if (fileData.contains(qPrintable(flipcc("MOGP"))) == false)
  {
    LOGFILE.Error("File %s is not a valid WMO Group file...", qPrintable(groupFilename));
    return;
  }

  //LOGFILE.Debug("WMO Group file validated! Commencing reading...");

  if (file->isOpen() == false)
  {
    LOGFILE.Warn("File is no longer open. Attempting to re-open it...");
    if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
    {
      LOGFILE.Error("Unable to open file %s for second reading...", qPrintable(groupFilename));
      return;
    }
  }

  // Time to read the chunk data
  fpos_t filePos = 12;
  do
  {
    QString chunk = flipcc(fileData.mid(filePos, 4));

    uint32_t size = getQBANumber<uint32_t>(fileData.mid(filePos + 4, 4));

    // Technically speaking, the entire file is subchunks of MOGP, so we have to adjust the size...
    if (chunk == "MOGP")
    {
      size = sizeof(MOGP_Header);
    }

    //LOGFILE.Debug("Checking Chunk: %s, Size: %i", qPrintable(chunk), size);

    if (chunk == "MOGP")
    {
      LOGFILE.Debug("MOGP chunk found");
      header = new MOGP_Header;
      header->read(&fileData, filePos + 8, size);
      getGroupName();
    }
    else if (chunk == "MOPY")
    {
      LOGFILE.Debug("MOPY chunk found");
      MOPYChunk = new MOPY_Chunk;
      MOPYChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOVI")
    {
      LOGFILE.Debug("MOVI chunk found");
      MOVIChunk = new MOVI_Chunk;
      MOVIChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOVT")
    {
      LOGFILE.Debug("MOVT chunk found");
      MOVTChunk = new MOVT_Chunk;
      MOVTChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MONR")
    {
      LOGFILE.Debug("MONR chunk found");
      MONRChunk = new MONR_Chunk;
      MONRChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOTV")
    {
      LOGFILE.Debug("MOTV chunk found");
      MOTVChunk = new MOTV_Chunk;
      MOTVChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOLV")
    {
      LOGFILE.Debug("MOLV chunk found");
    }
    else if (chunk == "MOIN")
    {
      LOGFILE.Debug("MOIN chunk found");
    }
    else if (chunk == "MOBA")
    {
      LOGFILE.Debug("MOBA chunk found");
    }
    else if (chunk == "MOLR")
    {
      LOGFILE.Debug("MOLR chunk found");
      MOLRChunk = new MOLR_Chunk;
      MOLRChunk->read(&fileData.mid(filePos + 8, size));
    }
    else if (chunk == "MODR")
    {
      LOGFILE.Debug("MODR chunk found");
      MODRChunk = new MODR_Chunk;
      MODRChunk->read(&fileData.mid(filePos + 8, size));
    }
    else if (chunk == "MOBN")
    {
      LOGFILE.Debug("MOBN chunk found");
    }
    else if (chunk == "MOBR")
    {
      LOGFILE.Debug("MOBR chunk found");
    }
    else if (chunk == "MOCV")
    {
      LOGFILE.Debug("MOCV chunk found");
    }
    else if (chunk == "MLIQ")
    {
      LOGFILE.Debug("MLIQ chunk found");
    }
    else if (chunk == "MORI")
    {
      LOGFILE.Debug("MORI chunk found");
    }
    else if (chunk == "MORB")
    {
      LOGFILE.Debug("MORB chunk found");
    }
    else if (chunk == "MOTA")
    {
      LOGFILE.Debug("MOTA chunk found");
    }
    else if (chunk == "MOBS")
    {
      LOGFILE.Debug("MOBS chunk found");
    }
    else if (chunk == "MDAL")
    {
      LOGFILE.Debug("MDAL chunk found");
    }
    else if (chunk == "MOPL")
    {
      LOGFILE.Debug("MOPL chunk found");
    }
    else if (chunk == "MOPB")
    {
      LOGFILE.Debug("MOPB chunk found");
    }
    else if (chunk == "MOLS")
    {
      LOGFILE.Debug("MOLS chunk found");
    }
    else if (chunk == "MOLP")
    {
      LOGFILE.Debug("MOLP chunk found");
    }
    else if (chunk == "MOLM")
    {
      LOGFILE.Debug("MOLM chunk found");
    }
    else if (chunk == "MOLD")
    {
      LOGFILE.Debug("MOLD chunk found");
    }

    filePos += 8 + size;
  } while (filePos < fileData.size());

  processChunks();
}

void WMOGroup::getGroupName()
{
  groupName = rootFile->MOGNChunk->getStringFromIndex(header->groupName);
  //LOGFILE.Debug("getGroupnName Offset: %i, Result: %s", header->groupName, qPrintable(groupName));
}

void WMOGroup::processChunks()
{
  if (MOVTChunk == NULL || MOVIChunk == NULL || MOPYChunk == NULL || MOVTChunk->Verticies.count() <= 0 || MOVIChunk->Indicies.count() <= 0 || MOPYChunk->Flags.count() <= 0)
  {
    LOGFILE.Info("Unable to get Vertex or Index info for groupfile %s.", qPrintable(groupFilename));
    return;
  }

  PointData = MOVTChunk->Verticies;
  NormalData = MONRChunk->Normals;
  if (MOTVChunk != NULL) {
    PointUVData = MOTVChunk->UvCoords;
  }
  if (MODRChunk != NULL) {
    DoodadIDs = *MODRChunk->Indexs;
  }
  if (MOLRChunk != NULL) {
    LightIDs = *MOLRChunk->Indexs;
  }
  for (uint32_t i = 0; i < (uint32_t)MOVIChunk->Indicies.count(); i+=3)
  {
    PolyData a;
    a.pointID0 = MOVIChunk->Indicies.at(i + 0);
    a.pointID1 = MOVIChunk->Indicies.at(i + 1);
    a.pointID2 = MOVIChunk->Indicies.at(i + 2);
    PolygonData.push_back(a);
  }
  for (uint32_t i = 0; i < (uint32_t)MOPYChunk->Flags.count(); i++)
  {
    MOPY_Chunk::Polyflags f = MOPYChunk->Flags.at(i);
    uint8_t mat = MOPYChunk->MaterialIDs.at(i);
    MaterialFlags.push_back(f);
    MaterialIDs.push_back(mat);
  }

}
