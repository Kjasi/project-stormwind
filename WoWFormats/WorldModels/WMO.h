#pragma once
#include <Foundation/Classes/ModelClass.h>
#include "WMORoot.h"
#include "WMOGroup.h"
#include <QtCore/qlist.h>
#include <QtCore/qdir.h>

class WMO : public ModelClass
{
public:
  WMO(QString input_filename);
  ~WMO();

  QString getRootFileName() { return fileDirectory.absoluteFilePath(rootFileName); }
  QStringList getTextureList();
  QStringList getDoodadList();

  void buildModelObject();
  void buildCollisionObject();
  bool isLoaded() { return loaded; }
  bool isObjectBuilt() { return objectBuilt; }

private:
  bool loaded = false;
  bool objectBuilt = false;
  bool placementBuilt = false;
  QDir fileDirectory;
  QString inputFilename;
  QString rootFileName;
  QStringList groupFileNames;
  QStringList groupLODFileNames;

  WMORoot *rootFile = nullptr;
  QList<WMOGroup*> *groupFiles = nullptr;

  void findFilenames();

  void getRootFile();
  void getGroupFiles();

  void buildPlacementData();
};