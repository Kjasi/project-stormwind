#pragma once

struct MODS_Chunk
{
  struct MODSData
  {
    char Name[20];			// Name of this Doodad Set
    uint32_t startIndex;	// Index of first doodad instance in this set
    uint32_t count;			// Number of doodad instances in this set
    char pad[4];
  };

  QList<MODSData> DoodadSetData;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MODS_Chunk...");
    QByteArray ba = data->mid((uint32_t)start, (uint32_t)size);

    //LOGFILE.Debug("Size: %i, entries: %i", size, size/sizeof(MODSData));
    for (uint32_t i = 0; i < size; i += sizeof(MODSData))
    {
      //LOGFILE.Debug("i: %i", i);
      QByteArray mods = ba.mid(i, sizeof(MODSData));
      MODSData *a = reinterpret_cast<MODSData*>(mods.data());

      DoodadSetData.push_back(*a);
    }
  }
};