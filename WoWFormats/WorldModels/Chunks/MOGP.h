#pragma once
#include <WoWFormats/CommonClasses.h>

struct MOGP_Header
{
  enum SecondFlags
  {
    flag2_CanCutTerrain = 1,        // >= Mists has portal planes to cut
  };

  uint32_t groupName;				// offset into MOGN
  uint32_t descriptiveGroupName;	// offset into MOGN
  uint32_t flags;					// see https://wowdev.wiki/WMO#group_flags
  CAaBox boundingBox;				// as with flags, same as in corresponding MOGI entry
  uint16_t portalIndexStart;		// index into MOPR
  uint16_t portalCount;			// number of MOPR items used after portalStart
  uint16_t transBatchCount;
  uint16_t intBatchCount;
  uint16_t extBatchCount;
  uint16_t paddingOrBatch4;
  uint8_t fogIDs[4];				// ids in MFOG
  uint32_t groupLiquid;			// see in the MLIQ chunk
  uint32_t uniqueWMOGroupID;		// Part of the WMOAreaTable database
  uint32_t flags2;
  uint32_t unknown;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOGP_Header...");
    QByteArray headerData = data->mid(start, (uint32_t)size);
    MOGP_Header *a = reinterpret_cast<MOGP_Header*>(headerData.data());
    groupName = a->groupName;
    descriptiveGroupName = a->descriptiveGroupName;
    flags = a->flags;
    boundingBox = a->boundingBox;
    portalIndexStart = a->portalIndexStart;
    portalCount = a->portalCount;
    transBatchCount = a->transBatchCount;
    intBatchCount = a->intBatchCount;
    extBatchCount = a->extBatchCount;
    paddingOrBatch4 = a->paddingOrBatch4;
    fogIDs[0] = a->fogIDs[0];
    fogIDs[1] = a->fogIDs[1];
    fogIDs[2] = a->fogIDs[2];
    fogIDs[3] = a->fogIDs[3];
    groupLiquid = a->groupLiquid;
    uniqueWMOGroupID = a->uniqueWMOGroupID;
    flags2 = a->flags2;
    unknown = a->unknown;
  };
};