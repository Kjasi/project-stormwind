#pragma once
#include <Foundation/Functions.h>

struct MOVI_Chunk
{
  QList<size_t> Indicies;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOVI_Chunk...");
    QByteArray allIndicies = data->mid(start, (uint32_t)size);

    for (uint32_t i = 0; i < (uint32_t)allIndicies.size(); i += 6)
    {
      Indicies.push_back(getQBANumber<size_t>(allIndicies.mid(i + 0, 2)));
      Indicies.push_back(getQBANumber<size_t>(allIndicies.mid(i + 2, 2)));
      Indicies.push_back(getQBANumber<size_t>(allIndicies.mid(i + 4, 2)));
    }
  }
};