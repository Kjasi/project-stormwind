#pragma once
#include <Foundation/Classes/Vectors.h>

struct MOTV_Chunk
{
  QList<Vec2D> UvCoords;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOTV_Chunk...");

    QByteArray allData = data->mid(start, (uint32_t)size);

    for (uint32_t i = 0; i < (uint32_t)allData.size(); i += 8)
    {
      Vec2D a;
      a.x = getQBANumber<float>(allData.mid(i + 0, 4));
      a.y = getQBANumber<float>(allData.mid(i + 4, 4));
      a.y = -a.y + 1.0f;
      // LOGFILE.Debug("Vert X: %f, Y: %f", a.x, a.y);

      UvCoords.push_back(a);
    }

  }
};