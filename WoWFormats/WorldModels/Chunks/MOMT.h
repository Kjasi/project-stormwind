#pragma once
#include <WoWFormats/CommonClasses.h>

struct MOMTData
{
  struct flagList {
    uint32_t UNLIT : 1;                    // disable lighting logic in shader (but can still use vertex colors)
    uint32_t UNFOGGED : 1;                 // disable fog shading (rarely used)
    uint32_t UNCULLED : 1;                 // two-sided
    uint32_t EXTLIGHT : 1;                 // darkened, the intern face of windows are flagged 0x08
    uint32_t SIDN : 1;                     // (bright at night, unshaded) (used on windows and lamps in Stormwind, for example) (see emissive color)
    uint32_t WINDOW : 1;                   // lighting related (flag checked in CMapObj::UpdateSceneMaterials)
    uint32_t CLAMP_S : 1;                  // tex clamp S (force this material's textures to use clamp s addressing)
    uint32_t CLAMP_T : 1;                  // tex clamp T (force this material's textures to use clamp t addressing)
    uint32_t flag_0x100 : 1;
    uint32_t : 23;                         // unused as of 7.0.1.20994
  } flags;
  uint32_t shader;
  uint32_t blendMode;							// Blending: see EGxBlend
  uint32_t diffuseNameIndex;					// offset into MOTX. texture_1, 2 and 3 are start positions for texture filenames in the MOTX data block ; texture_1 for the first texture, texture_2 for the second (see shaders), etc. texture_1 defaults to "createcrappygreentexture.blp". 
  CAbgra sidnColor;							// emissive color; The sidnColor CImVector at offset 0x10 is used with the SIDN (self-illuminated day night) scalar from CDayNightObject to light exterior window glows (see flag 0x10 above). The scalar is interpolated out of a static table in the client, based on the time of day.
  CAbgra frameSidnColor;						// sidn emissive color; set at runtime; gets sidn-manipulated emissive color; The color value eventually is copied into offset 0x14 (frameSidnColor) after being manipulated by the SIDN scalar. This manipulation occurs in CMapObj::UpdateMaterials.
  uint32_t envNameIndex;
  CAbgra diffColor;
  uint32_t ground_type;						// according to CMapObjDef::GetGroundType
  uint32_t texture_2;
  CAbgra color_2;								// color_2 is diffuse color : CWorldView::GatherMapObjDefGroupLiquids(): geomFactory->SetDiffuseColor((CArgba*)(smo+7));
  uint32_t flags_2;
  uint32_t runTimeData[4];         // This data is explicitly nulled upon loading. Contains textures or similar stuff.
};

// Materials Chunk
struct MOMT_Chunk
{
  QList<MOMTData> MaterialData;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOMT_Chunk...");
    QByteArray ba = data->mid((uint32_t)start, (uint32_t)size);

    //LOGFILE.Debug("Size: %i, entries: %i", size, size/sizeof(MOMTData));
    for (uint32_t i = 0; i < (uint32_t)size; i += sizeof(MOMTData))
    {
      //LOGFILE.Debug("i: %i", i);
      QByteArray momt = ba.mid(i, sizeof(MOMTData));
      MOMTData *a = reinterpret_cast<MOMTData*>(momt.data());
      //LOGFILE.Debug("Material ID: %u, flags: %u, shader: %u, blendMode: %u, diffuseNameIndex: %u, ", i/ sizeof(MOMTData), a->flags, a->shader, a->blendMode, a->diffuseNameIndex);

      MaterialData.push_back(*a);
    }
  }


  /*


void CMapObj::CreateMaterial (unsigned int materialId)
{
  assert (m_materialCount);
  assert (m_materialTexturesList);
  assert (materialId < m_materialCount);

  if (++m_materialTexturesList[materialId].refcount <= 1)
  {
    SMOMaterial* material = &m_smoMaterials[materialId];

    const char* texNames[3];
    texNames[0] = &m_textureFilenamesRaw[material->firstTextureOffset];
    texNames[1] = &m_textureFilenamesRaw[material->secondTextureOffset];
    texNames[2] = &m_textureFilenamesRaw[material->thirdTextureOffset];
    if ( *texNames[0] )
      texNames[0] = "createcrappygreentexture.blp";

    assert (material->shader < SMOMaterial::SH_COUNT);

    int const textureCount
      ( CShaderEffect::s_enableShaders
      ? s_wmoShaderMetaData[material->shader].texturesWithShader
      : s_wmoShaderMetaData[material->shader].texturesWithoutShader
      );

    int textures_set (0);

    for (; textures_set < textureCount; ++textures_set)
    {
      if (!texNames[textures_set])
      {
        material->shader = MapObjOpaque;
        textures_set = 1;
        break;
      }
    }

    for (; textures_set < 3; ++textures_set)
    {
      texNames[textures_set] = nullptr;
    }

    if (material->shader == MapObjTwoLayerTerrain && texNames[1])
    {
      texNames[1] = insert_specular_suffix (texNames[1]);
    }

    int flags (std::max (m_field_2C, 12));

    const char* parent_name (m_field_9E8 & 1 ? m_filename : nullptr);

    m_materialTexturesList[materialId]->textures[0] = texNames[0] ? CMap::CreateTexture (texNames[0], parent_name, flags) : nullptr;
    m_materialTexturesList[materialId]->textures[1] = texNames[1] ? CMap::CreateTexture (texNames[1], parent_name, flags) : nullptr;
    m_materialTexturesList[materialId]->textures[2] = texNames[2] ? CMap::CreateTexture (texNames[2], parent_name, flags) : nullptr;
  }
}
  
  */
};