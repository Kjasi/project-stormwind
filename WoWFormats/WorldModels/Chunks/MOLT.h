﻿#pragma once
#include <WoWFormats/CommonClasses.h>

struct WMOLight
{
  enum LightType
  {
    OMNI = 0,
    SPOT,
    DIRECTIONAL,
    AMBIENT,
  };

  uint8_t type;
  uint8_t useAtten;
  uint8_t pad[2];
  CAbgra color;
  Vec3F position;
  float intensity;
  float unk[4];		// Probably Rotation for spot lights
  float attenStart;
  float attenEnd;
};

struct MOLT_Chunk
{
  QList<WMOLight> lightList;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOLT_Chunk...");
    QByteArray ba = data->mid(start, (int)size);

    //LOGFILE.Debug("Size: %i, entries: %i", size, size/sizeof(WMOLight));
    for (uint32_t i = 0; i < size; i += sizeof(WMOLight))
    {
      //LOGFILE.Debug("i: %i", i);
      QByteArray molt = ba.mid(i, sizeof(WMOLight));
      WMOLight *a = reinterpret_cast<WMOLight*>(molt.data());

      a->position.InvertY();
      a->position.SwapYZ();

      //LOGFILE.Debug("Light ID: %u, type: %u, Position X: %f, Y: %f, Z: %f, intensity: %f, useAttenuation: %u, attEnd: %f | unk0: %f, unk1: %f, unk2: %f, unk3: %f", i/ sizeof(WMOLight), a->type, a->position.x, a->position.y, a->position.z, a->intensity, a->useAtten, a->attenEnd, a->unk[0], a->unk[1], a->unk[2], a->unk[3]);
      lightList.push_back(*a);
    }
  }
};