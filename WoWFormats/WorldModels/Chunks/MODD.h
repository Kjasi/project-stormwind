#pragma once
#include <WoWFormats/CommonClasses.h>

struct MODD_Chunk
{
  struct MODDRawData
  {
    uint32_t nameIndex : 24;			// reference offset into MODN, or MODI, depending on version and presence.
    uint32_t flag_AcceptProjTex : 1;
    uint32_t flag_0x2 : 1;				// MapStaticEntity::field_34 |= 1 (if set, MapStaticEntity::AdjustLighting is _not_ called)
    uint32_t flag_0x4 : 1;
    uint32_t flag_0x8 : 1;
    uint32_t unused : 4;				// unused as of 7.0.1.20994
    Vec3F Position;						// X, Z, -Y
    Vec4F Rotation;						// X, Y, Z, W
    float Scale;						// Scale Factor
    CAbgra Color;						// overrides pc_sunColor
  };

  struct MODDData
  {
    uint32_t nameIndex : 24;			// reference offset into MODN, or MODI, depending on version and presence.
    uint32_t flag_AcceptProjTex : 1;
    uint32_t flag_0x2 : 1;				// MapStaticEntity::field_34 |= 1 (if set, MapStaticEntity::AdjustLighting is _not_ called)
    uint32_t flag_0x4 : 1;
    uint32_t flag_0x8 : 1;
    uint32_t unused : 4;				// unused as of 7.0.1.20994
    Vec3D Position;						// X, Z, -Y
    Quaternion Rotation;				// X, Y, Z, W
    double Scale;						// Scale Factor
    CAbgra Color;						// overrides pc_sunColor
  };

  QList<MODDData> DoodadInstanceData;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MODS_Chunk...");
    QByteArray ba = data->mid((uint32_t)start, (uint32_t)size);

    //LOGFILE.Debug("Size: %i, entries: %i", size, size/sizeof(MODDRawData));
    for (uint32_t i = 0; i < (uint32_t)size; i += sizeof(MODDRawData))
    {
      //LOGFILE.Debug("i: %i", i);
      QByteArray modd = ba.mid(i, sizeof(MODDRawData));
      MODDRawData *a = reinterpret_cast<MODDRawData*>(modd.data());

      MODDData b;
      b.nameIndex = a->nameIndex;		
      b.flag_AcceptProjTex = a->flag_AcceptProjTex;
      b.flag_0x2 = a->flag_0x2;			
      b.flag_0x4 = a->flag_0x4;
      b.flag_0x8 = a->flag_0x8;
      b.unused = a->unused;
      b.Scale = a->Scale;
      b.Color = a->Color;

      a->Position.InvertY();
      a->Position.SwapYZ();
      b.Position = a->Position;
      b.Position *= 1.09356244376;	// Not sure why this is needed, but this fixes a lot of alignment issues.

      Quaternion rot;
      rot.x = a->Rotation.x;
      rot.y = a->Rotation.y;
      rot.z = a->Rotation.z;
      rot.w = a->Rotation.w;
      b.Rotation = rot;

      DoodadInstanceData.push_back(b);
    }
  }
};