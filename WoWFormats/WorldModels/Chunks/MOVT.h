#pragma once
#include <Foundation/Functions.h>
#include <Foundation/Classes/Vectors.h>

struct MOVT_Chunk
{
  QList<Vec3D> Verticies;

  void read(QByteArray *data, fpos_t start, size_t size) 
  {
    LOGFILE.Info("Reading MOVT_Chunk...");
    QByteArray allVerts = data->mid(start, (uint32_t)size);

    for (uint32_t i = 0; i < (uint32_t)allVerts.size() ; i+=12)
    {
      Vec3D a; 
      a.x = getQBANumber<float>(allVerts.mid(i + 0, 4));
      a.z = getQBANumber<float>(allVerts.mid(i + 4, 4));
      a.y = getQBANumber<float>(allVerts.mid(i + 8, 4));
      a.InvertZ();
      //LOGFILE.Debug("Vert X: %f, Y: %f, Z: %f", a.x, a.y, a.z);

      Verticies.push_back(a);
    }
  };
};