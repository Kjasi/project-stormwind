#pragma once
#include <Foundation/Functions.h>

struct MOPY_Chunk
{
  struct Polyflags
  {
    uint8_t F_UNK_0x01 : 1;
    uint8_t F_NOCAMCOLLIDE : 1;
    uint8_t F_DETAIL : 1;
    uint8_t F_COLLISION : 1; // Turns off rendering of water ripple effects. May also do more. Should be used for ghost material triangles.
    uint8_t F_HINT : 1;
    uint8_t F_RENDER : 1;
    uint8_t F_UNK_0x40 : 1;
    uint8_t F_COLLIDE_HIT : 1;

    bool isTransFace() { return F_UNK_0x01 && (F_DETAIL || F_RENDER); }
    bool isColor() { return !F_COLLISION; }
    bool isRenderFace() { return F_RENDER && !F_DETAIL; }
    bool isCollidable() { return F_COLLISION || isRenderFace(); }

    explicit Polyflags(uint8_t byte)
    {
      F_COLLIDE_HIT	 = ((byte >> 7) & 1) != 0;
      F_UNK_0x40		 = ((byte >> 6) & 1) != 0;
      F_RENDER		 = ((byte >> 5) & 1) != 0;
      F_HINT			 = ((byte >> 4) & 1) != 0;
      F_COLLISION		 = ((byte >> 3) & 1) != 0;
      F_DETAIL		 = ((byte >> 2) & 1) != 0;
      F_NOCAMCOLLIDE	 = ((byte >> 1) & 1) != 0;
      F_UNK_0x01		 = ((byte >> 0) & 1) != 0;
    }
  };

  QList<uint8_t> MaterialIDs;		// index into MOMT, 0xff for collision faces
  QList<Polyflags> Flags;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOPY_Chunk...");

    QByteArray allData = data->mid(start, (uint32_t)size);

    for (uint32_t i = 0; i < (uint32_t)allData.size(); i += 2)
    {
      uint8_t fi = getQBANumber<uint8_t>(allData.mid(i + 0, 1));
      Polyflags f = Polyflags(fi);
      Flags.push_back(f);
      //LOGFILE.Debug("Flags: %i, Transparent: %s, Color: %s, RenderFace: %s, Collide: %s", fi, f.isTransFace() ? "true" : "false", f.isColor() ? "true" : "false", f.isRenderFace() ? "true" : "false", f.isCollidable() ? "true" : "false");

      int m = getQBANumber<uint8_t>(allData.mid(i + 1, 1));
      MaterialIDs.push_back(m);
      //LOGFILE.Debug("MatID: %i", m);
    }
  }
};