#pragma once
#include <Foundation/Classes/Vectors.h>

struct MONR_Chunk
{
  QList<Vec3D> Normals;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MONR_Chunk...");

    QByteArray allNormals = data->mid(start, (uint32_t)size);

    for (uint32_t i = 0; i < (uint32_t)allNormals.size(); i += 12)
    {
      Vec3D a;
      a.x = getQBANumber<float>(allNormals.mid(i + 0, 4));
      a.z = getQBANumber<float>(allNormals.mid(i + 4, 4));
      a.y = getQBANumber<float>(allNormals.mid(i + 8, 4));
      a.InvertZ();
      //LOGFILE.Debug("Vert X: %f, Y: %f, Z: %f", a.x, a.y, a.z);

      Normals.push_back(a);
    }
  }
};