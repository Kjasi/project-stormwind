﻿#pragma once
#include <WoWFormats/CommonClasses.h>

struct MOHD_Chunk
{
  uint32_t nTextures;
  uint32_t nGroups;
  uint32_t nPortals;
  uint32_t nLights;
  uint32_t nDoodadNames;
  uint32_t nDoodadDefs;
  uint32_t nDoodadSets;
  CArgba ambientColor;
  uint32_t wmoID;
  CAaBox boundingBox;
  struct {
    uint16_t do_not_attenuate_vertices_based_on_distance_to_portal : 1;
    uint16_t skip_base_color : 1;                       // do not add base (ambient) color (of MOHD) to MOCVs. apparently does more, e.g. required for multiple MOCVs
    uint16_t use_liquid_type_dbc_id : 1;                // use real liquid type ID from DBCs instead of local one. See MLIQ for further reference.
    uint16_t lighten_interiors : 1;                     // makes iterior groups much brighter, effects MOCV rendering. Used e.g.in Stormwind for having shiny bright interiors,
    uint16_t lod : 1;
    uint16_t : 11;                                      // unused as of Legion (20994)
  } flags;
  uint16_t numLODs;										// includes base lod (→ numLod = 3 means '.wmo', 'lod0.wmo' and 'lod1.wmo')

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading MOHD_Chunk...");
    QByteArray headerData = data->mid(start, (int)size);
    MOHD_Chunk *a = reinterpret_cast<MOHD_Chunk*>(headerData.data());
    nTextures = a->nTextures;
    nGroups = a->nGroups;
    nPortals = a->nPortals;
    nLights = a->nLights;
    nDoodadNames = a->nDoodadNames;
    nDoodadDefs = a->nDoodadDefs;
    nDoodadSets = a->nDoodadSets;
    ambientColor = a->ambientColor;
    wmoID = a->wmoID;
    boundingBox = a->boundingBox;
    flags = a->flags;
    numLODs = a->numLODs;
  };
};