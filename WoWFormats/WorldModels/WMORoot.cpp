#include "WMORoot.h"
#include <qfile.h>
#include <qbuffer.h>

WMORoot::WMORoot(QString root_filename)
  :rootFilename(root_filename)
{
}

void WMORoot::read()
{
  LOGFILE.Info("Reading WMO Root file \"%s\"...", qPrintable(rootFilename));
  if (validateFile(rootFilename, "WMO Root") == false) { return; }

  //LOGFILE.Debug("WMO Root file is a valid file. Checking to see if it's really a WMO...");

  QFile *file = new QFile(rootFilename);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(rootFilename));
    return;
  }
  QByteArray fileData = file->readAll();
  file->close();
  file->deleteLater();
  if (flipcc(fileData.left(4)) != "MVER")
  {
    LOGFILE.Error("File %s is not a compatible World of Warcraft WMO file.", qPrintable(rootFilename));
    return;
  }

  if (fileData.contains(qPrintable(flipcc("MOHD"))) == false)
  {
    LOGFILE.Error("Tried to load a Group WMO file into the Root file class.");
    return;
  }

  //LOGFILE.Debug("WMO Root file validated! Commencing reading...");

  // Time to read the chunk data
  fpos_t filePos = 12;
  do
  {
    QString chunk = flipcc(fileData.mid(filePos, 4));
    uint32_t size = getQBANumber<uint32_t>(fileData.mid(filePos + 4, 4));

    LOGFILE.Debug("Checking Chunk: %s, Size: %i", qPrintable(chunk), size);

    if (chunk == "MOHD")
    {
      LOGFILE.Debug("MOHD chunk found");
      header = new MOHD_Chunk;
      header->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOTX")
    {
      LOGFILE.Debug("MOTX chunk found");
      MOTXChunk = new MOTX_Chunk;
      MOTXChunk->read(&fileData.mid(filePos + 8, size));

      LOGFILE.Debug("Num Textures: %i", MOTXChunk->strings->count());
      for (uint32_t i = 0; i < (uint32_t)MOTXChunk->strings->count(); i++)
      {
        QString a = MOTXChunk->strings->at(i);
        LOGFILE.Debug("Texture #%i: %s", i, qPrintable(a));
      }
    }
    else if (chunk == "MOMT")
    {
      LOGFILE.Debug("MOMT chunk found");
      MOMTChunk = new MOMT_Chunk;
      MOMTChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MOUV")
    {
      LOGFILE.Debug("MOUV chunk found");
    }
    else if (chunk == "MOGN")
    {
      LOGFILE.Debug("MOGN chunk found");
      MOGNChunk = new MOGN_Chunk;
      MOGNChunk->read(&fileData.mid(filePos + 8, size));
    }
    else if (chunk == "MOGI")
    {
      LOGFILE.Debug("MOGI chunk found");
    }
    else if (chunk == "MOSB")
    {
      LOGFILE.Debug("MOSB chunk found");
    }
    else if (chunk == "MOPV")
    {
      LOGFILE.Debug("MOPV chunk found");
    }
    else if (chunk == "MOPT")
    {
      LOGFILE.Debug("MOPT chunk found");
    }
    else if (chunk == "MOPR")
    {
      LOGFILE.Debug("MOPR chunk found");
    }
    else if (chunk == "MOVV")
    {
      LOGFILE.Debug("MOVV chunk found");
    }
    else if (chunk == "MOVB")
    {
      LOGFILE.Debug("MOVB chunk found");
    }
    else if (chunk == "MOLT")
    {
      LOGFILE.Debug("MOLT chunk found");
      MOLTChunk = new MOLT_Chunk;
      MOLTChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MODS")
    {
      LOGFILE.Debug("MODS chunk found");
      MODSChunk = new MODS_Chunk;
      MODSChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MODN")
    {
      LOGFILE.Debug("MODN chunk found");
      MODNChunk = new MODN_Chunk;
      MODNChunk->read(&fileData.mid(filePos + 8, size));
    }
    else if (chunk == "MODD")
    {
      LOGFILE.Debug("MODD chunk found");
      MODDChunk = new MODD_Chunk;
      MODDChunk->read(&fileData, filePos + 8, size);
    }
    else if (chunk == "MFOG")
    {
      LOGFILE.Debug("MFOG chunk found");
    }
    else if (chunk == "MCVP")
    {
      LOGFILE.Debug("MCVP chunk found");
    }
    else if (chunk == "GFID")
    {
      LOGFILE.Debug("GFID chunk found");
    }

    filePos += 8 + size;
  } while (filePos < fileData.size());
}