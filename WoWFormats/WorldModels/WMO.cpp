#include "WMO.h"
#include <ProjectStormwind/Utilities/common.h>
#include <Foundation/Classes/Matrix.h>
#include <WoWFormats/Textures/BLP.h>
#include <QtCore/qfile.h>
#include <QRegularExpression>

WMO::WMO(QString input_filename)
  :inputFilename(input_filename)
{
  double time_startf = getCurrentTime();
  LOGFILE.Debug("Start Time: %f", time_startf);
  findFilenames();

  if (rootFileName.isEmpty() == true || groupFileNames.count() <= 0)
  {
    if (rootFileName.isEmpty() == true)
    {
      LOGFILE.Error("Unable to load %s: Could not get the filename for the root file.", qPrintable(inputFilename));
    }
    else {
      LOGFILE.Error("Unable to load %s: Could not get any group names.", qPrintable(inputFilename));
    }
    return;
  }

  getRootFile();
  if (rootFile == nullptr)
  {
    LOGFILE.Error("Unable to load %s: Root file could not be loaded.", qPrintable(inputFilename));
    return;
  }

  getGroupFiles();
  if (groupFiles->count() <= 0)
  {
    LOGFILE.Error("Unable to load %s: Group files could not be loaded.", qPrintable(inputFilename));
    return;
  }

  buildModelObject();
  buildPlacementData();
  if (SETTINGS.value("disablerenderer", false).toBool() == false)
  {
    buildVulkanData();
  }

  loaded = true;

  double time_endf = getCurrentTime();
  LOGFILE.Debug("End Time: %f", time_endf);
  LOGFILE.Info("Read WMO file successfully in %.3f seconds.\n", (time_endf - time_startf));
}

WMO::~WMO()
{
  delete rootFile;
  delete groupFiles;
}

void WMO::findFilenames()
{
  LOGFILE.Info("Finding WMO filenames...");
  if (validateFile(inputFilename, "WMO") == false) { return; }	

  QFile *file = new QFile(inputFilename);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(inputFilename));
    return;
  }
  QByteArray fileData = file->readAll();
  file->close();
  file->deleteLater();
  if (fileData.left(4).toStdString().c_str() != flipcc("MVER"))
  {
    LOGFILE.Error("File %s is not a WMO file.", qPrintable(inputFilename));
    return;
  }

  int lastSlash = inputFilename.lastIndexOf("/");

  if (fileData.contains(qPrintable(flipcc("MOHD"))) == true)
  {
    LOGFILE.Debug("Selected file was Root WMO file.");
    rootFileName = inputFilename.mid(lastSlash + 1);
  }
  else {
    rootFileName = inputFilename.mid(lastSlash + 1, inputFilename.lastIndexOf("_") - (lastSlash + 1));
    rootFileName.append(".wmo");
    LOGFILE.Debug("Selected file was a Group WMO file.");
  }

  fileDirectory.setPath(inputFilename.left(lastSlash));
  QString FilenameNoExt = rootFileName.left(rootFileName.lastIndexOf("."));
  LOGFILE.Debug("rootFileName: %s (%s) from %s", qPrintable(rootFileName), qPrintable(FilenameNoExt), qPrintable(fileDirectory.absolutePath()));

  QStringList filters;
  filters << QString("%1_*.wmo").arg(FilenameNoExt);

  QList<QString> listToCheck;
  QStringList files = fileDirectory.entryList(filters, QDir::Files);

  LOGFILE.Debug("Num Files Found: %i", files.count());
  for (int32_t i = 0; i < files.count(); i++)
  {
    // only use ones with our current name and 3 numbers
    QRegularExpression re("(.+)_\\d\\d\\d\\.wmo");
    QRegularExpressionMatch match = re.match(files.at(i));
    bool isMatched = match.hasMatch() && match.captured(1) == FilenameNoExt;
    LOGFILE.Debug("Checking %s for match: %s | %s", qPrintable(files.at(i)), (isMatched?"true":"false"), qPrintable(match.captured(1)));
    if (isMatched == false) continue;

    QString fname = fileDirectory.absoluteFilePath(files.at(i));
    if (listToCheck.contains(fname) == true)
    {
      continue;
    }
    listToCheck.push_back(fname);
  }

  for (int32_t i = 0; i < listToCheck.count(); i++)
  {
    QString fn = listToCheck.at(i);
    //LOGFILE.Debug("Checking for file: %s", qPrintable(fn));

    QFile fpo(fn);
    if (fpo.open(QIODevice::ReadOnly))
    {
      if (fn.contains("_lod1") || fn.contains("_lod2") || fn.contains("_lod3"))   // I have yet to find an lod3, but just to be sure...
      {
        LOGFILE.Info("Found LOD file: %s", qPrintable(fn));
        groupLODFileNames.push_back(fn);
      }
      else {
        LOGFILE.Info("Found file: %s", qPrintable(fn));
        groupFileNames.push_back(fn);
      }
      fpo.close();
    }
    fpo.deleteLater();
  }
}

void WMO::getRootFile()
{
  rootFile = new WMORoot(fileDirectory.absoluteFilePath(rootFileName));
  rootFile->read();
}

void WMO::getGroupFiles()
{
  groupFiles = new QList<WMOGroup*>;
  for (size_t i = 0; i < groupFileNames.count(); i++)
  {
    WMOGroup *g = new WMOGroup(rootFile, groupFileNames.at((uint32_t)i));
    g->read();
    groupFiles->push_back(g);
  }
}

QStringList WMO::getTextureList()
{
  if (rootFile->MOTXChunk->strings->count() > 0)
  {
    return *rootFile->MOTXChunk->strings;
  }
  else if (rootFile->MOMTChunk->MaterialData.count() > 0)
  {
    QStringList list;
    for (int i = 0; i < rootFile->MOMTChunk->MaterialData.count(); i++)
    {
      list.push_back(QString("Texture File ID %1").arg(rootFile->MOMTChunk->MaterialData.at(i).diffuseNameIndex));
    }
    list.sort();
    return list;
  }
  else {
    LOGFILE.Error("Unable to retrive WMO texture list.");
  }
  return QStringList();
}

QStringList WMO::getDoodadList()
{
  QStringList list;
  if (rootFile->MODNChunk->strings->count() > 0)
  {
    list.append(*rootFile->MODNChunk->strings);
  }
  else if (rootFile->MODIChunk != NULL && rootFile->MODIChunk->Indexs->count() > 0)
  {
    for (int i = 0; i < rootFile->MODIChunk->Indexs->count(); i++)
    {
      list.push_back(QString("M2 File ID %1").arg(rootFile->MODIChunk->Indexs->at(i)));
    }
  }
  list.sort();
  return list;
}

void WMO::buildModelObject()
{
  LOGFILE.Info("Building Model Object for WMO file...");

  if (modelScene == nullptr) modelScene = new ModelScene;

  modelScene->Name = rootFileName.mid(rootFileName.lastIndexOf('/'), rootFileName.lastIndexOf('.') - rootFileName.lastIndexOf('/'));
  modelObject = new ModelObject;
  ModelMeshData *collision = new ModelMeshData;

  modelObject->objectName = rootFileName.mid(rootFileName.lastIndexOf('/'), rootFileName.lastIndexOf('.') - rootFileName.lastIndexOf('/'));
  modelObject->objectName.append("Mesh");

  uint64_t offset = 0;
  for (uint32_t gn = 0; gn < (uint32_t)groupFiles->count(); gn++)
  {
    WMOGroup *g = groupFiles->at(gn);
    ModelMeshData *model = new ModelMeshData;
    model->originalFilename = g->groupFilename;
    model->groupName = g->groupName;

    LOGFILE.Debug("Processing Group #%i, %s...", gn, qPrintable(g->groupName));
    for (uint32_t i = 0; i < (uint32_t)g->PointData.count(); i++)
    {
      ModelPoint a;
      a.Position = g->PointData.at(i);
      a.NormalVector = g->NormalData.at(i);
      if (g->PointUVData.count() > 0) a.UVPosition = g->PointUVData.at(i);
      model->pointList.push_back(a);
      model->boundingBox.updateMinAndMax(a.Position);
    }
    for (uint32_t i = 0; i < (uint32_t)g->PolygonData.count(); i++)
    {
      MOPY_Chunk::Polyflags matFlags = g->MaterialFlags.at(i);
      uint8_t matID = g->MaterialIDs.at(i);
      
      // skip collision polys
      if (matFlags.F_COLLISION == true)
      {
        continue;
      }

      uint32_t p0 = g->PolygonData.at(i).pointID0 + offset;
      uint32_t p1 = g->PolygonData.at(i).pointID1 + offset;
      uint32_t p2 = g->PolygonData.at(i).pointID2 + offset;

      ModelPolygon poly;
      poly.point2 = p0;
      poly.point1 = p1;
      poly.point0 = p2;
      poly.materialID = matID;
      //LOGFILE.Debug("Setting Poly Points: %i, %i, %i | Offset: %i | Final Points: %i, %i, %i", p0, p1, p2, offset, poly.point0, poly.point1, poly.point2);

      model->polygonList.push_back(poly);
    }
    offset += g->PointData.count();

    modelObject->boundingBox.updateMinAndMax(model->boundingBox);
    modelObject->MeshList.push_back(model);
  }

  LOGFILE.Debug("Material Count: %i", rootFile->MOMTChunk->MaterialData.count());
  for (uint32_t i = 0; i < (uint32_t)rootFile->MOMTChunk->MaterialData.count(); i++)
  {
    ModelMaterial modMat;
    QString filename = rootFile->MOTXChunk->getStringFromIndex(rootFile->MOMTChunk->MaterialData.at(i).diffuseNameIndex);
    modMat.Name = filename;
    modMat.ColorTextureID = modelObject->TextureList.count();
    modelObject->TextureList.push_back(filename);
    
    LOGFILE.Debug("Texture: %s", qPrintable(modMat.Name));
    modelObject->materialList.push_back(modMat);
  }

  for (uint32_t i = 0; i < (uint32_t)modelObject->TextureList.count(); i++)
  {
    BLPInfo *a = ReadBLPFile(WoWDir.absoluteFilePath(modelObject->TextureList.at(i)));
    modelObject->ImageList.push_back(a->getImageData());
  }

  if (collision->polygonList.count() > 0)
    modelObject->MeshList.push_back(collision);

  modelScene->Objects.push_back(modelObject);

  objectBuilt = true;
  LOGFILE.Info("Model building complete.");
}

void WMO::buildCollisionObject()
{
  if (collisionObject == nullptr) collisionObject = new ModelObject;

}

void WMO::buildPlacementData()
{
  LOGFILE.Info("Building WMO Placement Data...");
  placementData = new PlacementData;

  // ADT Parent
  PlacementObject pWMO;
  pWMO.objectType = PLACEMENTOBJECTTYPE_WMO;
  int nameStart = rootFileName.lastIndexOf("/") + 1;
  int nameEnd = rootFileName.lastIndexOf(".");
  pWMO.Name = rootFileName.mid(nameStart, nameEnd - nameStart);

  if (rootFile->MODSChunk == NULL && rootFile->MODDChunk == NULL)
  {
    return;
  }

  // Doodad Sets
  for (uint32_t i = 0; i < (uint32_t)rootFile->MODSChunk->DoodadSetData.count(); i++)
  {
    // Set Parent
    PlacementObject pObj;
    MODS_Chunk::MODSData dds = rootFile->MODSChunk->DoodadSetData.at(i);
    pObj.objectType = PLACEMENTOBJECTTYPE_NULL;
    pObj.Name = dds.Name;

    // Per-Group splitting. Makes user sorting easier.
    for (int32_t g = 0; g < groupFiles->count(); g++)
    {
      WMOGroup *group = groupFiles->at(g);

      PlacementObject gObj;
      gObj.objectType = PLACEMENTOBJECTTYPE_NULL;
      gObj.Name = group->groupName;

      // Doodad Instances
      int ddlength = QString::number(group->DoodadIDs.count()).length();
      for (int32_t ddid = 0; ddid < group->DoodadIDs.count(); ddid++)
      {
        PlacementObject ddObj;
        MODD_Chunk::MODDData a = rootFile->MODDChunk->DoodadInstanceData.at(group->DoodadIDs.at(ddid));

        ddObj.objectType = PLACEMENTOBJECTTYPE_DOODAD;
        ddObj.Location = a.Position * YARDStoMETERS;
        Matrix4D rotMatrix = Matrix4D::newRotateQuaternion(a.Rotation);
        ddObj.setRotation(a.Rotation);
        ddObj.Scale = Vec3D(a.Scale);

        if (rootFile->MODIChunk != NULL && rootFile->MODIChunk->Indexs->count() > 0)
        {
          // Don't have a system for this yet.
        }
        else {
          ddObj.Name = QString("%1_DD%2_%3").arg(group->groupName).arg(QString::number(ddid), ddlength, QChar('0')).arg(rootFile->MODNChunk->getStringFromIndex(a.nameIndex));
          ddObj.Name.replace(".M2", ".m2", Qt::CaseSensitive);
          ddObj.Name.replace(".mdx", ".m2", Qt::CaseInsensitive);
          ddObj.Name.replace(".mdl", ".m2", Qt::CaseInsensitive);
        }
        gObj.children.push_back(ddObj);
      }

      // Lights
      int lightlength = QString::number(group->LightIDs.count()).length();
      for (int32_t lid = 0; lid < group->LightIDs.count(); lid++)
      {
        int32_t LightID = group->LightIDs.at(lid);
        int lIDlength = QString::number(group->LightIDs.at(lid)).length();
        
        // Light Data
        PlacementLight pLight;
        WMOLight a = rootFile->MOLTChunk->lightList.at(LightID);
        pLight.objectType = PLACEMENTOBJECTTYPE_LIGHT;
        pLight.Name = QString("%1_L%2_Light%3").arg(group->groupName).arg(QString::number(lid), lightlength, QChar('0')).arg(QString::number(LightID), lIDlength, QChar('0'));
        pLight.OriginOffset = Vec3D(0.0);
        pLight.Location = a.position;
        pLight.setRotation(Vec3D(0.0));
        pLight.Scale = Vec3D(1.0);
        pLight.useAttenuation = (bool)a.useAtten;
        pLight.sourceRadius = a.attenStart;
        pLight.attenuationRadius = a.attenEnd;
        pLight.lightColor = a.color.toRGBAColor();
        pLight.lightIntensity = a.intensity;

        switch (a.type)
        {
        case WMOLight::LightType::AMBIENT:
          LOGFILE.Error("Ambient Lights not yet supported. Skipping Light. Light ID: %i, Object: %s", LightID, rootFileName);
          continue;
          break;
        case WMOLight::LightType::DIRECTIONAL:
          LOGFILE.Error("Directional Lights not yet supported. Skipping Light. Light ID: %i, Object: %s", LightID, rootFileName);
          continue;
          break;
        case WMOLight::LightType::SPOT:
          pLight.lightType = PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT;
          LOGFILE.Warn("Spot Lights not yet fully supported. Light ID: %i, Object: %s", LightID, rootFileName);
          break;
        case WMOLight::LightType::OMNI:
        default:
          pLight.lightType = PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT;
          break;
        }
        gObj.childrenLights.push_back(pLight);
      }

      pObj.children.push_back(gObj);
    }

    // Add to Placement Data
    pWMO.children.push_back(pObj);
  }

  placementData->Objects->push_back(pWMO);
  placementBuilt = true;
}
