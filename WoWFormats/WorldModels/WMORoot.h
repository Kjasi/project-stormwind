#pragma once
#include <QtCore/QString>

// Subchunks
#include "Chunks/MOHD.h"
#include "Chunks/MOTX.h"
#include "Chunks/MOMT.h"
#include "Chunks/MOGN.h"
#include "Chunks/MODN.h"
#include "Chunks/MODI.h"
#include "Chunks/MODS.h"
#include "Chunks/MODD.h"
#include "Chunks/MOLT.h"

class WMORoot
{
public:
  QString rootFilename;
  WMORoot(QString root_filename);

  void read();

  // SubChunks
  MOHD_Chunk *header = NULL;
  MOTX_Chunk *MOTXChunk = NULL;
  MOMT_Chunk *MOMTChunk = NULL;
  MOGN_Chunk *MOGNChunk = NULL;
  MODN_Chunk *MODNChunk = NULL;
  MODI_Chunk *MODIChunk = NULL;
  MODS_Chunk *MODSChunk = NULL;
  MODD_Chunk *MODDChunk = NULL;
  MOLT_Chunk *MOLTChunk = NULL;
};