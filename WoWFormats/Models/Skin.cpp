#include "Skin.h"
#include <QtCore/qfile.h>
#include <Foundation/Functions.h>

SkinBlock::SkinBlock(QByteArray *data, fpos_t start, M2Header *model_header)
  : fileData(data), fileOrigin(start), modelHeader(model_header)
{}

void SkinBlock::read()
{
  // Read headers
  for (uint32_t i = 0; i < modelHeader->skin_profiles.num; i++)
  {
    LOGFILE.Debug("Reading Skin Header...");
    QByteArray hdata = fileData->mid(fileOrigin, sizeof(SkinHeader));
    uint32_t offset = 0;
    SkinHeader a;

    a.verticies.read(hdata, offset);
    a.indices.read(hdata, offset += 8);
    a.bones.read(hdata, offset += 8);
    a.submeshes.read(hdata, offset += 8);
    a.batches.read(hdata, offset += 8);
    a.boneCountMax = getQBANumber<uint32_t>(hdata.mid(offset += 8, 4));

    // Since Cataclysm, shadow_batches
    if (modelHeader->version >= 265)
    {
      a.shadowBatches.read(hdata, offset += 4);
    }
    headers.push_back(a);
  }

  // Read Data
  for (uint32_t sp = 0; sp < modelHeader->skin_profiles.num; sp++)
  {
    SkinData skd;

    // Read Verts
    if (headers.at(sp).verticies.num > 0)
    {
      LOGFILE.Debug("Reading Skin verticies list...");
      QByteArray data = fileData->mid(headers.at(sp).verticies.offset, headers.at(sp).verticies.num * sizeof(uint16_t));
      for (uint32_t i = 0; i < headers.at(sp).verticies.num; i++)
      {
        uint16_t a = getQBANumber<uint16_t>(data.mid(i * sizeof(uint16_t), 2));
        skd.VertIDList.push_back(a);
      }
    }

    // Indices
    if (headers.at(sp).indices.num > 0)
    {
      LOGFILE.Debug("Reading Skin indices...");
      QByteArray data = fileData->mid(headers.at(sp).indices.offset, headers.at(sp).indices.num * sizeof(uint16_t));
      for (uint32_t i = 0; i < headers.at(sp).indices.num; i++)
      {
        skd.IndexList.push_back(getQBANumber<uint16_t>(data.mid((i) * sizeof(uint16_t), 2)));
      }
    }

    // Submeshes
    if (headers.at(sp).submeshes.num > 0)
    {
      LOGFILE.Debug("Reading Skin Submeshes...");
      QByteArray data = fileData->mid(headers.at(sp).submeshes.offset, headers.at(sp).submeshes.num * sizeof(M2SkinGroup));
      if (modelHeader->version < 260)
        data = fileData->mid(headers.at(sp).submeshes.offset, headers.at(sp).submeshes.num * sizeof(M2SkinGroupVanilla));
      for (uint32_t i = 0; i < headers.at(sp).submeshes.num; i++)
      {
        M2SkinGroup a = *reinterpret_cast<M2SkinGroup*>(data.mid(sizeof(M2SkinGroup) * i, sizeof(M2SkinGroup)).data());
        if (modelHeader->version < 260)
          a = reinterpret_cast<M2SkinGroupVanilla*>(data.mid(sizeof(M2SkinGroupVanilla) * i, sizeof(M2SkinGroupVanilla)).data());

        skd.SubmeshList.push_back(a);
      }
    }

    // Texture data
    if (headers.at(sp).batches.num > 0)
    {
      LOGFILE.Debug("Reading Texture Units...");
      QByteArray data = fileData->mid(headers.at(sp).batches.offset, headers.at(sp).batches.num * sizeof(SkinTextureUnit));
      for (uint32_t i = 0; i < headers.at(sp).batches.num; i++)
      {
        SkinTextureUnit a = *reinterpret_cast<SkinTextureUnit*>(data.mid(sizeof(SkinTextureUnit) * i, sizeof(SkinTextureUnit)).data());
        skd.TextureUnits.push_back(a);
      }
    }

    skinData.push_back(skd);
  }

  LOGFILE.Debug("Finished reading Skin data.");
}

SkinFile::SkinFile(QString filename, M2Header *model_header)
  : fileName(filename), modelHeader(model_header)
{}

void SkinFile::read()
{
  LOGFILE.Debug("Opening Skin file...");
  // open Skin file
  QFile *file = NULL;

  QByteArray fData;
  LOGFILE.Info("Reading Skin file \"%s\"...", qPrintable(fileName));
  if (validateFile(fileName, "Skin") == false) { return; }

  //LOGFILE.Debug("Skin file is a valid file. Checking to see if it's really a Skin model...");

  file = new QFile(fileName);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(fileName));
    return;
  }
  fData = file->readAll();
  if (fData.left(4) != "SKIN")
  {
    file->close();
    LOGFILE.Error("File %s is not a Skin file.", qPrintable(fileName));
    return;
  }

  if (file->isOpen() == false)
  {
    LOGFILE.Warn("File is no longer open. Attempting to re-open it...");
    if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
    {
      LOGFILE.Error("Unable to open file %s for second reading...", qPrintable(fileName));
      return;
    }
  }
  fileData = new QByteArray(fData);

  // Read header
  {
    LOGFILE.Debug("Reading Skin Header...");
    QByteArray hdata = fileData->mid(4, sizeof(SkinHeader));
    uint32_t offset = 0;

    header.verticies.read(hdata, offset);
    header.indices.read(hdata, offset += 8);
    header.bones.read(hdata, offset += 8);
    header.submeshes.read(hdata, offset += 8);
    header.batches.read(hdata, offset += 8);
    header.boneCountMax = getQBANumber<uint32_t>(hdata.mid(offset += 8, 4));

    // Since Cataclysm, shadow_batches
    if (modelHeader->version >= 265)
    {
      header.shadowBatches.read(hdata, offset += 4);
    }
  }

  // Read Verts
  if (header.verticies.num > 0)
  {
    LOGFILE.Debug("Reading Skin verticies list...");
    QByteArray data = fileData->mid(header.verticies.offset, header.verticies.num * sizeof(uint16_t));
    for (uint32_t i = 0; i < header.verticies.num; i++)
    {
      uint16_t a = getQBANumber<uint16_t>(data.mid(i * sizeof(uint16_t), 2));
      skinData.VertIDList.push_back(a);
    }
  }

  // Indices
  if (header.indices.num > 0)
  {
    LOGFILE.Debug("Reading Skin indices...");
    QByteArray data = fileData->mid(header.indices.offset, header.indices.num * sizeof(uint16_t));
    for (uint32_t i = 0; i < header.indices.num; i++)
    {
      skinData.IndexList.push_back(getQBANumber<uint16_t>(data.mid((i) * sizeof(uint16_t), 2)));
    }
  }

  // Submeshes
  if (header.submeshes.num > 0)
  {
    LOGFILE.Debug("Reading Skin Submeshes...");
    QByteArray data = fileData->mid(header.submeshes.offset, header.submeshes.num * sizeof(M2SkinGroup));
    if (modelHeader->version < 260)
      data = fileData->mid(header.submeshes.offset, header.submeshes.num * sizeof(M2SkinGroupVanilla));
    for (uint32_t i = 0; i < header.submeshes.num; i++)
    {
      M2SkinGroup a = *reinterpret_cast<M2SkinGroup*>(data.mid(sizeof(M2SkinGroup) * i, sizeof(M2SkinGroup)).data());
      if (modelHeader->version < 260)
        a = reinterpret_cast<M2SkinGroupVanilla*>(data.mid(sizeof(M2SkinGroupVanilla) * i, sizeof(M2SkinGroupVanilla)).data());

      skinData.SubmeshList.push_back(a);
    }
  }

  // Texture data
  if (header.batches.num > 0)
  {
    LOGFILE.Debug("Reading Skin Texture data...");
    QByteArray data = fileData->mid(header.batches.offset, header.batches.num * sizeof(SkinTextureUnit));
    for (uint32_t i = 0; i < header.batches.num; i++)
    {
      SkinTextureUnit a = *reinterpret_cast<SkinTextureUnit*>(data.mid(sizeof(SkinTextureUnit) * i, sizeof(SkinTextureUnit)).data());
      skinData.TextureUnits.push_back(a);
    }
  }

  LOGFILE.Debug("Finished reading Skin data.");
}