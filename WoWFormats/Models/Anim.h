#pragma once
#include "Headers/M2Common.h"
#include "Headers/M2Header.h"

struct M2SequenceFlags
{
  uint32_t setBlendedAnim : 1;		// Sets 0x80 when loaded. (M2Init)
  uint32_t flag_unk_0x02 : 1;
  uint32_t flag_unk_0x04 : 1;
  uint32_t flag_unk_0x08 : 1;
  uint32_t allEntiresLoaded : 1;		// apparently set during runtime in CM2Shared::LoadLowPrioritySequence for all entries of a loaded sequence (including aliases)
  uint32_t animInM2File : 1;			// primary bone sequence -- If set, the animation data is in the .m2 file. If not set, the animation data is in an .anim file.
  uint32_t hasNext : 1;				// has next / is alias (To find the animation data, the client skips these by following aliasNext until an animation without 0x40 is found.)
  uint32_t BlendedAnimation : 1;		// Blended animation (if either side of a transition has 0x80, lerp between end->start states, unless end==start by comparing bone values)
  uint32_t flag_unk_0x100 : 1;		// sequence stored in model?
  uint32_t flag_unk_0x200 : 1;
  uint32_t flag_unk_0x400 : 1;
  uint32_t flag_unk_0x800 : 1;		// seen in Legion 24500 models
  uint32_t flag_unk_0x1000 : 1;
  uint32_t flag_unk_0x2000 : 1;
  uint32_t flag_unk_0x4000 : 1;
  uint32_t flag_unk_0x8000 : 1;
  uint32_t flag_unk_0x10000 : 1;
  uint32_t flag_unk_0x20000 : 1;
  uint32_t flag_unk_0x40000 : 1;
  uint32_t flag_unk_0x80000 : 1;
  uint32_t flag_unk_0x100000 : 1;
  uint32_t flag_unk_0x200000 : 1;
};

// For Vanilla through Burning Crusade
struct M2SequenceBC
{
  uint16_t id;					// Animation id in AnimationData.dbc
  uint16_t variationIndex;		// Sub-animation id: Which number in a row of animations this one is.
  uint32_t start_timestamp;
  uint32_t end_timestamp;
  float movespeed;				// This is the speed the character moves with in this animation.
  M2SequenceFlags flags;			// See above.
  int16_t frequency;				// This is used to determine how often the animation is played. For all animations of the same type, this adds up to 0x7FFF (32767).
  uint16_t _padding;
  M2Range replay;					// May both be 0 to not repeat. Client will pick a random number of repetitions within bounds if given.
  uint32_t blendTime;				// The client blends (lerp) animation states between animations where the end and start values differ. This specifies how long that blending takes. Values: 0, 50, 100, 150, 200, 250, 300, 350, 500.
                  // The client blends between this sequence and the next sequence for blendTimeOut milliseconds.
                  // For blendTime, the client plays both sequences simultaneously while interpolating between their animation transforms.
  M2Bounds bounds;
  int16_t variationNext;			// id of the following animation of this AnimationID, points to an Index or is -1 if none.
  uint16_t aliasNext;				// id in the list of animations. Used to find actual animation if this sequence is an alias (flags & 0x40)

  void read(QByteArray *data, fpos_t start, size_t size);
};


// For WotLK through Mists
struct M2SequenceMists
{
  uint16_t id;					// Animation id in AnimationData.dbc
  uint16_t variationIndex;		// Sub-animation id: Which number in a row of animations this one is.
  uint32_t duration;				// The length of this animation sequence in milliseconds.
  float movespeed;				// This is the speed the character moves with in this animation.
  M2SequenceFlags flags;			// See above.
  int16_t frequency;				// This is used to determine how often the animation is played. For all animations of the same type, this adds up to 0x7FFF (32767).
  uint16_t _padding;
  M2Range replay;					// May both be 0 to not repeat. Client will pick a random number of repetitions within bounds if given.
  uint32_t blendTime;				// The client blends (lerp) animation states between animations where the end and start values differ. This specifies how long that blending takes. Values: 0, 50, 100, 150, 200, 250, 300, 350, 500.
                  // The client blends between this sequence and the next sequence for blendTimeOut milliseconds.
                  // For blendTime, the client plays both sequences simultaneously while interpolating between their animation transforms.
  M2Bounds bounds;
  int16_t variationNext;			// id of the following animation of this AnimationID, points to an Index or is -1 if none.
  uint16_t aliasNext;				// id in the list of animations. Used to find actual animation if this sequence is an alias (flags & 0x40)

  void read(QByteArray *data, fpos_t start, size_t size);
};

// Warlords of Draenor to Current
struct M2Sequence
{
  uint16_t id;					// Animation id in AnimationData.dbc
  uint16_t variationIndex;		// Sub-animation id: Which number in a row of animations this one is.
  uint32_t duration;				// The length of this animation sequence in milliseconds.
  float movespeed;				// This is the speed the character moves with in this animation.
  M2SequenceFlags flags;			// See above.
  int16_t frequency;				// This is used to determine how often the animation is played. For all animations of the same type, this adds up to 0x7FFF (32767).
  uint16_t _padding;
  M2Range replay;					// May both be 0 to not repeat. Client will pick a random number of repetitions within bounds if given.
  uint16_t blendTimeIn;			// The client blends (lerp) animation states between animations where the end and start values differ. This specifies how long that blending takes. Values: 0, 50, 100, 150, 200, 250, 300, 350, 500.
  uint16_t blendTimeOut;			// The client blends between this sequence and the next sequence for blendTimeOut milliseconds.
                  // For both blendTimeIn and blendTimeOut, the client plays both sequences simultaneously while interpolating between their animation transforms.
  M2Bounds bounds;
  int16_t variationNext;			// id of the following animation of this AnimationID, points to an Index or is -1 if none.
  uint16_t aliasNext;				// id in the list of animations. Used to find actual animation if this sequence is an alias (flags & 0x40)

  void read(QByteArray *data, fpos_t start, size_t size);

  M2Sequence() {}
  M2Sequence(const M2Sequence& v) { *this = v; };
  M2Sequence(const M2SequenceBC& v) : id(v.id), variationIndex(v.variationIndex), duration(v.end_timestamp - v.start_timestamp), movespeed(v.movespeed), flags(v.flags), frequency(v.frequency), _padding(v._padding), replay(v.replay), blendTimeIn(v.blendTime), blendTimeOut(v.blendTime), bounds(v.bounds), variationNext(v.variationNext), aliasNext(v.aliasNext) {}
  M2Sequence(const M2SequenceMists& v) : id(v.id), variationIndex(v.variationIndex), duration(v.duration), movespeed(v.movespeed), flags(v.flags), frequency(v.frequency), _padding(v._padding), replay(v.replay), blendTimeIn(v.blendTime), blendTimeOut(v.blendTime), bounds(v.bounds), variationNext(v.variationNext), aliasNext(v.aliasNext) {}
};