#pragma once
#include <Foundation/Classes/ModelClass.h>
#include <Foundation/Classes/WoWVersion.h>
#include "Headers/M2Header.h"
#include "M21.h"

enum ModelFormat {
  MODELFORMAT_MD20 = 0,
  MODELFORMAT_MD21,
};

class M2: public ModelClass
{
public:
  ModelFormat modelFormat = MODELFORMAT_MD20;
  int modelVersion = 0;

  M2(QString input_filename, cWoWVersion wow_version, bool silent = false, bool version_only = false);
  ~M2();

  QString getModelName() { return inputFilename; }
  QStringList getTextureList() { QStringList a; a.append(*textureFiles); a.append(*replaceableTextureFiles); return a; };

  void buildModelObject();
  void buildCollisionObject();
  bool isLoaded() { return loaded; }
  bool isObjectBuilt() { return objectBuilt; }

private:
  bool loaded = false;
  bool objectBuilt = false;
  cWoWVersion wowVersion;
  QString inputFilename;
  QByteArray *fileData = NULL;
  QStringList *textureFiles = NULL;
  QStringList *replaceableTextureFiles = NULL;
  QList<M2Texture> textures;

  void getVersions();
  void getHeader();
  void getTextures();
  void getModelData();
  void getExpansion();

  void buildPlacementData() {};	// Not needed for M2s

  M2Header *modelHeader = NULL;
  M20 *M20Data = NULL;
  M21 *M21Data = NULL;
};