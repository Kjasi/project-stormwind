#include "M2.h"
#include "Headers/M2HeaderVersion.h"
#include "Headers/M2Header01.h"
#include "Headers/M2Header02.h"
#include "Headers/M2Header03.h"
#include <WoWFormats/Textures/BLP.h>
#include <ProjectStormwind/Utilities/common.h>

M2::M2(QString input_filename, cWoWVersion wow_version, bool silent, bool version_only)
  : inputFilename(input_filename), wowVersion(wow_version)
{
  textureFiles = new QStringList;
  replaceableTextureFiles = new QStringList;

  double time_startf = getCurrentTime();
  if (silent == false) LOGFILE.Debug("Start Time: %f", time_startf);

  if (inputFilename.isEmpty() == true)
  {
    LOGFILE.Error("Unable to load %s: Could not get the filename for the file.", qPrintable(inputFilename));
    return;
  }

  if (validateFile(inputFilename, "M2") == false) { return; }

  // Read the file
  QFile *file = new QFile(inputFilename);
  if (!file->open(QIODevice::OpenModeFlag::ReadOnly))
  {
    LOGFILE.Error("Unable to open %s for reading...", qPrintable(inputFilename));
    return;
  }
  
  QByteArray fData = file->readAll();
  file->close();
  if (fData.left(4) != "MD20" && fData.left(4) != "MD21")
  {
    LOGFILE.Error("File %s is not a M2 file.", qPrintable(inputFilename));
    return;
  }

  fileData = &fData;

  getVersions();
  if (version_only == true) return;
  getHeader();
  getExpansion();
  getTextures();
  getModelData();

  buildModelObject();
  buildVulkanData();

  loaded = true;

  double time_endf = getCurrentTime();
  fileData->clear();
  fileData = NULL;
  if (silent == false) LOGFILE.Debug("End Time: %f", time_endf);
  if (silent == false) LOGFILE.Info("Read M2 file successfully in %.3f seconds.\n", (time_endf - time_startf));
}

M2::~M2()
{
  if (textureFiles != NULL) delete textureFiles;
  if (replaceableTextureFiles != NULL) delete replaceableTextureFiles;
  if (modelHeader != NULL) delete modelHeader;
  if (M20Data != NULL) delete M20Data;
  if (M21Data != NULL) delete M21Data;
}

void M2::getVersions()
{
  M2HeaderVersion *a = new M2HeaderVersion;
  QString mode = QString(fileData->mid(0, 4));
  if (mode == "MD21")
  {
    a->read(fileData, 8, 8);
    modelFormat = ModelFormat::MODELFORMAT_MD21;
    modelVersion = a->version;
  }
  else {
    a->read(fileData, 0, 8);
    modelFormat = ModelFormat::MODELFORMAT_MD20;
    modelVersion = a->version;
  }
}

void M2::getHeader()
{
  if (modelVersion >= 256 && modelVersion <= 257)
  {
    modelHeader = new M2Header;
    M2Header01 *a = new M2Header01;
    a->read(fileData, 8, 8+sizeof(M2Header01));
    modelHeader->getFromHeader(a);
  }
  else if (modelVersion >= 260 && modelVersion <= 263)
  {
    modelHeader = new M2Header;
    M2Header02 *a = new M2Header02;
    a->read(fileData, 8, 8 + sizeof(M2Header02));
    modelHeader->getFromHeader(a);
  }
  else if (modelVersion >= 264)
  {
    modelHeader = new M2Header;
    M2Header03 *a = new M2Header03;
    uint32_t offset = fileData->indexOf("MD20");
    a->read(fileData, offset + 8, 8 + sizeof(M2Header03));
    modelHeader->getFromHeader(a);
  }
}

void M2::getTextures()
{
  LOGFILE.Info("Gathering M2 Texture filenames...");
  if (modelHeader->textures.num > 0 && modelHeader->textures.offset > 0)
  {
    QList<M2Texture> texfiles;

    // Get Header data
    for (uint32_t i = 0; i < modelHeader->textures.num; i++)
    {
      QByteArray data = fileData->mid(modelHeader->textures.offset + (i * sizeof(M2Texture)), sizeof(M2Texture));
      M2Texture a = *reinterpret_cast<M2Texture*>(data.data());
      texfiles.push_back(a);
    }

    LOGFILE.Debug("Texture File count: %i", texfiles.count());
    for (uint32_t i = 0; i < (uint32_t)texfiles.count(); i++)
    {
      M2Texture a = texfiles.at(i);

      LOGFILE.Debug("M2Texture: Type: %i, Filename Offset: %i, filename Len: %i", a.type, a.filename.offset, a.filename.num);

      // Actual Files
      if (a.type == 0)
      {
        QString filename = fileData->mid(a.filename.offset, a.filename.num);
        if (filename.isEmpty() == false)
        {
          textureFiles->push_back(filename);
          LOGFILE.Debug("Adding \"%s\" to the filenames...", qPrintable(filename));
        }
        else {
          LOGFILE.Error("Unable to get actual filename for file #%i.");
          textureFiles->push_back(QString());
          continue;
        }
      }
      else {
        // Replacable files
        LOGFILE.Warn("Unable to get replacable files at this time. Database manager has not been developed.");
      }
    }
  }
}

void M2::getModelData()
{
  LOGFILE.Info("Getting Model Data...");
  if (modelFormat == MODELFORMAT_MD21)
  {
    M21Data = new M21(inputFilename, modelHeader);
    M21Data->read();
  }
  else if (modelFormat == MODELFORMAT_MD20)
  {
    M20Data = new M20(inputFilename, modelHeader);
    M20Data->read();
  }
}

void M2::getExpansion()
{
  if (modelHeader->version < 256 || wowVersion.Major == 0)
  {
    modelHeader->expansion = EXPANSION_BETA;
    LOGFILE.Fatal("Beta version detected! Beta data files are not supported!");
  }
  else if ((modelHeader->version >= 256 && modelHeader->version <= 257) || wowVersion.Major == 1)
  {
    modelHeader->expansion = EXPANSION_VANILLA;
  }
  else if ((modelHeader->version >= 260 && modelHeader->version <= 263) || wowVersion.Major == 2)
  {
    modelHeader->expansion = EXPANSION_BURNINGCRUSADE;
  }
  else if (modelHeader->version == 264 || wowVersion.Major == 3)
  {
    modelHeader->expansion = EXPANSION_WRATHOFTHELICHKING;
  }
  else if ((modelHeader->version >= 265 && modelHeader->version <= 271) || wowVersion.Major == 4)
  {
    modelHeader->expansion = EXPANSION_CATACLYSM;
  }
  else if (modelHeader->version == 272)
  {
    if (modelFormat == MODELFORMAT_MD21)
    {
      modelHeader->expansion = EXPANSION_LEGION;
      return;
    }

    // Cataclysm
    if (wowVersion.Major == 4)
    {
      modelHeader->expansion = EXPANSION_CATACLYSM;
      return;
    }

    // Mists
    M2Flags f = modelHeader->globalFlags;
    if ((f.flag_load_phys_data == true && f.flag_texture_transforms_use_bone_sequences == false && f.flag_unk_0x2000 == false) || wowVersion.Major == 5)
    {
      modelHeader->expansion = EXPANSION_MISTSOFPANDARIA;
      return;
    }

    // Warlords
    if (wowVersion.Major == 6)
    {
      modelHeader->expansion = EXPANSION_WARLORDSOFDRAENOR;
      return;
    }

    // If we haven't hit it before now, assume Legion
    modelHeader->expansion = EXPANSION_LEGION;
  }
  else if (modelHeader->version == 273)
  {
    modelHeader->expansion = EXPANSION_LEGION;
  }
  else if (modelHeader->version == 274)
  {
    if (wowVersion.Major == 8)
    {
      modelHeader->expansion = EXPANSION_BATTLEFORAZEROTH;
      return;
    }

    // Else, assume Legion
    modelHeader->expansion = EXPANSION_LEGION;
  }
  else {
    if (modelFormat == MODELFORMAT_MD21)
    {
      LOGFILE.Error("Unknown expansion detected! Will continue to work, as if the current version. Some data may be missing...");
      LOGFILE.Debug("Version: %i", modelHeader->version);
      modelHeader->expansion = EXPANSION_CURRENT;
    }
    else {
      LOGFILE.Fatal("Unknown expansion detected! Unable to assume model format! Can not continue.");
      LOGFILE.Debug("Version: %i, Model Format: %i", modelHeader->version, modelFormat);
      modelHeader->expansion = EXPANSION_UNKNOWN;
    }
  }
}

void M2::buildModelObject()
{
  LOGFILE.Info("Building Model Object for M2 file...");
  QString postSlash = inputFilename.mid(inputFilename.lastIndexOf("/") + 1);

  if (modelScene == nullptr) modelScene = new ModelScene;
  modelScene->Name = postSlash.mid(0, postSlash.lastIndexOf("."));

  modelObject = new ModelObject;
  modelObject->objectName = postSlash.mid(0, postSlash.lastIndexOf("."));
  modelObject->objectName.append("Mesh");

  // Gather data to local
  M20* m20Data = NULL;
  if (modelFormat == MODELFORMAT_MD21)
  {
    m20Data = M21Data->M20Data;
  }
  else if (modelFormat == MODELFORMAT_MD20)
  {
    m20Data = M20Data;
  }
  if (m20Data == NULL)
  {
    LOGFILE.Fatal("Could not build model. Unable to get M20Data!");
    return;
  }

  QList<M2Vertex> Vertices = m20Data->Vertices;
  QList<M2Geoset> Geosets = m20Data->Geosets;
  QList<M2BoneRaw> RawBoneList = m20Data->RawBoneList;
  QList<uint16_t> KeyboneList = m20Data->KeyboneList;
  QList<M2Material> Materials = m20Data->Materials;
  QList<uint16_t> TextureLookup = m20Data->TextureLookup;

  // Build Material
  for (uint32_t t = 0; t < (uint32_t)Materials.count(); t++)
  {
    ModelMaterial a;
    a.Name = Materials.at(t).Name;
    a.ColorTextureID = modelObject->TextureList.count();
    modelObject->TextureList.push_back(Materials.at(t).ColorTexture);

    modelObject->materialList.push_back(a);
  }

  for (uint32_t i = 0; i < (uint32_t)modelObject->TextureList.count(); i++)
  {
    QString filename = modelObject->TextureList.at(i);
    if (filename.length() == 0 || filename.contains(".null")) {
      modelObject->ImageList.push_back(&ImageData::generateSolidTexture(RGBAColor(0.5, 0.5, 0.5, 1.0)));
      continue;
    }
    BLPInfo *a = ReadBLPFile(WoWDir.absoluteFilePath(filename));
    modelObject->ImageList.push_back(a->getImageData());
  }

  // Process data
  size_t verts = 0;
  for (uint32_t g = 0; g < (uint32_t)Geosets.count(); g++)
  {
    M2Geoset geo = Geosets.at(g);
    ModelMeshData *model = new ModelMeshData;
    model->groupName = geo.IDName;
    uint32_t matID = TextureLookup.at(geo.TextureUnit.textureLookupIndex);

    for (uint32_t i = 0; i < geo.VertCount; i++)
    {
      uint32_t pnt = geo.VertStart + i;

      ModelPoint a;
      a.Position = Vertices.at(pnt).pos;
      a.UVPosition = Vertices.at(pnt).UvCoord[0];
      a.NormalVector = Vertices.at(pnt).normal;
      model->boundingBox.updateMinAndMax(a.Position);
      model->pointList.push_back(a);
    }

    for (uint32_t i = 0; i < (uint32_t)geo.Polygons.count(); i++)
    {
      Triangles index = geo.Polygons.at(i);
      ModelPolygon a;
      a.point2 = index.Indice0;
      a.point1 = index.Indice1;
      a.point0 = index.Indice2;

      a.materialID = matID;

      model->polygonList.push_back(a);
    }
    // verts += geo.VertCount;

    modelObject->boundingBox.updateMinAndMax(model->boundingBox);
    modelObject->MeshList.push_back(model);
  }

  // Build Skeleton
  for (uint32_t i = 0; i < (uint32_t)RawBoneList.count(); i++)
  {
    M2BoneRaw rawBone = RawBoneList.at(i);
    ModelBone *bone = new ModelBone;
    bone->BoneID = i;
    bone->BoneName = m20Data->BoneNames.value(i);
    //bone->Position = rawBone.Translation;

    modelObject->Skeleton.push_back(bone);
  }
  if (RawBoneList.count() > 0)
  {
    modelObject->isAnimated = true;
  }
  
  modelScene->Objects.push_back(modelObject);

  objectBuilt = true;
}

void M2::buildCollisionObject()
{
}
