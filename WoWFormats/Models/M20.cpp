#include "M20.h"
#include <qfileinfo.h>
#include <qfile.h>
#include <qvector.h>

M20::M20(QString file_name, M2Header *modelHeader)
  : fileName(file_name), fromM21(false), header(modelHeader)
{}

M20::M20(QByteArray *inc_file_data, fpos_t origin, QString file_name, M2Header *modelHeader)
  : fileName(file_name), fileOrigin(origin), fileData(inc_file_data), fromM21(true), header(modelHeader)
{}

void M20::read()
{
  QFile *file = NULL;

  int selectedLOD = 0;	// In the future, we might make this selectable.

  if (fromM21 == false)
  {
    QByteArray fData;
    LOGFILE.Info("Reading MD20 file \"%s\"...", qPrintable(fileName));
    if (validateFile(fileName, "MD20") == false) { return; }

    //LOGFILE.Debug("M2 file is a valid file. Checking to see if it's really a M2 model...");

    file = new QFile(fileName);
    if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
    {
      LOGFILE.Error("Unable to open file %s for reading...", qPrintable(fileName));
      return;
    }
    fData = file->readAll();
    if (fData.left(4) != "MD20")
    {
      file->close();
      LOGFILE.Error("File %s is not a MD20 file.", qPrintable(fileName));
      return;
    }

    if (file->isOpen() == false)
    {
      LOGFILE.Warn("File is no longer open. Attempting to re-open it...");
      if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
      {
        LOGFILE.Error("Unable to open file %s for second reading...", qPrintable(fileName));
        return;
      }
    }
    fileData = new QByteArray(fData);
  }
  else {
    LOGFILE.Info("Reading MD20 block...");
  }

  // Vertexs
  if (header->vertices.num > 0 && header->vertices.offset > 0)
  {
    LOGFILE.Debug("Reading Vertexes...");
    QByteArray data = fileData->mid(fileOrigin + header->vertices.offset, sizeof(M2Vertex)*header->vertices.num);
    std::vector<M2Vertex> rawVerts;
    M2Vertex *mv = (M2Vertex *)data.data();
    rawVerts.assign(mv, mv + header->vertices.num);
    Vertices = QList<M2Vertex>::fromVector(QVector<M2Vertex>::fromStdVector(rawVerts));

    LOGFILE.Debug("Vertex Count: %i, RawVerts: %i, Vertices Count: %i", header->vertices.num, rawVerts.size(), Vertices.count());
    for (uint32_t i = 0; i < (uint32_t)Vertices.count(); i++)
    {
      M2Vertex a = Vertices.value(i);
      a.UvCoord->InvertY();
      a.pos.SwapYZ();
      a.pos.InvertZ();
      a.normal.SwapYZ();
      a.normal.InvertZ();
      Vertices.replace(i, a);
      // LOGFILE.Debug("Pos X: %f, Y: %f, Z: %f | UVs: U: %f, V: %f | Normal X: %f, Y: %f, Z: %f", Vertices.value(i).pos.x, Vertices.value(i).pos.y, Vertices.value(i).pos.z, Vertices.value(i).UvCoord[0].x, Vertices.value(i).UvCoord[0].y, Vertices.value(i).normal.x, Vertices.value(i).normal.y, Vertices.value(i).normal.z);
    }
  }

  // Textures/Materials
  if (header->textures.num > 0 && header->textures.offset > 0)
  {
    LOGFILE.Debug("Reading Textures at offset %i...", header->textures.offset);
    QByteArray data = fileData->mid(fileOrigin + header->textures.offset, sizeof(M2Texture)*header->textures.num);

    for (uint32_t i = 0; i < header->textures.num; i++)
    {
      M2Texture a;
      a.type = getQBANumber<uint32_t>(data.mid(sizeof(M2Texture) * i + 0, sizeof(uint32_t)));
      a.flags = getQBANumber<uint32_t>(data.mid(sizeof(M2Texture) * i + 4, sizeof(uint32_t)));
      a.filename.num = getQBANumber<uint32_t>(data.mid(sizeof(M2Texture) * i + 8, sizeof(uint32_t)));
      a.filename.offset = getQBANumber<uint32_t>(data.mid(sizeof(M2Texture) * i + 12, sizeof(uint32_t)));

      M2Material mat;


      if (a.type == 0)
      {
        QString n = QString(fileData->mid(fileOrigin + a.filename.offset, a.filename.num));
        LOGFILE.Debug("Texture %i Type 0 name: %s", i, qPrintable(n));
        mat.Name = n;
        mat.ColorTexture = n;
      }
      else {
        QString n = M2CharTextureTypeStrings().value(a.type, "BadType");
        LOGFILE.Debug("Texture %i Type %i name: %s", i, a.type, qPrintable(n));
        mat.Name = n;
        mat.ColorTexture = QString("%1.null").arg(n);
      }

      Materials.push_back(mat);
    }
  }

  // Texture Lookup
  if (header->texture_lookup_table.num > 0 && header->texture_lookup_table.offset > 0)
  {
    QByteArray data = fileData->mid(fileOrigin + header->texture_lookup_table.offset, sizeof(uint16_t)*header->texture_lookup_table.num);
    for (uint32_t i = 0; i < header->texture_lookup_table.num; i++)
    {
      uint16_t a = getQBANumber<uint16_t>(data.mid(sizeof(uint16_t) * i, sizeof(uint16_t)));
      TextureLookup.push_back(a);
    }
  }

  // Skin/View Data
  if (header->skin_profiles.num > 0 && header->skin_profiles.offset > 0)
  {
    LOGFILE.Debug("Reading Skins at offset %i...", header->skin_profiles.offset);
    SkinBlock *a = new SkinBlock(fileData, header->skin_profiles.offset, header);
    a->read();
    
    // Process data
    SkinData skd = a->skinData.at(selectedLOD);
    Geosets = getGeoData(skd);
  }
  else {
    LOGFILE.Debug("Looking for %i Skin files...", header->num_skin_profiles);

    QList<QString> skinFileNames;
    QString baseName = fileName.left(fileName.lastIndexOf("."));
    bool hasLODinName = false;

    for (uint32_t i = 0; i < header->num_skin_profiles; i++)
    {
      QString testName = SkinFileExists(baseName, i);
      if (testName.isEmpty() == true) continue;

      if (testName.contains("_lod"))
        hasLODinName = true;

      skinFileNames.push_back(testName);
      LOGFILE.Debug("Found skin file %s!", qPrintable(testName));
    }

    QString skinTemplate = "%1%2.skin";
    if (hasLODinName == true)
      skinTemplate = "%1_lod%2.skin";

    QString LODName = QString(skinTemplate).arg(baseName).arg(QString::number(selectedLOD), 2, QChar('0'));
    LOGFILE.Debug("LODName: %s", qPrintable(LODName));

    if (skinFileNames.count() == 0)
    {
      LOGFILE.Fatal("Unable to find any skin files for %s. Aborting read...", qPrintable(fileName));
      return;
    }
    if (skinFileNames.contains(LODName) == false)
    {
      LOGFILE.Fatal("Selected Skin LOD for %s was not found. Aborting read...", qPrintable(fileName));
      return;
    }

    uint32_t index = 0;
    for (uint32_t i = 0; i < (uint32_t)skinFileNames.count(); i++)
    {
      QString testName = QString("%1%2.skin").arg(baseName).arg(QString("0%1").arg(i));
      if (LODName == testName)
      {
        index = i;
      }
    }

    SkinFile *a = new SkinFile(skinFileNames.at(index), header);
    a->read();

    // Process data
    SkinData skd = a->skinData;
    Geosets = getGeoData(skd);
  }

  // Keybones
  if (header->key_bone_lookup.offset > 0 && header->key_bone_lookup.num > 0)
  {
    LOGFILE.Debug("Reading all %i Key Bones...", header->key_bone_lookup.num);

    for (uint32_t b = 0; b < header->bones.num; b++)
    {
      uint16_t a = getQBANumber<uint16_t>(fileData->mid((uint32_t)fileOrigin + header->key_bone_lookup.offset + (sizeof(uint16_t) * b), sizeof(uint16_t)));
      KeyboneList.push_back(a);
    }
  }

  // Bones
  if (header->bones.offset > 0 && header->bones.num > 0)
  {
    LOGFILE.Debug("Reading all the Bones...");
    LOGFILE.Debug("Bones Count: %i", header->bones.num);
    for (uint32_t b = 0; b < header->bones.num; b++)
    {
      uint32_t byteOffset = 88;
      if (header->expansion == EXPANSION_VANILLA)
      {
        byteOffset -= 4;		// Remove BoneNameCRC
      }
      if (header->expansion < EXPANSION_WRATHOFTHELICHKING)
      {
        byteOffset += (8 * 3);	// Add M2Offset for FrameRange
      }

      M2BoneRaw *a = new M2BoneRaw;
      a->read(*fileData, fileOrigin + header->bones.offset + (byteOffset * b), header);
      BoneNames.push_back(getBoneIDName(*a));
      RawBoneList.push_back(*a);
    }
  }

  if (file != NULL && file->isOpen() == true)
  {
    file->close();
  }
  LOGFILE.Debug("Completed M20 read...");
}

// Process Geoset Data
QList<M2Geoset> M20::getGeoData(SkinData skd)
{
  QList<M2Geoset> rGeo;

  LOGFILE.Debug("Num Texture Units: %i, Num Submeshes: %i", skd.TextureUnits.count(), skd.SubmeshList.count());

  for (uint32_t i = 0; i < (uint32_t)skd.SubmeshList.count(); i++)
  {
    M2SkinGroup a = skd.SubmeshList.at(i);
    LOGFILE.Debug("Processing Submesh #%i with ID: %i", i, a.skinSectionId);
    M2Geoset geo;
    geo.ID = a.skinSectionId;
    geo.buildIDName();
    geo.VertStart = a.vertexStart;
    geo.VertCount = a.vertexCount;
    for (uint32_t t = 0; t < (uint32_t)skd.TextureUnits.count(); t++)
    {
      if (skd.TextureUnits.at(t).materialIndex == i)
      {
        geo.TextureUnit = skd.TextureUnits.at(t);
        /*
        LOGFILE.Debug("Texture Unit Details %i:", t);
        LOGFILE.Debug("	Texture Unit Index: %i", geo.TextureUnit.texUnit);
        LOGFILE.Debug("	Material Count: %i", geo.TextureUnit.materialCount);
        LOGFILE.Debug("	Material Index: %i", geo.TextureUnit.materialIndex);
        LOGFILE.Debug("	Geoset Index: %i", geo.TextureUnit.geosetIndex);
        LOGFILE.Debug("	Tex Lookup Index: %i", geo.TextureUnit.textureLookupIndex);
        LOGFILE.Debug("	Tex Coord Lookup Index: %i", geo.TextureUnit.textureLookupCoordIndex);
        LOGFILE.Debug("	Tex Transparency Index: %i", geo.TextureUnit.textureTransparencyIndex);
        LOGFILE.Debug("	Tex Transform Combo Index: %i", geo.TextureUnit.textureAnimIndex);
        LOGFILE.Debug("	Flags Index: %i", geo.TextureUnit.flagsIndex);
        LOGFILE.Debug("	Color Index: %i", geo.TextureUnit.colorIndex);
        */
        break;
      }
    }

    uint32_t istart = a.indexStart | +(a.Level << 16);

    //LOGFILE.Debug("Index Start: %i | %i, Index Count: %i", a.indexStart, istart, a.indexCount);
    //LOGFILE.Debug("Vert Start: %i, Vert Count: %i | VertIDList size: %i", a.vertexStart, a.vertexCount, skd.VertIDList.count());
    for (uint32_t t = 0; t < a.indexCount; t += 3)
    {
      Triangles newTri;

      uint32_t index0 = skd.IndexList.at(istart + t + 0);
      uint32_t index1 = skd.IndexList.at(istart + t + 1);
      uint32_t index2 = skd.IndexList.at(istart + t + 2);

      //LOGFILE.Debug("Indexes | 0: %i, 1: %i, 2: %i", index0, index1, index2);
      newTri.Indice0 = index0;
      newTri.Indice1 = index1;
      newTri.Indice2 = index2;

      geo.Polygons.push_back(newTri);
    }

    rGeo.push_back(geo);
  }
  return rGeo;
}

QString M20::SkinFileExists(QString filename, int skinID)
{
  // Pre BFA
  QString testName = QString("%1%2.skin").arg(filename).arg(QString::number(skinID), 2, QChar('0'));
  QFileInfo *info = new QFileInfo(testName);
  if (info->exists())
  {
    return testName;
  }

  testName = QString("%1_lod%2.skin").arg(filename).arg(QString::number(skinID), 2, QChar('0'));
  info = new QFileInfo(testName);
  if (info->exists())
  {
    return testName;
  }

  //LOGFILE.Warn("Unable to find skin file: %s", qPrintable(testName));
  return QString();
}

QString M20::getBoneIDName(M2BoneRaw bone)
{
  QString idName;

  int32_t kboneID = bone.key_bone_id;

  switch (kboneID)
  {
  case -1:
    break;
  case 0:
    idName = "ArmLeft";
    break;
  case 1:
    idName = "ArmRight";
    break;
  case 2:
    idName = "ShoulderLeft";
    break;
  case 3:
    idName = "ShoulderRight";
    break;
  case 4:
    idName = "SpineLow";
    break;
  case 5:
    idName = "Waist";
    break;
  case 6:
    idName = "Head";
    break;
  case 7:
    idName = "Jaw";
    break;
  case 8:
    idName = "IndexFingerRight";
    break;
  case 9:
    idName = "MiddleFingerRight";
    break;
  case 10:
    idName = "PinkyFingerRight";
    break;
  case 11:
    idName = "RingFingerRight";
    break;
  case 12:
    idName = "ThumbRight";
    break;
  case 13:
    idName = "IndexFingerLeft";
    break;
  case 14:
    idName = "MiddleFingerLeft";
    break;
  case 15:
    idName = "PinkyFingerLeft";
    break;
  case 16:
    idName = "RingFingerLeft";
    break;
  case 17:
    idName = "ThumbLeft";
    break;
  case 18:
    idName = "$BTH";
    break;
  case 19:
    idName = "WeaponRight";
    break;
  case 20:
    idName = "WeaponLeft";
    break;
  case 21:
    idName = "BreathSource";
    break;
  case 22:
    idName = "_Name";
    break;
  case 23:
    idName = "_NameMount";
    break;
  case 24:
    idName = "$CHD";
    break;
  case 25:
    idName = "$CCH";
    break;
  case 26:
    idName = "Root";
    break;
  case 27:
    idName = "Wheel1";
    break;
  case 28:
    idName = "Wheel2";
    break;
  case 29:
    idName = "Wheel3";
    break;
  case 30:
    idName = "Wheel4";
    break;
  case 31:
    idName = "Wheel5";
    break;
  case 32:
    idName = "Wheel6";
    break;
  case 33:
    idName = "Wheel7";
    break;
  case 34:
    idName = "Wheel8";
    break;
  default:
    idName = QString("Bone_%1").arg(QString::number(kboneID));
    break;
  }

  if (!idName.isEmpty())
    LOGFILE.Debug("Returned Bone Name: %s", qPrintable(idName));
  return idName;
}

void M2Geoset::buildIDName()
{
  /*
  ** starts with 01, not 00 with the exception of entry 0, which is the skin.
  DNE = Does Not Exist (AKA Empty Geoset)

  0000: Skin
  00**: Hair: {1-21: various hairstyles}
  01**: Facial1: {1-8: varies} (usually beard, but not always)
  02**: Facial2: {1: none (DNE), 2-6: varies} (usually mustache, but not always)
  03**: Facial3: {1: none (DNE), 2-11: varies} (usually sideburns, but not always)
  04**: Glove: {1-4}
  05**: Boots: {1-5}
  06**:
  07**: Ears: {1: none (DNE), 2: ears}
  08**: Wristbands / Sleeves: {1: none (DNE), 2: normal, 3: ruffled}
  09**: Kneepads / Legcuffs: {1: none (DNE), 2: long, 3: short}
  10**: Chest: {1: none (DNE), 2: ??? (exists but purpose unknown)}
  11**: Pants: {1: regular, 2: short skirt, 4: armored pants}
  12**: Tabard: {1: none (DNE), 2: tabard}
  13**: Trousers: {1: legs, 2: dress}
  14**:
  15**: Cloak: {1-10: various cloak styles}
  16**:
  17**: Eyeglows: {1: none (DNE), 2: racial eyeglow, 3: DK eyeglow}
  18**: Belt / bellypack: {1: none (sometimes DNE), 2: bulky belt}
  19**: Tail (in Legion this group also has Undead bones)
  20**: Feet: {1: none, 2: feet}
  21**:
  22**:
  23**: New in Legion (in addition to character models also exist in DH collections) { 1: hands for blood elf/night elf }
  24**: Horns (only exist in DH collections) {1-x}
  25**: Blindfolds (only exist in DH collections) {1-x}
  */


  if (ID == 0)
  {
    IDName = "Skin";
  }
  else if (ID < 100)
  {
    IDName = QString("%1_%2").arg("Hair").arg(QString::number(ID), 2, QChar('0'));
  }
  else if (ID < 200)
  {
    IDName = QString("%1_%2").arg("Facial1").arg(QString::number(ID - 100), 2, QChar('0'));
  }
  else if (ID < 300)
  {
    IDName = QString("%1_%2").arg("Facial2").arg(QString::number(ID - 200), 2, QChar('0'));
  }
  else if (ID < 400)
  {
    IDName = QString("%1_%2").arg("Facial3").arg(QString::number(ID - 300), 2, QChar('0'));
  }
  else if (ID < 500)
  {
    IDName = QString("%1_%2").arg("Glove").arg(QString::number(ID - 400), 2, QChar('0'));
  }
  else if (ID < 600)
  {
    IDName = QString("%1_%2").arg("Boots").arg(QString::number(ID - 500), 2, QChar('0'));
  }
  else if (ID < 700)
  {
    IDName = QString("%1_%2").arg("GeosetID600").arg(QString::number(ID - 600), 2, QChar('0'));
  }
  else if (ID < 800)
  {
    IDName = QString("%1_%2").arg("Ears").arg(QString::number(ID - 700), 2, QChar('0'));
  }
  else if (ID < 900)
  {
    IDName = QString("%1_%2").arg("WristbandSleeve").arg(QString::number(ID - 800), 2, QChar('0'));
  }
  else if (ID < 1000)
  {
    IDName = QString("%1_%2").arg("KneepadLegcuff").arg(QString::number(ID - 900), 2, QChar('0'));
  }
  else if (ID < 1100)
  {
    IDName = QString("%1_%2").arg("Chest").arg(QString::number(ID - 1000), 2, QChar('0'));
  }
  else if (ID < 1200)
  {
    IDName = QString("%1_%2").arg("Pants").arg(QString::number(ID - 1100), 2, QChar('0'));
  }
  else if (ID < 1300)
  {
    IDName = QString("%1_%2").arg("Tabard").arg(QString::number(ID - 1200), 2, QChar('0'));
  }
  else if (ID < 1400)
  {
    IDName = QString("%1_%2").arg("Trousers").arg(QString::number(ID - 1300), 2, QChar('0'));
  }
  else if (ID < 1500)
  {
    IDName = QString("%1_%2").arg("GeosetID1400").arg(QString::number(ID - 1400), 2, QChar('0'));
  }
  else if (ID < 1600)
  {
    IDName = QString("%1_%2").arg("Cloak").arg(QString::number(ID - 1500), 2, QChar('0'));
  }
  else if (ID < 1700)
  {
    IDName = QString("%1_%2").arg("GeosetID1600").arg(QString::number(ID - 1600), 2, QChar('0'));
  }
  else if (ID < 1800)
  {
    IDName = QString("%1_%2").arg("Eyeglows").arg(QString::number(ID - 1700), 2, QChar('0'));
  }
  else if (ID < 1900)
  {
    IDName = QString("%1_%2").arg("Belt").arg(QString::number(ID - 1800), 2, QChar('0'));
  }
  else if (ID < 2000)
  {
    IDName = QString("%1_%2").arg("Tail").arg(QString::number(ID - 1900), 2, QChar('0'));
  }
  else if (ID < 2100)
  {
    IDName = QString("%1_%2").arg("Feet").arg(QString::number(ID - 2000), 2, QChar('0'));
  }
  else if (ID < 2200)
  {
    IDName = QString("%1_%2").arg("GeosetID2100").arg(QString::number(ID - 2100), 2, QChar('0'));
  }
  else if (ID < 2300)
  {
    IDName = QString("%1_%2").arg("GeosetID2200").arg(QString::number(ID - 2200), 2, QChar('0'));
  }
  else if (ID < 2400)
  {
    IDName = QString("%1_%2").arg("GeosetID2300").arg(QString::number(ID - 2300), 2, QChar('0'));
  }
  else if (ID < 2500)
  {
    IDName = QString("%1_%2").arg("Horns").arg(QString::number(ID - 2400), 2, QChar('0'));
  }
  else if (ID < 2600)
  {
    IDName = QString("%1_%2").arg("Blindfolds").arg(QString::number(ID - 2500), 2, QChar('0'));
  }
  else {
    IDName = QString("%1_%2").arg("UnknownGeosetID").arg(QString::number(ID), 4, QChar('0'));
  }
}
