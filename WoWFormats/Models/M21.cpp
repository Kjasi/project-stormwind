#include "M21.h"
#include <qfile.h>

M21::M21(QString file_name, M2Header *modelHeader)
  : fileName(file_name), header(modelHeader)
{}

void M21::read()
{
  LOGFILE.Info("Reading MD21 file \"%s\"...", qPrintable(fileName));
  if (validateFile(fileName, "MD21") == false) { return; }

  //LOGFILE.Debug("M2 file is a valid file. Checking to see if it's really a M2 model...");

  QFile *file = new QFile(fileName);
  if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
  {
    LOGFILE.Error("Unable to open file %s for reading...", qPrintable(fileName));
    return;
  }
  QByteArray fileData = file->readAll();
  if (fileData.left(4) != "MD21")
  {
    file->close();
    LOGFILE.Error("File %s is not a MD21 file.", qPrintable(fileName));
    return;
  }

  if (file->isOpen() == false)
  {
    LOGFILE.Warn("File is no longer open. Attempting to re-open it...");
    if (file->open(QIODevice::OpenModeFlag::ReadOnly) == false)
    {
      LOGFILE.Error("Unable to open file %s for second reading...", qPrintable(fileName));
      return;
    }
  }

  //LOGFILE.Debug("M2 file validated! Commencing reading...");

  // Time to read the chunk data
  fpos_t filePos = 8;
  do
  {
    QString chunk = fileData.mid(filePos, 4);

    uint32_t size = getQBANumber<uint32_t>(fileData.mid(filePos + 4, 4));

    if (chunk == "PFID")
    {
      LOGFILE.Debug("PFID chunk found");
    }
    else if (chunk == "SFID")
    {
      LOGFILE.Debug("SFID chunk found");
    }
    else if (chunk == "AFID")
    {
      LOGFILE.Debug("AFID chunk found");
    }
    else if (chunk == "BFID")
    {
      LOGFILE.Debug("BFID chunk found");
    }
    else if (chunk == "MD20")
    {
      LOGFILE.Debug("MD20 chunk found");
      M20Data = new M20(&fileData, filePos, fileName, header);
      M20Data->read();
    }
    else if (chunk == "TXAC")
    {
      LOGFILE.Debug("TXAC chunk found");
    }
    else if (chunk == "EXPT")
    {
      LOGFILE.Debug("EXPT chunk found");
    }
    else if (chunk == "EXP2")
    {
      LOGFILE.Debug("EXP2 chunk found");
    }
    else if (chunk == "PABC")
    {
      LOGFILE.Debug("PABC chunk found");
    }
    else if (chunk == "PADC")
    {
      LOGFILE.Debug("PADC chunk found");
    }
    else if (chunk == "PSBC")
    {
      LOGFILE.Debug("PSBC chunk found");
    }
    else if (chunk == "PEDC")
    {
      LOGFILE.Debug("PEDC chunk found");
    }
    else if (chunk == "SKID")
    {
      LOGFILE.Debug("SKID chunk found");
    }
    else if (chunk == "TXID")
    {
      LOGFILE.Debug("TXID chunk found");
    }
    else if (chunk == "LDV1")
    {
      LOGFILE.Debug("LDV1 chunk found");
    }

    filePos += 8 + size;
  } while (filePos < fileData.size());


  file->close();
}
