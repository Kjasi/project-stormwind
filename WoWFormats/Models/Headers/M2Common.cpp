#include "M2Common.h"
#include "M2Header.h"
#include <Foundation/LogFile.h>
#include <Foundation/Functions.h>

void M2Bounds::read(QByteArray data, uint32_t start)
{
  extent.min.x = getQBANumber<float>(data.mid(start + 0, 4));
  extent.min.y = getQBANumber<float>(data.mid(start + 4, 4));
  extent.min.z = getQBANumber<float>(data.mid(start + 8, 4));
  extent.max.x = getQBANumber<float>(data.mid(start + 12, 4));
  extent.max.y = getQBANumber<float>(data.mid(start + 16, 4));
  extent.max.z = getQBANumber<float>(data.mid(start + 20, 4));
  radius = getQBANumber<float>(data.mid(start + 24, 4));
}

size_t M2BoneRaw::read(QByteArray data, uint32_t start, M2Header * modelHeader)
{
  size_t bytes = 0;
  //LOGFILE.Debug("Reading Bone at offset %i...", start);

  key_bone_id = getQBANumber<int32_t>(data.mid(start + (uint32_t)bytes, sizeof(int32_t)));
  bytes += sizeof(int32_t);
  //LOGFILE.Debug("KBoneID: %i", key_bone_id);

  flags = getQBANumber<uint32_t>(data.mid(start + (uint32_t)bytes, sizeof(uint32_t)));
  bytes += sizeof(uint32_t);
  //LOGFILE.Debug("Flags: %i", flags);

  parent_bone = getQBANumber<int16_t>(data.mid(start + (uint32_t)bytes, sizeof(int16_t)));
  bytes += sizeof(int16_t);
  //LOGFILE.Debug("Parent ID: %i", parent_bone);

  submesh_id = getQBANumber<int16_t>(data.mid(start + (uint32_t)bytes, sizeof(int16_t)));
  bytes += sizeof(int16_t);
  //LOGFILE.Debug("Submesh: %i", submesh_id);

  if (modelHeader->version > 257)
  {
    boneNameCRC = getQBANumber<uint32_t>(data.mid(start + (uint32_t)bytes, sizeof(uint32_t)));
    bytes += sizeof(uint32_t);
    //LOGFILE.Debug("boneNameCRC: %i", boneNameCRC);
  }

  //LOGFILE.Debug("Reading Translation Track...");
  bytes += Translation.read(data, start + (uint32_t)bytes, modelHeader);
  //LOGFILE.Debug("Reading Rotation Track...");
  bytes += Rotation.read(data, start + (uint32_t)bytes, modelHeader);
  //LOGFILE.Debug("Reading Scale Track...");
  bytes += Scale.read(data, start + (uint32_t)bytes, modelHeader);

  pivotPoint.x = getQBANumber<float>(data.mid(start + (uint32_t)bytes + 0, sizeof(float)));
  pivotPoint.y = getQBANumber<float>(data.mid(start + (uint32_t)bytes + 4, sizeof(float)));
  pivotPoint.z = getQBANumber<float>(data.mid(start + (uint32_t)bytes + 8, sizeof(float)));
  bytes += sizeof(Vec4F);

  //LOGFILE.Debug("Pivot: %f, %f, %f", pivotPoint.x, pivotPoint.y, pivotPoint.z);

  return bytes;
}

void M2CompQuat::read(QByteArray data, uint32_t start)
{
  srcX = getQBANumber<int16_t>(data.mid(start + 0, 2));
  srcY = getQBANumber<int16_t>(data.mid(start + 2, 2));
  srcZ = getQBANumber<int16_t>(data.mid(start + 4, 2));
  srcW = getQBANumber<int16_t>(data.mid(start + 6, 2));

  quaternion = QuaternionF(
    float(srcX < 0 ? srcX + 32768 : srcX - 32767) / 32767.0f,
    float(srcY < 0 ? srcY + 32768 : srcY - 32767) / 32767.0f,
    float(srcZ < 0 ? srcZ + 32768 : srcZ - 32767) / 32767.0f,
    float(srcW < 0 ? srcW + 32768 : srcW - 32767) / 32767.0f);
}
