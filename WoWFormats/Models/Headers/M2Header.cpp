#include "M2Header.h"
#include "M2Header01.h"
#include "M2Header02.h"
#include "M2Header03.h"

void M2Header::getFromHeader(M2Header01 *h)
{
  version = h->version;
  name = h->name;
  globalFlags = h->globalFlags;
  global_loops = h->global_loops;
  sequences = h->sequences;
  sequence_lookups = h->sequence_lookups;
  playable_animation_lookup = h->playable_animation_lookup;

  bones = h->bones;
  key_bone_lookup = h->key_bone_lookup;
  vertices = h->vertices;

  skin_profiles = h->skin_profiles;

  colors = h->colors;
  textures = h->textures;
  texture_weights = h->texture_weights;
  unknown = h->unknown;
  texture_transforms = h->texture_transforms;
  replacable_texture_lookup = h->replacable_texture_lookup;
  materials = h->materials;
  bone_lookup_table = h->bone_lookup_table;
  texture_lookup_table = h->texture_lookup_table;
  tex_unit_lookup_table = h->tex_unit_lookup_table;
  transparency_lookup_table = h->transparency_lookup_table;
  texture_transforms_lookup_table = h->texture_transforms_lookup_table;

  bounding_box = h->bounding_box;
  collision_box = h->collision_box;

  collision_triangles = h->collision_triangles;
  collision_vertices = h->collision_vertices;
  collision_normals = h->collision_normals;
  attachments = h->attachments;
  attachment_lookup_table = h->attachment_lookup_table;
  events = h->events;
  lights = h->lights;
  cameras = h->cameras;
  camera_lookup_table = h->camera_lookup_table;
  ribbon_emitters = h->ribbon_emitters;
  particle_emitters = h->particle_emitters;
}

void M2Header::getFromHeader(M2Header02 *h)
{
  version = h->version;
  name = h->name;
  globalFlags = h->globalFlags;
  global_loops = h->global_loops;
  sequences = h->sequences;
  sequence_lookups = h->sequence_lookups;
  playable_animation_lookup = h->playable_animation_lookup;

  bones = h->bones;
  key_bone_lookup = h->key_bone_lookup;
  vertices = h->vertices;

  skin_profiles = h->skin_profiles;

  colors = h->colors;
  textures = h->textures;
  texture_weights = h->texture_weights;
  unknown = h->unknown;
  texture_transforms = h->texture_transforms;
  replacable_texture_lookup = h->replacable_texture_lookup;
  materials = h->materials;
  bone_lookup_table = h->bone_lookup_table;
  texture_lookup_table = h->texture_lookup_table;
  tex_unit_lookup_table = h->tex_unit_lookup_table;
  transparency_lookup_table = h->transparency_lookup_table;
  texture_transforms_lookup_table = h->texture_transforms_lookup_table;

  bounding_box = h->bounding_box;
  collision_box = h->collision_box;

  collision_triangles = h->collision_triangles;
  collision_vertices = h->collision_vertices;
  collision_normals = h->collision_normals;
  attachments = h->attachments;
  attachment_lookup_table = h->attachment_lookup_table;
  events = h->events;
  lights = h->lights;
  cameras = h->cameras;
  camera_lookup_table = h->camera_lookup_table;
  ribbon_emitters = h->ribbon_emitters;
  particle_emitters = h->particle_emitters;
  if (globalFlags.flag_use_texture_combiner_combos)
  {
    textureCombinerCombos = h->textureCombinerCombos;
  }
  else {
    textureCombinerCombos = M2Offset();
  }
}
void M2Header::getFromHeader(M2Header03 *h)
{
  version = h->version;
  name = h->name;
  globalFlags = h->globalFlags;
  global_loops = h->global_loops;
  sequences = h->sequences;
  sequence_lookups = h->sequence_lookups;

  bones = h->bones;
  key_bone_lookup = h->key_bone_lookup;
  vertices = h->vertices;

  num_skin_profiles = h->num_skin_profiles;

  colors = h->colors;
  textures = h->textures;
  texture_weights = h->texture_weights;
  texture_transforms = h->texture_transforms;
  replacable_texture_lookup = h->replacable_texture_lookup;
  materials = h->materials;
  bone_lookup_table = h->bone_lookup_table;
  texture_lookup_table = h->texture_lookup_table;
  tex_unit_lookup_table = h->tex_unit_lookup_table;
  transparency_lookup_table = h->transparency_lookup_table;
  texture_transforms_lookup_table = h->texture_transforms_lookup_table;

  bounding_box = h->bounding_box;
  collision_box = h->collision_box;

  collision_triangles = h->collision_triangles;
  collision_vertices = h->collision_vertices;
  collision_normals = h->collision_normals;
  attachments = h->attachments;
  attachment_lookup_table = h->attachment_lookup_table;
  events = h->events;
  lights = h->lights;
  cameras = h->cameras;
  camera_lookup_table = h->camera_lookup_table;
  ribbon_emitters = h->ribbon_emitters;
  particle_emitters = h->particle_emitters;
  if (globalFlags.flag_use_texture_combiner_combos)
  {
    textureCombinerCombos = h->textureCombinerCombos;
  }
  else {
    textureCombinerCombos = M2Offset();
  }
}