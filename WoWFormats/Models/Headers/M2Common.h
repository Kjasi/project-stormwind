#pragma once
#include <Foundation/Functions.h>
#include <WoWFormats/CommonClasses.h>
#include <QtCore/qmap.h>

struct M2Header;

struct M2CompQuat
{
  int16_t srcX, srcY, srcZ, srcW;
  QuaternionF quaternion;

  void read(QByteArray data, uint32_t start);
};

struct M2Bounds
{
  CAaBox extent;		// min / max([1].z, 2.0277779f) - 0.16f seems to be the maximum camera height, 
  float radius;		// detail doodad draw dist = clamp(bounding_sphere_radius * detailDoodadDensityFade * detailDoodadDist, ...)

  void read(QByteArray data, uint32_t start);
};

struct M2Offset
{
  uint32_t num;
  uint32_t offset;	// Relative to start of file for MD20 or the MD21 block.

  M2Offset() { num = 0; offset = 0; }
  void read(QByteArray data, uint32_t start)
  {
    num = getQBANumber<uint32_t>(data.mid(start + 0, 4));
    offset = getQBANumber<uint32_t>(data.mid(start + 4, 4));
  }
};

struct M2Flags
{
  uint32_t flag_tilt_x : 1;
  uint32_t flag_tilt_y : 1;
  uint32_t : 1;
  // BC
  uint32_t flag_use_texture_combiner_combos : 1;      // add textureCombinerCombos array to end of data
  uint32_t : 1;
  // Mists
  uint32_t flag_load_phys_data : 1;
  uint32_t : 1;
  // Warlords
  uint32_t flag_unk_0x80 : 1;                         // with this flag unset, demon hunter tattoos stop glowing. since Cata (4.0.1.12911) "every" model now has this flag
  uint32_t flag_camera_related : 1;                   // TODO: verify version
  // Legion
  uint32_t flag_new_particle_record : 1;              // In CATA: new version of ParticleEmitters. By default, length of M2ParticleOld is 476, but if 0x200 is set or if version is bigger than 271, length of M2ParticleOld is 492.
  uint32_t flag_unk_0x400 : 1;
  uint32_t flag_texture_transforms_use_bone_sequences : 1; // ? WoD 0x800 -- When set, texture transforms are animated using the sequence being played on the bone found by index in tex_unit_lookup_table[textureTransformIndex], instead of using the sequence being played on the model's first bone. Example model: 6DU_HellfireRaid_FelSiege03_Creature
  uint32_t flag_unk_0x1000 : 1;
  uint32_t flag_unk_0x2000 : 1;                       // seen in various legion models
  uint32_t flag_unk_0x4000 : 1;
  uint32_t flag_unk_0x8000 : 1;                       // seen in UI_MainMenu_Legion
  uint32_t flag_unk_0x10000 : 1;
  uint32_t flag_unk_0x20000 : 1;
  uint32_t flag_unk_0x40000 : 1;
  uint32_t flag_unk_0x80000 : 1;
  uint32_t flag_unk_0x100000 : 1;
  uint32_t flag_unk_0x200000 : 1;                     // apparently: use 24500 upgraded model format: chunked .anim files, change in the exporter reordering sequence+bone blocks before name

  M2Flags() {};
  M2Flags(uint32_t) {};
};

struct M2Range {
  uint32_t minimum;
  uint32_t maximum;
};

template<typename T>
class M2Track
{
public:
  uint16_t interpolation_type;
  uint16_t global_sequence;
  M2Offset frameRange;	// Pair of UInt32s. Min & Max time. Not needed after WotLK.
  M2Offset timestamps;
  M2Offset values;

  size_t read(QByteArray data, uint32_t start, M2Header *modelHeader)
  {
    uint32_t bytes = 0;

    interpolation_type = getQBANumber<int16_t>(data.mid(start + bytes, sizeof(int16_t)));
    bytes += sizeof(int16_t);
    //LOGFILE.Debug("Interpolation Type: %u", interpolation_type);
    global_sequence = getQBANumber<int16_t>(data.mid(start + bytes, sizeof(int16_t)));
    bytes += sizeof(int16_t);
    //LOGFILE.Debug("Global Sequence: %u", global_sequence);

    // Not needed after Cata, as each animation is the entire length of the file, not a subsection of a single animation.
    if (modelHeader->version < 265)
    {
      frameRange.read(data, start + bytes);
      //LOGFILE.Debug("FrameRange Num: %u, Offset: %u", frameRange.num, frameRange.offset);
      bytes += sizeof(M2Offset);

      timestamps.read(data, start + bytes);
      //LOGFILE.Debug("Timestamps Num: %u, Offset: %u", timestamps.num, timestamps.offset);
      bytes += sizeof(M2Offset);
    }
    else {
      timestamps.read(data, start + bytes);
      //LOGFILE.Debug("Timestamps Num: %u, Offset: %u", timestamps.num, timestamps.offset);
      bytes += sizeof(M2Offset);
    }

    values.read(data, start + bytes);
    //LOGFILE.Debug("Values Num: %u, Offset: %u", values.num, values.offset);
    bytes += sizeof(M2Offset);

    return bytes;
  }
};

class M2BoneRaw
{
public:
  int32_t key_bone_id;            // Back-reference to the key bone lookup table. -1 if this is no key bone.
  enum
  {
    spherical_billboard = 0x8,
    cylindrical_billboard_lock_x = 0x10,
    cylindrical_billboard_lock_y = 0x20,
    cylindrical_billboard_lock_z = 0x40,
    transformed = 0x200,
    kinematic_bone = 0x400,       // MoP+: allow physics to influence this bone
    helmet_anim_scaled = 0x1000,  // set blend_modificator to helmetAnimScalingRec.m_amount for this bone
  };
  uint32_t flags;
  int16_t parent_bone;            // Parent bone ID or -1 if there is none.
  uint16_t submesh_id;            // Mesh part ID OR uDistToParent?
  union {                         // only >= BC?
    struct {
      uint16_t uDistToFurthDesc;
      uint16_t uZRatioOfChain;
    } CompressData;               // No model has ever had this part of the union used. (Unverified)
    uint32_t boneNameCRC;         // these are for debugging only. their bone names match those in key bone lookup.
  };
  M2Track<Vec3F> Translation;
  M2Track<QuaternionF> Rotation;
  M2Track<Vec3F> Scale;
  Vec3F pivotPoint;

  size_t read(QByteArray data, uint32_t start, M2Header *modelHeader);
};

inline static QMap<int, QString> M2CharTextureTypeStrings()
{
  QMap<int, QString> map;

  map.insert(0, "Filename");
  map.insert(1, "Skin");
  map.insert(2, "Object Skin");
  map.insert(3, "Weapon Blade");
  map.insert(4, "Weapon Handle");
  map.insert(5, "Enviroment");
  map.insert(6, "Hair");
  map.insert(7, "Facial Hair");
  map.insert(8, "Skin Extra");
  map.insert(9, "UI Skin");
  map.insert(10, "Tauren Mane");
  map.insert(11, "Monster 1");
  map.insert(12, "Monster 2");
  map.insert(13, "Monster 3");
  map.insert(14, "Item Icon");
  map.insert(15, "Guild Background Color");
  map.insert(16, "Guild Emblem Color");
  map.insert(17, "Guild Border Color");
  map.insert(18, "Guild Emblem");

  return map;
}