#pragma once
#include <stdint.h>
#include <Foundation/Constants.h>
#include "M2Common.h"

struct M2Header01;
struct M2Header02;
struct M2Header03;

// Unified Header for M2 files
struct M2Header
{
  uint32_t version;
  Expansion expansion = EXPANSION_CURRENT;
  M2Offset name;
  M2Flags globalFlags;
  
  M2Offset global_loops;						// Timestamps used in global looping animations.
  M2Offset sequences;							// Information about the animations in the model.
  M2Offset sequence_lookups;					// Mapping of sequence IDs to the entries in the Animation sequences block.
  M2Offset playable_animation_lookup;			// Only used in Vanilla & BC.

  M2Offset bones;								// MAX_BONES = 0x100 => Creature\SlimeGiant\GiantSlime.M2 has 312 bones (Wrath)
  M2Offset key_bone_lookup;					// Lookup table for key skeletal bones.
  M2Offset vertices;

  M2Offset skin_profiles;						// LOD offsets. Only used for Vanilla & BC
  uint32_t num_skin_profiles;					// Number of LODs (.skin) files. Used starting with WotLK

  M2Offset colors;                            // Color and alpha animations definitions.
  M2Offset textures;
  M2Offset texture_weights;					// Transparency of textures.
  M2Offset unknown;							// Only appears in Vanilla & BC
  M2Offset texture_transforms;
  M2Offset replacable_texture_lookup;
  M2Offset materials;							// Blending modes / render flags.
  M2Offset bone_lookup_table;
  M2Offset texture_lookup_table;
  M2Offset tex_unit_lookup_table;				// Not used after WotLK
  M2Offset transparency_lookup_table;
  M2Offset texture_transforms_lookup_table;

  M2Bounds bounding_box;
  M2Bounds collision_box;

  M2Offset collision_triangles;
  M2Offset collision_vertices;
  M2Offset collision_normals;
  M2Offset attachments;						// position of equipped weapons or effects
  M2Offset attachment_lookup_table;
  M2Offset events;							// Used for playing sounds when dying and a lot else.
  M2Offset lights;							// Lights are mainly used in loginscreens but in wands and some doodads too.
  M2Offset cameras;							// The cameras are present in most models for having a model in the character tab. 
  M2Offset camera_lookup_table;
  M2Offset ribbon_emitters;					// Things swirling around. See the CoT-entrance for light-trails.
  M2Offset particle_emitters;
  M2Offset textureCombinerCombos;				// When set, textures blending is overriden by the associated array. Only used BC+, and when flag_use_texture_combiner_combos is true.

  M2Header() {};
  M2Header(M2Header01 *h) { getFromHeader(h); };
  M2Header(M2Header02 *h) { getFromHeader(h); };
  M2Header(M2Header03 *h) { getFromHeader(h); };

  void getFromHeader(M2Header01 *h);
  void getFromHeader(M2Header02 *h);
  void getFromHeader(M2Header03 *h);
};
