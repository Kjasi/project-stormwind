#pragma once
#include "M2Common.h"
#include <Foundation/Functions.h>

// Header for The Burning Crusade
struct M2Header02
{
  uint32_t version;
  M2Offset name;
  M2Flags globalFlags;
  M2Offset global_loops;						// Timestamps used in global looping animations.
  M2Offset sequences;							// Information about the animations in the model.
  M2Offset sequence_lookups;					// Mapping of sequence IDs to the entries in the Animation sequences block.
  M2Offset playable_animation_lookup;			// Only used in Vanilla & BC.

  M2Offset bones;								// MAX_BONES = 0x100 => Creature\SlimeGiant\GiantSlime.M2 has 312 bones (Wrath)
  M2Offset key_bone_lookup;					// Lookup table for key skeletal bones.

  M2Offset vertices;
  M2Offset skin_profiles;						// LODs. Removed and put into .skin files in WotLK.

  M2Offset colors;                            // Color and alpha animations definitions.
  M2Offset textures;
  M2Offset texture_weights;					// Transparency of textures.
  M2Offset unknown;							// Only appears in Vanilla & BC
  M2Offset texture_transforms;
  M2Offset replacable_texture_lookup;
  M2Offset materials;							// Blending modes / render flags.
  M2Offset bone_lookup_table;
  M2Offset texture_lookup_table;
  M2Offset tex_unit_lookup_table;
  M2Offset transparency_lookup_table;
  M2Offset texture_transforms_lookup_table;

  M2Bounds bounding_box;
  M2Bounds collision_box;

  M2Offset collision_triangles;
  M2Offset collision_vertices;
  M2Offset collision_normals;
  M2Offset attachments;						// position of equipped weapons or effects
  M2Offset attachment_lookup_table;
  M2Offset events;							// Used for playing sounds when dying and a lot else.
  M2Offset lights;							// Lights are mainly used in loginscreens but in wands and some doodads too.
  M2Offset cameras;							// The cameras are present in most models for having a model in the character tab. 
  M2Offset camera_lookup_table;
  M2Offset ribbon_emitters;					// Things swirling around. See the CoT-entrance for light-trails.
  M2Offset particle_emitters;
  M2Offset textureCombinerCombos;				// When set, textures blending is overriden by the associated array.

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    LOGFILE.Info("Reading M2 Header 02...");
    QByteArray headerData = data->mid(start-4, (uint32_t)size);
    int offset = 0;

    version = getQBANumber<uint32_t>(headerData.mid(offset += 0, 4));
    name.read(headerData, offset += 4);
    globalFlags = static_cast<M2Flags>(getQBANumber<uint32_t>(headerData.mid(offset += 8, 4)));
    global_loops.read(headerData, offset += 4);
    sequences.read(headerData, offset += 8);
    sequence_lookups.read(headerData, offset += 8);
    playable_animation_lookup.read(headerData, offset += 8);

    bones.read(headerData, offset += 8);
    key_bone_lookup.read(headerData, offset += 8);

    vertices.read(headerData, offset += 8);
    skin_profiles.read(headerData, offset += 8);

    colors.read(headerData, offset += 8);
    textures.read(headerData, offset += 8);
    texture_weights.read(headerData, offset += 8);
    unknown.read(headerData, offset += 8);
    texture_transforms.read(headerData, offset += 8);
    replacable_texture_lookup.read(headerData, offset += 8);
    materials.read(headerData, offset += 8);
    bone_lookup_table.read(headerData, offset += 8);
    texture_lookup_table.read(headerData, offset += 8);
    tex_unit_lookup_table.read(headerData, offset += 8);
    transparency_lookup_table.read(headerData, offset += 8);
    texture_transforms_lookup_table.read(headerData, offset += 8);

    bounding_box.read(headerData, offset += 8);
    collision_box.read(headerData, offset += sizeof(bounding_box));

    collision_triangles.read(headerData, offset += sizeof(collision_box));
    collision_vertices.read(headerData, offset += 8);
    collision_normals.read(headerData, offset += 8);
    attachments.read(headerData, offset += 8);
    attachment_lookup_table.read(headerData, offset += 8);
    events.read(headerData, offset += 8);
    lights.read(headerData, offset += 8);
    cameras.read(headerData, offset += 8);
    camera_lookup_table.read(headerData, offset += 8);
    ribbon_emitters.read(headerData, offset += 8);
    particle_emitters.read(headerData, offset += 8);

    if (globalFlags.flag_use_texture_combiner_combos)
    {
      textureCombinerCombos.read(headerData, offset += 8);
    }
    else {
      textureCombinerCombos = M2Offset();
    }
  };
};