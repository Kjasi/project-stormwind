#pragma once

struct M2HeaderVersion
{
  uchar magic[4];
  uint32_t version;

  void read(QByteArray *data, fpos_t start, size_t size)
  {
    // LOGFILE.Info("Reading M2 Header Versions...");
    QByteArray headerData = data->mid(start, (uint32_t)size);
    M2HeaderVersion *a = reinterpret_cast<M2HeaderVersion*>(headerData.data());
    magic[0] = a->magic[0];
    magic[1] = a->magic[1];
    magic[2] = a->magic[2];
    magic[3] = a->magic[3];
    version = a->version;
  };
};