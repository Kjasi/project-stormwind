#pragma once
#include "M20.h"

class M21
{
public:
  M21(QString file_name, M2Header *modelHeader);
  ~M21() {};

  void read();

  M20* M20Data = NULL;

private:
  QString fileName;
  QByteArray *data;
  M2Header *header;
};