#pragma once
#include "Headers/M2Header.h"

struct SkinHeader
{
  M2Offset verticies;				// Vertex IDs
  M2Offset indices;				// Index for Polygons
  M2Offset bones;					// Bones
  M2Offset submeshes;				// Polygon Groups
  M2Offset batches;				// Textures
  uint32_t boneCountMax;			// WoW takes this and divides it by the number of bones in each submesh, then stores the biggest one. Maximum number of bones per drawcall for each view. Related to (old) GPU numbers of registers. Values seen : 256, 64, 53, 21
  M2Offset shadowBatches;			// Cata+ Not sure...
};

struct M2SkinGroupVanilla
{
  uint16_t skinSectionId;			// Mesh Part ID
  uint16_t Level;					// (level << 16) is added (|ed) to startTriangle and alike to avoid having to increase those fields to uint32s.
  uint16_t vertexStart;			// Starting vertex number.
  uint16_t vertexCount;			// Number of vertices.
  uint16_t indexStart;			// Starting triangle index (that's 3* the number of triangles drawn so far).
  uint16_t indexCount;			// Number of triangle indices.
  uint16_t boneCount;				// Number of elements in the bone lookup table. Max seems to be 256 in Wrath. Shall be != 0.
  uint16_t boneComboIndex;		// Starting index in the bone lookup table.
  uint16_t boneInfluences;		// <= 4
  uint16_t centerBoneIndex;
  Vec3F centerPosition;			// Average position of all the vertices in the sub mesh.
};

struct M2SkinGroup
{
  /*
  Mesh Part ID

  ** starts with 01, not 00 with the exception of entry 0, which is the skin.
  DNE = Does Not Exist (AKA Empty Geoset)

  0000: Skin
  00**: Hair: {1-21: various hairstyles}
  01**: Facial1: {1-8: varies} (usually beard, but not always)
  02**: Facial2: {1: none (DNE), 2-6: varies} (usually mustache, but not always)
  03**: Facial3: {1: none (DNE), 2-11: varies} (usually sideburns, but not always)
  04**: Glove: {1-4}
  05**: Boots: {1-5}
  06**:
  07**: Ears: {1: none (DNE), 2: ears}
  08**: Wristbands / Sleeves: {1: none (DNE), 2: normal, 3: ruffled}
  09**: Kneepads / Legcuffs: {1: none (DNE), 2: long, 3: short}
  10**: Chest: {1: none (DNE), 2: ??? (exists but purpose unknown)}
  11**: Pants: {1: regular, 2: short skirt, 4: armored pants}
  12**: Tabard: {1: none (DNE), 2: tabard}
  13**: Trousers: {1: legs, 2: dress}
  14**:
  15**: Cloak: {1-10: various cloak styles}
  16**:
  17**: Eyeglows: {1: none (DNE), 2: racial eyeglow, 3: DK eyeglow}
  18**: Belt / bellypack: {1: none (sometimes DNE), 2: bulky belt}
  19**: Tail (in Legion this group also has Undead bones)
  20**: Feet: {1: none, 2: feet}
  23**: New in Legion (in addition to character models also exist in DH collections) { 1: hands for blood elf/night elf }
  24**: Horns (only exist in DH collections) {1-x}
  25**: Blindfolds (only exist in DH collections) {1-x}
  */
  uint16_t skinSectionId;
  uint16_t Level;					// (level << 16) is added (|ed) to startTriangle and alike to avoid having to increase those fields to uint32s.
  uint16_t vertexStart;			// Starting vertex number.
  uint16_t vertexCount;			// Number of vertices.
  uint16_t indexStart;			// Starting triangle index (that's 3* the number of triangles drawn so far).
  uint16_t indexCount;			// Number of triangle indices.
  uint16_t boneCount;				// Number of elements in the bone lookup table. Max seems to be 256 in Wrath. Shall be != 0.
  uint16_t boneComboIndex;		// Starting index in the bone lookup table.

  /* <= 4
  from <=BC documentation: Highest number of bones needed at one time in this Submesh --Tinyn (wowdev.org)
  In 2.x this is the amount of of bones up the parent-chain affecting the submesh --NaK
  Highest number of bones referenced by a vertex of this submesh. 3.3.5a and suspectedly all other client revisions. -- Skarn
  */
  uint16_t boneInfluences;
  uint16_t centerBoneIndex;
  Vec3F centerPosition;			// Average position of all the vertices in the sub mesh.
  Vec3F sortCenterPosition;		// BC+ The center of the box when an axis aligned box is built around the vertices in the submesh.
  float sortRadius = 0;			// BC+ Distance of the vertex farthest from CenterBoundingBox.

  M2SkinGroup() {};
  M2SkinGroup(const M2SkinGroup& v) { *this = v; };
  M2SkinGroup(const M2SkinGroupVanilla& v) : skinSectionId(v.skinSectionId), Level(v.Level), vertexStart(v.vertexStart), vertexCount(v.vertexCount), indexStart(v.indexStart), indexCount(v.indexCount), boneCount(v.boneCount), boneComboIndex(v.boneComboIndex), boneInfluences(v.boneInfluences), centerBoneIndex(v.centerBoneIndex), centerPosition(v.centerPosition) {};
  M2SkinGroup(M2SkinGroupVanilla* v) { *this = M2SkinGroup(*v); };
};

struct SkinTextureUnit
{
  uint8_t flags;							// Usually 16 for static textures, and 0 for animated textures. &0x1: materials invert something; &0x2: transform &0x4: projected texture; &0x10: something batch compatible; &0x20: projected texture?; &0x40: use textureWeights
  uint8_t priorityPlane;
  uint16_t shader_id;						// See below.
  uint16_t materialIndex;					// Material this texture is part of (index into mat)
  uint16_t geosetIndex;					// See below.
  int16_t colorIndex;						// A Color out of the Colors-Block or -1 if none.
  uint16_t flagsIndex;					// The renderflags used on this texture-unit.
  uint16_t texUnit;						// Index into the texture unit lookup table.
  uint16_t materialCount;					// 1 to 4. Used for shaders. Also seems to be the number of textures to load, starting at the texture lookup in the next field (0x10).
  uint16_t textureLookupIndex;			// Index into Texture lookup table
  uint16_t textureLookupCoordIndex;		// Index into the texture unit lookup table.
  uint16_t textureTransparencyIndex;		// Index into transparency lookup table.
  uint16_t textureAnimIndex;				// Index into uvanimation lookup table. 
};

struct Triangles
{
  uint32_t Indice0;
  uint32_t Indice1;
  uint32_t Indice2;
};

struct SkinData
{
  QList<uint32_t> VertIDList;
  QList<uint32_t> IndexList;
  QList<M2SkinGroup> SubmeshList;
  QList<SkinTextureUnit> TextureUnits;
};

class SkinBlock
{
public:
  QList<SkinData> skinData;

  SkinBlock(QByteArray *data, fpos_t start, M2Header *model_header);

  void read();

private:
  fpos_t fileOrigin = 0;
  QByteArray *fileData = NULL;

  M2Header *modelHeader = NULL;
  QList<SkinHeader> headers;
};

class SkinFile
{
public:
  SkinData skinData;

  SkinFile(QString filename, M2Header *model_header);

  void read();
  QString getFilename() { return fileName; }

private:
  QString fileName;
  QByteArray *fileData = NULL;

  M2Header *modelHeader = NULL;
  SkinHeader header;
};