#pragma once
#include "Headers/M2Header.h"
#include "Skin.h"

// Sub Structures
struct M2Vertex
{
  Vec3F pos;
  uint8_t boneWeights[4];
  uint8_t boneIndices[4];
  Vec3F normal;
  Vec2F UvCoord[2];
};

struct M2Geoset
{
  uint32_t ID;
  QString IDName;
  uint32_t VertStart;
  uint32_t VertCount;
  QList<Triangles> Polygons;
  SkinTextureUnit TextureUnit;

  void buildIDName();
};

struct M2Texture
{
  uint32_t type;	// A Texture type. Non-zero values need to use M2CharTextureTypeStrings() to get the "texture's" name. 0 values use the filename.
  uint32_t flags;
  M2Offset filename;
};

struct M2Material
{
  QString Name;
  QString ColorTexture;
};

class M20
{
public:
  M20(QString file_name, M2Header *modelHeader);
  M20(QByteArray* inc_file_data, fpos_t origin, QString file_name, M2Header *modelHeader);
  ~M20() {};

  void read();
  QString getBoneIDName(M2BoneRaw bone);

  QList<M2Vertex> Vertices;
  QList<M2Geoset> Geosets;
  QList<M2BoneRaw> RawBoneList;
  QList<QString> BoneNames;
  QList<uint16_t> KeyboneList;
  QList<M2Material> Materials;
  QList<uint16_t> TextureLookup;

private:
  QString fileName;
  bool fromM21 = false;

  M2Header *header;
  fpos_t fileOrigin = 0;
  QByteArray *fileData = NULL;

  QList<M2Geoset> getGeoData(SkinData skd);
  QString SkinFileExists(QString filename, int skinID);
};