#pragma once
#include "DBC.h"
#include "DB2.h"

enum DatabaseFormat
{
  DBFORMAT_DBC = 0,
  DBFORMAT_DB2,
};

class DatabaseFile
{
public:


private:
  DatabaseFormat dbFormat;

  DBC_File *DBC = NULL;
  DB2_File *DB2 = NULL;
};