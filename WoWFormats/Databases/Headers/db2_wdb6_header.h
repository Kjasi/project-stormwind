#pragma once
#include <stdint.h>

// 7.2.0.23436 - 7.3.5.25600

struct db2_wdb6_header
{
  uint32_t magic;                                               // 'WDB6'
  uint32_t record_count;
  uint32_t field_count;                                         // this counts arrays as '1' field
  uint32_t record_size;
  uint32_t string_table_size;                                   // if flags & 0x01 != 0, this field takes on a new meaning - it becomes an absolute offset to the beginning of the offset_map
  uint32_t table_hash;
  uint32_t layout_hash;                                         // used to be 'build', but now this is a hash field that changes only when the structure of the data changes
  uint32_t min_id;
  uint32_t max_id;
  uint32_t locale;                                              // as seen in TextWowEnum
  uint32_t copy_table_size;
  uint16_t flags;                                               // possible values are listed in Known Flag Meanings
  uint16_t id_index;                                            // this is the index of the field containing ID values; this is ignored if flags & 0x04 != 0
  uint32_t total_field_count;                                   // new in WDB6, includes columns only expressed in the 'common_data_table', unlike field_count
  uint32_t common_data_table_size;                              // new in WDB6, size of new block called 'common_data_table'
};