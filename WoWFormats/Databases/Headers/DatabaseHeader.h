#pragma once
#include "dbc_header.h"
#include "db2_header.h"
#include "db2_wdb4_header.h"
#include "db2_wdb5_header.h"
#include "db2_wdb6_header.h"
#include "db2_wdc1_header.h"
#include "db2_wdc2_header.h"

// Global Merged DB Header

enum DatabaseHeaderFormat
{
  DBHEADERFORMAT_DBC = 0,
  DBHEADERFORMAT_DB2,
  DBHEADERFORMAT_DB2_WDB4,
  DBHEADERFORMAT_DB2_WDB5,
  DBHEADERFORMAT_DB2_WDB6,
  DBHEADERFORMAT_DB2_WDC1,
  DBHEADERFORMAT_DB2_WDC2,
    
  DBHEADERFORMAT_LATEST = DBHEADERFORMAT_DB2_WDC2,
};

struct DatabaseHeader
{
  uint32_t magic;								// Determines the header Type
  uint32_t record_count;						// records per file
  uint32_t field_count;						// fields per record
  uint32_t record_size;						// sum (sizeof (field_type_i)) | 0 <= i < field_count. field_type_i is NOT defined in the files.
  uint32_t string_table_size;					// string block almost always contains at least one zero-byte
  uint32_t table_hash;
  uint32_t layout_hash;						// used to be 'build', but after build 21737, this is a new hash field that changes only when the structure of the data changes
  uint32_t min_id;
  uint32_t max_id;
  uint32_t locale;							// as seen in TextWowEnum
  uint32_t flags;                             // in WDB3, this field was in the WoW executable's DBCMeta instead; possible values are listed in Known Flag Meanings
  uint16_t id_index;							// new in WDB5 (and only after build 21737), this is the index of the field containing ID values; this is ignored if flags & 0x04 != 0
  uint32_t total_field_count;					// new in WDB6, includes columns only expressed in the 'common_data_table', unlike field_count
  uint32_t bitpacked_data_offset;				// relative position in record where bitpacked data begins; not important for parsing the file
  uint32_t lookup_column_count;
  uint32_t field_storage_info_size;
  uint32_t common_data_size;					// new in WDB6, size of new block called 'common_data_table_size'. Renamed 'common_data_size' in WDC1.
  uint32_t pallet_data_size;
  uint32_t section_count;						// new to WDC2, this is number of sections of data (records + copy table + id list + relationship map = a section)

  /* Obsolete. Might be used for backwards compatibility */
  
  uint32_t offset_map_offset;					// Offset to array of struct {uint32_t offset; uint16_t size;}[max_id - min_id + 1];
  uint32_t id_list_size;						// List of ids present in the DB file
  uint32_t relationship_data_size;		
  uint32_t copy_table_size;					// always zero in WDB2 (?) - see WDB3 for information on how to parse this. Removed WDC2
  uint32_t timestamp_last_written;			// set to time(0); when writing in WowClientDB2_Base::Save(). Not seen in WDB5+

  // Let us know which header our source is.
  DatabaseHeaderFormat headerType = DBHEADERFORMAT_LATEST;

  void readHeader(dbc_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_block_size;
    
    headerType = DBHEADERFORMAT_DBC;
  }
  void readHeader(db2_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.build;
    timestamp_last_written = header.timestamp_last_written;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    copy_table_size = header.copy_table_size;

    headerType = DBHEADERFORMAT_DB2;
  }
  void readHeader(db2_wdb4_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.build;
    timestamp_last_written = header.timestamp_last_written;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    copy_table_size = header.copy_table_size;
    flags = header.flags;

    headerType = DBHEADERFORMAT_DB2_WDB4;
  }
  void readHeader(db2_wdb5_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.layout_hash;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    copy_table_size = header.copy_table_size;
    flags = header.flags;
    id_index = header.id_index;

    headerType = DBHEADERFORMAT_DB2_WDB5;
  }
  void readHeader(db2_wdb6_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.layout_hash;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    copy_table_size = header.copy_table_size;
    flags = header.flags;
    id_index = header.id_index;
    total_field_count = header.total_field_count;
    common_data_size = header.common_data_table_size;

    headerType = DBHEADERFORMAT_DB2_WDB6;
  }
  void readHeader(db2_wdc1_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.layout_hash;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    copy_table_size = header.copy_table_size;
    flags = header.flags;
    id_index = header.id_index;
    total_field_count = header.total_field_count;
    field_storage_info_size = header.field_storage_info_size;
    common_data_size = header.common_data_size;
    pallet_data_size = header.pallet_data_size;
    relationship_data_size = header.relationship_data_size;

    headerType = DBHEADERFORMAT_DB2_WDC1;
  }
  void readHeader(db2_wdc2_header header)
  {
    magic = header.magic;
    record_count = header.record_count;
    field_count = header.field_count;
    record_size = header.record_size;
    string_table_size = header.string_table_size;
    table_hash = header.table_hash;
    layout_hash = header.layout_hash;
    min_id = header.min_id;
    max_id = header.max_id;
    locale = header.locale;
    flags = header.flags;
    id_index = header.id_index;
    total_field_count = header.total_field_count;
    field_storage_info_size = header.field_storage_info_size;
    common_data_size = header.common_data_size;
    pallet_data_size = header.pallet_data_size;
    section_count = header.section_count;

    headerType = DBHEADERFORMAT_DB2_WDC2;
  }
};