#pragma once
#include <stdint.h>

// Vanilla - WotLK
struct dbc_header
{
  uint32_t magic;				  // always 'WDBC'
  uint32_t record_count;	// records per file
  uint32_t field_count;		// fields per record
  uint32_t record_size;		// sum (sizeof (field_type_i)) | 0 <= i < field_count. field_type_i is NOT defined in the files.
  uint32_t string_block_size;
};