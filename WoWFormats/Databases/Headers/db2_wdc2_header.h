#pragma once
#include <stdint.h>

// 8.0.1.26231+

struct db2_wdc2_header
{
  uint32_t magic;                  // 'WDC2'
  uint32_t record_count;           // this is for all sections combined now
  uint32_t field_count;
  uint32_t record_size;
  uint32_t string_table_size;      // this is for all sections combined now
  uint32_t table_hash;             // hash of the table name
  uint32_t layout_hash;            // this is a hash field that changes only when the structure of the data changes
  uint32_t min_id;
  uint32_t max_id;
  uint32_t locale;                 // as seen in TextWowEnum
  uint16_t flags;                  // possible values are listed in Known Flag Meanings
  uint16_t id_index;               // this is the index of the field containing ID values; this is ignored if flags & 0x04 != 0
  uint32_t total_field_count;      // from WDC1 onwards, this value seems to always be the same as the 'field_count' value
  uint32_t bitpacked_data_offset;  // relative position in record where bitpacked data begins; not important for parsing the file
  uint32_t lookup_column_count;
  uint32_t field_storage_info_size;
  uint32_t common_data_size;
  uint32_t pallet_data_size;
  uint32_t section_count;          // new to WDC2, this is number of sections of data (records + copy table + id list + relationship map = a section)
};