#pragma once
#include <stdint.h>

// 7.0.1.20810 - 7.0.3.21414

struct db2_wdb4_header
{
  uint32_t magic;                                               // 'WDB4'
  uint32_t record_count;
  uint32_t field_count;                                         // array fields count as the size of array for WDB4
  uint32_t record_size;
  uint32_t string_table_size;                                   // if flags & 0x01 != 0, this field takes on a new meaning - it becomes an absolute offset to the beginning of the offset_map
  uint32_t table_hash;
  uint32_t build;
  uint32_t timestamp_last_written;                              // set to time(0); when writing in WowClientDB2_Base::Save()
  uint32_t min_id;
  uint32_t max_id;
  uint32_t locale;                                              // as seen in TextWowEnum
  uint32_t copy_table_size;
  uint32_t flags;                                               // in WDB3, this field was in the WoW executable's DBCMeta instead; possible values are listed in Known Flag Meanings
};