#pragma once
#include <Foundation/Classes/RGBAColor.h>

enum BLPEncoding {
  BLPENCODE_UNCOMPRESSED = 1,
  BLPENCODE_DXT = 2,
};

enum BLPAlphaDepth {
  BLPALPHADEPTH_0 = 0,
  BLPALPHADEPTH_1 = 1,
  BLPALPHADEPTH_4 = 4,
  BLPALPHADEPTH_8 = 8,
};

enum BLPAlphaEncoding {
  BLPALPHAENCODE_DTX1 = 0,
  BLPALPHAENCODE_DTX3 = 1,
  BLPALPHAENCODE_DTX5 = 7,
};

enum BLPFormat {
  BLPFORMAT_JPEG = 0,

  BLPFORMAT_PALLETED_NOALPHA = (BLPENCODE_UNCOMPRESSED << 16) | (BLPALPHADEPTH_0 << 8),
  BLPFORMAT_PALLETED_ALPHA1 = (BLPENCODE_UNCOMPRESSED << 16) | (BLPALPHADEPTH_1 << 8),
  BLPFORMAT_PALLETED_ALPHA8 = (BLPENCODE_UNCOMPRESSED << 16) | (BLPALPHADEPTH_8 << 8),

  BLPFORMAT_DTX1_NOALPHA = (BLPENCODE_DXT << 16) | (BLPALPHADEPTH_0 << 8) | BLPALPHAENCODE_DTX1,
  BLPFORMAT_DTX1_ALPHA1 = (BLPENCODE_DXT << 16) | (BLPALPHADEPTH_1 << 8) | BLPALPHAENCODE_DTX1,
  BLPFORMAT_DTX3_ALPHA4 = (BLPENCODE_DXT << 16) | (BLPALPHADEPTH_4 << 8) | BLPALPHAENCODE_DTX3,
  BLPFORMAT_DTX3_ALPHA8 = (BLPENCODE_DXT << 16) | (BLPALPHADEPTH_8 << 8) | BLPALPHAENCODE_DTX3,
  BLPFORMAT_DTX5_ALPHA8 = (BLPENCODE_DXT << 16) | (BLPALPHADEPTH_8 << 8) | BLPALPHAENCODE_DTX5,

  BLPFORMAT_NONE = 99999999
};

struct BGRAColors {
  unsigned char B, G, R, A;

private:
  BGRAColors RGBASwap(const BGRAColors in) {
    BGRAColors rgba;
    rgba.R = in.B;
    rgba.G = in.G;
    rgba.B = in.R;
    rgba.A = in.A;
    return rgba;
  }
public:

  unsigned char* toRGBAchar() {
    unsigned char* c;
    BGRAColors color = RGBASwap(*this);
    c = &color.R;
    c++;
    c = &color.G;
    c++;
    c = &color.B;
    c++;
    c = &color.A;
    c++;
    return c;
  }

  RGBAColor toRGBAColor()
  {
    return RGBAColor((unsigned int)R, (unsigned int)G, (unsigned int)B, (unsigned int)A);
  }
};

struct BLP_Header {
  char FileType[4];			// always 'BLP2'
  uint32_t Version;			// Version, always 1
  uint8_t Compression;		// Compression: 0 for JPEG (not supported), 1 for Paletted, 2 for DXTC, 3 & 4 for ARGB8888
  uint8_t AlphaDepth;			// Alpha channel bit depth: 0, 1 or 8
  uint8_t PixelFormat;		// 0 = DTX1, 1 = DTX3, 2 = ARGB8888, 3 = ARGB1555, 4 = ARGB444, 5 = RGB565, 7 = DTX5
  union {
    uint8_t hasMipLevels;	// 0 or 1
    uint8_t numMipLevels;	// Replace with number of Mip-Map levels
  };
  uint32_t ResX;				// X resolution (power of 2)
  uint32_t ResY;				// Y resolution (power of 2)
  uint32_t MipOffset[16];		// offsets for every mipmap level (or 0 when there is no more mipmap level)
  uint32_t MipSize[16];		// sizes for every mipmap level (or 0 when there is no more mipmap level)
  BGRAColors Palette[256];	// Palette
};