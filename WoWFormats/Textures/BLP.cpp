#include "blp.h"
#include "Dependancies/LibSquish/squish.h"
#include <QtGui/qimage.h>
#include <Foundation/LogFile.h>
#include <ProjectStormwind/Utilities/common.h>

unsigned int getWidth(BLPInfo *pic, unsigned int MipLevel)
{
  // Keep the system from trying to read mipmaps beyond the range we have.
  if (MipLevel >= pic->numMipLevels)
    MipLevel = pic->numMipLevels - 1;

  return pic->Header.ResX >> MipLevel;
}
unsigned int getHeight(BLPInfo *pic, unsigned int MipLevel)
{
  // Keep the system from trying to read mipmaps beyond the range we have.
  if (MipLevel >= pic->numMipLevels)
    MipLevel = pic->numMipLevels - 1;

  return pic->Header.ResY >> MipLevel;
}

BLPFormat getFormat(BLP_Header *Header)
{
  if (Header->Version == 0)
    return BLPFORMAT_JPEG;

  if (Header->Compression == BLPENCODE_UNCOMPRESSED)
    return BLPFormat((Header->Compression << 16) | (Header->AlphaDepth << 8));

  return BLPFormat((Header->Compression << 16) | (Header->AlphaDepth << 8) | Header->PixelFormat);
};

// Read the BLP file, store the data, and return a pointer
blp * blp::read(const std::string &filename)
{
  FILE *f;
  fopen_s(&f, filename.c_str(), "r");
  if (!f) {
    LOGFILE.Error("Failed to open the file. Aborting read.");
    return NULL;
  }
  fseek(f, 0, SEEK_END);
  long size = ftell(f);
  fseek(f, 0, SEEK_SET);

  uint8_t *data;
  fread(&data, size, 1, f);
  fclose(f);

  blp *img = new blp(data);

  return img;
};

blp::blp(uint8_t *data) {
  LOGFILE.Info("Attempting to Read File Data...");
  _data = data;
};

#define Def_Header \
  ((BLP_Header *) _data)

void blp::printinfo() {
  LOGFILE.Info("Header Data:");
  LOGFILE.Info("\t\tFiletype: \"%c%c%c%c\"", Def_Header->FileType[0], Def_Header->FileType[1], Def_Header->FileType[2], Def_Header->FileType[3]);
  LOGFILE.Info("\t\tVersion: %i", Def_Header->Version);
  LOGFILE.Info("\t\tCompression: %i", Def_Header->Compression);
  LOGFILE.Info("\t\tPixel Format: %i", Def_Header->PixelFormat);
  LOGFILE.Info("\t\tX: %i", Def_Header->ResX);
  LOGFILE.Info("\t\tY: %i", Def_Header->ResY);
  LOGFILE.Info("\t\tMip Setting: %i", Def_Header->hasMipLevels);
}

BGRAColors* BLP2_Convert_Palette_NoAlpha(uint8_t* Src, BLPInfo *pic, uint32_t width, uint32_t height)
{
  //LOGFILE.Debug("Reading Indexed, No Alpha image...");
  BGRAColors* buffer = new BGRAColors[width * height];
  BGRAColors* dest = buffer;

  for (unsigned int y = 0; y < height; ++y)
  {
    for (unsigned int x = 0; x < width; ++x)
    {
      *dest = pic->Header.Palette[*Src];
      dest->A = 0xFF;
      //LOGFILE.Write("Palette: R:%i, G:%i, B:%i, A:%i", dest->R, dest->G, dest->B, dest->A);

      ++Src;
      ++dest;
    }
  }

  //LOGFILE.Debug("Completed Indexed, No Alpha image reading.");
  return buffer;
}


BGRAColors* BLP2_Convert_Palette_Alpha1(uint8_t* Src, BLPInfo *pic, uint32_t width, uint32_t height)
{
  //LOGFILE.Debug("Reading Indexed, Alpha 1 image...");
  BGRAColors* buffer = new BGRAColors[width * height];
  BGRAColors* dest = buffer;

  uint8_t* indices = Src;
  uint8_t* alpha = Src + width * height;
  uint8_t counter = 0;

  for (unsigned int y = 0; y < height; ++y)
  {
    for (unsigned int x = 0; x < width; ++x)
    {
      *dest = pic->Header.Palette[*indices];
      dest->A = (*alpha & (1 << counter) ? 0xFF : 0X00);

      ++indices;
      ++dest;
      ++counter;

      if (counter == 8)
      {
        ++alpha;
        counter = 0;
      }
    }
  }

  //LOGFILE.Debug("Completed Indexed, Alpha 1 image reading.");
  return buffer;
}

BGRAColors* BLP2_Convert_Palette_Alpha8(uint8_t* Src, BLPInfo *pic, uint32_t width, uint32_t height)
{
  //LOGFILE.Debug("Reading Indexed, Alpha 8 image...");
  BGRAColors* buffer = new BGRAColors[width * height];
  BGRAColors* dest = buffer;

  uint8_t* indices = Src;
  uint8_t* alpha = Src + width * height;

  for (unsigned int y = 0; y < height; ++y)
  {
    for (unsigned int x = 0; x < width; ++x)
    {
      *dest = pic->Header.Palette[*indices];
      dest->A = *alpha;

      //LOGFILE.Write("Palette: R:%i, G:%i, B:%i, A:%i", dest->R, dest->G, dest->B, dest->A);

      ++indices;
      ++alpha;
      ++dest;
    }
  }

  //LOGFILE.Debug("Completed Indexed, Alpha 8 image reading.");
  return buffer;
}

BGRAColors* BLP2_Convert_DXT(uint8_t* Src, BLPInfo *pic, uint32_t width, uint32_t height, int flags)
{
  //LOGFILE.Debug("Reading DTX image...");
  squish::u8* rgba = new squish::u8[width * height * 4];
  BGRAColors* buffer = new BGRAColors[width * height];

  squish::u8* Src2 = rgba;
  BGRAColors* dest = buffer;

  squish::DecompressImage(rgba, width, height, Src, flags);


  for (unsigned int y = 0; y < height; ++y) {
    for (unsigned int x = 0; x < width; ++x) {
      //LOGFILE.Debug("DTX %i/%i: R:%d, G:%d, B:%d, A:%d", ((y*height)+x), (width*height)-1, Src2[0], Src2[1], Src2[2], Src2[3]);
      dest->R = Src2[0];
      dest->G = Src2[1];
      dest->B = Src2[2];
      dest->A = Src2[3];

      Src2 += 4;
      ++dest;
    }
  }

  delete[] rgba;

  //LOGFILE.Debug("Completed DTX image reading.");
  return buffer;
}


BGRAColors* GetBLPData(FILE* fp, BLPInfo *pic, unsigned int MipMapLevel)
{
  //LOGFILE.Info("Reading BLP File Data...");
  BGRAColors* Destination = 0;
  uint32_t offset;
  uint32_t size;

  // Keep the system from trying to read mipmaps beyond the range we have.
  if (MipMapLevel >= pic->numMipLevels)
    MipMapLevel = pic->numMipLevels - 1;

  unsigned int width = getWidth(pic, MipMapLevel);
  unsigned int height = getHeight(pic, MipMapLevel);

  offset = pic->Header.MipOffset[MipMapLevel];
  size = pic->Header.MipSize[MipMapLevel];

  pic->MipSize = size;

  //LOGFILE.Debug("MipMap Level: %i, MipMap Size: %i", MipMapLevel, size);

  pic->Source = new unsigned char[size];

  fseek(fp, offset, SEEK_SET);
  fread(pic->Source, sizeof(unsigned char), size, fp);

  //LOGFILE.Write("Source[0]: %s, [1]: %s, [2]: %s, [3]: %s", pic->Source[0], pic->Source[1], pic->Source[2], pic->Source[3]);

  switch (getFormat(&pic->Header)) {
  case BLPFORMAT_JPEG: break;

  case BLPFORMAT_PALLETED_NOALPHA:
    //LOGFILE.Debug("Found Palette, No Alpha image.");
    Destination = BLP2_Convert_Palette_NoAlpha(pic->Source, pic, width, height);
    break;
  case BLPFORMAT_PALLETED_ALPHA1:
    //LOGFILE.Debug("Found Palette, Alpha 1 image.");
    Destination = BLP2_Convert_Palette_Alpha1(pic->Source, pic, width, height);
    break;
  case BLPFORMAT_PALLETED_ALPHA8:
    //LOGFILE.Debug("Found Palette, Alpha 8 image.");
    Destination = BLP2_Convert_Palette_Alpha8(pic->Source, pic, width, height);
    break;

  case BLPFORMAT_DTX1_NOALPHA:
    //LOGFILE.Debug("Found DXT1 No Alpha image.");
    Destination = BLP2_Convert_DXT(pic->Source, pic, width, height, squish::kDxt1);
    break;
  case BLPFORMAT_DTX1_ALPHA1:
    //LOGFILE.Debug("Found DXT1 Alpha 1 image.");
    Destination = BLP2_Convert_DXT(pic->Source, pic, width, height, squish::kDxt1);
    break;
  case BLPFORMAT_DTX3_ALPHA4:
  case BLPFORMAT_DTX3_ALPHA8:
    //LOGFILE.Debug("Found DXT3 Alpha 4/8 image.");
    Destination = BLP2_Convert_DXT(pic->Source, pic, width, height, squish::kDxt3);
    break;
  case BLPFORMAT_DTX5_ALPHA8:
    //LOGFILE.Debug("Found DXT5 Alpha 8 image.");
    Destination = BLP2_Convert_DXT(pic->Source, pic, width, height, squish::kDxt5);
    break;

  default: break;
  }

  //LOGFILE.Info("Finished Reading BLP File Data.");
  return Destination;
}

bool ReadColors(FILE* fp, BLPInfo *pic, unsigned int MipMapLevel)
{
  BGRAColors *b = GetBLPData(fp, pic, MipMapLevel);
  if (b)
  {
    unsigned int width = getWidth(pic, MipMapLevel);
    unsigned int height = getHeight(pic, MipMapLevel);

    BGRAColors* Source = b;// +(height - 1) * width;
    pic->rgba = new BGRAColors[(width*height)];
    uint32_t j = 0;

    for (uint32_t i = 0; i < (width*height); i++) {
      //LOGFILE.Debug("Source %i/%i: R:%i, G:%i, B:%i, A:%i", i, ((width*height)-1), Source[i].R, Source[i].G, Source[i].B, Source[i].A);
      pic->rgba[i] = Source[i];
      //LOGFILE.Debug("RGBA %i/%i: R:%i, G:%i, B:%i, A:%i", i, ((width*height) - 1), pic->rgba[i].R, pic->rgba[i].G, pic->rgba[i].B, pic->rgba[i].A);

    }
    return true;
  }
  else {
    LOGFILE.Error("Get BLP Data failed.");
    return false;
  }
  return false;
}

BLPInfo *ReadBLPFile(QString Filename)
{
  // LOGFILE.Info("Attempting to Read BLP File Data...");
  BLPInfo *pic = new BLPInfo;
  BLP_Header header;

  //pic = (BLPInfo *)calloc(sizeof(BLPInfo), 1);
  if (!pic) {
    return NULL;
  }
  char FileType[4];

  FILE* fp;
  fopen_s(&fp, qPrintable(Filename), "rb");
  if (!fp)
  {
    LOGFILE.Error("Unable to open %s...", qPrintable(Filename));
    return NULL;
  }

  fread(FileType, sizeof(uint8_t), 4, fp);
  if (strncmp(FileType, "BLP2", 4) != 0) {
    LOGFILE.Error("File is not a BLP2 image.");
    fclose(fp);
    return NULL;
  }
  fseek(fp, 0, SEEK_SET);

  fread(&header, sizeof(BLP_Header), 1, fp);
  pic->numMipLevels = 0;
  while ((header.MipOffset[pic->numMipLevels] != 0) && (pic->numMipLevels < 16))
    ++pic->numMipLevels;

  pic->Header = header;
  QString fname = Filename;
  fname.replace(WoWDir.currentPath(), "", Qt::CaseInsensitive);
  pic->setFileName(fname);

  //LOGFILE.Write("Checking File compatability...");
  if (strncmp(header.FileType, "BLP2", 4) == 1) {
    LOGFILE.Error("Image was not a recognizable BLP2 file. Filetype: \"%s\" VS \"%s\"",header.FileType,"BLP2");
    fclose(fp);
    return NULL;
  }
  if (header.Version != 1) {
    LOGFILE.Error("Version Number Failed. Version: %i",pic->Header.Version);
    fclose(fp);
    return NULL;
  }

  // LOGFILE.Debug("BLP Header Data:");
  // LOGFILE.Debug("\t\tFiletype: \"%s\"", qPrintable(QString(header.FileType)));
  // LOGFILE.Debug("\t\tVersion: %i", header.Version);
  // LOGFILE.Debug("\t\tCompression: %i", header.Compression);
  // LOGFILE.Debug("\t\tAlpha Depth: %i", header.AlphaDepth);
  // LOGFILE.Debug("\t\tPixel Format: %i", header.PixelFormat);
  // LOGFILE.Debug("\t\tX: %i", header.ResX);
  // LOGFILE.Debug("\t\tY: %i", header.ResY);
  // LOGFILE.Debug("\t\tMip Setting: %i", header.hasMipLevels);


  //pic->ImageFormat = getFormat(&header);		// Causes a fatal Vulkan error at the moment.

  pic->nColors = 255;

  if (!ReadColors(fp, pic, 0)) {
    LOGFILE.Error("Error reading color data.");
    fclose(fp);
    return NULL;
  }

  fclose(fp);
  return pic;
}

bool BLPInfo::hasAlpha()
{
  if ((ImageFormat != BLPFormat::BLPFORMAT_PALLETED_NOALPHA) && (ImageFormat != BLPFormat::BLPFORMAT_DTX1_NOALPHA)) {
    return true;
  }
  // LOGFILE.Debug("Has alpha channel: %s", (hasAlpha == true ? "true" : "false"));
  return false;
}

void BLPInfo::getData(QImage *image)
{
  //LOGFILE.Info("Getting BLP Image...");

  uchar *cdata = new uchar[(Header.ResX*Header.ResY)];
  QByteArray *qba = new QByteArray();

  // Build Indexed image data
  for (uint32_t y = 0; y < Header.ResY; ++y) {
    for (uint32_t x = 0; x < Header.ResX; ++x) {
      QString hex;
      QColor color;

      int j = (y*Header.ResX) + x;
      int alpha = 255;
      if (hasAlpha() == true) {
        alpha = rgba[j].A;
      }
      color.setRgb(rgba[j].R, rgba[j].G, rgba[j].B, alpha);

      //LOGFILE.Debug("Setting Pixel %i to %08X", j, color.rgba());
      if (hasAlpha() == true) {
        cdata[j] = color.rgba();
        qba->push_back(color.rgba());
      }
      else {
        cdata[j] = color.rgb();
      }
    }
  }

  //LOGFILE.Debug("Building Image from data...");
  image->loadFromData(cdata, (Header.ResX*Header.ResY), "BLP2");
}

ImageData* BLPInfo::getImageData()
{
  ImageData* img = new ImageData;
  getImageData(img);
  return img;
}

void BLPInfo::getImageData(ImageData *image)
{
  //LOGFILE.Info("Getting BLP ImageData...");
  image->ColorType = RGBAColorType::RGB;
  image->sourceFilename = sourceFilename;
  if (hasAlpha() == true)
  {
    image->ColorType = RGBAColorType::RGBA;
  }
  image->setWidthHeight(Header.ResX, Header.ResY);

  for (size_t y = 0; y < Header.ResY; ++y) {
    for (size_t x = 0; x < Header.ResX; ++x) {
      size_t j = (y*Header.ResX) + x;

      RGBAColor color = rgba[j].toRGBAColor();
      //LOGFILE.Debug("Getting Color[%i]: R: %i, G: %i, B: %i, A: %i", j, color.toInts().R, color.toInts().G, color.toInts().B, color.toInts().A);

      image->ColorData->addTo(color);
      image->ColorDataRaw->addTo(color);
    }
  }
  //LOGFILE.Info("Finished gathering BLP ImageData.");
}
