#pragma once
#include <QtCore/qstring.h>
#include <Foundation/Classes/ImageData.h>
#include "BLPHeaders.h"

class QImage;

class BLPInfo{
public:
  BLP_Header Header;
  BGRAColors *rgba = 0;
  uint32_t nColors;
  BGRAColors *ColorMap = 0;
  unsigned char *Source = 0;
  BLPFormat ImageFormat = BLPFORMAT_NONE;
  QString sourceFilename = QString();

  uint8_t numMipLevels;
  uint32_t MipSize;

  BLPInfo() : nColors(256) { sourceFilename = QString(); }
  ~BLPInfo() {
    if (rgba)
      delete rgba;
    if (ColorMap)
      delete ColorMap;
    if (Source)
      delete Source;
  };

  bool hasAlpha();
  void getData(QImage *image);
  ImageData* getImageData();
  void getImageData(ImageData *image);
  void setFileName(QString name) { sourceFilename = name; }
};

class blp {
public:
  static blp *read(const std::string &);		// Read Data and return a pointer;

  blp(uint8_t *data);							// Constructor from raw data read from a file;
  ~blp() { delete[] _data; };				// Deconstructor

  // bool saveTGA(const std::string &) const;	// Save image as TGA
  // bool savePNG(const std::string &) const;	// Save image as PNG

  void printinfo();	// Print info about the BLP file to the log file.

private:
  uint8_t * _data;								// Image data							
};

BLPInfo* ReadBLPFile(QString Filename);