#include "ImagePNGPlugin.h"
#include "libs/LodePNG/lodepng.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
using namespace ProjectSW;

ImagePluginPNG::ImagePluginPNG() : PluginExportImage("ImageExporterPNG")
{
  LOGFILE.setFile(LOG_FILENAME);
  PluginName = tr("PNG Image Exporter");
  setName(tr("PNG Image Exporter").toStdString());
  PluginFilterName = tr("PNG");
  FileExtensions.push_back("png");
  HeightmapFileExtensions.push_back("png");
  ImageOutTypes.push_back(LANDSCAPEHEIGHT);
  ImageOutTypes.push_back(MASKLAYER);
  ImageOutTypes.push_back(COLOR);
  ImageOutTypes.push_back(COLORALPHA);
}

int ImagePluginPNG::compatibleAppVersion() const
{
  return PLUGINEXPORTIMAGEVERSION;	// TODO
}

QTranslator *ImagePluginPNG::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginI_PNG_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginI_PNG_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

bool ImagePluginPNG::writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type, PluginImageDepth image_depth)
{
  // We can't support 32bit color depth images, so if it is asked for, fail.
  if (image_depth == IMGDEPTH_32BIT)
  {
    return false;
  }

  QString extension = getFileExtensions().value(extensionIndex);
  //LOGFILE.Info("Getting data for file %s...", file_name.toStdString().c_str());
  std::vector<uchar> inarray;
  lodepng::State state;
  state.info_raw.bitdepth = 8;
  switch (image_data->ColorType)
  {
  case RGBAColorType::Grey:
    state.info_raw.colortype = LodePNGColorType::LCT_GREY;
    break;
  case RGBAColorType::GreyAlpha:
    state.info_raw.colortype = LodePNGColorType::LCT_GREY_ALPHA;
    break;
  case RGBAColorType::RGB:
    state.info_raw.colortype = LodePNGColorType::LCT_RGB;
    break;
  case RGBAColorType::RGBA:
  default:
    state.info_raw.colortype = LodePNGColorType::LCT_RGBA;
    break;
  }

  if (image_type == IMGTYPE_HEIGHTMAP)
  {
    extension = getHeightmapExtensions().value(extensionIndex);
  }

  if (image_depth == IMGDEPTH_16BIT)
  {
    state.info_raw.bitdepth = 16;
    inarray = image_data->getDataAsUCharVector(true, true);
  }
  else {
    inarray = image_data->getDataAsUCharVector();
  }

  std::vector<unsigned char> outarray;
  lodepng::encode(outarray, inarray, image_data->getWidth(), image_data->getHeight(), state);
  lodepng::save_file(outarray, file_name.toStdString());
  return true;
}