#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define imgPNG_iid "org.ProjectStormwind.plugin.exporters.image.png"

class ImagePluginPNG : public QObject, public PluginExportImage
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID imgPNG_iid FILE "exportImgPng.json")
  Q_INTERFACES(ProjectSW::PluginExportImage)

public:
  ImagePluginPNG();

  int compatibleAppVersion() const;
  bool writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type = IMGTYPE_RGB, PluginImageDepth image_depth = IMGDEPTH_8BIT);
  PluginImageDepth getMaxHeightmapDepth() { return IMGDEPTH_16BIT; }
  QTranslator *pluginTranslator(QString locale);
};