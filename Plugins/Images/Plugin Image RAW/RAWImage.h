#pragma once

// Format: http://www.rastertek.com/dx11ter08.html

class RAWImage
{
private:
  struct HeightMapType
  {
    float x, y, z;
    float nx, ny, nz;
    float r, g, b;
  };

private:
  HeightMapType* m_heightMap;

public:
  void writeRawHeightMap(char* filename);
};