#include "ImageRAWPlugin.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
using namespace ProjectSW;

ImagePluginRAW::ImagePluginRAW() : PluginExportImage("ImageExporterRAW")
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("RAW Image Exporter").toStdString());
  PluginName = tr("RAW Image Exporter");
  PluginFilterName = tr("RAW Image");
  FileExtensions.push_back("raw");
  HeightmapFileExtensions.push_back("r16");
  ImageOutTypes.push_back(LANDSCAPEHEIGHT);
  //ImageOutTypes.push_back(MASKLAYER);
  //ImageOutTypes.push_back(COLOR);
  //ImageOutTypes.push_back(COLORALPHA);
}

int ImagePluginRAW::compatibleAppVersion() const
{
  return PLUGINEXPORTIMAGEVERSION;	// TODO
}

QTranslator *ImagePluginRAW::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginI_RAW_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginI_RAW_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

bool ImagePluginRAW::writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type, PluginImageDepth image_depth)
{
  QString extension = getFileExtensions().value(extensionIndex);
  if (image_type == IMGTYPE_HEIGHTMAP)
  {
    extension = getHeightmapExtensions().value(extensionIndex);
  }
  return false;
}