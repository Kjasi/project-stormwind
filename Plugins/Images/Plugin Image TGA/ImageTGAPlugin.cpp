#include "ImageTGAPlugin.h"
#include "FreeImage.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
using namespace ProjectSW;

ImagePluginTGA::ImagePluginTGA() : PluginExportImage("ImageExporterTGA")
{
  LOGFILE.setFile(LOG_FILENAME);
  PluginName = tr("TGA Image Exporter");
  setName(tr("TGA Image Exporter").toStdString());
  PluginFilterName = tr("TGA");
  FileExtensions.push_back("tga");
  FileExtensions.push_back("icb");
  FileExtensions.push_back("vda");
  FileExtensions.push_back("vst");
  ImageOutTypes.push_back(MASKLAYER);
  ImageOutTypes.push_back(COLOR);
  ImageOutTypes.push_back(COLORALPHA);
}

int ImagePluginTGA::compatibleAppVersion() const
{
  return PLUGINEXPORTIMAGEVERSION;	// TODO
}

QTranslator *ImagePluginTGA::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginI_TGA_" + locale + ".qm").exists() == false)
  {
    LOGFILE.Error("Unable to find TGA plugin translation...");
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginI_TGA_" + locale + ".qm");
  if (loaded == false)
  {
    LOGFILE.Error("Error occured while loading TGA plugin translation...");
    return nullptr;
  }
  return translator;
}

bool ImagePluginTGA::writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type, PluginImageDepth image_depth)
{
  //LOGFILE.Debug("Setting up TGA data...");

  int samplesperpixel = 3;
  unsigned int bitDepth = 24;
  switch (image_data->ColorType)
  {
  case RGBAColorType::Grey:
    samplesperpixel = 1;
    bitDepth = 8;
    break;
  case RGBAColorType::GreyAlpha:
    samplesperpixel = 2;
    bitDepth = 16;
    break;
  case RGBAColorType::RGBA:
    samplesperpixel = 4;
    bitDepth = 32;
    break;
  case RGBAColorType::RGB:
  default:
    samplesperpixel = 3;
    bitDepth = 24;
    break;
  }
  int pitch = image_data->getWidth() * samplesperpixel;

  //LOGFILE.Debug("Samples per Pixel: %i, BitDepth: %i, pitch: %i", samplesperpixel, bitDepth, pitch);

  //LOGFILE.Debug("Loading image from raw bits...");
  FIBITMAP *imgdata = FreeImage_ConvertFromRawBits(image_data->getDataAsUCharRef(true), image_data->getWidth(), image_data->getHeight(), pitch, bitDepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, true);

  //LOGFILE.Debug("Saving image...");
  if (!FreeImage_Save(FIF_TARGA, imgdata, qPrintable(file_name), TARGA_DEFAULT))
  {
    LOGFILE.Error("Unable to save image...");
    return false;
  }
  return true;
}