#include "ImageTIFFPlugin.h"
#include "FreeImage.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
using namespace ProjectSW;

ImagePluginTIFF::ImagePluginTIFF() : PluginExportImage("ImageExporterTIFF")
{
  LOGFILE.setFile(LOG_FILENAME);
  PluginName = tr("TIFF Image Exporter");
  setName(tr("TIFF Image Exporter").toStdString());
  PluginFilterName = tr("TIFF");
  FileExtensions.push_back("tif");
  FileExtensions.push_back("tiff");
  HeightmapFileExtensions.push_back("tif");
  HeightmapFileExtensions.push_back("tiff");
  ImageOutTypes.push_back(LANDSCAPEHEIGHT);
  ImageOutTypes.push_back(MASKLAYER);
  ImageOutTypes.push_back(COLOR);
  ImageOutTypes.push_back(COLORALPHA);
}

int ImagePluginTIFF::compatibleAppVersion() const
{
  return PLUGINEXPORTIMAGEVERSION;	// TODO
}

QTranslator *ImagePluginTIFF::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginI_TIFF_" + locale + ".qm").exists() == false)
  {
    LOGFILE.Error("Unable to find TIFF plugin translation...");
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginI_TIFF_" + locale + ".qm");
  if (loaded == false)
  {
    LOGFILE.Error("Error occured while loading TIFF plugin translation...");
    return nullptr;
  }
  return translator;
}

bool ImagePluginTIFF::writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type, PluginImageDepth image_depth)
{
  //LOGFILE.Debug("Setting up TIFF data...");

  int samplesperpixel = 3;
  unsigned int bitDepth = 24;
  switch (image_data->ColorType)
  {
  case RGBAColorType::Grey:
    samplesperpixel = 1;
    bitDepth = 8;
    break;
  case RGBAColorType::GreyAlpha:
    samplesperpixel = 2;
    bitDepth = 16;
    break;
  case RGBAColorType::RGBA:
    samplesperpixel = 4;
    bitDepth = 32;
    break;
  case RGBAColorType::RGB:
  default:
    samplesperpixel = 3;
    bitDepth = 24;
    break;
  }
  int pitch = image_data->getWidth() * samplesperpixel;

  //LOGFILE.Debug("Samples per Pixel: %i, BitDepth: %i, pitch: %i", samplesperpixel, bitDepth, pitch);

  //LOGFILE.Debug("Loading image from raw bits...");
  FIBITMAP *imgdata = FreeImage_ConvertFromRawBits(image_data->getDataAsUCharRef(true), image_data->getWidth(), image_data->getHeight(), pitch, bitDepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, true);

  //LOGFILE.Debug("Saving image...");
  if (!FreeImage_Save(FIF_TIFF, imgdata, qPrintable(file_name), TIFF_DEFAULT))
  {
    LOGFILE.Error("Unable to save image...");
    return false;
  }
  return true;
}