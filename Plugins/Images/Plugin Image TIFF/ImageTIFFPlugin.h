#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define imgTIFF_iid "org.ProjectStormwind.plugin.exporters.image.tiff"

class ImagePluginTIFF : public QObject, public PluginExportImage
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID imgTIFF_iid FILE "exportImgTIFF.json")
  Q_INTERFACES(ProjectSW::PluginExportImage)

public:
  ImagePluginTIFF();

  int compatibleAppVersion() const;
  bool writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type = IMGTYPE_RGB, PluginImageDepth image_depth = IMGDEPTH_8BIT);
  PluginImageDepth getMaxHeightmapDepth() { return IMGDEPTH_16BIT; }
  QTranslator *pluginTranslator(QString locale);
};