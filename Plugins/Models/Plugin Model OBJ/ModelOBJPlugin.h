#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtextstream.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define meshOBJ_iid "org.ProjectStormwind.plugin.exporters.model.obj"

class ModelPluginOBJ : public QObject, public PluginExportModel
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID meshOBJ_iid FILE "exportModelOBJ.json")
  Q_INTERFACES(ProjectSW::PluginExportModel)

public:
  ModelPluginOBJ();

  int compatibleAppVersion() const;
  bool writeFile(QString file_name, ModelScene *scene, int extensionIndex, bool showOptions);
  void writeMatFile(QTextStream *stream, ModelObject *modelData, uint8_t matId);
  QTranslator *pluginTranslator(QString locale);

};