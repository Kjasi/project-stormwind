<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>ModelPluginOBJ</name>
    <message>
        <location filename="../ModelOBJPlugin.cpp" line="10"/>
        <location filename="../ModelOBJPlugin.cpp" line="11"/>
        <source>Wavefront OBJ Model Exporter</source>
        <translation>Wavefront OBJ Model Exporter</translation>
    </message>
    <message>
        <location filename="../ModelOBJPlugin.cpp" line="12"/>
        <source>Wavefront OBJ</source>
        <translation>Wavefront OBJ</translation>
    </message>
    <message>
        <location filename="../ModelOBJPlugin.cpp" line="80"/>
        <source>Wavefront OBJ Exported by Project Stormwind</source>
        <comment>Identifier written at the top of every OBJ file.</comment>
        <translation>Wavefront OBJ Exported by Project Stormwind</translation>
    </message>
    <message>
        <location filename="../ModelOBJPlugin.cpp" line="83"/>
        <source>NOTE! This model contained animation. The initial, un-animated, pose will be exported.</source>
        <comment>A note that indicates that this model contains animation, and the initial pose, (the raw, un-animated polygons) will be what is exported.</comment>
        <translation>NOTE! This model contained animation. The initial, un-animated, pose will be exported.</translation>
    </message>
    <message>
        <location filename="../ModelOBJPlugin.cpp" line="104"/>
        <source>Wavefront OBJ Material Exported by Project Stormwind</source>
        <comment>Identifier written at the top of every MTL file.</comment>
        <translation>Wavefront OBJ Material Exported by Project Stormwind</translation>
    </message>
</context>
</TS>
