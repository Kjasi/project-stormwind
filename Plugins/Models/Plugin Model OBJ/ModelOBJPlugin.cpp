#include "ModelOBJPlugin.h"
#include <Foundation/Settings.h>
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
using namespace ProjectSW;

ModelPluginOBJ::ModelPluginOBJ() : PluginExportModel("ModelExporterOBJ")
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("Wavefront OBJ Model Exporter").toStdString());
  PluginName = tr("Wavefront OBJ Model Exporter");
  PluginFilterName = tr("Wavefront OBJ");
  FileExtensions.push_back("obj");
}

int ModelPluginOBJ::compatibleAppVersion() const
{
  return PLUGINEXPORTMODELVERSION;	// TODO
}

QTranslator *ModelPluginOBJ::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginM_OBJ_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginM_OBJ_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

bool ModelPluginOBJ::writeFile(QString file_name, ModelScene *scene, int extensionIndex, bool showOptions)
{
  int totalPoints = 0;
  int totalPolys = 0;

  // Scan for points & polys in the scene...
  for (uint32_t j = 0; j < (uint32_t)scene->Objects.count(); j++)
  {
    ModelObject *mesh = scene->Objects.at(j);

    for (uint32_t i = 0; i < (uint32_t)mesh->MeshList.count(); i++)
    {
      totalPoints += mesh->MeshList.at(i)->pointList.count();
      totalPolys += mesh->MeshList.at(i)->polygonList.count();
    }
  }
  if (totalPoints == 0 || totalPolys == 0)
  {
    LOGFILE.Error("No Points or polys for mesh: %s", qPrintable(file_name));
    return false;
  }

  QFile data(file_name);
  if (data.open(QFile::WriteOnly | QFile::Truncate) == false)
  {
    return false;
  }
  int idx = file_name.lastIndexOf("/") + 1;
  QString srcName = file_name.mid(idx, file_name.lastIndexOf(".") - idx);

  bool isAnim = false;
  for (uint32_t j = 0; j < (uint32_t)scene->Objects.count(); j++)
  {
    ModelObject *mesh = scene->Objects.at(j);

    if (mesh->isAnimated == true)
    {
      isAnim = true;
      break;
    }

    QTextStream stream(&data);
    stream << QString("# %1\n\n").arg(tr("Wavefront OBJ Exported by Project Stormwind", "Identifier written at the top of every OBJ file."));
    if (isAnim == true)
    {
      stream << QString("# %1\n\n").arg(tr("NOTE! This model contained animation. The initial, un-animated, pose will be exported.", "A note that indicates that this model contains animation, and the initial pose, (the raw, un-animated polygons) will be what is exported."));
    }

    bool hasMats = false;
    if (mesh->materialList.count() > 0) hasMats = true;

    QFile *matData = NULL;
    QTextStream *matStream = NULL;
    if (hasMats == true)
    {
      QString matFileNameShort = QString("%1.mtl").arg(srcName);
      QString matFileName = file_name.left(file_name.lastIndexOf("/"));
      matFileName.append("/").append(matFileNameShort);

      matData = new QFile(matFileName);
      if (matData->open(QFile::WriteOnly | QFile::Truncate) == false)
      {
        return false;
      }
      matStream = new QTextStream(matData);

      *matStream << QString("# %1\n\n").arg(tr("Wavefront OBJ Material Exported by Project Stormwind", "Identifier written at the top of every MTL file."));

      stream << QString("mtllib %1\n\n").arg(matFileNameShort);
    }
    QList<uint8_t> matWritten;

    stream << "# Points\n";
    stream.flush();
    for (int m = 0; m < mesh->MeshList.count(); m++)
    {
      for (int i = 0; i < mesh->MeshList.at(m)->pointList.count(); i++)
      {
        ModelPoint p = mesh->MeshList.at(m)->pointList.at((int)i);
        stream << QString("v %1 %2 %3\n").arg(p.Position.x).arg(p.Position.y).arg(p.Position.z);
      }
    }
    stream.flush();
    for (int m = 0; m < mesh->MeshList.count(); m++)
    {
      for (int i = 0; i < mesh->MeshList.at(m)->pointList.count(); i++)
      {
        ModelPoint p = mesh->MeshList.at(m)->pointList.at((int)i);
        stream << QString("vt %1 %2\n").arg(p.UVPosition.x).arg(p.UVPosition.y);
      }
    }
    stream.flush();
    for (int m = 0; m < mesh->MeshList.count(); m++)
    {
      for (int i = 0; i < mesh->MeshList.at(m)->pointList.count(); i++)
      {
        ModelPoint p = mesh->MeshList.at(m)->pointList.at((int)i);
        stream << QString("vn %1 %2 %3\n").arg(p.NormalVector.x).arg(p.NormalVector.y).arg(p.NormalVector.z);
      }
    }

    stream << "\n# Polygon faces\n";
    QString on = mesh->objectName;
    on.replace(" ", "_", Qt::CaseInsensitive);
    on.replace(".", "_", Qt::CaseInsensitive);
    stream << "o " << qPrintable(on) << "\n";
    stream.flush();
    for (int m = 0; m < mesh->MeshList.count(); m++)
    {
      ModelMeshData *md = mesh->MeshList.value(m);
      QString gn = md->groupName;
      if (md->groupName.isEmpty())
      {
        if (mesh->MeshList.count() > 1)
        {
          gn = QString("Group %1").arg(m);
        }
        else {
          gn = "all";
        }
      }
      gn.replace(" ", "_", Qt::CaseInsensitive);
      gn.replace(".", "_", Qt::CaseInsensitive);
      if (gn == "Skin") gn.prepend(QString("%1 ").arg(on));
      stream << "g " << qPrintable(gn) << "\n" << "s 1\n";
      stream.flush();

      QString matName = "none";
      for (int i = 0; i < md->polygonList.count(); i++)
      {
        ModelPolygon p = md->polygonList.at((int)i);
        if (hasMats == true)
        {
          ModelMaterial mat = mesh->materialList.at(p.materialID);
          QString matID = QString("%1").arg(mat.Name);
          matID = matID.mid(matID.lastIndexOf("\\") + 1);
          if (matID != matName)
          {
            stream << QString("usemtl %1\n").arg(matID);
            matName = matID;
            if (matWritten.contains(p.materialID) == false)
            {
              writeMatFile(matStream, mesh, p.materialID);
              matWritten.push_back(p.materialID);
            }
          }
        }
        stream << QString("f %1/%1/%1 %2/%2/%2 %3/%3/%3\n").arg(p.point2 + 1).arg(p.point1 + 1).arg(p.point0 + 1);
        stream.flush();
      }
    }
    stream.flush();

    if (matData != NULL && matData->isOpen() == true)
      matData->close();
  }

  data.close();

  return true;
}

void ModelPluginOBJ::writeMatFile(QTextStream *stream, ModelObject *modelData, uint8_t matId)
{
  if (modelData->materialList.count() <= 0) return;
  ModelMaterial mat = modelData->materialList.at(matId);
  QString matName = mat.Name;
  matName = matName.mid(matName.lastIndexOf("\\") + 1);

  *stream << QString("newmtl %1\n").arg(matName);
  *stream << QString("Ka 1.000 1.000 1.000\n");
  *stream << QString("Kd 0.750 0.750 0.750\n");
  if (mat.hasSpecular == true)
  {
    *stream << QString("Ks 1.000 1.000 1.000\n");
  }
  else {
    *stream << QString("Ks 0.000 0.000 0.000\n");
  }
  *stream << QString("Ns 300.0\n");

  QString prefTextureExporter = SETTINGS.value("preferedExporter/textures").toString();
  int extStart = prefTextureExporter.indexOf(" (*.") + 4;
  int extEnd = prefTextureExporter.indexOf(" ", extStart);
  if (extEnd <= 0) {
      extEnd = prefTextureExporter.indexOf(")", extStart);
  }
  QString ext = prefTextureExporter.mid(extStart, extEnd - extStart);
  //QString ext = "png";
  
  if (modelData->TextureList.count() > 0 && mat.ColorTextureID > -1)
  {
    QString tex = modelData->TextureList.at(mat.ColorTextureID);
    tex.replace(".blp", "." + ext.toLower());
    tex.replace(".BLP", "." + ext.toUpper());
    *stream << QString("map_Kd %1\n").arg(tex);
  }
  if (modelData->TextureList.count() > 0 && mat.SpecularTextureID > -1)
  {
    QString tex = modelData->TextureList.at(mat.SpecularTextureID);
    tex.replace(".blp", "." + ext);
    *stream << QString("map_Ns %s\n").arg(tex);
  }
  *stream << "\n";

  stream->flush();
}