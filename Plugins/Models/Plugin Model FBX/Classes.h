#pragma once
#include <fbxsdk.h>
#include <Foundation/Classes/JsonSettings.hpp>
#include <Foundation/LogFile.h>

struct FBXOptionsForExporting : public JsonSetting
{
  fbxsdk::FbxString fileVersion;
  int fileFormat;					// 0 = Binary
  bool exportMeshes;
  bool exportSkeletons;
  bool exportAnimations;
  bool exportLights;
  bool exportCameras;
  bool embedTextures;

  FBXOptionsForExporting()
  {
    fileVersion = FBX_DEFAULT_FILE_COMPATIBILITY;
    fileFormat = 0;				// Default to Binary
    exportMeshes = true;
    exportSkeletons = true;
    exportAnimations = false;
    exportLights = true;
    exportCameras = true;
    embedTextures = false;
  }

  QString getVariables()
  {
    QJsonObject a = QJsonObject();

    a.insert("fileVersion", QString(fileVersion));
    LOGFILE.Debug("Get Var: FileVersion: %s", fileVersion);
    a.insert("fileFormat", fileFormat);
    a.insert("exportMeshes", exportMeshes);
    a.insert("exportSkeletons", exportSkeletons);
    a.insert("exportAnimations", exportAnimations);
    a.insert("exportLights", exportLights);
    a.insert("exportCameras", exportCameras);
    a.insert("embedTextures", embedTextures);

    return getJsonString(a);
  }

  void setVariables(QString values)
  {
    QJsonObject a = getJsonObject(values);

    fileVersion = qPrintable(a.value("fileVersion").toString());
    LOGFILE.Debug("Set Var: FileVersion: %s, value: %s", fileVersion, qPrintable(a.value("fileVersion").toString()));
    fileFormat = a.value("fileFormat").toInt();
    exportMeshes = a.value("exportMeshes").toBool();
    exportSkeletons = a.value("exportSkeletons").toBool();
    exportAnimations = a.value("exportAnimations").toBool();
    exportLights = a.value("exportLights").toBool();
    exportCameras = a.value("exportCameras").toBool();
    embedTextures = a.value("embedTextures").toBool();
  }
};