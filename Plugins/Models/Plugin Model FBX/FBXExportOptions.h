#pragma once
#include <QtWidgets/qdialog.h>
#include "ui_FBXExportOptions.h"
#include "Classes.h"

class FBXOptionsExport : public QDialog
{
  Q_OBJECT
public:
  FBXOptionsExport(QWidget *parent = 0);

  FBXOptionsForExporting getExportOptions() { return exportOptions; }
  void setExportOptions(FBXOptionsForExporting options) { exportOptions = options; updateUI(); }
  bool Start = false;

private slots:
  void on_checkBoxExportMesh_clicked(bool value);
  void on_checkBoxExportSkeleton_clicked(bool value);
  void on_checkBoxExportLights_clicked(bool value);
  void on_checkBoxExportCameras_clicked(bool value);
  void on_checkBoxExportAnimations_clicked(bool value);
  void on_checkBoxEmbedTextures_clicked(bool value);

  void on_okButton_pressed();
  void on_cancelButton_pressed();

  void comboBoxFileVersion_currentTextChanged(QString text);

private:
  Ui::FBXExportOptions *ui;

  void updateUI();

  FBXOptionsForExporting exportOptions;
  FBXOptionsForExporting backupExportOptions;
};