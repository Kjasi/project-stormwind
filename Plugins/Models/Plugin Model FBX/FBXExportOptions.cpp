#include "FBXExportOptions.h"

FBXOptionsExport::FBXOptionsExport(QWidget *parent): ui(new Ui::FBXExportOptions)
{
  backupExportOptions = exportOptions;
  ui->setupUi(this);

  // Newest always first / Default
  ui->comboBoxFileVersion->addItem(FBX_2019_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2018_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2016_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2014_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2013_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2012_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2011_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2010_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2009_00_V7_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2009_00_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2006_11_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2006_08_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2006_02_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_2005_08_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_60_COMPATIBLE);
  ui->comboBoxFileVersion->addItem(FBX_53_MB55_COMPATIBLE);

  connect(ui->comboBoxFileVersion, &QComboBox::currentTextChanged, this, &FBXOptionsExport::comboBoxFileVersion_currentTextChanged);

  updateUI();
}

void FBXOptionsExport::updateUI()
{
  ui->checkBoxEmbedTextures->setChecked(exportOptions.embedTextures);
  ui->checkBoxExportAnimations->setChecked(exportOptions.exportAnimations);
  ui->checkBoxExportCameras->setChecked(exportOptions.exportCameras);
  ui->checkBoxExportLights->setChecked(exportOptions.exportLights);
  ui->checkBoxExportMesh->setChecked(exportOptions.exportMeshes);
  ui->checkBoxExportSkeleton->setChecked(exportOptions.exportSkeletons);

  ui->comboBoxFileVersion->setCurrentText(QString(exportOptions.fileVersion));
}

void FBXOptionsExport::on_checkBoxExportMesh_clicked(bool value)
{
  exportOptions.exportMeshes = value;
  updateUI();
}

void FBXOptionsExport::on_checkBoxExportSkeleton_clicked(bool value)
{
  exportOptions.exportSkeletons = value;
  updateUI();
}

void FBXOptionsExport::on_checkBoxExportLights_clicked(bool value)
{
  exportOptions.exportLights = value;
  updateUI();
}

void FBXOptionsExport::on_checkBoxExportCameras_clicked(bool value)
{
  exportOptions.exportCameras = value;
  updateUI();
}

void FBXOptionsExport::on_checkBoxExportAnimations_clicked(bool value)
{
  exportOptions.exportAnimations = value;
  updateUI();
}

void FBXOptionsExport::on_checkBoxEmbedTextures_clicked(bool value)
{
  exportOptions.embedTextures = value;
  updateUI();
}

void FBXOptionsExport::on_okButton_pressed()
{
  backupExportOptions = exportOptions;
  Start = true;
}

void FBXOptionsExport::on_cancelButton_pressed()
{
  exportOptions = backupExportOptions;
  Start = false;
}

void FBXOptionsExport::comboBoxFileVersion_currentTextChanged(QString text)
{
  exportOptions.fileVersion = qPrintable(text);
  updateUI();
}
