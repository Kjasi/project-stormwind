#include "ModelFBXPlugin.h"
#include <Foundation/Functions.h>
#include <Foundation/saveFiles.hpp>
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
#include <fbxsdk.h>
#include <fbxsdk/fileio/fbxexporter.h>
#include <Dependancies/FreeImage/FreeImage.h>
using namespace ProjectSW;
using namespace fbxsdk;

ModelPluginFBX::ModelPluginFBX() : PluginExportModel("ModelExporterFBX"), lSdkManager(0), lScene(0), nodeMesh(0), nodeSkeleton(0), nodeLights(0), nodeCameras(0)
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("FBX Model Exporter").toStdString());
  PluginName = tr("FBX Model Exporter");
  PluginFilterName = tr("FBX");
  FileExtensions.push_back("fbx");
}

int ModelPluginFBX::compatibleAppVersion() const
{
  return PLUGINEXPORTMODELVERSION;	// TODO
}

bool ModelPluginFBX::getOptions(bool showOptions)
{
  if (showOptions == true)
  {
    optionsExportWindow = new FBXOptionsExport();
    optionsExportWindow->setExportOptions(exportOptions);
    int ok = optionsExportWindow->exec();

    if (ok)
    {
      exportOptions = optionsExportWindow->getExportOptions();
      setOptionsJson(exportOptions.getVariables());
      return true;
    }
    else {
      return false;
    }
  }
  return true;
}

QTranslator *ModelPluginFBX::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginM_FBX_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginM_FBX_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

bool ModelPluginFBX::writeFile(QString file_name, ModelScene *scene, int extensionIndex, bool showOptions)
{
  int totalPoints = 0;
  int totalPolys = 0;
  exportOptions.setVariables(getOptionsJson());
  destinationFileName = unifySlashes(file_name);

  // Scan for points & polys in the scene...
  for (uint32_t j = 0; j < (uint32_t)scene->Objects.count(); j++)
  {
    ModelObject *mesh = scene->Objects.at(j);

    for (uint32_t i = 0; i < (uint32_t)mesh->MeshList.count(); i++)
    {
      totalPoints += mesh->MeshList.at(i)->pointList.count();
      totalPolys += mesh->MeshList.at(i)->polygonList.count();
    }
  }
  if (totalPoints == 0 || totalPolys == 0)
  {
    LOGFILE.Error("No Points or polys for mesh: %s", qPrintable(file_name));
    return false;
  }


  LOGFILE.Info("Initializing FBX Writer...");
  // Initialize the SDK manager. This object handles memory management.
  lSdkManager = FbxManager::Create();
  if (!lSdkManager)
  {
    FBXSDK_printf("Call to FbxExporter::Initialize() failed.");
    qErrnoWarning("Call to FbxExporter::Initialize() failed.\n");
    LOGFILE.Error("Call to FbxExporter::Initialize() failed.");
    return false;
  }

  // Create an IOSettings object. IOSROOT is defined in Fbxiosettingspath.h.
  FbxIOSettings * ios = FbxIOSettings::Create(lSdkManager, IOSROOT);
  lSdkManager->SetIOSettings(ios);

  // Get our export options.
  bool ok = getOptions(showOptions);
  if (ok == false)
  {
    LOGFILE.Info("User cancelled Options. Aborting Save...");
    lSdkManager->Destroy();
    return false;
  }

  if (exportOptions.exportAnimations == false && exportOptions.exportCameras == false && exportOptions.exportLights == false && exportOptions.exportMeshes == false && exportOptions.exportSkeletons == false)
  {
    FBXSDK_printf("User has disabled all FBX export options. Nothing to export, so we're done?");
    qWarning("User has disabled all FBX export options. Nothing to export, so we're done?\n");
    LOGFILE.Info("User has disabled all FBX export options. Nothing to export, so we're done?");
    lSdkManager->Destroy();
    return true;
  }

  modelScene = scene;

  // Create an exporter.
  FbxExporter* lExporter = FbxExporter::Create(lSdkManager, "");

  // Set the Options
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_MATERIAL,			true);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_TEXTURE,			true);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_EMBEDDED,			exportOptions.embedTextures);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_SHAPE,			true);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_GOBO,				true);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_ANIMATION,		true);
  (*(lSdkManager->GetIOSettings())).SetBoolProp(EXP_FBX_GLOBAL_SETTINGS,	true);

  // Set the file's version. (Allows compatibility with previous FBX editions)
  lExporter->SetFileExportVersion(exportOptions.fileVersion);
  int lFileFormat = exportOptions.fileFormat;

  if ((lFileFormat < 0 || lFileFormat >= lSdkManager->GetIOPluginRegistry()->GetWriterFormatCount()) && exportOptions.embedTextures == false)
  {
    // Write in fall back format unless no ASCII format found
    lFileFormat = lSdkManager->GetIOPluginRegistry()->GetNativeWriterFormat();
    //Try to export in ASCII if possible
    int lFormatIndex, lFormatCount = lSdkManager->GetIOPluginRegistry()->GetWriterFormatCount();
    for (lFormatIndex = 0; lFormatIndex < lFormatCount; lFormatIndex++)
    {
      if (lSdkManager->GetIOPluginRegistry()->WriterIsFBX(lFormatIndex))
      {
        FbxString lDesc = lSdkManager->GetIOPluginRegistry()->GetWriterFormatDescription(lFormatIndex);
        const char *lASCII = "ascii";
        if (lDesc.Find(lASCII) >= 0)
        {
          lFileFormat = lFormatIndex;
          break;
        }
      }
    }
  }

  // Initialize the exporter.
  bool lExportStatus = lExporter->Initialize(qPrintable(file_name), lFileFormat, lSdkManager->GetIOSettings());
  if (!lExportStatus) {
    FBXSDK_printf("Call to FbxExporter::Initialize() failed. Error returned: %s", lExporter->GetStatus().GetErrorString());
    qErrnoWarning("Call to FbxExporter::Initialize() failed. Error returned: %s\n\n", lExporter->GetStatus().GetErrorString());
    LOGFILE.Error("Call to FbxExporter::Initialize() failed. Error returned: %s", lExporter->GetStatus().GetErrorString());
    lSdkManager->Destroy();
    return false;
  }

  // Create our scene!
  lScene = FbxScene::Create(lSdkManager, "myScene");
  if (!lScene)
  {
    FBXSDK_printf("Unable to create FBX Scene.");
    qErrnoWarning("Unable to create FBX Scene.\n");
    LOGFILE.Error("Unable to create FBX Scene.");
    lSdkManager->Destroy();
    return false;
  }

  QStringList keywords;
  for (int i = 0; i < modelScene->Objects.count(); i++)
  {
    ModelObject *obj = modelScene->Objects.at(i);
    keywords.append(getChildNames(obj));
  }
  for (int i = 0; i < modelScene->Cameras.count(); i++)
  {
    keywords.push_back(modelScene->Cameras.at(i)->Name);
  }

  // Apply some basic information to the FBX file
  FbxDocumentInfo* sceneInfo = FbxDocumentInfo::Create(lSdkManager, "SceneInfo");
  LOGFILE.Debug("Making FBX scene with name: %s", qPrintable(modelScene->Name));
  QString sceneName = modelScene->Name;
  if (sceneName.isEmpty()) { sceneName = "Project Stormwind FBX Exported Scene"; }
  sceneInfo->mTitle = qPrintable(sceneName);
  sceneInfo->mSubject = qPrintable(tr("A World of Warcraft model, exported to FBX by Project Stormwind."));
  sceneInfo->mAuthor = qPrintable(PROJECTSTORMWIND_TITLE);
  sceneInfo->mRevision = qPrintable(QString(PROJECTSTORMWIND_VERSION));
  sceneInfo->mKeywords = qPrintable(keywords.join(" "));
  //sceneInfo->mComment = "FBX supports comments. Might use later...";
  lScene->SetSceneInfo(sceneInfo);

  // Get the root node of the scene.
  FbxNode* lRootNode = lScene->GetRootNode();

  // Build the mesh and it's materials.
  if (exportOptions.exportMeshes == true)
  {
    buildMesh();
    buildMaterials();
    lRootNode->AddChild(nodeMesh);
  }

  // Build our Skeletons/Rigs
  if (exportOptions.exportSkeletons == true)
  {
    buildSkeleton();
    lRootNode->AddChild(nodeSkeleton);
  }

  // Add the Lights
  if (exportOptions.exportLights == true)
  {
    buildLights();
    lRootNode->AddChild(nodeLights);
  }

  // Export the Cameras
  if (exportOptions.exportCameras == true)
  {
    buildCameras();
    lRootNode->AddChild(nodeCameras);
  }

  // Bind the mesh to the Skeleton, and save the bind pose
  if (exportOptions.exportMeshes == true && exportOptions.exportSkeletons == true)
  {
    linkMeshAndSkeleton();
    saveBindPose();
  }

  // Store the Rest Poses
  if (exportOptions.exportSkeletons == true)
  {
    saveRestPose();
  }

  // Animation
  if (exportOptions.exportAnimations)
  {
    addAnimations();
  }

  // Export the scene to the file.
  bool result = lExporter->Export(lScene);

  if (result == false)
  {
    LOGFILE.Error("An error has occured while writing the FBX file.");
  }
  else {
    LOGFILE.Info("FBX file has been successfully written!");
  }

  // Delete exported textures...
  for (auto it : exportedTextures)
  {
    QFile file(it);
    file.remove();
  }

  // Destroy our SDK objects.
  lExporter->Destroy();
  lSdkManager->Destroy();

  return result;
}

QStringList ModelPluginFBX::getChildNames(ModelObject * obj)
{
  QStringList list;

  list.push_back(obj->objectName);
  for (int i = 0; i < obj->cameraList.count(); i++)
  {
    list.push_back(obj->cameraList.at(i)->Name);
  }

  for (int i = 0; i < obj->childObjects.count(); i++)
  {
    if (obj->childObjects.at(i)->childObjects.count() > 0)
    {
      list.append(getChildNames(obj->childObjects.at(i)));
    }
    else {
      list.push_back(obj->childObjects.at(i)->objectName);
    }
  }

  return list;
}

void ModelPluginFBX::buildMesh()
{
  LOGFILE.Info("Building FBX Mesh...");
  int modelMaterialCount = 0;

  nodeMesh = FbxNode::Create(lSdkManager, qPrintable(modelScene->Name));

  LOGFILE.Debug("Objects to export: %i", modelScene->Objects.count());
  for (uint32_t ms = 0; ms < (uint32_t)modelScene->Objects.count(); ms++)
  {	
    ModelObject *modelObject = modelScene->Objects.at(ms);
    FbxMesh *fbxmesh = FbxMesh::Create(lSdkManager, qPrintable(modelObject->objectName));
    LOGFILE.Debug("Making FBX mesh with name: %s", qPrintable(modelObject->objectName));

    size_t meshVertCount = 0;
    LOGFILE.Debug("Object Mesh Count: %i", modelObject->MeshList.count());
    for (int i = 0; i < modelObject->MeshList.count(); i++)
    {
      meshVertCount += modelObject->MeshList.at(i)->pointList.count();
    }

    fbxmesh->InitControlPoints((int)meshVertCount);
    FbxVector4 *fbxVerts = fbxmesh->GetControlPoints();

    // Layers are a way to store many different data types for the exported mesh. Most programs only support having 1 layer, so no need to make multiple...
    FbxLayer* layer = fbxmesh->GetLayer(0);
    if (layer == 0)
    {
      fbxmesh->CreateLayer();
      layer = fbxmesh->GetLayer(0);
    }

    // Normals
    // Set Normal values to per-Vertex (or Control Point, as FBX calls it)
    FbxLayerElementNormal* layer_normal = FbxLayerElementNormal::Create(fbxmesh, "");
    layer_normal->SetMappingMode(FbxGeometryElement::eByControlPoint);
    layer_normal->SetReferenceMode(FbxGeometryElement::eDirect);

    // Create UV for Diffuse channel.
    FbxLayerElementUV* layer_texcoord = FbxLayerElementUV::Create(fbxmesh, "DiffuseUV");
    layer_texcoord->SetMappingMode(FbxLayerElement::eByControlPoint);
    layer_texcoord->SetReferenceMode(FbxLayerElement::eDirect);
    layer->SetUVs(layer_texcoord, FbxLayerElement::eTextureDiffuse);

    // create the materials.
    /* Each polygon face will be assigned a unique material.
    */
    FbxGeometryElementMaterial* lMaterialElement = fbxmesh->CreateElementMaterial();
    lMaterialElement->SetMappingMode(FbxGeometryElement::eByPolygon);
    lMaterialElement->SetReferenceMode(FbxGeometryElement::eIndexToDirect);

    uint32_t vertCounter = 0;
    for (int32_t meshListID = 0; meshListID < modelObject->MeshList.count(); meshListID++)
    {
      ModelMeshData *mesh = modelObject->MeshList.value(meshListID);
      LOGFILE.Debug("Processing Mesh Group: %s", qPrintable(mesh->groupName));

      FbxGeometryElementPolygonGroup* lPolygonGroupElement = fbxmesh->CreateElementPolygonGroup();
      lPolygonGroupElement->SetName(qPrintable(mesh->groupName));

      // Verticies
      for (int32_t i = 0; i < mesh->pointList.count(); i++)
      {
        ModelPoint v = mesh->pointList.at(i);
        //LOGFILE.Debug("Vert %05i:  |  X: %f  |  Y: %f  |  Z: %f", i, v.Position.x, v.Position.y, v.Position.z);
        fbxVerts[vertCounter + i] = FbxVector4(v.Position.x, v.Position.y, v.Position.z);
        layer_normal->GetDirectArray().Add(FbxVector4(v.NormalVector.x, v.NormalVector.y, v.NormalVector.z));
        layer_texcoord->GetDirectArray().Add(FbxVector2(v.UVPosition.x, v.UVPosition.y));
      }
      vertCounter += mesh->pointList.count();

      // Polygons
      for (int32_t i = 0; i < mesh->polygonList.count(); i++)
      {
        ModelPolygon p = mesh->polygonList.at(i);
        //LOGFILE.Debug("Polygon %04i: MatID: %i  |  P0: %05i  |  P2: %05i  |  P1: %05i", i, p.materialID, p.point0, p.point2, p.point1);
        fbxmesh->BeginPolygon(p.materialID + modelMaterialCount, -1, -1, false);
        fbxmesh->AddPolygon(p.point0);
        fbxmesh->AddPolygon(p.point2);
        fbxmesh->AddPolygon(p.point1);
        fbxmesh->EndPolygon();
        lPolygonGroupElement->GetIndexArray().Add(meshListID);
      }
    }
    fbxmesh->BuildMeshEdgeArray();
    fbxmesh->SetMeshSmoothness(FbxMesh::eFine);
    nodeMesh->SetNodeAttribute(fbxmesh);
    modelMaterialCount += modelObject->materialList.count();
  }
  nodeMesh->SetShadingMode(FbxNode::eTextureShading);
}

void ModelPluginFBX::buildMaterials()
{
  for (uint32_t ms = 0; ms < (uint32_t)modelScene->Objects.count(); ms++)
  {
    ModelObject *modelObject = modelScene->Objects.at(ms);

    for (int32_t i = 0; i < modelObject->materialList.count(); i++)
    {
      ModelMaterial mat = modelObject->materialList.at(i);

      // Material Name
      FbxString mat_name = qPrintable(getJustFilesName(mat.Name));
      FbxSurfacePhong* material = FbxSurfacePhong::Create(lSdkManager, mat_name.Buffer());
      material->Ambient.Set(FbxDouble3(0.25, 0.25, 0.25));
      material->Specular.Set(FbxDouble3(0.0, 0.0, 0.0));

      // Texture names
      QString texColor = unifySlashes(modelObject->TextureList.at(mat.ColorTextureID));
      QString texColor2 = texColor.mid(0, texColor.lastIndexOf("."));
      QString texColorFile = QString("%1/%2.tga").arg(SETTINGS.value("outputdir").toString()).arg(texColor2);
      // LOGFILE.Debug("MatName: %s, texColor: %s", mat_name, qPrintable(texColorFile));

      // texColorFile is where the textures are. If embedTextures is enabled, it copies them into the FBX.
      // Make sure the textures are exported if we're embeding them.
      if (exportOptions.embedTextures == true && texColor.contains(".null") == false)
      {
        bool fileExists = false;

        // The various possibilites to check
        QString destColorFile = QString("%1/%2.tga").arg(destinationFileName.mid(0, destinationFileName.lastIndexOf("/"))).arg(texColor2.mid(texColor2.lastIndexOf("/")+1));

        // See if the textures already exist in the outputdir.
        if (QFileInfo(texColorFile).exists() == true)
        {
          fileExists = true;
        }
        // Check our destination directory...
        else if (QFileInfo(destColorFile).exists() == true)
        {
          texColorFile = destColorFile;
          fileExists = true;
        }

        // Export image if it does not exist
        if (fileExists == false)
        {
          LOGFILE.Debug("Unable to find existing texture. Exporting a new copy...");
          QString srcFile = QString("%1.blp").arg(QDir(SETTINGS.value("wowdir").toString()).absoluteFilePath(texColor2));
          //LOGFILE.Debug("SourceFile: %s, Destination: %s", qPrintable(srcFile), qPrintable(destColorFile));

          BLPInfo *textureBLP = ReadBLPFile(srcFile);
          ImageData *imgData = textureBLP->getImageData();

          int samplesperpixel = 3;
          unsigned int bitDepth = 24;
          switch (imgData->ColorType)
          {
          case RGBAColorType::Grey:
            samplesperpixel = 1;
            bitDepth = 8;
            break;
          case RGBAColorType::GreyAlpha:
            samplesperpixel = 2;
            bitDepth = 16;
            break;
          case RGBAColorType::RGBA:
            samplesperpixel = 4;
            bitDepth = 32;
            break;
          case RGBAColorType::RGB:
          default:
            samplesperpixel = 3;
            bitDepth = 24;
            break;
          }

          int pitch = imgData->getWidth() * samplesperpixel;
          FIBITMAP *freeImgData = FreeImage_ConvertFromRawBits(imgData->getDataAsUCharRef(true), imgData->getWidth(), imgData->getHeight(), pitch, bitDepth, FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, true);

          if (freeImgData)
          {
            if (!FreeImage_Save(FIF_TARGA, freeImgData, qPrintable(destColorFile), TARGA_DEFAULT))
            {
              LOGFILE.Error("We were unable to export %s for inclusion in FBX...", qPrintable(srcFile));
            }
            else {
              texColorFile = destColorFile;
              exportedTextures.push_back(destColorFile);
            }
          }
          else {
            LOGFILE.Error("We were unable to export %s for inclusion in FBX...", qPrintable(srcFile));
          }
        }
      }

      // Generate Texture
      // Assume that the file we need is in the export path
      FbxFileTexture* texture = FbxFileTexture::Create(lSdkManager, qPrintable(getJustFilesName(texColor)));
      texture->SetFileName(qPrintable(texColorFile));
      texture->SetTextureUse(FbxTexture::eStandard);
      texture->SetMappingType(FbxTexture::eUV);
      texture->SetMaterialUse(FbxFileTexture::eModelMaterial);
      texture->SetSwapUV(false);
      texture->SetTranslation(0.0, 0.0);
      texture->SetScale(1.0, 1.0);
      texture->SetRotation(0.0, 0.0);
      texture->UVSet.Set(FbxString("DiffuseUV"));

      // Apply Texture to Material
      material->Diffuse.ConnectSrcObject(texture);

      // Apply Material to Mesh
      nodeMesh->AddMaterial(material);
    }
  }
}

void ModelPluginFBX::buildSkeleton()
{
}

void ModelPluginFBX::buildLights()
{
}

void ModelPluginFBX::buildCameras()
{
  // Cameras
  if (exportOptions.exportCameras == true)
  {
    QList<ModelObject*> ObjectsWithCameras;

    for (uint32_t i = 0; i < (uint32_t)modelScene->Objects.count(); i++)
    {
      if (modelScene->Objects.at(i)->cameraList.count() > 0)
      {
        ObjectsWithCameras.push_back(modelScene->Objects.at(i));
      }
    }

    if (modelScene->Cameras.count() > 0 || ObjectsWithCameras.count() > 0)
    {
      // Foreach Scene Camera

      // Foreach ObjectsWithCameras Camera
    }
  }
}

void ModelPluginFBX::linkMeshAndSkeleton()
{
}

void ModelPluginFBX::saveBindPose()
{
}

void ModelPluginFBX::saveRestPose()
{
}

void ModelPluginFBX::addAnimations()
{
}
