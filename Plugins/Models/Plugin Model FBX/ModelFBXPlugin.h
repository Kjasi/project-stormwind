#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtextstream.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include "Classes.h"
#include "FBXExportOptions.h"
using namespace ProjectSW;

#define meshFBX_iid "org.ProjectStormwind.plugin.exporters.model.fbx"

class ModelPluginFBX : public QObject, public PluginExportModel
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID meshFBX_iid FILE "exportModelFBX.json")
  Q_INTERFACES(ProjectSW::PluginExportModel)

public:
  ModelPluginFBX();
  ~ModelPluginFBX() {};

  int compatibleAppVersion() const;
  bool getOptions(bool showOptions);
  bool writeFile(QString file_name, ModelScene *scene, int extensionIndex, bool showOptions);
  QTranslator *pluginTranslator(QString locale);

private:
  FBXOptionsExport *optionsExportWindow = NULL;
  FBXOptionsForExporting exportOptions;
  ModelScene *modelScene = NULL;
  QString destinationFileName;
  QStringList exportedTextures;

  // FBX Nodes
  FbxManager* lSdkManager = 0;
  FbxScene* lScene = 0;

  FbxNode *nodeMesh = 0;
  FbxNode *nodeSkeleton = 0;
  FbxNode *nodeLights = 0;
  FbxNode *nodeCameras = 0;

  QStringList getChildNames(ModelObject *obj);

  void buildMesh();
  void buildMaterials();
  void buildSkeleton();
  void buildLights();
  void buildCameras();
  void linkMeshAndSkeleton();
  void saveBindPose();
  void saveRestPose();
  void addAnimations();
};
