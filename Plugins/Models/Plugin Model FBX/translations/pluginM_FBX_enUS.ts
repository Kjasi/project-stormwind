<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>FBXExportOptions</name>
    <message>
        <source>FBX Export Options</source>
        <translation>FBX Export Options</translation>
    </message>
    <message>
        <source>File Version</source>
        <translation>File Version</translation>
    </message>
    <message>
        <source>Export Bones</source>
        <translation>Export Bones</translation>
    </message>
    <message>
        <source>Export Mesh</source>
        <translation>Export Mesh</translation>
    </message>
    <message>
        <source>Embed Textures</source>
        <translation>Embed Textures</translation>
    </message>
    <message>
        <source>Export Lights</source>
        <translation>Export Lights</translation>
    </message>
    <message>
        <source>Export Cameras</source>
        <translation>Export Cameras</translation>
    </message>
    <message>
        <source>Export Animations</source>
        <translation>Export Animations</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>ModelPluginFBX</name>
    <message>
        <source>FBX Model Exporter</source>
        <translation>FBX Model Exporter</translation>
    </message>
    <message>
        <source>FBX</source>
        <translation>FBX</translation>
    </message>
    <message>
        <source>A World of Warcraft model, exported to FBX by Project Stormwind.</source>
        <translation>A World of Warcraft model, exported to FBX by Project Stormwind.</translation>
    </message>
</context>
</TS>
