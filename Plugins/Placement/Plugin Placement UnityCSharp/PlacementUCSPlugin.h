#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtextstream.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define placementUnityCS_iid "org.ProjectStormwind.plugin.exporters.placement.unity.cs"

class PlacementPluginUnityCS : public QObject, public PluginExportPlacement
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID placementUnityCS_iid FILE "exportPlacementUCS.json")
  Q_INTERFACES(ProjectSW::PluginExportPlacement)

public:
  PlacementPluginUnityCS();

  int compatibleAppVersion() const;
  bool writeFile(QString file_name, PlacementData *data, int extensionIndex);
  QTranslator *pluginTranslator(QString locale);

private:
  void startFunction(QTextStream &stream);
  void endFunction(QTextStream &stream);

  void writeProcess(QTextStream &stream, QString parentObjectName, PlacementObject *parentObj);

  void writeNull(QTextStream &stream, PlacementObject obj, QString objName, QString uniqueObjName, QString parentName = QString(), PlacementObjectType parentType = PlacementObjectType::PLACEMENTOBJECTTYPE_NULL);
  void writeLight(QTextStream &stream, PlacementLight obj, QString objName, QString uniqueObjName, QString parentName = QString());
  void writeCamera(QTextStream &stream, PlacementCamera obj, QString objName, QString uniqueObjName, QString parentName = QString());
  void writeAmbientSound(QTextStream &stream, PlacementSound obj, QString objName, QString uniqueObjName, QString parentName = QString());
};