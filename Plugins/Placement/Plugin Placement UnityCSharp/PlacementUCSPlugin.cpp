#include "PlacementUCSPlugin.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
using namespace ProjectSW;

#define METERStoCENTIMETERS 100

QMap<QString, int> nullObjects;
QMap<QString, int> lightObjects;
QMap<QString, int> cameraObjects;
QMap<int, QString> parentNames;
uint32_t numFunctions = 0;

PlacementPluginUnityCS::PlacementPluginUnityCS() : PluginExportPlacement("PlacementExporterUnityCS")
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("Unity C# Placement Exporter").toStdString());
  PluginName = tr("Unity C# Placement Exporter");
  PluginFilterName = tr("Unity C# Placement Code");
  FileExtensions.push_back("cs");
}

int PlacementPluginUnityCS::compatibleAppVersion() const
{
  return PLUGINEXPORTPLACEMENTVERSION;	// TODO
}

QTranslator *PlacementPluginUnityCS::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginP_UCS_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginP_UCS_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

void PlacementPluginUnityCS::startFunction(QTextStream & stream)
{
  stream << "	void Process" << numFunctions << "(string parentName)\n	{\n";
  stream << "		GameObject parentObject = GameObject.Find(parentName);\n\n";
}

void PlacementPluginUnityCS::endFunction(QTextStream & stream)
{
  numFunctions++;
  stream << "	}\n\n";
}

void PlacementPluginUnityCS::writeProcess(QTextStream &stream, QString parentObjectName, PlacementObject *parentObj)
{
  // Unity functions have a code limit, and the code needs to be split! 300 is an arbitrary number.
  uint32_t UnityLimit = 300;
  uint32_t numItems = parentObj->getNumChildren() + parentObj->getNumChildrenLights() + parentObj->getNumChildrenCameras() + parentObj->getNumChildrenSounds();
    
  float numFuncsF = ceil((float)numItems / (float)UnityLimit);
  uint32_t numFuncs = numFuncsF;

  QList<PlacementObject> ParentObjects;
  QList<QString> ParentUniqueNames;

  for (uint32_t f = 0; f < numFuncs; f++)
  {
    startFunction(stream);
    parentNames[numFunctions] = parentObjectName;

    uint32_t startPoint = (UnityLimit * f);
    uint32_t numToProcess = UnityLimit;
    if ((startPoint + UnityLimit) > numItems)
    {
      numToProcess = numItems - startPoint;
    }
    uint32_t endPoint = startPoint + numToProcess;

    // Debug String
    // stream << "		// startPoint: " << startPoint << ", numToProcess: " << numToProcess << ", endPoint: " << endPoint << "\n";
    for (uint32_t i = startPoint; i < endPoint; i++)
    {
      if (i < parentObj->getNumChildren())
      {
        PlacementObject a = parentObj->children.at(i);

        QString objName = a.Name;
        QString uniqueObjName;
        sanitizeNames(nullObjects, objName, uniqueObjName);

        if (a.getNumAllChildren() > 0) { ParentObjects.push_back(a); ParentUniqueNames.push_back(uniqueObjName); }
        
        writeNull(stream, a, objName, uniqueObjName, parentObjectName, parentObj->objectType);
      } 
      else if (i < parentObj->getNumChildren() + parentObj->getNumChildrenLights())
      {
        PlacementLight a = parentObj->childrenLights.at(i - (parentObj->getNumChildren()));

        QString objName = a.Name;
        QString uniqueObjName;
        sanitizeNames(nullObjects, objName, uniqueObjName);

        if (a.getNumAllChildren() > 0) { ParentObjects.push_back(a); ParentUniqueNames.push_back(uniqueObjName); }

        writeLight(stream, a, objName, uniqueObjName, parentObjectName);
      }
      else if (i < parentObj->getNumChildren() + parentObj->getNumChildrenLights() + parentObj->getNumChildrenCameras())
      {
        PlacementCamera a = parentObj->childrenCameras.at(i - (parentObj->getNumChildren() + parentObj->getNumChildrenLights()));

        QString objName = a.Name;
        QString uniqueObjName;
        sanitizeNames(nullObjects, objName, uniqueObjName);

        if (a.getNumAllChildren() > 0) { ParentObjects.push_back(a); ParentUniqueNames.push_back(uniqueObjName); }

        writeCamera(stream, a, objName, uniqueObjName, parentObjectName);
      }
      else
      {
        PlacementSound a = parentObj->childrenSounds.at(i - (parentObj->getNumChildren() + parentObj->getNumChildrenLights() + parentObj->getNumChildrenCameras()));

        QString objName = a.Name;
        QString uniqueObjName;
        sanitizeNames(nullObjects, objName, uniqueObjName);

        if (a.getNumAllChildren() > 0) { ParentObjects.push_back(a); ParentUniqueNames.push_back(uniqueObjName); }

        writeAmbientSound(stream, a, objName, uniqueObjName, parentObjectName);
      }
    }

    endFunction(stream);
  }

  if (ParentObjects.count() > 0)
  {
    for (int i = 0; i < ParentObjects.count(); i++)
    {
      PlacementObject parentObj = ParentObjects.at(i);
      QString parentUniqueName = ParentUniqueNames.at(i);

      writeProcess(stream, parentUniqueName, &parentObj);
    }
  }
}

void PlacementPluginUnityCS::writeNull(QTextStream &stream, PlacementObject obj, QString objName, QString uniqueObjName, QString parentName, PlacementObjectType parentType)
{
  if (objName.isEmpty() == true) return;	// No data. Abort!

  // Basic GameObject Data
  Vec3D loc = (obj.Location + obj.OriginOffset);
  Quaternion rot = obj.Rotation;
  stream << "		GameObject " << uniqueObjName << " = PlaceOrUpdate(\"" << objName << "\", \"" << uniqueObjName << "\", " << (parentName.isEmpty() == true ? "null" : "parentObject") << ", ";
  stream << "new Vector3(" << QString::number(-loc.x, 'f', 6) << "f, " << QString::number(loc.y, 'f', 6) << "f, " << QString::number(loc.z, 'f', 6) << "f), ";
  stream << "new Quaternion(" << QString::number(rot.x, 'f', 6) << "f, " << QString::number(-rot.z, 'f', 6) << "f, " << QString::number(rot.y, 'f', 6) << "f, " << QString::number(rot.w, 'f', 6) << "f), ";
  stream << QString::number(obj.Scale.x, 'f', 6) << "f);\n";
}

void PlacementPluginUnityCS::writeLight(QTextStream & stream, PlacementLight obj, QString objName, QString uniqueObjName, QString parentName)
{
  if (objName.isEmpty() == true) return;	// No data. Abort!

  // Basic GameObject Data
  Vec3D loc = (obj.Location + obj.OriginOffset);
  Quaternion rot = obj.Rotation;
  stream << "		if (ImportLights == true)\n		{\n";
  stream << "			GameObject " << uniqueObjName << " = PlaceOrUpdate(\"" << objName << "\", \"" << uniqueObjName << "\", " << (parentName.isEmpty() == true ? "null" : "parentObject") << ", ";
  stream << "new Vector3(" << QString::number(-loc.x, 'f', 6) << "f, " << QString::number(loc.y, 'f', 6) << "f, " << QString::number(loc.z, 'f', 6) << "f), ";
  stream << "new Quaternion(" << QString::number(rot.x, 'f', 6) << "f, " << QString::number(-rot.z, 'f', 6) << "f, " << QString::number(rot.y, 'f', 6) << "f, " << QString::number(rot.w, 'f', 6) << "f), ";
  stream << QString::number(obj.Scale.x, 'f', 6) << "f);\n";

  // Light-Specific Data
  stream << "			SetOrUpdateLight(" << uniqueObjName << ", ";
  switch (obj.lightType)
  {
  case PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT:
    stream << "LightType.Spot, ";
    break;
  case PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT:
  default:
    stream << "LightType.Point, ";
    break;
  }
  stream << "new Color(" << QString::number(obj.lightColor.R, 'f', 6) << "f," << QString::number(obj.lightColor.G, 'f', 6) << "f," << QString::number(obj.lightColor.B, 'f', 6) << "f," << QString::number(obj.lightColor.A, 'f', 6) << "f), ";
  stream << QString::number(obj.lightIntensity * 6, 'f', 6) << "f, ";
  if (obj.useAttenuation == true)
  {
    stream << QString::number(obj.attenuationRadius * 2, 'f', 6) << "f";
  }
  else {
    stream << "0.0f";
  }
  stream << ");\n";
  stream << "		}\n";
}

void PlacementPluginUnityCS::writeCamera(QTextStream & stream, PlacementCamera obj, QString objName, QString uniqueObjName, QString parentName)
{
  if (objName.isEmpty() == true) return;	// No data. Abort!

  // Basic GameObject Data
  Vec3D loc = (obj.Location + obj.OriginOffset);
  Quaternion rot = obj.Rotation;
  stream << "		GameObject " << uniqueObjName << " = PlaceOrUpdate(\"" << objName << "\", \"" << uniqueObjName << "\", " << (parentName.isEmpty() == true ? "null" : "parentObject") << ", ";
  stream << "new Vector3(" << QString::number(-loc.x, 'f', 6) << "f, " << QString::number(loc.y, 'f', 6) << "f, " << QString::number(loc.z, 'f', 6) << "f), ";
  stream << "new Quaternion(" << QString::number(rot.x, 'f', 6) << "f, " << QString::number(-rot.z, 'f', 6) << "f, " << QString::number(rot.y, 'f', 6) << "f, " << QString::number(rot.w, 'f', 6) << "f), ";
  stream << QString::number(obj.Scale.x, 'f', 6) << "f);\n";

  // Camera-specific Data
  QString CameraCompName = QString("cameraComp%1").arg(uniqueObjName);
  stream << "		Camera " << CameraCompName << " = " << uniqueObjName << ".GetComponent<Camera>();\n";
  stream << "		if (" << CameraCompName << " == null)\n		{\n";
  stream << "		    " << CameraCompName << " = " << uniqueObjName << ".AddComponent<Camera>();\n";
  stream << "		}\n";
  stream << "		" << CameraCompName << ".fieldOfView = " << QString::number(obj.FieldOfView, 'f', 6) << "f;\n";
  stream << "		" << CameraCompName << ".aspect = " << QString::number(obj.AspectRatio, 'f', 6) << "f;\n";
}

void PlacementPluginUnityCS::writeAmbientSound(QTextStream & stream, PlacementSound obj, QString objName, QString uniqueObjName, QString parentName)
{
  // Basic GameObject Data
  Vec3D loc = (obj.Location + obj.OriginOffset);
  Quaternion rot = obj.Rotation;
  stream << "		GameObject " << uniqueObjName << " = PlaceOrUpdate(\"" << objName << "\", \"" << uniqueObjName << "\", " << (parentName.isEmpty() == true ? "null" : "parentObject") << ", ";
  stream << "new Vector3(" << QString::number(-loc.x, 'f', 6) << "f, " << QString::number(loc.y, 'f', 6) << "f, " << QString::number(loc.z, 'f', 6) << "f), ";
  stream << "new Quaternion(" << QString::number(rot.x, 'f', 6) << "f, " << QString::number(-rot.z, 'f', 6) << "f, " << QString::number(rot.y, 'f', 6) << "f, " << QString::number(rot.w, 'f', 6) << "f), ";
  stream << QString::number(obj.Scale.x, 'f', 6) << "f);\n";

  // Audio Source
  QString AudSrcName = QString("AudioSrc%1").arg(uniqueObjName);
  stream << "		AudioSource " << AudSrcName << " = " << uniqueObjName << ".GetComponent<AudioSource>();\n";
  stream << "		if (" << AudSrcName << " == null)\n		{\n";
  stream << "		    " << AudSrcName << " = " << uniqueObjName << ".AddComponent<AudioSource>();\n";
  stream << "		}\n";
  stream << "		" << AudSrcName << ".volume = " << QString::number(obj.Volume, 'f', 6) << "f;\n";
  stream << "		" << AudSrcName << ".minDistance = " << QString::number(obj.InnerRadius, 'f', 6) << "f;\n";
  stream << "		" << AudSrcName << ".maxDistance = " << QString::number(obj.OuterRadius, 'f', 6) << "f;\n";
}

bool PlacementPluginUnityCS::writeFile(QString file_name, PlacementData *data, int extensionIndex)
{
  if (data == nullptr || data == NULL)
  {
    LOGFILE.Error("Data was null. Unable to export placement.");
    return false;
  }
  QFile filedata(file_name);
  if (filedata.open(QFile::WriteOnly | QFile::Truncate) == false)
  {
    return false;
  }

  // Clear name map
  nullObjects.clear();

  QTextStream stream(&filedata);
  numFunctions = 0;

  // Need to expand this to support other data types...
  if (data->getNumObjects() <= 0 && data->getNumLights() <= 0)
  {
    LOGFILE.Error("Nothing to export. Aborting...");
    return false;
  }

  PlacementObject topObj = data->Objects->at(0);

  QString objName = topObj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  // Start the script
  QString shallowFileName = data->Objects->at(0).Name;
  QString className = QString("%1_%2").arg("WoWDataImporter").arg(uniqueObjName);
  stream << "using System.Collections;\nusing UnityEngine;\nusing UnityEditor;\n\n";
  stream << "#if UNITY_EDITOR\n\n";
  stream << "public class " << qPrintable(className) << " : ScriptableWizard\n{\n";

  stream << "	public bool ImportLights = true;\n";
  stream << "	public bool InvertXRotation = false;\n";
  stream << "	public bool InvertYRotation = false;\n";
  stream << "	public bool InvertZRotation = false;\n";
  stream << "	public float OffsetScale = 1.0f;\n\n";

  stream << "	[MenuItem(\"Project Stormwind/Import " << shallowFileName << " Placement Data...\")]\n";
  stream << "	static void " << qPrintable(className) << "Wizard()\n	{ \n		ScriptableWizard.DisplayWizard<" << qPrintable(className) << ">(\"" << qPrintable(tr("Import WoW Placement Data", "Unity Import popup title")) << "\", \"" << qPrintable(tr("Import Data", "Unity Import button label")) << "\");\n	}\n\n";

  // Place/Update function
  stream << "    // Place or Update GameObjects\n";
  stream << "    GameObject PlaceOrUpdate(string name, string uniqueName, GameObject parent, Vector3 position, Quaternion rotation, float scale)\n    {\n";

  stream << "        position.x = position.x * OffsetScale;\n";
  stream << "        position.y = position.y * OffsetScale;\n";
  stream << "        position.z = position.z * OffsetScale;\n";
  stream << "        if (InvertXRotation == true)\n        {\n";
  stream << "           rotation.x = -rotation.x;\n";
  stream << "        }\n";
  stream << "        if (InvertYRotation == true)\n        {\n";
  stream << "           rotation.y = -rotation.y;\n";
  stream << "        }\n";
  stream << "        if (InvertZRotation == true)\n        {\n";
  stream << "           rotation.z = -rotation.z;\n";
  stream << "        }\n\n";

  stream << "        if (GameObject.Find(uniqueName) != null)\n        {\n";
  stream << "            GameObject obj = GameObject.Find(uniqueName);\n            if (parent != null && GameObject.Find(parent.name) != null && obj.transform.parent == parent)\n            {\n                obj.transform.SetParent(parent.transform);\n            }\n            obj.transform.SetPositionAndRotation(position, rotation);\n            obj.transform.localScale = new Vector3(scale, scale, scale);\n            return obj;\n";
  stream << "        }\n        else\n        {\n";
  stream << "            GameObject obj = new GameObject(uniqueName);\n            if (parent != null)\n            {\n                obj.transform.SetParent(parent.transform);\n            }\n            obj.transform.SetPositionAndRotation(position, rotation);\n            obj.transform.localScale = new Vector3(scale, scale, scale);\n            return obj;\n";
  stream << "        }\n    }\n\n";

  // Light Component Update
  stream << "    // Set or Update Lights\n";
  stream << "    void SetOrUpdateLight(GameObject lightObj, LightType lType, Color lColor, float lIntensity, float lRange)\n    {\n";
  stream << "        Light lightComponent = lightObj.GetComponent<Light>();\n        if (lightComponent == null)\n        {\n            lightComponent = lightObj.AddComponent<Light>();\n        }\n";
  stream << "        lightComponent.type = lType;\n        lightComponent.color = lColor;\n        lightComponent.intensity = lIntensity;\n";
  stream << "        if (lRange > 0.0f ) lightComponent.range = lRange;\n";
  stream << "    }\n\n";

  // Write the Processors
  stream << "    // Process Functions, which contain the actual placement code...\n";
  writeProcess(stream, uniqueObjName, &topObj);

  // Build OnWizardCreate function
  stream << "	void OnWizardCreate()\n	{\n";

  writeNull(stream, topObj, objName, uniqueObjName);

  stream << "\n";

  // The functions
  stream << "		// Execute the Process Functions...\n";
  for (uint32_t i = 0; i < numFunctions; i++)
  {
    stream << "		Process" << i << "(\"" << parentNames[i] << "\");\n";
  }

  stream << "\n	}\n}";
  stream << "\n\n#endif";
  stream.flush();

  filedata.close();

  return true;
}