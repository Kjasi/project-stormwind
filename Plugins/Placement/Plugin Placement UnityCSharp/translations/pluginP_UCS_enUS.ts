<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>PlacementPluginUnityCS</name>
    <message>
        <source>Unity C# Placement Exporter</source>
        <translation>Unity C# Placement Exporter</translation>
    </message>
    <message>
        <source>Unity C# Placement Code</source>
        <translation>Unity C# Placement Code</translation>
    </message>
    <message>
        <source>Import WoW Placement Data</source>
        <comment>Unity Import popup title</comment>
        <translation>Import WoW Placement Data</translation>
    </message>
    <message>
        <source>Import Data</source>
        <comment>Unity Import button label</comment>
        <translation>Import Data</translation>
    </message>
</context>
</TS>
