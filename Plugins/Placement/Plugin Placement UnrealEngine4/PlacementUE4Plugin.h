#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtextstream.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define placementUE4_iid "org.ProjectStormwind.plugin.exporters.placement.ue4"

class PlacementPluginUE4 : public QObject, public PluginExportPlacement
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID placementUE4_iid FILE "exportPlacementUE4.json")
  Q_INTERFACES(ProjectSW::PluginExportPlacement)

public:
  PlacementPluginUE4();

  int compatibleAppVersion() const;
  bool writeFile(QString file_name, PlacementData *data, int extensionIndex);
  QTranslator *pluginTranslator(QString locale);

private:
  QString padding = "         ";

  void writeNullObject(QTextStream &stream, PlacementObject obj, QString parentName = QString());
  void writeLightObject(QTextStream &stream, PlacementLight obj, QString parentName = QString());
  void writeCameraObject(QTextStream &stream, PlacementCamera obj, QString parentName = QString());
  void writeAudioSoundObject(QTextStream &stream, PlacementSound obj, QString parentName = QString());

  void writeBasicOptions(QTextStream &stream, PlacementObject obj);
  void writeLightOptions(QTextStream &stream, PlacementLight obj);
  void writeAudioOptions(QTextStream &stream, PlacementSound obj);
  void writePosition(QTextStream &stream, PlacementObject obj);
  void writeRotation(QTextStream &stream, PlacementObject obj);
  void writeScale(QTextStream &stream, PlacementObject obj);

  bool exportForBlueprints = false;
};