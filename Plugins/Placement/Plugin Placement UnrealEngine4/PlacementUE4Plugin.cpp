#include "PlacementUE4Plugin.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
using namespace ProjectSW;

#define METERStoCENTIMETERS 100

QMap<QString, int> nullObjects;
QMap<QString, int> lightObjects;
QMap<QString, int> cameraObjects;


/*
Component listings, for blueprint pasteing

Begin Object Class=/Script/Engine.SceneComponent Name="Root2_GEN_VARIABLE"
   Mobility=Static
End Object
Begin Object Class=/Script/Engine.StaticMeshComponent Name="StaticMesh_GEN_VARIABLE"
   BodyInstance=(ObjectType=ECC_WorldStatic,CollisionProfileName="BlockAll")
   AttachParent=SceneComponent'"Root2_GEN_VARIABLE"'
   Mobility=Static
End Object
Begin Object Class=/Script/Engine.SceneComponent Name="Scene_GEN_VARIABLE"
   AttachParent=StaticMeshComponent'"StaticMesh_GEN_VARIABLE"'
   RelativeLocation=(X=-1890.000000,Y=5.000000,Z=330.000000)
   RelativeScale3D=(X=0.800000,Y=0.800000,Z=0.800000)
   Mobility=Static
End Object
Begin Object Class=/Script/Engine.StaticMeshComponent Name="Scaled offset mesh_GEN_VARIABLE"
   StaticMesh=StaticMesh'"/Game/WoWAssets/WORLD/GENERIC/HUMAN/PASSIVE_DOODADS/BOOKSHELVES/ABBEYSHELF01.ABBEYSHELF01"'
   BodyInstance=(ObjectType=ECC_WorldStatic,bEnableGravity=False,CollisionProfileName="BlockAll")
   AttachParent=SceneComponent'"Scene_GEN_VARIABLE"'
   RelativeLocation=(X=-175.000000,Y=0.000000,Z=500.000000)
   RelativeRotation=(Pitch=0.000000,Yaw=0.000013,Roll=0.000000)
   RelativeScale3D=(X=1.100000,Y=1.100000,Z=1.100000)
   Mobility=Static
End Object
Begin Object Class=/Script/Engine.PointLightComponent Name="PointLight_02_GEN_VARIABLE"
   SourceRadius=12.000000
   SoftSourceRadius=16.000000
   AttenuationRadius=1951.751587
   LightGuid=C817BDCD447334641D03DAB027CCFCDF
   Intensity=9623.795898
   LightColor=(B=105,G=255,R=20,A=255)
   AttachParent=SceneComponent'"Root2_GEN_VARIABLE"'
   RelativeLocation=(X=0.000000,Y=330.000000,Z=830.000000)
   RelativeRotation=(Pitch=0.000000,Yaw=-40.000080,Roll=-10.000031)
   Mobility=Static
End Object
Begin Object Class=/Script/Engine.AudioComponent Name="Audio numbah 5_GEN_VARIABLE"
   VolumeModulationMin=1.341571
   VolumeModulationMax=2.974439
   RelativeLocation=(X=820.000000,Y=0.000000,Z=240.000000)
   ComponentTags(0)="audio.ogg filename"
End Object

*/


PlacementPluginUE4::PlacementPluginUE4() : PluginExportPlacement("PlacementExporterUE4")
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("Unreal Engine 4 Placement Exporter").toStdString());
  PluginName = tr("Unreal Engine 4 Placement Exporter");
  PluginFilterName = tr("Unreal Engine 4 Placement Text");
  FileExtensions.push_back("copy");
}

int PlacementPluginUE4::compatibleAppVersion() const
{
  return PLUGINEXPORTPLACEMENTVERSION;	// TODO
}

QTranslator *PlacementPluginUE4::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginP_UE4_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginP_UE4_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

void PlacementPluginUE4::writeNullObject(QTextStream &stream, PlacementObject obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  // Open Actor
  stream << "      Begin Actor Class=/Script/Engine.Actor Name=" << uniqueObjName << " Archetype=/Script/Engine.Actor'/Script/Engine.Default__Actor'";
  if (parentName.isEmpty() == false)
  {
    stream << " ParentActor=" << parentName;
  }

  stream << "\n         Begin Object Class=/Script/Engine.SceneComponent Name=\"DefaultSceneRoot\"\n         End Object\n         Begin Object Name=\"DefaultSceneRoot\"\n";

  writePosition(stream, obj);
  writeRotation(stream, obj);
  writeScale(stream, obj);

  // Close actor
  stream << "            bVisualizeComponent=True\n            Mobility=Static\n            CreationMethod=Instance\n         End Object\n         RootComponent=SceneComponent'\"DefaultSceneRoot\"'\n         SpriteScale=";
  double spriteSize = 1.0;
  if (obj.objectType == PLACEMENTOBJECTTYPE_ADT)
  {
    spriteSize = 25.0;
  }
  else if (obj.objectType == PLACEMENTOBJECTTYPE_WMO)
  {
    spriteSize = 12.5;
  }
  stream << QString::number(spriteSize, 'f', 6) << "\n         ActorLabel=\"" << objName << "\"\n";

  writeBasicOptions(stream, obj);
  stream << "         InstanceComponents(0)=SceneComponent'\"DefaultSceneRoot\"'\n      End Actor\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginUE4::writeLightObject(QTextStream & stream, PlacementLight obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  QString arrowName = "ArrowComponent";
  QString uniqueArrowName = "ArrowComponent";

  if (obj.lightType == PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT) sanitizeNames(nullObjects, arrowName, uniqueArrowName);

  // Open Actor
  QString actorType = "/Script/Engine.PointLight";
  QString actorComponentType = "/Script/Engine.PointLightComponent";
  QString archType = "/Script/Engine.PointLight'/Script/Engine.Default__PointLight";
  QString archComponentType = "PointLightComponent'/Script/Engine.Default__PointLight";

  switch (obj.lightType)
  {
  case PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT:
    actorType = "/Script/Engine.SpotLight";
    archType = "/Script/Engine.SpotLight'/Script/Engine.Default__SpotLight";
    archComponentType = "SpotLightComponent'/Script/Engine.Default__SpotLight";
    break;
  case PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT:
  default:
    break;
  }

  stream << "      Begin Actor Class=" << actorType << " Name=" << uniqueObjName << " Archetype=" << archType << "'";
  if (parentName.isEmpty() == false)
  {
    stream << " ParentActor=" << parentName;
  }
  stream << "\n";
  if (obj.lightType == PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT)
  {
    stream << "         Begin Object Class=/Script/Engine.ArrowComponent Name=\"ArrowComponent0\" Archetype=ArrowComponent'/Script/Engine.Default__SpotLight:ArrowComponent0'\n         End Object\n";
  }
  stream << "         Begin Object Class=" << actorComponentType << " Name=\"LightComponent0\" Archetype=" << archComponentType << ":LightComponent0'\n         End Object\n";
  if (obj.lightType == PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT)
  {
    stream << "         Begin Object Name=\"ArrowComponent0\"\n            ArrowColor=(B=" << (int)(obj.lightColor.B*255.0) << ",G=" << (int)(obj.lightColor.G*255.0) << ",R=" << (int)(obj.lightColor.R*255.0) << ",A=" << (int)(obj.lightColor.A*255.0) << ")\n            AttachParent=\"LightComponent0\"\n         End Object\n";
  }
  stream << "         Begin Object Name=\"LightComponent0\"\n";

  // Unique Lighting Options

  switch (obj.lightType)
  {
  case PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT:
    //            InnerConeAngle=25.076193
    //            OuterConeAngle=45.390480

    break;
  case PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT:
  default:
    break;
  }

  Vec3D loc = (obj.Location + obj.OriginOffset) * METERStoCENTIMETERS;	// UE4 space is in Cenimeters.

  stream << "            SourceRadius=" << obj.sourceRadius * 1000 << "\n";
  stream << "            AttenuationRadius=" << obj.attenuationRadius * 1000 << "\n";
  stream << "            IntensityUnits=Candelas\n";
  stream << "            Intensity=" << obj.lightIntensity * 100 << "\n";
  stream << "            LightColor=(B=" << (int)(obj.lightColor.B*255.0) << ",G=" << (int)(obj.lightColor.G*255.0) << ",R=" << (int)(obj.lightColor.R*255.0) << ",A=" << (int)(obj.lightColor.A*255.0) << ")\n";
  writePosition(stream, obj);
  writeRotation(stream, obj);
  stream << "            Mobility=Movable\n            CastTranslucentShadows=false\n            bTransmission=True\n";
  stream << "         End Object\n";

  switch (obj.lightType)
  {
  case PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT:
    stream << "         SpotLightComponent=\"LightComponent0\"\n         ArrowComponent=\"ArrowComponent0\"\n";
    break;
  case PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT:
  default:
    stream << "         PointLightComponent=\"LightComponent0\"\n";
    break;
  }

  // Close actor
  double spriteSize = 5.0;
  stream << "         LightComponent=\"LightComponent0\"\n         RootComponent=\"LightComponent0\"\n         SpriteScale=" << QString::number(spriteSize, 'f', 6) << "\n         ActorLabel=\"" << objName << "\"\n         bIsEditorPreviewActor=False\n      End Actor\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginUE4::writeCameraObject(QTextStream & stream, PlacementCamera obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);


  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginUE4::writeAudioSoundObject(QTextStream & stream, PlacementSound obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  // Open Actor
  stream << "      Begin Actor Class=/Script/Engine.AmbientSound Name=" << uniqueObjName << " Archetype=/Script/Engine.AmbientSound'/Script/Engine.Default__AmbientSound'";
  if (parentName.isEmpty() == false)
  {
    stream << " ParentActor=" << parentName;
  }
  stream << "\n         Begin Object Class=/Script/Engine.AudioComponent Name=\"AudioComponent0\" Archetype=AudioComponent'/Script/Engine.Default__AmbientSound:AudioComponent0'\n         End Object\n         Begin Object Name=\"AudioComponent0\"\n";
  stream << "            bOverrideAttenuation=True\n            VolumeMultiplier=" << QString::number(obj.Volume, 'f', 6) << "\n";
  double falloff = obj.OuterRadius - obj.InnerRadius;

  stream << "            AttenuationOverrides=(AttenuationShapeExtents=(X=" << QString::number(obj.InnerRadius, 'f', 6) << ",Y=0.000000,Z=0.000000),FalloffDistance=" << QString::number(falloff, 'f', 6) << ")\n";
  writePosition(stream, obj);
  stream << "         End Object\n         AudioComponent=\"AudioComponent0\"\n         RootComponent=\"AudioComponent0\"\n         SpriteScale=2.500000\n         ActorLabel=\"" << objName << "\"\n";

  for (int i = 0; i < obj.InfoList.count(); i++)
  {
    stream << "         Tags(" << i << ")=\"" << qPrintable(obj.InfoList.at(i)) << "\"\n";
  }
  stream << "         bIsEditorPreviewActor=False\n      End Actor\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

bool PlacementPluginUE4::writeFile(QString file_name, PlacementData *data, int extensionIndex)
{
  if (data == nullptr || data == NULL)
  {
    LOGFILE.Error("Data was null. Unable to export placement.");
    return false;
  }
  QFile filedata(file_name);
  if (filedata.open(QFile::WriteOnly | QFile::Truncate) == false)
  {
    return false;
  }

  if (data->getNumObjects() <= 0 && data->getNumLights() <= 0)
  {
    LOGFILE.Error("Nothing to export. Aborting...");
    return false;
  }

  // Clear name map
  nullObjects.clear();

  QTextStream stream(&filedata);

  /*
  Can't seem to comment the document... Might write a "HowTo.txt" file...

  stream << "// " << tr("Project Stormwind Unreal Engine 4 Placement Export", "Description written at the top of every file") << "\n";
#if defined(Q_OS_MACOS)
  stream << "// " << tr("To use this file, simply copy everything in this file, (command+a, command+c) open Unreal Engine 4, and paste (command+v) into the level's viewport.", "Mac instructions on how to import this data into Unreal Engine 4") << "\n\n";
#else
  // Assume we're running on a Windows/Linux enviroment.
  stream << "// " << tr("To use this file, simply copy everything in this file, (ctrl+a, ctrl+c) open Unreal Engine 4, and paste (ctrl+v) into the level's viewport.", "PC instructions on how to import this data into Unreal Engine 4") << "\n\n";
#endif
  */

  // Start the level
  stream << "Begin Map\n   Begin Level\n";

  // Nulls
  for (uint32_t i = 0; i < data->getNumObjects(); i++)
  {
    PlacementObject a = data->Objects->at(i);
    writeNullObject(stream, a);
  }

  // Lights
  for (uint32_t i = 0; i < data->getNumLights(); i++)
  {
    PlacementLight a = data->Lights->at(i);
    writeLightObject(stream, a);
  }

  // Cameras
  for (uint32_t i = 0; i < data->getNumCameras(); i++)
  {
    PlacementCamera a = data->Cameras->at(i);
    writeCameraObject(stream, a);
  }

  // Sounds
  for (uint32_t i = 0; i < data->getNumSounds(); i++)
  {
    PlacementSound a = data->Sounds->at(i);
    writeAudioSoundObject(stream, a);
  }

  // End the level
  stream << "   End Level\nBegin Surface\nEnd Surface\nEnd Map";
  stream.flush();

  filedata.close();

  return true;
}

void PlacementPluginUE4::writeBasicOptions(QTextStream & stream, PlacementObject obj)
{
  // Info
  for (int i = 0; i < obj.InfoList.count(); i++)
  {
    if (exportForBlueprints == true)
    {
      stream << qPrintable(padding) << "   ComponentTags(" << i << ")=\"" << qPrintable(obj.InfoList.at(i)) << "\"\n";
    }
    else {
      stream << qPrintable(padding) << "   Tags(" << i << ")=\"" << qPrintable(obj.InfoList.at(i)) << "\"\n";
    }
  }
}

void PlacementPluginUE4::writeLightOptions(QTextStream & stream, PlacementLight obj)
{
}

void PlacementPluginUE4::writeAudioOptions(QTextStream & stream, PlacementSound obj)
{
}

void PlacementPluginUE4::writePosition(QTextStream & stream, PlacementObject obj)
{
  Vec3D loc = (obj.Location + obj.OriginOffset) * METERStoCENTIMETERS;	// UE4 space is in Cenimeters.

  // Don't write if no data to write!
  if (loc == 0.0) return;

  stream << qPrintable(padding) << "   RelativeLocation=(X=" << QString::number(loc.x, 'f', 6) << ",Y=" << QString::number(loc.z, 'f', 6) << ",Z=" << QString::number(loc.y, 'f', 6) << ")\n";	// Position is Z-Up!
}

void PlacementPluginUE4::writeRotation(QTextStream & stream, PlacementObject obj)
{
  Vec3D rot = obj.Rotation3D;

  // Don't write if no data to write!
  if (rot == 0.0) return;

  if (obj.objectType == PlacementObjectType::PLACEMENTOBJECTTYPE_WMO)
  {
    rot.y = -rot.y;
    rot.y += 90.0f;
  }

  stream << qPrintable(padding) << "   RelativeRotation=(Pitch=" << QString::number(-rot.x, 'f', 6) << ",Yaw=" << QString::number(rot.y, 'f', 6) << ",Roll=" << QString::number(rot.z, 'f', 6) << ")\n";
}

void PlacementPluginUE4::writeScale(QTextStream & stream, PlacementObject obj)
{
  // Don't write if no data to write!
  if (obj.Scale.x == 1.0) return;

  stream << qPrintable(padding) << "   RelativeScale3D=(X=" << QString::number(obj.Scale.x, 'f', 6) << ",Y=" << QString::number(obj.Scale.z, 'f', 6) << ",Z=" << QString::number(obj.Scale.y, 'f', 6) << ")\n";
}