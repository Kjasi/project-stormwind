<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>PlacementPluginUE4</name>
    <message>
        <location filename="../PlacementUE4Plugin.cpp" line="65"/>
        <location filename="../PlacementUE4Plugin.cpp" line="66"/>
        <source>Unreal Engine 4 Placement Exporter</source>
        <translation>Unreal Engine 4 Placement Exporter</translation>
    </message>
    <message>
        <location filename="../PlacementUE4Plugin.cpp" line="67"/>
        <source>Unreal Engine 4 Placement Text</source>
        <translation>Unreal Engine 4 Placement Text</translation>
    </message>
</context>
</TS>
