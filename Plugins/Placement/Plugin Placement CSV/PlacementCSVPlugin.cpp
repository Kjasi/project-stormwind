#include "PlacementCSVPlugin.h"
#include <ProjectStormwind/Defines.h>
#include <QtCore/qfile.h>
#include <QtCore/qdir.h>
using namespace ProjectSW;

#define METERStoCENTIMETERS 100

QMap<QString, int> nullObjects;
QMap<QString, int> lightObjects;
QMap<QString, int> cameraObjects;

PlacementPluginCSV::PlacementPluginCSV() : PluginExportPlacement("PlacementExporterCSV")
{
  LOGFILE.setFile(LOG_FILENAME);
  setName(tr("Comma Spaced Value Placement Exporter").toStdString());
  PluginName = tr("Comma Spaced Value Placement Exporter");
  PluginFilterName = tr("Comma Spaced Value Placement Text");
  FileExtensions.push_back("csv");
}

int PlacementPluginCSV::compatibleAppVersion() const
{
  return PLUGINEXPORTPLACEMENTVERSION;	// TODO
}

QTranslator *PlacementPluginCSV::pluginTranslator(QString locale)
{
  if (QFile(":/translations/pluginP_CSV_" + locale + ".qm").exists() == false)
  {
    return nullptr;
  }
  QTranslator *translator = new QTranslator(this);

  // you may need to change "this" to something else above
  bool loaded = translator->load(":/translations/pluginP_CSV_" + locale + ".qm");
  if (loaded == false)
  {
    return nullptr;
  }
  return translator;
}

void PlacementPluginCSV::writeNullObject(QTextStream &stream, PlacementObject obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  stream << uniqueObjName << "," << objName << ",Object";
  writePosition(stream, obj);
  writeRotation(stream, obj);
  writeScale(stream, obj);
  stream << "," << parentName;

  stream << "\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginCSV::writeLightObject(QTextStream & stream, PlacementLight obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  stream << uniqueObjName << "," << objName << ",Light";
  writePosition(stream, obj);
  writeRotation(stream, obj);
  writeScale(stream, obj);
  stream << "," << parentName;

  stream << ",";
  switch (obj.lightType)
  {
  case PlacementLightType::PLACEMENTLIGHTTYPE_SPOTLIGHT:
    stream << "spot";
    break;
  case PlacementLightType::PLACEMENTLIGHTTYPE_POINTLIGHT:
  default:
    stream << "point";
    break;
  }

  stream << "," << (int)(obj.lightColor.R*255.0) << "," << (int)(obj.lightColor.G*255.0) << "," << (int)(obj.lightColor.B*255.0) << "," << (int)(obj.lightColor.A*255.0);
  stream << "," << obj.lightIntensity;
  stream << "," << obj.sourceRadius;
  stream << "," << obj.attenuationRadius;

  stream << "\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginCSV::writeCameraObject(QTextStream & stream, PlacementCamera obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  stream << uniqueObjName << "," << objName << ",Camera";
  writePosition(stream, obj);
  writeRotation(stream, obj);
  writeScale(stream, obj);
  stream << "," << parentName;

  // Skip Light Options
  stream << ",,,,,,,,";

  stream << "," << obj.AspectRatio << "," << obj.FieldOfView;

  stream << "\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

void PlacementPluginCSV::writeAudioSoundObject(QTextStream & stream, PlacementSound obj, QString parentName)
{
  // Sanitize!
  QString objName = obj.Name;
  QString uniqueObjName;
  sanitizeNames(nullObjects, objName, uniqueObjName);

  stream << uniqueObjName << "," << objName << ",Camera";
  writePosition(stream, obj);
  writeRotation(stream, obj);
  writeScale(stream, obj);
  stream << "," << parentName;

  // skip Light and Camera values
  stream << ",,,,,,,,,,";

  stream << "," << obj.Volume << "," << obj.InnerRadius << "," << obj.OuterRadius;

  stream << "\n";

  for (uint32_t i = 0; i < obj.getNumChildren(); i++)
  {
    PlacementObject a = obj.children.at(i);
    writeNullObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenLights(); i++)
  {
    PlacementLight a = obj.childrenLights.at(i);
    writeLightObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenCameras(); i++)
  {
    PlacementCamera a = obj.childrenCameras.at(i);
    writeCameraObject(stream, a, uniqueObjName);
  }
  for (uint32_t i = 0; i < obj.getNumChildrenSounds(); i++)
  {
    PlacementSound a = obj.childrenSounds.at(i);
    writeAudioSoundObject(stream, a, uniqueObjName);
  }
}

bool PlacementPluginCSV::writeFile(QString file_name, PlacementData *data, int extensionIndex)
{
  if (data == nullptr || data == NULL)
  {
    LOGFILE.Error("Data was null. Unable to export placement.");
    return false;
  }
  QFile filedata(file_name);
  if (filedata.open(QFile::WriteOnly | QFile::Truncate) == false)
  {
    return false;
  }

  if (data->getNumObjects() <= 0 && data->getNumLights() <= 0)
  {
    LOGFILE.Error("Nothing to export. Aborting...");
    return false;
  }

  // Clear name map
  nullObjects.clear();

  QTextStream stream(&filedata);

  stream << "UniqueID,Filename,ObjectType,PositionX,PositionY,PositionZ,RotationPitch,RotationYaw,RotationRoll,RotationQuaternionX,RotationQuaternionY,RotationQuaternionZ,RotationQuaternionW,Scale,ParentUniqueID,LightType,LightColorR,LightColorG,LightColorB,LightColorA,LightIntensity,LightSourceRadius,LightAttenuationRadius,CameraAspectRatio,CameraFieldOfView,SoundVolume,SoundInnerRadius,SoundOuterRadius\n";

  // Nulls
  for (uint32_t i = 0; i < data->getNumObjects(); i++)
  {
    PlacementObject a = data->Objects->at(i);
    writeNullObject(stream, a);
  }

  // Lights
  for (uint32_t i = 0; i < data->getNumLights(); i++)
  {
    PlacementLight a = data->Lights->at(i);
    writeLightObject(stream, a);
  }

  // Cameras
  for (uint32_t i = 0; i < data->getNumCameras(); i++)
  {
    PlacementCamera a = data->Cameras->at(i);
    writeCameraObject(stream, a);
  }

  // Sounds
  for (uint32_t i = 0; i < data->getNumSounds(); i++)
  {
    PlacementSound a = data->Sounds->at(i);
    writeAudioSoundObject(stream, a);
  }

  // End the level
  stream.flush();

  filedata.close();

  return true;
}

void PlacementPluginCSV::writePosition(QTextStream & stream, PlacementObject obj)
{
  Vec3D loc = (obj.Location + obj.OriginOffset);

  stream << "," << QString::number(loc.x, 'f', 6) << "," << QString::number(loc.z, 'f', 6) << "," << QString::number(loc.y, 'f', 6);	// Position is Z-Up!
}

void PlacementPluginCSV::writeRotation(QTextStream & stream, PlacementObject obj)
{
  Vec3D rot = obj.Rotation3D;

  stream << "," << QString::number(rot.x, 'f', 6) << "," << QString::number(rot.y, 'f', 6) << "," << QString::number(rot.z, 'f', 6);
  stream << "," << QString::number(obj.Rotation.x, 'f', 6) << "," << QString::number(obj.Rotation.y, 'f', 6) << "," << QString::number(obj.Rotation.z, 'f', 6) << "," << QString::number(obj.Rotation.w, 'f', 6);
}

void PlacementPluginCSV::writeScale(QTextStream & stream, PlacementObject obj)
{
  stream << "," << QString::number(obj.Scale.x, 'f', 6);
}