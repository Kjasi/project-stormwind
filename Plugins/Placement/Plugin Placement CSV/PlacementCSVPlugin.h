#pragma once
#include <QtCore/qobject.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtextstream.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <Foundation/LogFile.h>
using namespace ProjectSW;

#define placementCSV_iid "org.ProjectStormwind.plugin.exporters.placement.csv"

class PlacementPluginCSV : public QObject, public PluginExportPlacement
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID placementCSV_iid FILE "exportPlacementCSV.json")
  Q_INTERFACES(ProjectSW::PluginExportPlacement)

public:
  PlacementPluginCSV();

  int compatibleAppVersion() const;
  bool writeFile(QString file_name, PlacementData *data, int extensionIndex);
  QTranslator *pluginTranslator(QString locale);

private:

  void writeNullObject(QTextStream &stream, PlacementObject obj, QString parentName = QString());
  void writeLightObject(QTextStream &stream, PlacementLight obj, QString parentName = QString());
  void writeCameraObject(QTextStream &stream, PlacementCamera obj, QString parentName = QString());
  void writeAudioSoundObject(QTextStream &stream, PlacementSound obj, QString parentName = QString());

  void writePosition(QTextStream &stream, PlacementObject obj);
  void writeRotation(QTextStream &stream, PlacementObject obj);
  void writeScale(QTextStream &stream, PlacementObject obj);

  bool exportForBlueprints = false;
};