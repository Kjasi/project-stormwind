#include "functions.h"
#include "LogFile.h"
#include <qfileinfo.h>

#ifdef WIN32
#include "Windows.h"
#endif

QString ReadNextString(QByteArray *byteArray)
{
  size_t bytes = 0;
  int strn = 0;
  int c;
  char str[200];
  char cc;
  do {
    //LOGFILE.Write("Getting Char...");
    c = byteArray->at((int)(bytes));
    cc = (char)c;
    //LOGFILE.Write("Char Result: %c",cc);
    str[strn++] = cc;
    //LOGFILE.Write("Current Str: %s",str);
    bytes++;
  } while (cc != '\0');
  QString str2 = str;
  return str2;
}

void ReadStringsIntoList(QStringList* StringList, FILE * file, size_t size)
{
  size_t bytes = 0;
  while (bytes < size) {
    int strn = 0;
    std::string str2;
    int c;
    char str[200];
    char cc;
    do {
      //LOGFILE.Write("Getting Char...");
      c = fgetc(file);
      cc = (char)c;
      //LOGFILE.Write("Char Result: %c",cc);
      str[strn++] = cc;
      //LOGFILE.Write("Current Str: %s",str);
      bytes++;
    } while (cc != '\0');
    str2 = str;
    //LOGFILE.Write("Final String: \"%s\"\nString 2: \"%s\"",str,str2.data());
    if (str2 != "") {
      StringList->push_back(str2.c_str());
    }
  }

  bool displayStringList = true;
  if (displayStringList == true)
  {
    LOGFILE.Debug("Checking StringList...");
    for (int x = 0; x<StringList->size(); x++) {
      LOGFILE.Debug("StringList[%i] = \"%s\"", x, qPrintable(StringList->at(x)));
    }
  }

  //LOGFILE.Write("Finished Reading Strings into Array...");
}

void ReadStringsIntoList(QStringList* StringList, QByteArray *byteArray, size_t offset, size_t size)
{
  size_t bytes = 0;
  while (bytes < size) {
    int strn = 0;
    QString str2;
    int c;
    char str[200];
    char cc;
    do {
      //LOGFILE.Write("Getting Char...");
      c = byteArray->at((int)(bytes + offset));
      cc = (char)c;
      //LOGFILE.Write("Char Result: %c",cc);
      str[strn++] = cc;
      //LOGFILE.Write("Current Str: %s",str);
      bytes++;
    } while (cc != '\0');
    str2 = str;
    //LOGFILE.Write("Final String: \"%s\"\nString 2: \"%s\"",str,str2.data());
    if (str2.isEmpty() == false) {
      StringList->push_back(str2);
    }
  }

  bool displayStringList = false;
  if (displayStringList == true)
  {
    LOGFILE.Debug("Checking StringList...");
    for (int x = 0; x<StringList->size(); x++) {
      LOGFILE.Debug("StringList[%i] = \"%s\"", x, qPrintable(StringList->at(x)));
    }
  }

  //LOGFILE.Write("Finished Reading Strings into Array...");
}

double getCurrentTime() {
  SYSTEMTIME st;
  memset(&st, 0, sizeof(SYSTEMTIME));
  double ourTime = 0;
  GetLocalTime(&st);
  ourTime += (st.wDay * 86400) + (st.wHour * 3600) + (st.wMinute * 60) + st.wSecond + (double)(st.wMilliseconds / 1000.0f);
  return ourTime;
}

QString getJustFilesName(QString originalFilename)
{
  QString filename = unifySlashes(originalFilename);
  filename.mid(originalFilename.lastIndexOf("/") + 1, originalFilename.lastIndexOf(".") - (originalFilename.lastIndexOf("/") + 1));
  return filename;
}

QString unifySlashes(QString originalFilename)
{
  return originalFilename.replace("\\", "/");
}

bool validateFile(QString filename, QString logtypename)
{
  //LOGFILE.Info("Validating file...");
  if (filename.isNull() || filename.isEmpty())
  {
    LOGFILE.Error("Unable to read %s file: No filename specified.", qPrintable(logtypename));
    return false;
  }

  QFileInfo fi(filename);
  if (fi.exists() == false)
  {
    LOGFILE.Error("Unable to read %s file: The file could not be found.", qPrintable(logtypename));
    return false;
  }
  if (fi.isFile() == false)
  {
    LOGFILE.Error("Unable to read %s file: Specified filename is not a file.", qPrintable(logtypename));
    return false;
  }
  if (fi.isReadable() == false)
  {
    LOGFILE.Error("Unable to read %s file: The file is not readable.", qPrintable(logtypename));
    return false;
  }
  if (fi.size() <= 0)
  {
    LOGFILE.Error("Unable to read %s file: The file has a size of 0.", qPrintable(logtypename));
    return false;
  }

  //LOGFILE.Debug("%s file is a valid file.", qPrintable(logtypename));
  return true;
}