#pragma once

enum SaveFileErrorType
{
  SIET_None = 0,
  SEIT_BadFilter,
  SEIT_BadFilename,
  SEIT_NoPlugins,
  SEIT_NoPluginsOfType,
  SEIT_PluginNotFound,
  SEIT_PluginError,
  SEIT_NoData,
};

enum ADTResolution {
  ADTRES_025 = 0,
  ADTRES_05,
  ADTRES_ORIGINAL,
  ADTRES_2X,
  ADTRES_4X,
  ADTRES_CRYLUM_256,
  ADTRES_CRYLUM_512,
  ADTRES_CRYLUM_1024,
  ADTRES_CRYLUM_2048,
  ADTRES_CRYLUM_4096,
  ADTRES_UE4_253,
  ADTRES_UE4_505,
  ADTRES_UE4_1009,
  ADTRES_UE4_2017,
  ADTRES_UE4_4033,
  ADTRES_UNITY_257,
  ADTRES_UNITY_513,
  ADTRES_UNITY_1025,
  ADTRES_UNITY_2049,
  ADTRES_UNITY_4097,

  ADTRES_MAX
};



enum SourceFileType {
  SOURCE_ADT = 0,
  SOURCE_WMO,
  SOURCE_M2,
  SOURCE_BLP,
  SOURCE_WDL,

  SOURCE_NONE
};

extern ADTResolution ADTOutputResolution;