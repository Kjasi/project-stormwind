#pragma once
#include <Foundation/Classes/Vectors.h>

struct MeshPositioning
{
  uint32_t MeshID;
  Vec3D Position;
  Vec3D Rotation;
  Vec3D Scale;
};