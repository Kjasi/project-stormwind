#pragma once
#include "glm/glm.hpp"

struct UniformBufferObject {
  glm::mat4 Projection;
  glm::mat4 modelView;
  glm::vec4 viewPos;
  glm::vec3 LightPosition = glm::vec3(0.0f, 2.0f, 1.0f);
};

struct UniformBufferObjectFrag
{
  int ColorMode = 0;
  float AlphaClip = 0.333333333f;
};