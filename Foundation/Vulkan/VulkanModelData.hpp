#pragma once
#include <Foundation/Classes/Vertex.h>
#include <Foundation/Classes/ImageData.h>
#include <Foundation/LogFile.h>
#include <qvector.h>

struct VulkanMaterialProperties
{
  glm::vec4 ambientColor = glm::vec4(0.25f);
  glm::vec4 diffuseColor = glm::vec4(1.0f);
  glm::vec4 specularColor = glm::vec4(1.0f);
  float opacity = 1.0f;
};

struct MaterialData : public VulkanMaterialProperties
{
  QString name = "UnknownMaterial";
  int32_t textureGroupID = -1;
  bool alphaAsSpecular = false;

  bool operator ==(const MaterialData& second) const
  {
    return (name == second.name) && (textureGroupID == second.textureGroupID) && (alphaAsSpecular == second.alphaAsSpecular);
  }
  bool operator ==(const MaterialData* second) const
  {
    return (name == second->name) && (textureGroupID == second->textureGroupID) && (alphaAsSpecular == second->alphaAsSpecular);
  }
};

struct PartData
{
  QString partName = "UnknownPart";
  QVector<uint64_t> indices;
  uint64_t baseOffset = 0;

  uint32_t materialID = 0;
};

struct TextureGroup
{
  ImageData diffuse;
  ImageData specular;

  TextureGroup() : diffuse(ImageData::generateWhiteTexture(0, 0)), specular(ImageData::generateBlackTexture(0, 0)) {};

  // Only check the filenames. The rest can change, even if it's the same image loaded...
  bool operator ==(const TextureGroup& second) const
  {
    return (diffuse.sourceFilename == second.diffuse.sourceFilename) && (specular.sourceFilename == second.specular.sourceFilename);
  }
  // Only check the filenames. The rest can change, even if it's the same image loaded...
  bool operator ==(const TextureGroup* second) const
  {
    return (diffuse.sourceFilename == second->diffuse.sourceFilename) && (specular.sourceFilename == second->specular.sourceFilename);
  }
};

struct VulkanModelData
{
  QVector<Vertex> Verticies;
  QList<PartData> meshData = QList<PartData>();
  QList<TextureGroup*> *textureList = new QList<TextureGroup*>();
  QList<MaterialData> materials = QList<MaterialData>();
  QString Name = "UnknownModel";
};

class VulkanSceneData
{
public:
  QList<VulkanModelData> modelData = QList<VulkanModelData>();
  QList<TextureGroup*> *sharedTextures = new QList<TextureGroup*>();
  QList<MaterialData> sharedMaterials = QList<MaterialData>();

  float zoom = -3.5f;
  glm::vec3 rotation = glm::vec3();
  glm::vec3 cameraPos = glm::vec3();
  float zoomScale = 1.0f;
  float moveScale = 1.0f;
  float rotateScale = 1.0f;

  void addModel(VulkanModelData model)
  {
    LOGFILE.Info("Adding Model to Vulkan Scene...");
    //if (modelData.count() <= 0)
    //{
    //	modelData.push_back(model);
    //	return;
    //}

    QList<MaterialData> matList = model.materials;

    // More than one model exists in this scene!
    // Check for and manage shared textures
    LOGFILE.Debug("Number of Textures in model %s: %i", qPrintable(model.Name), model.textureList->count());
    for (uint32_t i = 0; i < (uint32_t)model.textureList->count(); i++)
    {
      // If existing shared texture exists...
      LOGFILE.Debug("Shared Texture Count: %i", sharedTextures->count());
      if (sharedTextures->count() > 0 && sharedTextures->contains(model.textureList->at(i)))
      {
        LOGFILE.Debug("Matching Shared Texture found!");
        setModelTextureID(&model, i, sharedTextures->lastIndexOf(model.textureList->at(i)));
        model.textureList->takeAt(i);
      }
      else {
        // Check to see if any other models share this texture...
        for (uint32_t m = 0; m < (uint32_t)modelData.count(); m++)
        {
          VulkanModelData *md = &modelData.value(m);
          QList<TextureGroup*> *mList = md->textureList;
          if (mList->contains(model.textureList->at(i)))
          {
            LOGFILE.Debug("Matching Texture found! Moving to shared...");
            // Found another model with this texture! Move to Shared, and set both data!
            uint32_t c = sharedTextures->count();
            sharedTextures->push_back(model.textureList->at(i));
            setModelTextureID(md, mList->lastIndexOf(model.textureList->at(i)), c);
            setModelTextureID(&model, i, c);

            md->textureList->takeAt(mList->lastIndexOf(model.textureList->at(i)));
            model.textureList->takeAt(i);
          }
        }
      }
    }

    // Check for and manage shared textures
    for (uint32_t i = 0; i < (uint32_t)model.materials.count(); i++)
    {
      // If existing shared material exists...
      if (sharedMaterials.count() > 0 && sharedMaterials.contains(matList.at(i)))
      {
        setModelMaterialID(&model, i, sharedMaterials.lastIndexOf(matList.at(i)));
        model.materials.takeAt(i);
      }
      else {
        // Check to see if any other models share this texture...
        for (uint32_t m = 0; m < (uint32_t)modelData.count(); m++)
        {
          VulkanModelData *md = &modelData.value(m);
          QList<MaterialData> mList = md->materials;
          if (mList.contains(matList.at(i)))
          {
            // Found another model with this texture! Move to Shared, and set both data!
            uint32_t c = sharedMaterials.count();
            sharedMaterials.push_back(matList.at(i));
            setModelMaterialID(md, mList.lastIndexOf(matList.at(i)), c);
            setModelMaterialID(&model, i, c);

            md->materials.takeAt(mList.lastIndexOf(matList.at(i)));
            model.materials.takeAt(i);
          }
        }
      }
    }
    modelData.push_back(model);
  }

private:
  void setModelTextureID(VulkanModelData* model, int32_t ID, int32_t newID)
  {
    for (uint32_t i = 0; i < (uint32_t)model->materials.count(); i++)
    {
      if (model->materials.at(i).textureGroupID == ID)
      {
        model->materials[i].textureGroupID = newID;
      }
    }
  }
  void setModelMaterialID(VulkanModelData* model, int32_t ID, int32_t newID)
  {
    for (uint32_t i = 0; i < (uint32_t)model->meshData.count(); i++)
    {
      if (model->meshData.at(i).materialID == ID)
      {
        model->meshData[i].materialID = newID;
      }
    }
  }

};