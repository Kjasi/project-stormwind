#pragma once
#include <Vulkan/vulkan.hpp>
#include <glm/glm.hpp>
#include <qstring.h>
#include <qlist.h>
#include "VulkanBuffer.hpp"
#include "VulkanDevice.hpp"
#include "VulkanModelData.hpp"
#include "VulkanImage.hpp"
#include <Foundation/Vulkan/UniformBufferObject.h>

enum vulkanDisplayType
{
  DISPLAYTYPE_SOLID = 0,
  DISPLAYTYPE_TEXTURED,
  DISPLAYTYPE_TEXTURESOLID,
  DISPLAYTYPE_TEXTUREBLEND,
  DISPLAYTYPE_WIREFRAME,
  DISPLAYTYPE_WIREFRAMEONSOLID,
  DISPLAYTYPE_WIREFRAMEONTEXTURE,
  DISPLAYTYPE_WIREFRAMEONTEXTURESOLID,
  DISPLAYTYPE_WIREFRAMEONTEXTUREBLEND,
};

struct VulkanMaterial
{
  QString name;
  VulkanMaterialProperties properties;
  VulkanImage *diffuse = 0;
  VulkanImage *specular = 0;
  VkDescriptorSet descriptorSet;
  bool isBlended = false;

  VulkanMaterial()
  {
    diffuse = new VulkanImage;
    specular = new VulkanImage;
  }
};

struct VulkanScenePart
{
  uint32_t indexBase;		// Starting index for scene buffer
  uint32_t indexCount;

  VulkanMaterial *material;
};

class VulkanScene
{
public:
  std::vector<VulkanMaterial> materials;
  std::vector<VulkanScenePart> meshes;

  struct {
    vks::Buffer VS;
    vks::Buffer FG;
  } uniformBuffers;

  UniformBufferObject uboVS;
  UniformBufferObjectFrag uboFG;

  // Pipelines
  // Other pipelines can include: Double-sided, Wireframe, Shaded Wireframe, Wireframe-on-Top-of-Solid
  struct {
    VkPipeline solid = VK_NULL_HANDLE;						// Solid, single-color
    VkPipeline solidCull = VK_NULL_HANDLE;					// Solid, single-color, Culled Backface
    VkPipeline wireframe = VK_NULL_HANDLE;					// Wireframe
    VkPipeline wireframeCull = VK_NULL_HANDLE;				// Wireframe, Culled Backface
    VkPipeline textured = VK_NULL_HANDLE;					// Textured, single-sided pipeline
    VkPipeline texturedCull = VK_NULL_HANDLE;				// Textured, single-sided pipeline, Culled Backface
    VkPipeline texturedSolid = VK_NULL_HANDLE;				// Textured Solid, single-sided pipeline
    VkPipeline texturedSolidCull = VK_NULL_HANDLE;			// Textured Solid, single-sided pipeline, Culled Backface
    VkPipeline textureBlend = VK_NULL_HANDLE;				// Textured Blending, single-sided pipeline
    VkPipeline textureBlendCull = VK_NULL_HANDLE;			// Textured Blending, single-sided pipeline, Culled Backface
  } pipelines;

  // Shared pipeline layout
  VkPipelineLayout pipelineLayout;

  VulkanScene(vks::VulkanDevice *vulkanDevice, VkQueue queue);
  ~VulkanScene();

  void load(VulkanSceneData *data, VkCommandBuffer copyCommand);
  void load(VkCommandBuffer copyCommand);
  void loadData(VulkanSceneData *data) { sourceData = data; }
  void generateSquare(ImageData* texture = nullptr, float aspect = 1.0);
  void render(VkCommandBuffer cmdBuffer, vulkanDisplayType displayType, bool enableCulling = false);

private:
  vks::VulkanDevice *vulkanDevice;
  VkQueue queue;

  VkDescriptorPool descriptorPool;

  struct
  {
    VkDescriptorSetLayout material;
    VkDescriptorSetLayout scene;
  } descriptorSetLayouts;

  vks::Buffer vertexBuffer;
  vks::Buffer indexBuffer;

  VkDescriptorSet descriptorSetScene;

  const VulkanSceneData* sourceData;
  ImageData *testTexture;

  void loadMaterials();
  void loadMeshes(VkCommandBuffer copyCommand);
};