#include <Foundation/Functions.h>
#include <Foundation/LogFile.h>
#include "VulkanScene.h"
#include "Settings.h"

VulkanScene::VulkanScene(vks::VulkanDevice * vulkanDevice, VkQueue queue)
{
  this->vulkanDevice = vulkanDevice;
  this->queue = queue;

  // Prepare uniform buffer for global matrices
  VkMemoryRequirements memReqs;
  VkMemoryAllocateInfo memAlloc = vks::initializers::memoryAllocateInfo();
  VkBufferCreateInfo bufferCreateInfo = vks::initializers::bufferCreateInfo(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, sizeof(uboVS));
  VK_CHECK_RESULT(vkCreateBuffer(vulkanDevice->logicalDevice, &bufferCreateInfo, nullptr, &uniformBuffers.VS.buffer));
  vkGetBufferMemoryRequirements(vulkanDevice->logicalDevice, uniformBuffers.VS.buffer, &memReqs);
  memAlloc.allocationSize = memReqs.size;
  memAlloc.memoryTypeIndex = vulkanDevice->getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  VK_CHECK_RESULT(vkAllocateMemory(vulkanDevice->logicalDevice, &memAlloc, nullptr, &uniformBuffers.VS.memory));
  VK_CHECK_RESULT(vkBindBufferMemory(vulkanDevice->logicalDevice, uniformBuffers.VS.buffer, uniformBuffers.VS.memory, 0));
  VK_CHECK_RESULT(vkMapMemory(vulkanDevice->logicalDevice, uniformBuffers.VS.memory, 0, sizeof(uboVS), 0, (void **)&uniformBuffers.VS.mapped));
  uniformBuffers.VS.descriptor.offset = 0;
  uniformBuffers.VS.descriptor.buffer = uniformBuffers.VS.buffer;
  uniformBuffers.VS.descriptor.range = sizeof(uboVS);
  uniformBuffers.VS.device = vulkanDevice->logicalDevice;

  // Fragment Uniform Buffer
  VkBufferCreateInfo bufferCreateInfoFG = vks::initializers::bufferCreateInfo(VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, sizeof(uboFG));
  VK_CHECK_RESULT(vkCreateBuffer(vulkanDevice->logicalDevice, &bufferCreateInfoFG, nullptr, &uniformBuffers.FG.buffer));
  vkGetBufferMemoryRequirements(vulkanDevice->logicalDevice, uniformBuffers.FG.buffer, &memReqs);
  memAlloc.allocationSize = memReqs.size;
  memAlloc.memoryTypeIndex = vulkanDevice->getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
  VK_CHECK_RESULT(vkAllocateMemory(vulkanDevice->logicalDevice, &memAlloc, nullptr, &uniformBuffers.FG.memory));
  VK_CHECK_RESULT(vkBindBufferMemory(vulkanDevice->logicalDevice, uniformBuffers.FG.buffer, uniformBuffers.FG.memory, 0));
  VK_CHECK_RESULT(vkMapMemory(vulkanDevice->logicalDevice, uniformBuffers.FG.memory, 0, sizeof(uboFG), 0, (void **)&uniformBuffers.FG.mapped));
  uniformBuffers.FG.descriptor.offset = 0;
  uniformBuffers.FG.descriptor.buffer = uniformBuffers.FG.buffer;
  uniformBuffers.FG.descriptor.range = sizeof(uboFG);
  uniformBuffers.FG.device = vulkanDevice->logicalDevice;

  testTexture = new ImageData;
}

VulkanScene::~VulkanScene()
{
  vertexBuffer.destroy();
  indexBuffer.destroy();
  for (auto material : materials)
  {
    material.diffuse->destroy();
    material.specular->destroy();
  }
  vkDestroyPipelineLayout(vulkanDevice->logicalDevice, pipelineLayout, nullptr);
  vkDestroyDescriptorSetLayout(vulkanDevice->logicalDevice, descriptorSetLayouts.material, nullptr);
  vkDestroyDescriptorSetLayout(vulkanDevice->logicalDevice, descriptorSetLayouts.scene, nullptr);
  vkDestroyDescriptorPool(vulkanDevice->logicalDevice, descriptorPool, nullptr);
  if (pipelines.solid != VK_NULL_HANDLE)
  {
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.solid, nullptr);
    pipelines.solid = VK_NULL_HANDLE;
  }
  if (pipelines.solidCull != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.solidCull, nullptr);
    pipelines.solidCull = VK_NULL_HANDLE;
  }
  if (pipelines.textured != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.textured, nullptr);
    pipelines.textured = VK_NULL_HANDLE;
  }
  if (pipelines.texturedCull != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.texturedCull, nullptr);
    pipelines.texturedCull = VK_NULL_HANDLE;
  }
  if (pipelines.texturedSolid != VK_NULL_HANDLE) {
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.texturedSolid, nullptr);
    pipelines.texturedSolid = VK_NULL_HANDLE;
  }
  if (pipelines.texturedSolidCull != VK_NULL_HANDLE) {
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.texturedSolidCull, nullptr);
    pipelines.texturedSolidCull = VK_NULL_HANDLE;
  }
  if (pipelines.textureBlend != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.textureBlend, nullptr);
    pipelines.textureBlend = VK_NULL_HANDLE;
  }
  if (pipelines.textureBlendCull != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.textureBlendCull, nullptr);
    pipelines.textureBlendCull = VK_NULL_HANDLE;
  }
  if (pipelines.wireframe != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.wireframe, nullptr);
    pipelines.wireframe = VK_NULL_HANDLE;
  }
  if (pipelines.wireframeCull != VK_NULL_HANDLE){
    vkDestroyPipeline(vulkanDevice->logicalDevice, pipelines.wireframeCull, nullptr);
    pipelines.wireframeCull = VK_NULL_HANDLE;
  }
  uniformBuffers.VS.destroy();
  uniformBuffers.FG.destroy();

  delete sourceData;
  delete testTexture;
}

void VulkanScene::load(VulkanSceneData *data, VkCommandBuffer copyCommand)
{
  sourceData = data;
  load(copyCommand);
}

void VulkanScene::load(VkCommandBuffer copyCommand)
{
  double timer_startf = getCurrentTime();
  LOGFILE.Debug("Vulkan: Load Start: %f", timer_startf);
  double timer_matsf = 0;
  if (sourceData)
  {
    loadMaterials();
    timer_matsf = getCurrentTime();
    LOGFILE.Debug("Vulkan: Load Materials End: %f", timer_matsf);
    LOGFILE.Debug("Vulkan: Load All materials time in Seconds: %.3f", (timer_matsf - timer_startf));
    loadMeshes(copyCommand);
  }
  else {
    LOGFILE.Error("Unable to load Vulkan Scene Data...");
  }
  double timer_endf = getCurrentTime();
  LOGFILE.Debug("Vulkan: Load End: %f", timer_endf);
  LOGFILE.Debug("Vulkan: Model Load Time: %.3f", (timer_endf - timer_matsf));
  LOGFILE.Debug("Vulkan: Total Load Time in Seconds: %.3f", (timer_endf - timer_startf));
}

void VulkanScene::generateSquare(ImageData* texture, float aspect)
{
  VulkanSceneData *data = new VulkanSceneData;

  VulkanModelData model;
  model.Name = "The Quad";

  model.Verticies =
  {
    { { -1.0f, -1.0f / aspect,  0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0, 0.0 },{ 0.0, 0.0, 1.0 },{ 1.0f, 1.0f, 1.0f } },
    { {  1.0f, -1.0f / aspect,  0.0f },{ 1.0f, 1.0f, 1.0f },{ 1.0, 0.0 },{ 0.0, 0.0, 1.0 },{ 1.0f, 1.0f, 1.0f } },
    { {  1.0f,  1.0f / aspect,  0.0f },{ 1.0f, 1.0f, 1.0f },{ 1.0, 1.0 },{ 0.0, 0.0, 1.0 },{ 1.0f, 1.0f, 1.0f } },
    { { -1.0f,  1.0f / aspect,  0.0f },{ 1.0f, 1.0f, 1.0f },{ 0.0, 1.0 },{ 0.0, 0.0, 1.0 },{ 1.0f, 1.0f, 1.0f } },
  };

  // Setup indices
  PartData vertData;
  vertData.indices = {
    0, 1, 2, 2, 3, 0,
    2, 1, 0, 0, 3, 2,
  };

  vertData.materialID = 0;
  vertData.partName = "The Mesh";

  MaterialData mat;
  mat.name = "Test Texture";
  mat.textureGroupID = 0;

  TextureGroup *grp = new TextureGroup;
  if (texture != nullptr)
  {
    mat.name = "Texture";
    grp->diffuse = *texture;
  }
  else {
    if (testTexture->getSize() <= 0)
      testTexture->makeTestTexture();
    grp->diffuse = *testTexture;
  }

  model.meshData.push_back(vertData);
  model.materials.push_back(mat);
  model.textureList->push_back(grp);

  data->addModel(model);

  sourceData = data;
}

void VulkanScene::render(VkCommandBuffer cmdBuffer, vulkanDisplayType displayType, bool enableCulling)
{
  VkDeviceSize offsets[1] = { 0 };

  // Bind scene vertex and index buffers
  vkCmdBindVertexBuffers(cmdBuffer, 0, 1, &vertexBuffer.buffer, offsets);
  vkCmdBindIndexBuffer(cmdBuffer, indexBuffer.buffer, 0, VK_INDEX_TYPE_UINT32);

  for (size_t i = 0; i < meshes.size(); i++)
  {
    // We will be using multiple descriptor sets for rendering
    // In GLSL the selection is done via the set and binding keywords
    // VS: layout (set = 0, binding = 0) uniform UBO;
    // FS: layout (set = 1, binding = 0) uniform sampler2D samplerColorMap;

    //LOGFILE.Debug("Rendering Mesh #%i, with material \"%s\"", i, qPrintable(meshes[i].material->name));
    //LOGFILE.Debug("	Index Start: %i", meshes[i].indexBase);
    //LOGFILE.Debug("	Index Count: %i", meshes[i].indexCount);

    std::array<VkDescriptorSet, 2> descriptorSets;
    // Set 0: Scene descriptor set containing global matrices
    descriptorSets[0] = descriptorSetScene;
    // Set 1: Per-Material descriptor set containing bound images
    descriptorSets[1] = meshes[i].material->descriptorSet;

    // Pass material properies via push constants
    vkCmdPushConstants(
      cmdBuffer,
      pipelineLayout,
      VK_SHADER_STAGE_FRAGMENT_BIT,
      0,
      sizeof(VulkanMaterialProperties),
      &meshes[i].material->properties);

    vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, static_cast<uint32_t>(descriptorSets.size()), descriptorSets.data(), 0, NULL);

    switch (displayType)
    {
    case DISPLAYTYPE_SOLID:
      if (enableCulling == true) {
        //LOGFILE.Debug("Rendering Solid with Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.solidCull);
      }
      else {
        //LOGFILE.Debug("Rendering Solid without Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.solid);
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    case DISPLAYTYPE_WIREFRAME:
      if (enableCulling == true) {
        //LOGFILE.Debug("Rendering Wireframe with Culling...");
      }
      else {
        //LOGFILE.Debug("Rendering Wireframe without Culling...");
      }
      break;
    case DISPLAYTYPE_TEXTURED:
      if (enableCulling == true) {
        //LOGFILE.Debug("Rendering Textured with Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.texturedCull);
      }
      else {
        //LOGFILE.Debug("Rendering Textured without Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.textured);
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    case DISPLAYTYPE_TEXTURESOLID:
      if (enableCulling == true) {
        //LOGFILE.Debug("Rendering Textured Solid with Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.texturedSolidCull);
      }
      else {
        //LOGFILE.Debug("Rendering Textured Solid without Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.texturedSolid);
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    case DISPLAYTYPE_WIREFRAMEONSOLID:
      vkCmdSetLineWidth(cmdBuffer, 1.5f);
      if (enableCulling == true) {
        //LOGFILE.Debug("Rendering Wireframe on Solid with Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.solidCull);
      }
      else {
        //LOGFILE.Debug("Rendering Wireframe on Solid without Culling...");
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.solid);
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    case DISPLAYTYPE_WIREFRAMEONTEXTURE:
      vkCmdSetLineWidth(cmdBuffer, 1.5f);
      if (enableCulling == true) {
        if (meshes[i].material->isBlended == true)
        {
          //LOGFILE.Debug("Rendering Wireframe on Texture Blended with Culling...");
          vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.textureBlendCull);
        }
        else {
          //LOGFILE.Debug("Rendering Wireframe on Textured with Culling...");
          vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.texturedSolidCull);
        }
      }
      else {
        if (meshes[i].material->isBlended == true)
        {
          //LOGFILE.Debug("Rendering Wireframe on Texture Blended without Culling...");
          vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.textureBlend);
        }
        else {
          //LOGFILE.Debug("Rendering Wireframe on Textured without Culling...");
          vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.texturedSolid);
        }
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    default:
      break;
    }

    switch (displayType)
    {
    case DISPLAYTYPE_WIREFRAME:
    case DISPLAYTYPE_WIREFRAMEONSOLID:
    case DISPLAYTYPE_WIREFRAMEONTEXTURE:
      vkCmdSetLineWidth(cmdBuffer, 1.0f);
      if (enableCulling == true) {
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.wireframeCull);
      }
      else {
        vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.wireframe);
      }
      vkCmdDrawIndexed(cmdBuffer, meshes[i].indexCount, 1, meshes[i].indexBase, 0, 0);
      break;
    default:
      break;
    }

  }
}

void VulkanScene::loadMaterials()
{
  LOGFILE.Info("Vulkan: Scene Loading Materials...");

  double timer_startf = getCurrentTime();
  LOGFILE.Debug("Vulkan: Material Load Start: %f", timer_startf);

  size_t size = sourceData->sharedMaterials.count();
  for (uint32_t i = 0; i < (uint32_t)sourceData->modelData.count(); i++)
  {
    size += sourceData->modelData.at(i).materials.count();
  }
  materials.resize(size);
  uint32_t cMat = 0;
  // Shared Materials
  LOGFILE.Debug("Vulkan: Number of Shared Materials: %i", (uint32_t)sourceData->sharedMaterials.count());
  for (uint32_t i = 0; i < (uint32_t)sourceData->sharedMaterials.count(); i++, cMat++)
  {
    MaterialData thisMat = sourceData->sharedMaterials.at(i);

    materials[cMat].properties.ambientColor = thisMat.ambientColor;
    materials[cMat].properties.diffuseColor = thisMat.diffuseColor;
    materials[cMat].properties.specularColor = thisMat.specularColor;
    materials[cMat].properties.opacity = thisMat.opacity;
    materials[cMat].isBlended = (materials[cMat].properties.opacity == 1.0f) ? false : true;

    if ((materials[cMat].properties.opacity) > 0.0f)
      materials[cMat].properties.specularColor = glm::vec4(0.0f);

    materials[cMat].name = thisMat.name;
    LOGFILE.Debug("Vulkan: Shared Material \"%s\"", qPrintable(materials[cMat].name));

    if (sourceData->sharedTextures->count() > 0 && thisMat.textureGroupID > -1)
    {
      // Diffuse Texture
      ImageData diff = sourceData->sharedTextures->value(thisMat.textureGroupID)->diffuse;
      ImageData spec = sourceData->sharedTextures->value(thisMat.textureGroupID)->specular;

      if (SETTINGS.value("disablerendertextures", false).toBool() == false)
      {
        if (diff.isEmpty() == false)
        {
          //LOGFILE.Debug("	Shared Diffuse Texture from ImageData.");
          materials[cMat].diffuse->load(&diff, diff.getVulkanFormat(), vulkanDevice, queue);
        }
        else {
          //LOGFILE.Debug("	Diffuse Texture generated as White.");
          ImageData a = ImageData::generateWhiteTexture();
          materials[cMat].diffuse->load(&a, a.getVulkanFormat(), vulkanDevice, queue);
        }

        // Spec Texture
        if (spec.isEmpty() == false)
        {
          //LOGFILE.Debug("	Shared Specular Texture from ImageData.");
          materials[cMat].specular->load(&spec, spec.getVulkanFormat(), vulkanDevice, queue);
        }
        else {
          //LOGFILE.Debug("	Specular Texture generated as Black.");
          ImageData a = ImageData::generateBlackTexture();
          materials[cMat].specular->load(&a, a.getVulkanFormat(), vulkanDevice, queue);
        }
      }
    }
  }

  double timer_postsharedf = getCurrentTime();
  //LOGFILE.Debug("Post-Shared Materials Time: %f", timer_postsharedf);
  //LOGFILE.Debug("Seconds since Material Start: %.3f", (timer_postsharedf - timer_startf));

  // Model Materials
  LOGFILE.Debug("Vulkan: Number of Models: %i", (uint32_t)sourceData->modelData.count());
  for (uint32_t i = 0; i < (uint32_t)sourceData->modelData.count(); i++)
  {
    VulkanModelData model = sourceData->modelData.at(i);
    //LOGFILE.Debug("Number of Materials for Model: %i", (uint32_t)model.materials.count());
    for (uint32_t m = 0; m < (uint32_t)model.materials.count(); m++, cMat++)
    {
      double timer_MatStart = getCurrentTime();
      //LOGFILE.Debug("Material Start Time: %f", timer_MatStart);
      MaterialData thisMat = model.materials.at(m);

      materials[cMat].properties.ambientColor = thisMat.ambientColor;
      materials[cMat].properties.diffuseColor = thisMat.diffuseColor;
      materials[cMat].properties.specularColor = thisMat.specularColor;
      materials[cMat].properties.opacity = thisMat.opacity;
      materials[cMat].isBlended = (materials[cMat].properties.opacity == 1.0f) ? false : true;

      if ((materials[cMat].properties.opacity) > 0.0f)
        materials[cMat].properties.specularColor = glm::vec4(0.0f);

      materials[cMat].name = thisMat.name;
      //LOGFILE.Debug("Material \"%s\"", qPrintable(materials[cMat].name));

      double timer_MatSetup = getCurrentTime();
      //LOGFILE.Debug("Material Setup Time: %f", timer_MatSetup);
      //LOGFILE.Debug("Time Since Mat Start: %f", timer_MatSetup - timer_MatStart);

      if (thisMat.textureGroupID < 0)
      {
        LOGFILE.Warn("No Texture Group ID found. Skipping...");
        continue;
      }

      if (model.textureList->count() > 0)
      {
        // Diffuse Texture
        ImageData diff = model.textureList->value(thisMat.textureGroupID)->diffuse;
        ImageData spec = model.textureList->value(thisMat.textureGroupID)->specular;

        if (diff.isEmpty() == false)
        {
          //LOGFILE.Debug("	Diffuse Texture from ImageData.");
          materials[cMat].diffuse->load(&diff, diff.getVulkanFormat(), vulkanDevice, queue);
        }
        else {
          //LOGFILE.Debug("	Diffuse Texture generated as White.");
          ImageData a = ImageData::generateWhiteTexture();
          materials[cMat].diffuse->load(&a, a.getVulkanFormat(), vulkanDevice, queue);
        }

        // Spec Texture
        if (spec.isEmpty() == false)
        {
          //LOGFILE.Debug("	Specular Texture from ImageData.");
          materials[cMat].specular->load(&spec, spec.getVulkanFormat(), vulkanDevice, queue);
        }
        else {
          //LOGFILE.Debug("	Specular Texture generated as Black.");
          ImageData a = ImageData::generateBlackTexture();
          materials[cMat].specular->load(&a, a.getVulkanFormat(), vulkanDevice, queue);
        }
      }
      double timer_MatEnd = getCurrentTime();
      //LOGFILE.Debug("Material End Time: %f", timer_MatEnd);
      //LOGFILE.Debug("Time Since Mat Setup: %f", timer_MatEnd - timer_MatSetup);
      //LOGFILE.Debug("Time Since Mat Start: %f", timer_MatEnd - timer_MatStart);
    }
  }

  double timer_prevulkanf = getCurrentTime();
  //LOGFILE.Debug("PreVulkan Time: %f", timer_prevulkanf);
  //LOGFILE.Debug("Seconds since Shared Materials: %.3f", (timer_prevulkanf - timer_postsharedf));
  //LOGFILE.Debug("Seconds since Material Start:   %.3f", (timer_prevulkanf - timer_startf));

  // Generate descriptor sets for the materials

  // Descriptor pool
  std::vector<VkDescriptorPoolSize> poolSizes;
  poolSizes.push_back(vks::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, static_cast<uint32_t>(materials.size())));
  poolSizes.push_back(vks::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, static_cast<uint32_t>(materials.size())));
  poolSizes.push_back(vks::initializers::descriptorPoolSize(VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, static_cast<uint32_t>(materials.size())));

  VkDescriptorPoolCreateInfo descriptorPoolInfo =
    vks::initializers::descriptorPoolCreateInfo(
      static_cast<uint32_t>(poolSizes.size()),
      poolSizes.data(),
      static_cast<uint32_t>(materials.size()) + 1);

  VK_CHECK_RESULT(vkCreateDescriptorPool(vulkanDevice->logicalDevice, &descriptorPoolInfo, nullptr, &descriptorPool));

  // Descriptor set and pipeline layouts
  std::vector<VkDescriptorSetLayoutBinding> setLayoutBindings;
  VkDescriptorSetLayoutCreateInfo descriptorLayout;

  // Set 0: Scene matrices
  setLayoutBindings.push_back(vks::initializers::descriptorSetLayoutBinding(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_VERTEX_BIT,
    0));
  setLayoutBindings.push_back(vks::initializers::descriptorSetLayoutBinding(
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    1));
  descriptorLayout = vks::initializers::descriptorSetLayoutCreateInfo(
    setLayoutBindings.data(),
    static_cast<uint32_t>(setLayoutBindings.size()));
  VK_CHECK_RESULT(vkCreateDescriptorSetLayout(vulkanDevice->logicalDevice, &descriptorLayout, nullptr, &descriptorSetLayouts.scene));

  // Set 1: Material data
  setLayoutBindings.clear();
  setLayoutBindings.push_back(vks::initializers::descriptorSetLayoutBinding(
    VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    VK_SHADER_STAGE_FRAGMENT_BIT,
    0));
  descriptorLayout = vks::initializers::descriptorSetLayoutCreateInfo(
    setLayoutBindings.data(),
    static_cast<uint32_t>(setLayoutBindings.size()));
  VK_CHECK_RESULT(vkCreateDescriptorSetLayout(vulkanDevice->logicalDevice, &descriptorLayout, nullptr, &descriptorSetLayouts.material));

  // Setup pipeline layout
  std::array<VkDescriptorSetLayout, 2> setLayouts = { descriptorSetLayouts.scene, descriptorSetLayouts.material };
  VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = vks::initializers::pipelineLayoutCreateInfo(setLayouts.data(), static_cast<uint32_t>(setLayouts.size()));

  // We will be using a push constant block to pass material properties to the fragment shaders
  VkPushConstantRange pushConstantRange = vks::initializers::pushConstantRange(
    VK_SHADER_STAGE_FRAGMENT_BIT,
    sizeof(VulkanMaterialProperties),
    0);
  pipelineLayoutCreateInfo.pushConstantRangeCount = 1;
  pipelineLayoutCreateInfo.pPushConstantRanges = &pushConstantRange;

  VK_CHECK_RESULT(vkCreatePipelineLayout(vulkanDevice->logicalDevice, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout));

  // Material descriptor sets
  for (size_t i = 0; i < materials.size(); i++)
  {
    // Descriptor set
    VkDescriptorSetAllocateInfo allocInfo =
      vks::initializers::descriptorSetAllocateInfo(
        descriptorPool,
        &descriptorSetLayouts.material,
        1);

    VK_CHECK_RESULT(vkAllocateDescriptorSets(vulkanDevice->logicalDevice, &allocInfo, &materials[i].descriptorSet));

    std::vector<VkWriteDescriptorSet> writeDescriptorSets;

    // todo : only use image sampler descriptor set and use one scene ubo for matrices

    // Binding 0: Diffuse texture
    writeDescriptorSets.push_back(vks::initializers::writeDescriptorSet(
      materials[i].descriptorSet,
      VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
      0,
      &materials[i].diffuse->descriptor));

    vkUpdateDescriptorSets(vulkanDevice->logicalDevice, static_cast<uint32_t>(writeDescriptorSets.size()), writeDescriptorSets.data(), 0, NULL);
  }

  // Scene descriptor set
  VkDescriptorSetAllocateInfo allocInfo =
    vks::initializers::descriptorSetAllocateInfo(
      descriptorPool,
      &descriptorSetLayouts.scene,
      1);
  VK_CHECK_RESULT(vkAllocateDescriptorSets(vulkanDevice->logicalDevice, &allocInfo, &descriptorSetScene));

  std::vector<VkWriteDescriptorSet> writeDescriptorSets;
  // Binding 0 : Vertex shader uniform buffer
  writeDescriptorSets.push_back(vks::initializers::writeDescriptorSet(
    descriptorSetScene,
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    0,
    &uniformBuffers.VS.descriptor));
  // Binding 1 : Fragment shader uniform buffer
  writeDescriptorSets.push_back(vks::initializers::writeDescriptorSet(
    descriptorSetScene,
    VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
    1,
    &uniformBuffers.FG.descriptor));

  vkUpdateDescriptorSets(vulkanDevice->logicalDevice, static_cast<uint32_t>(writeDescriptorSets.size()), writeDescriptorSets.data(), 0, NULL);

  double timer_endf = getCurrentTime();
  LOGFILE.Debug("Vulkan: Load Materials End Time: %f", timer_endf);
  LOGFILE.Debug("Vulkan: Seconds since Pre-Vulkan:     %.3f", (timer_endf - timer_prevulkanf));
  LOGFILE.Debug("Vulkan: Seconds since Material Start: %.3f", (timer_endf - timer_startf));
}

void VulkanScene::loadMeshes(VkCommandBuffer copyCommand)
{
  //LOGFILE.Info("Loading Vulkan Scene Meshes...");
  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;
  uint32_t indexBase = 0;

  for (uint32_t i = 0; i < (uint32_t)sourceData->modelData.count(); i++)
  {
    //LOGFILE.Debug("Mesh: \"%s\"", qPrintable(sourceData->modelData.at(i).Name));

    for (uint32_t v = 0; v < (uint32_t)sourceData->modelData.at(i).Verticies.size(); v++)
    {
      vertices.push_back(sourceData->modelData.at(i).Verticies.at(v));
    }

    for (uint32_t p = 0; p < (uint32_t)sourceData->modelData.at(i).meshData.count(); p++)
    {
      VulkanScenePart vsp;
      PartData mesh = sourceData->modelData.at(i).meshData.value(p);
      //LOGFILE.Debug("	Part: \"%s\"", qPrintable(mesh.partName));
      //LOGFILE.Debug("		Material: \"%s\"", qPrintable(materials[mesh.materialID].name));
      //LOGFILE.Debug("		Num Faces: %i", (mesh.indices.size() / 3));

      vsp.material = &materials[mesh.materialID];
      vsp.indexBase = mesh.baseOffset;
      vsp.indexCount = mesh.indices.size();

      //LOGFILE.Debug("		Index Start: %i", indexBase);
      //LOGFILE.Debug("		Index Count: %i", mesh.indices.size());

      for (uint32_t mi = 0; mi < (uint32_t)mesh.indices.size(); mi++)
      {
        indices.push_back(mesh.indices.at(mi));
      }
      indexBase += mesh.indices.size();

      meshes.push_back(vsp);
    }
  }

  // Create buffers
  // For better performance we only create one index and vertex buffer to keep number of memory allocations down
  size_t vertexDataSize = vertices.size() * sizeof(Vertex);
  size_t indexDataSize = indices.size() * sizeof(uint32_t);

  vks::Buffer vertexStaging, indexStaging;

  // Vertex buffer
  // Staging buffer
  VK_CHECK_RESULT(vulkanDevice->createBuffer(
    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    &vertexStaging,
    static_cast<uint32_t>(vertexDataSize),
    vertices.data()));
  // Target
  VK_CHECK_RESULT(vulkanDevice->createBuffer(
    VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    &vertexBuffer,
    static_cast<uint32_t>(vertexDataSize)));

  // Index buffer
  VK_CHECK_RESULT(vulkanDevice->createBuffer(
    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
    VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
    &indexStaging,
    static_cast<uint32_t>(indexDataSize),
    indices.data()));
  // Target
  VK_CHECK_RESULT(vulkanDevice->createBuffer(
    VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
    VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
    &indexBuffer,
    static_cast<uint32_t>(indexDataSize)));

  // Copy
  VkCommandBufferBeginInfo cmdBufInfo = vks::initializers::commandBufferBeginInfo();
  VK_CHECK_RESULT(vkBeginCommandBuffer(copyCommand, &cmdBufInfo));

  VkBufferCopy copyRegion = {};

  copyRegion.size = vertexDataSize;
  vkCmdCopyBuffer(
    copyCommand,
    vertexStaging.buffer,
    vertexBuffer.buffer,
    1,
    &copyRegion);

  copyRegion.size = indexDataSize;
  vkCmdCopyBuffer(
    copyCommand,
    indexStaging.buffer,
    indexBuffer.buffer,
    1,
    &copyRegion);

  VK_CHECK_RESULT(vkEndCommandBuffer(copyCommand));

  VkSubmitInfo submitInfo = {};
  submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &copyCommand;

  VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitInfo, VK_NULL_HANDLE));
  VK_CHECK_RESULT(vkQueueWaitIdle(queue));

  //todo: fence
  vertexStaging.destroy();
  indexStaging.destroy();
}
