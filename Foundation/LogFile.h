/*
  LogFile Header
  Written by Alexander Aberle (Kjasi / Sephiroth3D)
  Copyright �2010-2019

  See LogFile.cpp for more info.
*/

#pragma once

#include <fstream>
#include <stdarg.h>
#ifdef QT_VERSION
#include <QtCore/qstring.h>
#endif

#define LOGFILE LoggingSystem::LogFile::instance()

using namespace std;

namespace LoggingSystem
{
  class LogFile {
  protected:
    enum AlertTypes
    {
      AlertInfo,
      AlertDebug,
      AlertWarn,
      AlertError,
      AlertFatal
    };

  private:
    bool useTimestamp;
    bool showDebug;
    ofstream m_stream;
    char* file;
    static LogFile *m_instance;
    string getTimeStamp();
    string BuildString(AlertTypes level, const char* format);
    void WriteStream(const char* format, va_list argp);
    void WriteStream(string format, va_list argp);
  public:
    void Write(const char* logline, ...);
    void Info(const char* logline, ...);
    void Debug(const char* logline, ...);
    void Warn(const char* logline, ...);
    void Error(const char* logline, ...);
    void Fatal(const char* logline, ...);
    void Write() { Write(""); };
    void Info() { Info(""); };
    void Debug() { Debug(""); };
    void Warn() { Warn(""); };
    void Error() { Error(""); };
    void Fatal() { Fatal(""); };
#ifdef QT_VERSION
    void Write(QString logline, ...);
    void Info(QString logline, ...);
    void Debug(QString logline, ...);
    void Warn(QString logline, ...);
    void Error(QString logline, ...);
    void Fatal(QString logline, ...);
#endif

    void setFile() { if (!file) file = "LogFile.log"; };
    void setFile(char* filename) { file = filename; };
    void setShowDebug(bool value = false) { showDebug = value; };
    void setUseTimestamp(bool value = true) { useTimestamp = value; };
    bool getShowDebug() { return showDebug; };
    bool getUseTimestamp() { return useTimestamp; };
    void truncateFile();

    static LogFile & instance()
    {
      if (LogFile::m_instance == 0)
        LogFile::m_instance = new LogFile();

      return *m_instance;
    }

    LogFile() {
      useTimestamp = true;
#ifdef _DEBUG
      showDebug = true;
#else
      showDebug = false;
#endif
      setFile();
    };
    LogFile(char* filename) {
      useTimestamp = true;
#ifdef _DEBUG
      showDebug = true;
#else
      showDebug = false;
#endif
      setFile(filename);
    };
    ~LogFile();
  };
}