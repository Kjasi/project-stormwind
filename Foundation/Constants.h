#pragma once
#include <QtCore/QStringList>
#include <Foundation/Classes/Math.h>

// Conversion Values
#define FEETtoMETERS (1.0 / 3.2808)
#define YARDStoMETERS (1.0 / 1.0936)

enum Expansion
{
  EXPANSION_VANILLA = 1,
  EXPANSION_BURNINGCRUSADE,
  EXPANSION_WRATHOFTHELICHKING,
  EXPANSION_CATACLYSM,
  EXPANSION_MISTSOFPANDARIA,
  EXPANSION_WARLORDSOFDRAENOR,
  EXPANSION_LEGION,
  EXPANSION_BATTLEFORAZEROTH,

  EXPANSION_MAX,
  EXPANSION_CURRENT = EXPANSION_BATTLEFORAZEROTH,
  EXPANSION_ALL = 20,
  EXPANSION_BETA = 0,
  EXPANSION_UNKNOWN = 999999,
};

// All the currently supported locales for WoW
const static QStringList Locales = { "ruRU", "itIT", "ptBR", "koKR", "zhTW", "zhCN", "esMX", "esES", "frFR", "deDE", "koKR", "enGB", "enUS" };