#pragma once
#include <QtCore/qstring.h>
#include <QtCore/qlist.h>
#include <QtCore/qbytearray.h>

// Flip 4 Characters
inline void flipchar(char *fcc) {
  char t;
  t = fcc[0];
  fcc[0] = fcc[3];
  fcc[3] = t;
  t = fcc[1];
  fcc[1] = fcc[2];
  fcc[2] = t;
};

inline QString flipcc(QString fcc) {
  QString r;
  r.push_back(fcc.data()[3]);
  r.push_back(fcc.data()[2]);
  r.push_back(fcc.data()[1]);
  r.push_back(fcc.data()[0]);
  return r;
};

inline QByteArray* flipba(QByteArray *fcc) {
  QByteArray n = *fcc;
  fcc->clear();
  for (int i = n.count(); i > 0; i--)
  {
    fcc->push_back(n.at(i - 1));
  }
  return fcc;
}

inline const char* flipba(QByteArray fcc) {
  QByteArray n;
  for (int i = fcc.count(); i > 0; i--)
  {
    n.push_back(fcc.at(i - 1));
  }
  return n.toStdString().c_str();
}

template <typename T> T getQBANumber(QByteArray d)
{
  uint32_t r = 0;
  flipba(&d);
  sscanf_s(d.toHex(), "%x", &r);
  //LOGFILE.Info("getQBANumber generic | d hex: %s | r: %i", qPrintable(d.toHex()), r);
  return static_cast<T>(r);
}
template <> float getQBANumber<float>(QByteArray d);
template <> double getQBANumber<double>(QByteArray d);

QString ReadNextString(QByteArray *byteArray);
void ReadStringsIntoList(QStringList* StringList, FILE *file, size_t size);
void ReadStringsIntoList(QStringList* StringList, QByteArray *byteArray, size_t offset, size_t size);
double getCurrentTime();
QString getJustFilesName(QString originalFilename);
QString unifySlashes(QString originalFilename);
bool validateFile(QString filename, QString logtypename);