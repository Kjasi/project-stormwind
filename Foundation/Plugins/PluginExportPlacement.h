#pragma once
#include <Foundation/Plugins/Plugin.h>
#include <Foundation/Classes/PlacementData.h>

#define PLUGINEXPORTPLACEMENTVERSION 1

namespace ProjectSW
{
  class PluginExportPlacement : public Plugin
  {
  public:
    PluginExportPlacement(QString id) : Plugin(id) {}
    virtual ~PluginExportPlacement() {}

    virtual bool writeFile(QString file_name, PlacementData *data, int extensionIndex) { return false; };

  protected:
    void sanitizeNames(QMap<QString, int> &map, QString &objectName, QString &uniqueObjectName);
  };
}

Q_DECLARE_INTERFACE(ProjectSW::PluginExportPlacement, "org.ProjectStormwind.plugin.exporters.placement")