#include "PluginExportPlacement.h"
#include <QMap>

void ProjectSW::PluginExportPlacement::sanitizeNames(QMap<QString, int>& map, QString & objectName, QString & uniqueObjectName)
{
  objectName.replace(" ", "_");
  objectName.replace("\\", "/");
  objectName.replace("$", "");
  objectName = qPrintable(objectName);
  uniqueObjectName = objectName;
  uniqueObjectName.replace("/", "");
  uniqueObjectName.replace("&", "");
  uniqueObjectName.replace(".m2", "_m2");
  uniqueObjectName.replace(".wmo", "_wmo");
  uniqueObjectName.replace(".adt", "_adt");

  if (map.contains(objectName) == true)
  {
    uniqueObjectName = QString("%1_%2").arg(uniqueObjectName).arg(QString::number(map.value(objectName)));
    map[objectName]++;
  }
  else {
    map[objectName] = 1;
  }
}