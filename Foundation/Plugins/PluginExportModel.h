#pragma once
#include <Foundation/Plugins/Plugin.h>
#include <Foundation/Classes/ModelObject.h>

#define PLUGINEXPORTMODELVERSION 1

namespace ProjectSW
{
  class PluginExportModel : public Plugin
  {
  public:
    PluginExportModel(QString id) : Plugin(id) {}
    virtual ~PluginExportModel() {}
    
    QList<ModelMeshType> getModelOutTypes() { return ModelOutTypes; };
    virtual bool writeFile(QString file_name, ModelScene *scene, int extensionIndex, bool showOptions) { return false; };

  protected:
    QList<ModelMeshType> ModelOutTypes;
  };
}

Q_DECLARE_INTERFACE(ProjectSW::PluginExportModel, "org.ProjectStormwind.plugin.exporters.model")