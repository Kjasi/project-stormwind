#pragma once
#include <Foundation/Plugins/Plugin.h>
#include <Foundation/Classes.h>
#include <Foundation/Classes/ImageData.h>

#define PLUGINEXPORTIMAGEVERSION 1

namespace ProjectSW
{
  enum ImageDestinationType
  {
    LANDSCAPEHEIGHT = 0,
    MASKLAYER,
    COLOR,
    COLORALPHA,

    IMGDESTTYPE_MAX
  };
  enum PluginImageDepth
  {
    IMGDEPTH_8BIT = 8,
    IMGDEPTH_16BIT = 16,
    IMGDEPTH_32BIT = 32
  };
  enum PluginImageType
  {
    IMGTYPE_RGB = 0,
    IMGTYPE_RGBA,
    IMGTYPE_HEIGHTMAP,
    IMGTYPE_GREY,
    IMGTYPE_GREYALPHA
  };

  class PluginExportImage : public Plugin
  {
  public:
    PluginExportImage(QString id) : Plugin(id) {}
    virtual ~PluginExportImage() {}

    QList<ImageDestinationType> getImageOutTypes() { return ImageOutTypes; };
    QList<QString> getHeightmapExtensions() { return HeightmapFileExtensions; };
    virtual bool writeImageFile(QString file_name, ImageData *image_data, int extensionIndex, PluginImageType image_type = IMGTYPE_RGB, PluginImageDepth image_depth = IMGDEPTH_8BIT) { return false; };
    virtual PluginImageDepth getMaxHeightmapDepth() { return PluginImageDepth::IMGDEPTH_8BIT; };

  protected:
    QList<ImageDestinationType> ImageOutTypes;
    QList<QString> HeightmapFileExtensions;
  };
}

Q_DECLARE_INTERFACE(ProjectSW::PluginExportImage, "org.ProjectStormwind.plugin.exporters.image")