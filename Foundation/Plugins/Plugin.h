#pragma once
#include <QtCore/qstring.h>
#include <QtCore/qplugin.h>
#include <QtCore/qtranslator.h>
#include <Foundation/Core/Component.h>

namespace ProjectSW
{
  class Kernel;

  class Plugin : public KJProductions::Component
  {
  public:
    Plugin(QString id);
    ~Plugin();

    virtual int compatibleAppVersion() const = 0;
    virtual void accept(KJProductions::Visitor *) {}
    virtual QTranslator *pluginTranslator(QString locale) = 0;

    QString getPluginFilterName() { return PluginFilterName; }
    QString getPluginName() { return PluginName; }
    QString id() { return m_id; }
    QList<QString> getFileExtensions() { return FileExtensions; };
    QString getOptionsJson();
    void setOptionsJson(QString options);

  protected:
    QString PluginName;
    QString PluginFilterName;
    QList<QString> FileExtensions;

  private:
    QString m_id;
    QString optionsJson;
  };
}