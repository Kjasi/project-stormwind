#include "Plugin.h"
#include "Settings.h"

using namespace ProjectSW;

Plugin::Plugin(QString id) : m_id(id) {}
Plugin::~Plugin() {}

QString ProjectSW::Plugin::getOptionsJson()
{
  SETTINGS.sync();
  SETTINGS.beginGroup("Plugin Options");
  optionsJson = SETTINGS.value(PluginName).toString();
  SETTINGS.endGroup();

  return optionsJson;
}

void ProjectSW::Plugin::setOptionsJson(QString options)
{
  optionsJson = options;

  SETTINGS.beginGroup("Plugin Options");
  SETTINGS.setValue(PluginName, optionsJson);
  SETTINGS.endGroup();
  SETTINGS.sync();
}
