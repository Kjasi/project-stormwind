#include "Settings.h"

ProjectSettings * ProjectSettings::m_instance = 0;

ProjectSettings & ProjectSettings::instance()
{
  if (ProjectSettings::m_instance == 0)
  {
    ProjectSettings::m_instance = new ProjectSettings("ProjectStormwind.ini", QSettings::IniFormat);
  }
  return *m_instance;
}