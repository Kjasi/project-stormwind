<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>QObject</name>
    <message>
        <location filename="../saveFiles.hpp" line="110"/>
        <source>Unable to find selected filter.</source>
        <comment>SaveFileErrorType for a bad filter.</comment>
        <translation>Unable to find selected filter.</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="113"/>
        <source>An invalid or empty filename was used.</source>
        <comment>SaveFileErrorType for a bad, invalid, or empty filename.</comment>
        <translation>An invalid or empty filename was used.</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="116"/>
        <source>No plugins found at all!</source>
        <comment>SaveFileErrorType for having no plugins at all.</comment>
        <translation>No plugins found at all!</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="119"/>
        <source>No exporting plugins of the proper type were found.</source>
        <comment>SaveFileErrorType for having no plugins of the right type.</comment>
        <translation>No exporting plugins of the proper type were found.</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="122"/>
        <source>Unable to find the plugin...</source>
        <comment>SaveFileErrorType for missing plugin.</comment>
        <translation>Unable to find the plugin...</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="125"/>
        <source>An error occured in the plugin itself.</source>
        <comment>SaveFileErrorType for a plugin error.</comment>
        <translation>An error occured in the plugin itself.</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="128"/>
        <source>No data to export.</source>
        <comment>SaveFileErrorType for no data.</comment>
        <translation>No data to export.</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="131"/>
        <source>An unknown error occured...</source>
        <comment>SaveFileErrorType for an Unknown error.</comment>
        <translation>An unknown error occured...</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="198"/>
        <location filename="../saveFiles.hpp" line="591"/>
        <location filename="../saveFiles.hpp" line="622"/>
        <source>Save File</source>
        <translation>Save File</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="222"/>
        <source>Select Export Root Directory</source>
        <translation>Select Export Root Directory</translation>
    </message>
    <message>
        <location filename="../saveFiles.hpp" line="738"/>
        <location filename="../saveFiles.hpp" line="843"/>
        <source>All files (*.*)</source>
        <comment>Specifies all the files in the folder.</comment>
        <translation>All files (*.*)</translation>
    </message>
    <message>
        <location filename="../Classes/WoWVersion.cpp" line="207"/>
        <source>Unable to Find WoW Application</source>
        <comment>Title for missing World of Warcraft application</comment>
        <translation>Unable to Find WoW Application</translation>
    </message>
    <message>
        <location filename="../Classes/WoWVersion.cpp" line="207"/>
        <source>Unable to find WoW.exe or WoW-64.exe in your current World of Warcraft directory.

Please open your preferences, and make sure the WoW directory points to either of these two files.</source>
        <comment>Message for missing Windows WoW application</comment>
        <translation>Unable to find WoW.exe or WoW-64.exe in your current World of Warcraft directory.

Please open your preferences, and make sure the WoW directory points to either of these two files.</translation>
    </message>
    <message>
        <location filename="../Classes/WoWVersion.cpp" line="260"/>
        <source>Unknown but supported WoW Version</source>
        <comment>Title for a popup about an unknown but supported WoW build.</comment>
        <translation>Unknown but supported WoW Version</translation>
    </message>
    <message>
        <location filename="../Classes/WoWVersion.cpp" line="260"/>
        <source>This WoW Version is not a known live build, but may still be supported. Incorrect results may occur.</source>
        <comment>An unsupported build was discovered, but it won&apos;t stop the program from working.</comment>
        <translation>This WoW Version is not a known live build, but may still be supported. Incorrect results may occur.</translation>
    </message>
</context>
</TS>
