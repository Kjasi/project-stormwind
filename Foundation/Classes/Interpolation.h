#pragma once
#include "../Constants.h"
#include "Vectors.h"
#include "RGBAColor.h"

/*

Based on the work of Paul Bourke.
Document here: http://paulbourke.net/miscellaneous/interpolation/

*/

struct Interpolate
{
  static inline double Linear(double point1, double point2, double target)
  {
    return (point1*(1 - target) + point2*target);
  };
  static inline double Cosine(double point1, double point2, double target)
  {
    double muu = (1 - cos(target*PI)) / 2;
    return (point1*(1 - muu) + point2*muu);
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline double Cubic(double prePoint, double point1, double point2, double postPoint, double target)
  {
    double a0, a1, a2, a3, muu;
    muu = target * target;

    a0 = postPoint - point2 - prePoint + point1;
    a1 = prePoint - point1 - a0;
    a2 = point2 - prePoint;
    a3 = point1;
    
    return (a0*target*muu + a1*muu + a2*target + a3);
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline double CatmullRom(double prePoint, double point1, double point2, double postPoint, double target)
  {
    double a0, a1, a2, a3, muu;
    muu = target * target;

    a0 = -0.5*prePoint + 1.5*point1 - 1.5*point2 + 0.5*postPoint;
    a1 = prePoint - 2.5*point1 + 2 * point2 - 0.5*postPoint;
    a2 = -0.5*prePoint + 0.5*point2;
    a3 = point1;

    return (a0*target*muu + a1*muu + a2*target + a3);
  };

  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline double Hermite(double prePoint, double point1, double point2, double postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    double m0, m1, mu2, mu3;
    double a0, a1, a2, a3;

    mu2 = target * target;
    mu3 = mu2 * target;
    m0 = (point1 - prePoint)*(1 + bias)*(1 - tension) / 2;
    m0 += (point2 - point1)*(1 - bias)*(1 - tension) / 2;
    m1 = (point2 - point1)*(1 + bias)*(1 - tension) / 2;
    m1 += (postPoint - point2)*(1 - bias)*(1 - tension) / 2;
    a0 = 2 * mu3 - 3 * mu2 + 1;
    a1 = mu3 - 2 * mu2 + target;
    a2 = mu3 - mu2;
    a3 = -2 * mu3 + 3 * mu2;

    return(a0*point1 + a1*m0 + a2*m1 + a3*point2);
  };



  static inline RGBAColor Linear(RGBAColor prePoint, RGBAColor point1, double target)
  {
    RGBAColor a;
    a.R = Linear(prePoint.R, point1.R, target);
    a.G = Linear(prePoint.G, point1.G, target);
    a.B = Linear(prePoint.B, point1.B, target);
    a.A = Linear(prePoint.A, point1.A, target);
    return a;
  }
  static inline RGBAColor Cosine(RGBAColor prePoint, RGBAColor point1, double target)
  {
    RGBAColor a;
    a.R = Cosine(prePoint.R, point1.R, target);
    a.G = Cosine(prePoint.G, point1.G, target);
    a.B = Cosine(prePoint.B, point1.B, target);
    a.A = Cosine(prePoint.A, point1.A, target);
    return a;
  }
  static inline RGBAColor Cubic(RGBAColor prePoint, RGBAColor point1, RGBAColor point2, RGBAColor postPoint, double target)
  {
    RGBAColor a;
    a.R = Cubic(prePoint.R, point1.R, point2.R, postPoint.R, target);
    a.G = Cubic(prePoint.G, point1.G, point2.G, postPoint.G, target);
    a.B = Cubic(prePoint.B, point1.B, point2.B, postPoint.B, target);
    a.A = Cubic(prePoint.A, point1.A, point2.A, postPoint.A, target);
    return a;
  }
  static inline RGBAColor CatmullRom(RGBAColor prePoint, RGBAColor point1, RGBAColor point2, RGBAColor postPoint, double target)
  {
    RGBAColor a;
    a.R = CatmullRom(prePoint.R, point1.R, point2.R, postPoint.R, target);
    a.G = CatmullRom(prePoint.G, point1.G, point2.G, postPoint.G, target);
    a.B = CatmullRom(prePoint.B, point1.B, point2.B, postPoint.B, target);
    a.A = CatmullRom(prePoint.A, point1.A, point2.A, postPoint.A, target);
    return a;
  }
  static inline RGBAColor Hermite(RGBAColor prePoint, RGBAColor point1, RGBAColor point2, RGBAColor postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    RGBAColor a;
    a.R = Hermite(prePoint.R, point1.R, point2.R, postPoint.R, target, tension, bias);
    a.G = Hermite(prePoint.G, point1.G, point2.G, postPoint.G, target, tension, bias);
    a.B = Hermite(prePoint.B, point1.B, point2.B, postPoint.B, target, tension, bias);
    a.A = Hermite(prePoint.A, point1.A, point2.A, postPoint.A, target, tension, bias);
    return a;
  }


  static inline Vec2F Linear(Vec2F prePoint, Vec2F point1, double target)
  {
    Vec2F a;
    a.x = (float)Linear(prePoint.x, point1.x, target);
    a.y = (float)Linear(prePoint.y, point1.y, target);
    return a;
  };
  static inline Vec2D Linear(Vec2D prePoint, Vec2D point1, double target)
  {
    Vec2D a;
    a.x = Linear(prePoint.x, point1.x, target);
    a.y = Linear(prePoint.y, point1.y, target);
    return a;
  };
  static inline Vec3F Linear(Vec3F prePoint, Vec3F point1, double target)
  {
    Vec3F a;
    a.x = (float)Linear(prePoint.x, point1.x, target);
    a.y = (float)Linear(prePoint.y, point1.y, target);
    a.z = (float)Linear(prePoint.z, point1.z, target);
    return a;
  };
  static inline Vec3D Linear(Vec3D prePoint, Vec3D point1, double target)
  {
    Vec3D a;
    a.x = Linear(prePoint.x, point1.x, target);
    a.y = Linear(prePoint.y, point1.y, target);
    a.z = Linear(prePoint.z, point1.z, target);
    return a;
  };
  static inline Vec4F Linear(Vec4F prePoint, Vec4F point1, double target)
  {
    Vec4F a;
    a.x = (float)Linear(prePoint.x, point1.x, target);
    a.y = (float)Linear(prePoint.y, point1.y, target);
    a.z = (float)Linear(prePoint.z, point1.z, target);
    a.w = (float)Linear(prePoint.w, point1.w, target);
    return a;
  };
  static inline Vec4D Linear(Vec4D prePoint, Vec4D point1, double target)
  {
    Vec4D a;
    a.x = Linear(prePoint.x, point1.x, target);
    a.y = Linear(prePoint.y, point1.y, target);
    a.z = Linear(prePoint.z, point1.z, target);
    a.w = Linear(prePoint.w, point1.w, target);
    return a;
  };


  // Might have to cut these. Document said they revert to Linear when applied in 3D...
  static inline Vec2F Cosine(Vec2F prePoint, Vec2F point1, double target)
  {
    Vec2F a;
    a.x = (float)Cosine(prePoint.x, point1.x, target);
    a.y = (float)Cosine(prePoint.y, point1.y, target);
    return a;
  };
  static inline Vec2D Cosine(Vec2D prePoint, Vec2D point1, double target)
  {
    Vec2D a;
    a.x = Cosine(prePoint.x, point1.x, target);
    a.y = Cosine(prePoint.y, point1.y, target);
    return a;
  };
  static inline Vec3F Cosine(Vec3F prePoint, Vec3F point1, double target)
  {
    Vec3F a;
    a.x = (float)Cosine(prePoint.x, point1.x, target);
    a.y = (float)Cosine(prePoint.y, point1.y, target);
    a.z = (float)Cosine(prePoint.z, point1.z, target);
    return a;
  };
  static inline Vec3D Cosine(Vec3D prePoint, Vec3D point1, double target)
  {
    Vec3D a;
    a.x = Cosine(prePoint.x, point1.x, target);
    a.y = Cosine(prePoint.y, point1.y, target);
    a.z = Cosine(prePoint.z, point1.z, target);
    return a;
  };
  static inline Vec4F Cosine(Vec4F prePoint, Vec4F point1, double target)
  {
    Vec4F a;
    a.x = (float)Cosine(prePoint.x, point1.x, target);
    a.y = (float)Cosine(prePoint.y, point1.y, target);
    a.z = (float)Cosine(prePoint.z, point1.z, target);
    a.w = (float)Cosine(prePoint.w, point1.w, target);
    return a;
  };
  static inline Vec4D Cosine(Vec4D prePoint, Vec4D point1, double target)
  {
    Vec4D a;
    a.x = Cosine(prePoint.x, point1.x, target);
    a.y = Cosine(prePoint.y, point1.y, target);
    a.z = Cosine(prePoint.z, point1.z, target);
    a.w = Cosine(prePoint.w, point1.w, target);
    return a;
  };


  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec2F Cubic(Vec2F prePoint, Vec2F point1, Vec2F point2, Vec2F postPoint, double target)
  {
    Vec2F a;
    a.x = (float)Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec2D Cubic(Vec2D prePoint, Vec2D point1, Vec2D point2, Vec2D postPoint, double target)
  {
    Vec2D a;
    a.x = Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec3F Cubic(Vec3F prePoint, Vec3F point1, Vec3F point2, Vec3F postPoint, double target)
  {
    Vec3F a;
    a.x = (float)Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = (float)Cubic(prePoint.z, point1.z, point2.z, postPoint.z, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec3D Cubic(Vec3D prePoint, Vec3D point1, Vec3D point2, Vec3D postPoint, double target)
  {
    Vec3D a;
    a.x = Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = Cubic(prePoint.z, point1.z, point2.z, postPoint.z, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec4F Cubic(Vec4F prePoint, Vec4F point1, Vec4F point2, Vec4F postPoint, double target)
  {
    Vec4F a;
    a.x = (float)Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = (float)Cubic(prePoint.z, point1.z, point2.z, postPoint.z, target);
    a.w = (float)Cubic(prePoint.w, point1.w, point2.w, postPoint.w, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec4D Cubic(Vec4D prePoint, Vec4D point1, Vec4D point2, Vec4D postPoint, double target)
  {
    Vec4D a;
    a.x = Cubic(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = Cubic(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = Cubic(prePoint.z, point1.z, point2.z, postPoint.z, target);
    a.w = Cubic(prePoint.w, point1.w, point2.w, postPoint.w, target);
    return a;
  };


  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec2F CatmullRom(Vec2F prePoint, Vec2F point1, Vec2F point2, Vec2F postPoint, double target)
  {
    Vec2F a;
    a.x = (float)CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec2D CatmullRom(Vec2D prePoint, Vec2D point1, Vec2D point2, Vec2D postPoint, double target)
  {
    Vec2D a;
    a.x = CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec3F CatmullRom(Vec3F prePoint, Vec3F point1, Vec3F point2, Vec3F postPoint, double target)
  {
    Vec3F a;
    a.x = (float)CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = (float)CatmullRom(prePoint.z, point1.z, point2.z, postPoint.z, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec3D CatmullRom(Vec3D prePoint, Vec3D point1, Vec3D point2, Vec3D postPoint, double target)
  {
    Vec3D a;
    a.x = CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = CatmullRom(prePoint.z, point1.z, point2.z, postPoint.z, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec4F CatmullRom(Vec4F prePoint, Vec4F point1, Vec4F point2, Vec4F postPoint, double target)
  {
    Vec4F a;
    a.x = (float)CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = (float)CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = (float)CatmullRom(prePoint.z, point1.z, point2.z, postPoint.z, target);
    a.w = (float)CatmullRom(prePoint.w, point1.w, point2.w, postPoint.w, target);
    return a;
  };
  // Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers
  static inline Vec4D CatmullRom(Vec4D prePoint, Vec4D point1, Vec4D point2, Vec4D postPoint, double target)
  {
    Vec4D a;
    a.x = CatmullRom(prePoint.x, point1.x, point2.x, postPoint.x, target);
    a.y = CatmullRom(prePoint.y, point1.y, point2.y, postPoint.y, target);
    a.z = CatmullRom(prePoint.z, point1.z, point2.z, postPoint.z, target);
    a.w = CatmullRom(prePoint.w, point1.w, point2.w, postPoint.w, target);
    return a;
  };


  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec2F Hermite(Vec2F prePoint, Vec2F point1, Vec2F point2, Vec2F postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec2F a;
    a.x = (float)Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = (float)Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    return a;
  };
  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec2D Hermite(Vec2D prePoint, Vec2D point1, Vec2D point2, Vec2D postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec2D a;
    a.x = Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    return a;
  };
  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec3F Hermite(Vec3F prePoint, Vec3F point1, Vec3F point2, Vec3F postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec3F a;
    a.x = (float)Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = (float)Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    a.z = (float)Hermite(prePoint.z, point1.z, point2.z, postPoint.z, target, tension, bias);
    return a;
  };
  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec3D Hermite(Vec3D prePoint, Vec3D point1, Vec3D point2, Vec3D postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec3D a;
    a.x = Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    a.z = Hermite(prePoint.z, point1.z, point2.z, postPoint.z, target, tension, bias);
    return a;
  };
  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec4F Hermite(Vec4F prePoint, Vec4F point1, Vec4F point2, Vec4F postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec4F a;
    a.x = (float)Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = (float)Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    a.z = (float)Hermite(prePoint.z, point1.z, point2.z, postPoint.z, target, tension, bias);
    a.w = (float)Hermite(prePoint.w, point1.w, point2.w, postPoint.w, target, tension, bias);
    return a;
  };
  /*
  Point1 and Point2 are the ones we want to lerp. prePoint and postPoint are helpers

  Tension: 1 is high, 0 normal, -1 is low
  Bias: 0 is even, positive is towards first segment, negative towards the other
  */
  static inline Vec4D Hermite(Vec4D prePoint, Vec4D point1, Vec4D point2, Vec4D postPoint, double target, double tension = 0.0, double bias = 0.0)
  {
    Vec4D a;
    a.x = Hermite(prePoint.x, point1.x, point2.x, postPoint.x, target, tension, bias);
    a.y = Hermite(prePoint.y, point1.y, point2.y, postPoint.y, target, tension, bias);
    a.z = Hermite(prePoint.z, point1.z, point2.z, postPoint.z, target, tension, bias);
    a.w = Hermite(prePoint.w, point1.w, point2.w, postPoint.w, target, tension, bias);
    return a;
  };
};