#pragma once

// Basic Math
#define PI32 3.14159265359f
#define PI64 3.1415926535897932384626433832795028841971693993751
#define HALFPI32 PI32 / 2
#define HALFPI64 PI64 / 2
#define RADIAN32 (180.0f / PI32)
#define DEGREES32 (PI32 / 180.0f)
#define RADIAN64 (180.0 / PI64)
#define DEGREES64 (PI64 / 180.0)

#define PI PI64
#define HALFPI HALFPI64

namespace Math
{
  const float ROUNDING_ERROR_f32 = 0.000001f;
  const double ROUNDING_ERROR_f64 = 0.00000001;

  template<class T>
  inline const T& min_(const T& a, const T& b)
  {
    return a < b ? a : b;
  }

  template<class T>
  inline const T& min_(const T& a, const T& b, const T& c)
  {
    return a < b ? min_(a, c) : min_(b, c);
  }

  template<class T>
  inline const T& max_(const T& a, const T& b)
  {
    return a < b ? b : a;
  }

  template<class T>
  inline const T& max_(const T& a, const T& b, const T& c)
  {
    return a < b ? max_(b, c) : max_(a, c);
  }

  template<class T>
  inline T abs_(const T& a)
  {
    return a < (T)0 ? -a : a;
  }

  template<class T>
  inline T lerp(const T& a, const T& b, const float t)
  {
    return (T)(a*(1.f - t)) + (b*t);
  }

  template <class T>
  inline const T clamp(const T& value, const T& low, const T& high)
  {
    return min_(max_(value, low), high);
  }

  inline bool equals(const double a, const double b, const double tolerance = ROUNDING_ERROR_f64)
  {
      return (a + tolerance >= b) && (a - tolerance <= b);
  }
  
  inline bool equals(const float a, const float b, const float tolerance = ROUNDING_ERROR_f32)
  {
      return (a + tolerance >= b) && (a - tolerance <= b);
  }

  inline float ClampAxis(float Angle)
  {
    // returns Angle in the range (-360,360)
    Angle = fmod(Angle, 360.f);

    if (Angle < 0.f)
    {
      // shift to [0,360) range
      Angle += 360.f;
    }

    return Angle;
  }

  inline float NormalizeAxis(float angle)
  {
    // returns Angle in the range [0,360)
    angle = ClampAxis(angle);

    if (angle > 180.f)
    {
      // shift to (-180,180]
      angle -= 360.f;
    }

    return angle;
  }

  inline double ClampAxis(double Angle)
  {
    // returns Angle in the range (-360,360)
    Angle = fmod(Angle, 360.0);

    if (Angle < 0.f)
    {
      // shift to [0,360) range
      Angle += 360.f;
    }

    return Angle;
  }

  inline double NormalizeAxis(double angle)
  {
    // returns Angle in the range [0,360)
    angle = ClampAxis(angle);

    if (angle > 180.0)
    {
      // shift to (-180,180]
      angle -= 360.0;
    }

    return angle;
  }
}