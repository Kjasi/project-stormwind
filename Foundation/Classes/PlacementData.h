#pragma once
#include "Vectors.h"
#include "RGBAColor.h"

enum PlacementObjectType
{
  PLACEMENTOBJECTTYPE_DOODAD = 0,
  PLACEMENTOBJECTTYPE_WMO,
  PLACEMENTOBJECTTYPE_ADT,
  PLACEMENTOBJECTTYPE_CAMERA,
  PLACEMENTOBJECTTYPE_LIGHT,
  PLACEMENTOBJECTTYPE_SOUND,
  PLACEMENTOBJECTTYPE_NULL,	// Used for any kind of parent object that doesn't have geometery
};

enum PlacementLightType
{
  PLACEMENTLIGHTTYPE_POINTLIGHT = 0,
  PLACEMENTLIGHTTYPE_SPOTLIGHT,
};

struct PlacementLight;
struct PlacementCamera;
struct PlacementSound;

struct PlacementObject
{
  PlacementObjectType objectType = PLACEMENTOBJECTTYPE_DOODAD;
  QString Name;
  Vec3D Location = Vec3D(0.0);
  Quaternion Rotation = Quaternion();
  Vec3D Rotation3D = Vec3D();
  Vec3D Scale = Vec3D(1.0);
  Vec3D OriginOffset = Vec3D(0.0);
  QStringList InfoList;

  void setRotation(Quaternion q)
  {
    Rotation = q;
    Rotation3D = q.toEuler();
  }
  void setRotation(Vec3D q)
  {
    Rotation = Quaternion::fromEuler(q);
    Rotation3D = q;
  }

  QList<PlacementObject> children;
  uint32_t getNumChildren() { return children.count(); }

  QList<PlacementLight> childrenLights;
  uint32_t getNumChildrenLights() { return childrenLights.count(); }

  QList<PlacementCamera> childrenCameras;
  uint32_t getNumChildrenCameras() { return childrenCameras.count(); }

  QList<PlacementSound> childrenSounds;
  uint32_t getNumChildrenSounds() { return childrenSounds.count(); }

  uint32_t getNumAllChildren() { return (children.count() + childrenLights.count() + childrenCameras.count() + childrenSounds.count()); }
};

struct PlacementLight : public PlacementObject
{
  PlacementLightType lightType = PLACEMENTLIGHTTYPE_POINTLIGHT;
  RGBAColor lightColor;
  double lightIntensity = 0;
  double sourceRadius = 0;
  bool useAttenuation = false;
  double attenuationRadius = 0;

  PlacementLight() {
    objectType = PLACEMENTOBJECTTYPE_LIGHT;
  }
};

struct PlacementCamera : public PlacementObject
{
  double FieldOfView;
  double AspectRatio = 1.777778;

  PlacementCamera() {
    objectType = PLACEMENTOBJECTTYPE_CAMERA;
  }
};

struct PlacementSound : public PlacementObject
{
  double InnerRadius;
  double OuterRadius;
  double Volume = 1.0;

  PlacementSound() {
    objectType = PLACEMENTOBJECTTYPE_SOUND;
  }
};

class PlacementData
{
public:
  PlacementData()
  {
    Objects = new QList<PlacementObject>;
    Lights = new QList<PlacementLight>;
    Cameras = new QList<PlacementCamera>;
    Sounds = new QList<PlacementSound>;
  };
  ~PlacementData()
  {
    delete Objects;
    delete Lights;
    delete Cameras;
    delete Sounds;
  };

  QList<PlacementObject> *Objects = NULL;
  uint32_t getNumObjects() { if (Objects == NULL) return 0; return Objects->count(); }
  uint32_t getNumObjectsAndChildren() {
    if (Objects == NULL) return 0;
    uint32_t num = Objects->count();
    for (int i = 0; i < Objects->count(); i++)
    {
      num += Objects->at(i).children.count();
      num += Objects->at(i).childrenCameras.count();
      num += Objects->at(i).childrenLights.count();
      num += Objects->at(i).childrenSounds.count();
    }
    
    return num;
  }

  QList<PlacementLight> *Lights = NULL;
  uint32_t getNumLights() { if (Lights == NULL) return 0; return Lights->count(); }
  uint32_t getNumLightsAndChildren() {
    if (Lights == NULL) return 0;
    uint32_t num = Lights->count();
    for (int i = 0; i < Lights->count(); i++)
    {
      num += Lights->at(i).children.count();
      num += Lights->at(i).childrenCameras.count();
      num += Lights->at(i).childrenLights.count();
      num += Lights->at(i).childrenSounds.count();
    }

    return num;
  }

  QList<PlacementCamera> *Cameras = NULL;
  uint32_t getNumCameras() { if (Cameras == NULL) return 0; return Cameras->count(); }
  uint32_t getNumCamerasAndChildren() {
    if (Cameras == NULL) return 0;
    uint32_t num = Cameras->count();
    for (int i = 0; i < Cameras->count(); i++)
    {
      num += Cameras->at(i).children.count();
      num += Cameras->at(i).childrenCameras.count();
      num += Cameras->at(i).childrenLights.count();
      num += Cameras->at(i).childrenSounds.count();
    }

    return num;
  }

  QList<PlacementSound> *Sounds = NULL;
  uint32_t getNumSounds() { if (Sounds == NULL) return 0; return Sounds->count(); }
  uint32_t getNumSoundsAndChildren() {
    if (Sounds == NULL) return 0;
    uint32_t num = Sounds->count();
    for (int i = 0; i < Sounds->count(); i++)
    {
      num += Sounds->at(i).children.count();
      num += Sounds->at(i).childrenCameras.count();
      num += Sounds->at(i).childrenLights.count();
      num += Sounds->at(i).childrenSounds.count();
    }

    return num;
  }

  uint32_t getNumChildren()
  {
    uint32_t num = getNumObjects();
    num += getNumLights();
    num += getNumCameras();
    num += getNumSounds();

    return num;
  }
  uint32_t getNumAllChildren()
  {
    uint32_t num = getNumObjectsAndChildren();
    num += getNumLightsAndChildren();
    num += getNumCamerasAndChildren();
    num += getNumSoundsAndChildren();

    return num;
  }

private:

};