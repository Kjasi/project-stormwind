#pragma once
#include "Vectors.h"

struct BoundingBox
{
  Vec3D min = Vec3D(99999999.9, 99999999.9, 99999999.9);
  Vec3D max = Vec3D(-99999999.9, -99999999.9, -99999999.9);

  void updateMin(Vec3D in);
  void updateMax(Vec3D in);
  void updateMinAndMax(Vec3D in);
  void updateMin(BoundingBox in);
  void updateMax(BoundingBox in);
  void updateMinAndMax(BoundingBox in);

  double getLength();
};