#pragma once
#include <QtCore/qlist.h>
#include "Interpolation.h"

template<class C>
class lerp
{
private:
  enum lerpType
  {
    TwoPointLerp = 0,
    ThreePointLerp,

    Unset
  };
  lerpType PointType;
  C StartPoint;
  C MiddlePoint;
  C EndPoint;
public:
  lerp() {
    StartPoint = new C;
    MiddlePoint = new C;
    EndPoint = new C;
    PointType = Unset;
  };
  lerp(C start, C end) { StartPoint = start; EndPoint = end; PointType = TwoPointLerp; };
  lerp(C start, C middle, C end) { StartPoint = start; MiddlePoint = middle; EndPoint = end; PointType = ThreePointLerp; };

  C getPoint(double target) {
    if (PointType == ThreePointLerp)
    {
      return doLerp(StartPoint, MiddlePoint, EndPoint, target);
    }
    return doLerp(StartPoint, EndPoint, target);
  };

  static C doCubicLerp(C prestart, C start, C end, C postend, double target) { return Interpolate::Cubic(prestart, start, end, postend, target); };
  static C doCatmullRomLerp(C prestart, C start, C end, C postend, double target) { return Interpolate::CatmullRom(prestart, start, end, postend, target); };
  static C doHermiteLerp(C prestart, C start, C end, C postend, double target, double tension = 0.0, double bias = 0.0) { return Interpolate::CatmullRom(prestart, start, end, postend, target); };
  static C doCosineLerp(C start, C end, double target) { return Interpolate::Cosine(start, end, target); };
  static C doLerp(C start, C end, double target) { return Interpolate::Linear(start, end, target); };
  static C doLerp(C start, C middle, C end, double target)
  {
    C i0 = lerp(start, middle).getPoint(target);
    C i1 = lerp(middle, end).getPoint(target);
    C r = lerp(i0, i1).getPoint(target);
    return r;
  };
  static C doBzLerp(QList<C> pointList, double target)
  {
    if (pointList.count() < 2) { return 0; }
    if (pointList.count() < 3) { return lerp::doLerp(pointList.at(0), pointList.at(1), target); }

    QList <lerp<C>> lerps;
    for (int i = 0; i < pointList.count()-1; i++)
    {
      lerp<C> l(pointList.at(i), pointList.at(i+1));
      lerps.push_back(l);
    }
    C val;
    for (int i = 0; i < lerps.count()-2; i++)
    {
      if (i == 0)
      {
        C i0 = lerps.at(i).getPoint(target);
        C i1 = lerps.at(i + 1).getPoint(target);
        val = lerp(i0, i1).getPoint(target);
      }
      else {
        val = lerp(val, lerps.at(i)).getPoint(target);
      }
    }
    return val;
  }

  C getStart() { return StartPoint; };
  C getMiddle() { return MiddlePoint; };
  C getEnd() { return EndPoint; };

  void setStart(C start) { StartPoint = start; };
  void setMiddle(C middle) { MiddlePoint = middle; PointType = ThreePointLerp; };
  void setEnd(C end) { EndPoint = end; };

  C blerp(lerp otherLerp, double target_x, double target_y) { return doBlerp(this, otherLerp, target_x, target_y); };
  static C doBlerp(lerp firstLerp, lerp secondLerp, double target_x, double target_y)
  {
    C first = firstLerp.getPoint(target_x);
    C second = secondLerp.getPoint(target_x);
    return doLerp(first, second, target_y);
  };
  static C doBlerp(lerp* firstLerp, lerp secondLerp, double target_x, double target_y)
  {
    C first = firstLerp->getPoint(target_x);
    C second = secondLerp.getPoint(target_x);
    return doLerp(first, second, target_y);
  };
};

template<class C>
struct lerp2Points
{
  C preStartPoint;
  C startPoint;
  C endPoint;
  C postEndPoint;

  lerp2Points()
  {
    preStartPoint = new C;
    startPoint = new C;
    endPoint = new C;
    postEndPoint = new C;
  };
};

template<class C>
class lerp2
{
private:
  lerp2Points<C> pointList;
public:
  lerp2()
  {
    pointList = new lerp2Points<C>;
  };
  lerp2(lerp2Points<C> points)
  {
    pointList = points;
  }
  lerp2(C prePoint, C point1, C point2, C postPoint)
  {
    pointList = new lerp2Points<C>;
    pointList.preStartPoint = prePoint;
    pointList.startPoint = point1;
    pointList.endPoint = point2;
    pointList.postEndPoint = postPoint;
  }

  static C doLerp(C start, C end, double target) { return Interpolate::Linear(start, end, target); };
  static C doCosineLerp(C start, C end, double target) { return Interpolate::Cosine(start, end, target); };
  static C doCubicLerp(C prestart, C start, C end, C postend, double target) { return Interpolate::Cubic(prestart, start, end, postend, target); };
  static C doCatmullRomLerp(C prestart, C start, C end, C postend, double target) { return Interpolate::CatmullRom(prestart, start, end, postend, target); };
  static C doHermiteLerp(C prestart, C start, C end, C postend, double target, double tension = 0.0, double bias = 0.0) { return Interpolate::CatmullRom(prestart, start, end, postend, target); };

  C getPoint(double target) { return doLerp(pointList.startPoint, pointList.endPoint, target); }
  C getCosinePoint(double target) { return doCosineLerp(pointList.startPoint, pointList.endPoint, target); }
  C getCubicPoint(double target) { return doCubicLerp(pointList.preStartPoint, pointList.startPoint, pointList.endPoint, pointList.postEndPoint, target); }
  C getCatmullRomPoint(double target) { return doCatmullRomLerp(pointList.preStartPoint, pointList.startPoint, pointList.endPoint, pointList.postEndPoint, target); }
  C getHermitePoint(double target, double tension = 0.0, double bias = 0.0) { return doHermiteLerp(pointList.preStartPoint, pointList.startPoint, pointList.endPoint, pointList.postEndPoint, target, tension, bias); }

  C blerp(lerp2 otherLerp, double target_X, double target_Y) { return doBlerp(this, otherLerp, target_X, target_Y); }
  C bcosinelerp(lerp2 otherLerp, double target_X, double target_Y) { return doBCosinelerp(this, otherLerp, target_X, target_Y); }
  C bcubiclerp(lerp2 preLerp, lerp2 otherLerp, lerp2 postLerp, double target_X, double target_Y) { return doBCubiclerp(preLerp, this, otherLerp, postLerp, target_X, target_Y); }
  C bcatmullromlerp(lerp2 preLerp, lerp2 otherLerp, lerp2 postLerp, double target_X, double target_Y) { return doBCatmullRomlerp(preLerp, this, otherLerp, postLerp, target_X, target_Y); }
  C bhermitelerp(lerp2 preLerp, lerp2 otherLerp, lerp2 postLerp, double target_X, double target_Y, double tension = 0.0, double bias = 0.0) { return doBHermitelerp(preLerp, this, otherLerp, postLerp, target_X, target_Y, tension, bias); }

  static C doBlerp(lerp2 firstLerp, lerp2 secondLerp, double target_X, double target_Y)
  {
    C first = firstLerp.getPoint(target_X);
    C second = secondLerp.getPoint(target_X);
    return doLerp(first, second, target_Y);
  }
  static C doBlerp(lerp2 *firstLerp, lerp2 secondLerp, double target_X, double target_Y)
  {
    C first = firstLerp->getPoint(target_X);
    C second = secondLerp.getPoint(target_X);
    return doLerp(first, second, target_Y);
  }
  static C doBCosinelerp(lerp2 firstLerp, lerp2 secondLerp, double target_X, double target_Y)
  {
    C first = firstLerp.getCosinePoint(target_X);
    C second = secondLerp.getCosinePoint(target_X);
    return doCosineLerp(first, second, target_Y);
  }
  static C doBCubiclerp(lerp2 preLerp, lerp2 firstLerp, lerp2 secondLerp, lerp2 postLerp, double target_X, double target_Y)
  {
    C pre = firstLerp.getCubicPoint(target_X);
    C first = firstLerp.getCubicPoint(target_X);
    C second = secondLerp.getCubicPoint(target_X);
    C post = secondLerp.getCubicPoint(target_X);
    return doCubicLerp(pre, first, second, post, target_Y);
  }
  static C doBCatmullRomlerp(lerp2 preLerp, lerp2 firstLerp, lerp2 secondLerp, lerp2 postLerp, double target_X, double target_Y)
  {
    C pre = firstLerp.getCatmullRomPoint(target_X);
    C first = firstLerp.getCatmullRomPoint(target_X);
    C second = secondLerp.getCatmullRomPoint(target_X);
    C post = secondLerp.getCatmullRomPoint(target_X);
    return doCatmullRomLerp(pre, first, second, post, target_Y);
  }
  static C doBHermitelerp(lerp2 preLerp, lerp2 firstLerp, lerp2 secondLerp, lerp2 postLerp, double target_X, double target_Y, double tension = 0.0, double bias = 0.0)
  {
    C pre = firstLerp.getHermitePoint(target_X, tension, bias);
    C first = firstLerp.getHermitePoint(target_X, tension, bias);
    C second = secondLerp.getHermitePoint(target_X, tension, bias);
    C post = secondLerp.getHermitePoint(target_X, tension, bias);
    return doHermiteLerp(pre, first, second, post, target_Y, tension, bias);
  }
};