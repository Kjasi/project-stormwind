#include "ExportOptions.h"

void ExportVariables::setVariables(QString data)
{
  QJsonObject a = getJsonObject(data);

  inputDir.setPath(a.value("inputdir").toString());
  outputDir.setPath(a.value("outputdir").toString());

  exportOptions.setVariables(a.value("exportOptions").toString());
}

QString ExportVariables::getVariables()
{
  QJsonObject a = QJsonObject();

  a.insert("inputdir", inputDir.absolutePath());
  a.insert("outputdir", outputDir.absolutePath());

  a.insert("exportOptions", exportOptions.getVariables());

  return getJsonString(a);
}

void ExportOptions::setVariables(QString data)
{
  QJsonObject a = getJsonObject(data);

  mirrorWoWFolders = a.value("mirrorWoW").toBool();
  exportAllTextures = a.value("exportAllBLPs").toBool();
  continueOnError = a.value("continueOnError").toBool();
  includeSubdirectories = a.value("includeSubdirectories").toBool();
  overwriteExistingFiles = a.value("overwriteExistingFiles").toBool();
  combineMeshes = a.value("combineMeshes").toBool();

  TextureFormat = a.value("TextureFormat").toInt();
  ModelFormat = a.value("ModelFormat").toInt();
  PlacementFormat = a.value("PlacementFormat").toInt();

  exportAllM2s = a.value("exportAllM2s").toBool();
  M2ExportMesh = a.value("M2ExportMesh").toBool();
  M2ExportTextures = a.value("M2ExportTextures").toBool();
  M2HighestRes = a.value("M2HighestRes").toBool();
  M2ExportAllLODs = a.value("M2ExportAllLODs").toBool();

  exportAllWMOs = a.value("exportAllWMOs").toBool();
  WMOExportMesh = a.value("WMOExportMesh").toBool();
  WMOExportTextures = a.value("WMOExportTextures").toBool();
  WMOExportSubmodels = a.value("WMOExportSubmodels").toBool();
  WMOExportPlacement = a.value("WMOExportPlacement").toBool();

  exportAllADTs = a.value("exportAllADTs").toBool();
  ADTExportMesh = a.value("ADTExportMesh").toBool();
  ADTExportTextures = a.value("ADTExportTextures").toBool();
  ADTExportSubmodels = a.value("ADTExportSubmodels").toBool();
  ADTExportPlacement = a.value("ADTExportPlacement").toBool();
  ADTExportMasks = a.value("ADTExportMasks").toBool();
  ADTSingleMaterial = a.value("ADTSingleMaterial").toBool();
  ADTBakeSingleTextures = a.value("ADTBakeSingleTextures").toBool();

  ADTExportHeightmap = a.value("ADTExportHeightmap").toBool();
  ADTHeightmapFormat = a.value("ADTHeightmapFormat").toInt();
  ADTHeightmapUseMiddlepoint = a.value("ADTHeightmapUseMiddlepoint").toBool();
}

QString ExportOptions::getVariables()
{
  QJsonObject a = QJsonObject();

  a.insert("mirrorWoW", mirrorWoWFolders);
  a.insert("exportAllBLPs", exportAllTextures);
  a.insert("continueOnError", continueOnError);
  a.insert("includeSubdirectories", includeSubdirectories);
  a.insert("overwriteExistingFiles", overwriteExistingFiles);
  a.insert("combineMeshes", combineMeshes);

  a.insert("TextureFormat", TextureFormat);
  a.insert("ModelFormat", ModelFormat);
  a.insert("PlacementFormat", PlacementFormat);

  a.insert("exportAllM2s", exportAllM2s);
  a.insert("M2ExportMesh", M2ExportMesh);
  a.insert("M2ExportTextures", M2ExportTextures);
  a.insert("M2HighestRes", M2HighestRes);
  a.insert("M2ExportAllLODs", M2ExportAllLODs);

  a.insert("exportAllWMOs", exportAllWMOs);
  a.insert("WMOExportMesh", WMOExportMesh);
  a.insert("WMOExportTextures", WMOExportTextures);
  a.insert("WMOExportSubmodels", WMOExportSubmodels);
  a.insert("WMOExportPlacement", WMOExportPlacement);

  a.insert("exportAllADTs", exportAllADTs);
  a.insert("ADTExportMesh", ADTExportMesh);
  a.insert("ADTExportTextures", ADTExportTextures);
  a.insert("ADTExportSubmodels", ADTExportSubmodels);
  a.insert("ADTExportPlacement", ADTExportPlacement);
  a.insert("ADTExportMasks", ADTExportMasks);
  a.insert("ADTSingleMaterial", ADTSingleMaterial);
  a.insert("ADTBakeSingleTextures", ADTBakeSingleTextures);

  a.insert("ADTExportHeightmap", ADTExportHeightmap);
  a.insert("ADTHeightmapFormat", ADTHeightmapFormat);
  a.insert("ADTHeightmapResolution", ADTHeightmapResolution);
  a.insert("ADTHeightmapUseMiddlepoint", ADTHeightmapUseMiddlepoint);

  return getJsonString(a);
}
