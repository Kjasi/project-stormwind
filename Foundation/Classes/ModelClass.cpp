#include "ModelClass.h"
#include "../Functions.h"
#include "../LogFile.h"
#include "../Settings.h"
#include <QtCore/QTime>
#include <QtCore/qmap.h>
#include <QtCore/qvector.h>
#include <QtCore/qrandom.h>

ModelClass::~ModelClass()
{
  delete modelScene;
  delete modelObject;
  delete collisionObject;
  delete placementData;
  delete scene;
}

void ModelClass::buildVulkanData(Vec3D offset)
{
  if (modelScene == nullptr)
  {
    buildModelObject();
  }
  if (modelScene == nullptr || modelScene->Objects.count() <= 0)
  {
    LOGFILE.Error("Unable to build Vulkan data. No Model Object built.");
    scene = nullptr;	// Ensure that there's no existing Vulkan data to send.
    return;
  }

  LOGFILE.Info("Building Vulkan Model Data...");
  scene = new VulkanSceneData;
  VulkanModelData *model = new VulkanModelData;
  BoundingBox bbox;

  if (SETTINGS.value("disablerenderer", false).toBool() == true) return;

  for (uint32_t mO = 0; mO < (uint32_t)modelScene->Objects.count(); mO++)
  {
    ModelObject *modelobject = modelScene->Objects.at(mO);

    bbox.updateMinAndMax(modelobject->boundingBox);
    LOGFILE.Debug("BoundingBox Min: %lf %lf %lf | Max: %lf %lf %lf | Len: %lf", bbox.min.x, bbox.min.y, bbox.min.z, bbox.max.x, bbox.max.y, bbox.max.z, bbox.getLength());

    // Generate our Materials
    double matStart = getCurrentTime();
    QList<MaterialData> matList;
    for (uint32_t m = 0; m < (uint32_t)modelobject->materialList.count(); m++)
    {
      ModelMaterial mat = modelobject->materialList.at(m);
      MaterialData matData;

      matData.name = mat.Name;
      matData.ambientColor = glm::vec4(0.25f);
      matData.diffuseColor = glm::vec4(1.0f);
      matData.specularColor = glm::vec4(1.0f);
      matData.alphaAsSpecular = mat.useAlphaAsSpecular;

      if (SETTINGS.value("disablerendertextures", false).toBool() == false)
      {
        TextureGroup* grp = new TextureGroup;
        if (mat.ColorTextureID > -1 && modelobject->ImageList.count() > 0 && modelobject->ImageList.at(mat.ColorTextureID) != NULL)
          grp->diffuse = *modelobject->ImageList.at(mat.ColorTextureID);
        if (mat.SpecularTextureID > -1 && modelobject->ImageList.count() > 0 && modelobject->ImageList.at(mat.SpecularTextureID) != NULL)
          grp->specular = *modelobject->ImageList.at(mat.SpecularTextureID);

        matData.textureGroupID = model->textureList->count();
        model->textureList->push_back(grp);
      }

      matData.opacity = 1.0f;

      matList.push_back(matData);
    }
    model->materials = matList;
    double matEnd = getCurrentTime();
    LOGFILE.Debug("Number of Materials: %i", matList.count());

    QVector<QList<uint64_t>> indiceMap;
    indiceMap.resize(matList.count());
    indiceMap.fill(QList<uint64_t>());

    // Build our full vert list!
    double vertStart = getCurrentTime();
    QVector<Vertex> originalVertList;
    for (int ml = 0; ml < modelobject->MeshList.count(); ml++)
    {
      ModelMeshData *modelData = modelobject->MeshList.at(ml);
      LOGFILE.Debug("Processing Model #%i/%i", ml, modelobject->MeshList.count() - 1);

      double colorStart = getCurrentTime();
      // Assign a random color to the wireframes
      double mdl_r = 1.0f;
      double mdl_g = 1.0f;
      double mdl_b = 1.0f;
      if (ml == 0)
      {
        mdl_r = 1.0;
        mdl_g = 1.0;
        mdl_b = 1.0;
      }
      else {
        do {
          QRandomGenerator qr;
          qr.seed((uint)QTime::currentTime().msec());
          qr.generate();
          mdl_r = (double)(qr.generate() % ((255 + 1) - 60) + 60) / 255.0;
          mdl_g = (double)(qr.generate() % ((255 + 1) - 60) + 60) / 255.0;
          mdl_b = (double)(qr.generate() % ((255 + 1) - 60) + 60) / 255.0;
        } while (mdl_r == 1.0 && mdl_g == 1.0 && mdl_b == 1.0);
      }
      LOGFILE.Debug("Model Color: %f, %f, %f", mdl_r, mdl_g, mdl_b);
      double colorEnd = getCurrentTime();

      double vertsStart = getCurrentTime();
      LOGFILE.Debug("Points Count: %i", modelData->pointList.count());
      for (uint32_t i = 0; i < (uint32_t)modelData->pointList.count(); i++)
      {
        ModelPoint p = modelData->pointList.at(i);
        Vertex v;
        v.Position = { (float)(p.Position.x + offset.x), -(float)(p.Position.y + offset.y), (float)(p.Position.z + offset.z) };
        //LOGFILE.Debug("Position: %f %f %f", v.Position.x, v.Position.y, v.Position.z);
        v.Color = { (float)p.VertexColor.R, (float)p.VertexColor.G, (float)p.VertexColor.B };
        //LOGFILE.Debug("Vertex Color: %f %f %f", v.Color.x, v.Color.y, v.Color.z);
        v.Normal = { (float)p.NormalVector.x, -(float)p.NormalVector.y, (float)p.NormalVector.z };
        //LOGFILE.Debug("Normal Vector: %f %f %f", v.Normal.x, v.Normal.y, v.Normal.z);
        v.uvCoords = { (float)p.UVPosition.x, -(float)p.UVPosition.y };
        //LOGFILE.Debug("UV Coords: %f %f", v.uvCoords.x, v.uvCoords.y);
        v.WireColor = { (float)mdl_r, (float)mdl_g, (float)mdl_b };

        originalVertList.push_back(v);
      }
      double vertsEnd = getCurrentTime();

      double polyStart = getCurrentTime();
      for (uint32_t i = 0; i < (uint32_t)modelData->polygonList.count(); i++)
      {
        ModelPolygon p = modelData->polygonList.at(i);

        if ((int)p.materialID >= indiceMap.count()) continue;
        QList<uint64_t> indexes = indiceMap.takeAt(p.materialID);

        uint64_t p0 = p.point0;
        uint64_t p1 = p.point1;
        uint64_t p2 = p.point2;

        indexes.push_back(p0);
        indexes.push_back(p1);
        indexes.push_back(p2);

        indiceMap.insert(p.materialID, indexes);
      }
      double polyEnd = getCurrentTime();

      //LOGFILE.Debug("Colors: %.3f, Verts: %.3f, Polys: %.3f, Total: %.3f", (colorEnd - colorStart), (vertsEnd - vertsStart), (polyEnd - polyStart), (colorEnd - colorStart) + (vertsEnd - vertsStart) + (polyEnd - polyStart));
    }
    model->Verticies = originalVertList;
    double vertEnd = getCurrentTime();

    double meshStart = getCurrentTime();
    for (int ml = 0; ml < modelobject->MeshList.count(); ml++)
    {
      ModelMeshData *modelData = modelobject->MeshList.at(ml);
      LOGFILE.Debug("Model %i: Points: %i, Polys: %i", ml, modelData->pointList.count(), modelData->polygonList.count());

      int matIndices = 0;

      for (int mt = 0; mt < matList.count(); mt++)
      {
        QList<uint64_t> indexList = indiceMap.value(mt);

        PartData vertData;
        vertData.materialID = mt;
        vertData.partName = (modelData->originalFilename.isEmpty() == false ? modelData->originalFilename : modelData->groupName);
        vertData.indices = indexList.toVector();
        vertData.baseOffset = matIndices;
        matIndices += indexList.count();
        model->meshData.push_back(vertData);
      }
    }
    double meshEnd = getCurrentTime();

    double finalStart = getCurrentTime();
    scene->addModel(*model);
    double finalEnd = getCurrentTime();

    //LOGFILE.Debug("Materials: %.3f, Verts: %.3f, Meshes: %.3f, Final: %.3f, Total: %.3f", (matEnd - matStart), (vertEnd - vertStart), (meshEnd - meshStart), (finalEnd - finalStart), (matEnd - matStart) + (vertEnd - vertStart) + (meshEnd - meshStart) + (finalEnd - finalStart));

  }

  // Assemble!
  Vec3D centerPoint = Vec3D((bbox.min + bbox.max) / Vec3D(2.0, 2.0, 2.0));
  LOGFILE.Debug("CenterPoint: %f %f %f", centerPoint.x, centerPoint.y, centerPoint.z);
  scene->rotation = { 0.0f, 25.0f, 0.0f };
  scene->cameraPos = { 0.0f, centerPoint.y, 0.0f };
  scene->zoom = -(bbox.getLength() / 1.5) / tan(45.0f / 2.0f);

  scene->moveScale = 4.0f;
  scene->zoomScale = 0.1f * bbox.getLength();
}