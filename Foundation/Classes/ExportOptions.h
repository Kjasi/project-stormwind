#pragma once
#include <QtCore/QDir>
#include "JsonSettings.hpp"

struct ExportOptions : public JsonSetting
{
  bool continueOnError = false;
  bool mirrorWoWFolders = true;
  bool includeSubdirectories = true;
  bool combineMeshes = false;
  bool overwriteExistingFiles = false;
  bool exportAllTextures = false;

  int TextureFormat = 0;
  int ModelFormat = 0;
  int PlacementFormat = 0;

  bool exportAllM2s = true;
  bool M2ExportMesh = true;
  bool M2ExportTextures = true;
  bool M2HighestRes = true;
  bool M2ExportAllLODs = false;

  bool exportAllWMOs = true;
  bool WMOExportMesh = true;
  bool WMOExportTextures = true;
  bool WMOExportSubmodels = true;
  bool WMOExportPlacement = true;

  bool exportAllADTs = true;
  bool ADTExportMesh = true;
  bool ADTExportTextures = true;
  bool ADTExportSubmodels = true;
  bool ADTExportPlacement = true;
  bool ADTExportMasks = true;
  bool ADTSingleMaterial = false;
  bool ADTBakeSingleTextures = false;

  bool ADTExportHeightmap = true;
  int ADTHeightmapFormat = 0;
  int ADTHeightmapResolution = 0;
  bool ADTHeightmapUseMiddlepoint = true;

  void setVariables(QString data);
  QString getVariables();
};

struct ExportVariables : public JsonSetting
{
  QDir wowDir;
  QDir inputDir;
  QDir outputDir;
  QString wowAppLocation;
  QString wowAppFilename;
  QString wowAppFilePath;

  ExportOptions exportOptions;

  void setVariables(QString data);
  QString getVariables();
};

struct ExportQuickSelection
{
  QString Name;
  ExportOptions exportOptions;
};