#pragma once

class QDir;
enum Expansion;

class cWoWVersion
{
public:
  int Major = 0;
  int Minor = 0;
  int Patch = 0;
  int Build = 0;
  QString ProductName;

  int lastKnownLiveBuild = 3980;          // Defaults to first Live build of Vanilla. Should be set automatically once WoWVersion is initialized.
  int lastKnownLiveClassicBuild = 31446;  // Defaults to first Live build of Classic. Should be set automatically once WoWVersion is initialized.

  cWoWVersion(int major = 0, int minor = 0, int patch = 0, int build = 0);

  bool isUnknown();
  bool isEmpty() { return isUnknown(); }
  bool isBlank() { return isUnknown(); }
  bool isExpansion(Expansion expansion);
  bool isPre(Expansion expansion);
  bool isPost(Expansion expansion);
  bool isMPQUser();
  bool isCASCUser();
  bool isSupportedBuild();
  bool isValidExpansion();
  bool isKnownLiveBuild();
  QList<uint32_t> getKnownLiveBuilds() { return knownLiveBuilds; };
  QList<uint32_t> getKnownLiveClassicBuilds() { return knownLiveClassicBuilds; };
  Expansion getExpansion();

  QString getString();

  void buildWoWVersion(QDir pathToExe, bool &show_warning);
  void buildWoWVersion(QString pathToExe, bool &show_warning);
private:
  QList<uint32_t> knownLiveBuilds;
  QList<uint32_t> knownLiveClassicBuilds;
  void buildKnownLiveBuildsList();
  void buildKnownLiveClassicBuildsList();
};