#pragma once
#include "glm/glm.hpp"
#include <QtGui/qvulkanfunctions.h>
#include <array>

struct Vertex
{
  glm::vec3 Position;
  glm::vec3 Color;
  glm::vec2 uvCoords;
  glm::vec3 Normal;
  glm::vec3 WireColor;

  bool operator ==(const Vertex& second) const
  {
    return (Position == second.Position) && (Color == second.Color) && (uvCoords == second.uvCoords) && (Normal == second.Normal) && (WireColor == second.WireColor);
  }
  bool operator ==(const Vertex* second) const
  {
    return (Position == second->Position) && (Color == second->Color) && (uvCoords == second->uvCoords) && (Normal == second->Normal) && (WireColor == second->WireColor);
  }

  static VkVertexInputBindingDescription getBindingDescription()
  {
    VkVertexInputBindingDescription bindingDescription = {};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(Vertex);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    return bindingDescription;
  }

  static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions()
  {
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions = {};
    attributeDescriptions.resize(5);

    attributeDescriptions[0].binding = 0;
    attributeDescriptions[0].location = 0;
    attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[0].offset = offsetof(Vertex, Position);

    attributeDescriptions[1].binding = 0;
    attributeDescriptions[1].location = 1;
    attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[1].offset = offsetof(Vertex, Color);

    attributeDescriptions[2].binding = 0;
    attributeDescriptions[2].location = 2;
    attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
    attributeDescriptions[2].offset = offsetof(Vertex, uvCoords);

    attributeDescriptions[3].binding = 0;
    attributeDescriptions[3].location = 3;
    attributeDescriptions[3].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[3].offset = offsetof(Vertex, Normal);

    attributeDescriptions[4].binding = 0;
    attributeDescriptions[4].location = 4;
    attributeDescriptions[4].format = VK_FORMAT_R32G32B32_SFLOAT;
    attributeDescriptions[4].offset = offsetof(Vertex, WireColor);

    return attributeDescriptions;
  }
};