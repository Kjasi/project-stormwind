#pragma once
#include <QtCore/qstringlist.h>
#include <QtCore/qlist.h>

enum RGBAColorType
{
  Grey = 1,
  GreyAlpha,
  RGB,
  RGBA
};

enum RGBAColorID
{
  COLORID_RED = 0,
  COLORID_GREEN,
  COLORID_BLUE,
  COLORID_ALPHA,

  COLORID_RGBONLY,
  COLORID_ALL,
};

class RGBAColor
{
  struct RGBIntColor
  {
    int R, G, B, A;
  };

public:
  double R, G, B, A;

  RGBAColor(double iR = 0.0, double iG = 0.0, double iB = 0.0, double iA = 1.0) {
    R = iR;
    G = iG;
    B = iB;
    A = iA;
  }
  RGBAColor(float iR, float iG = 0.0f, float iB = 0.0f, float iA = 1.0f) {
    R = iR;
    G = iG;
    B = iB;
    A = iA;
  }
  RGBAColor(int iR, int iG, int iB, int iA = 255) {
    R = double(iR) / 255.0;
    G = double(iG) / 255.0;
    B = double(iB) / 255.0;
    A = double(iA) / 255.0;
  }
  RGBAColor(unsigned int iR, unsigned int iG, unsigned int iB, unsigned int iA = 255) {
    R = double(iR) / 255.0;
    G = double(iG) / 255.0;
    B = double(iB) / 255.0;
    A = double(iA) / 255.0;
  }

  void setColors(double iR = 0.0, double iG = 0.0, double iB = 0.0, double iA = 1.0)
  {
    R = iR;
    G = iG;
    B = iB;
    A = iA;
  }
  void setColors(float iR, float iG = 0.0, float iB = 0.0, float iA = 1.0)
  {
    R = iR;
    G = iG;
    B = iB;
    A = iA;
  }
  void setColors(int iR, int iG, int iB, int iA = 255)
  {
    R = double(iR) / 255.0;
    G = double(iG) / 255.0;
    B = double(iB) / 255.0;
    A = double(iA) / 255.0;
  }
  void setColors(unsigned int iR, unsigned int iG, unsigned int iB, unsigned int iA = 255)
  {
    R = double(iR) / 255.0;
    G = double(iG) / 255.0;
    B = double(iB) / 255.0;
    A = double(iA) / 255.0;
  }
  void setColors(RGBAColor c)
  {
    R = c.R;
    G = c.G;
    B = c.B;
    A = c.A;
  }
  void setColors(RGBAColor *c)
  {
    R = c->R;
    G = c->G;
    B = c->B;
    A = c->A;
  }

  void setColorRed(double value)
  {
    R = value;
  }
  void setColorGreen(double value)
  {
    G = value;
  }
  void setColorBlue(double value)
  {
    B = value;
  }
  void setColorAlpha(double value)
  {
    A = value;
  }

  void setColorRed(float value)
  {
    R = value;
  }
  void setColorGreen(float value)
  {
    G = value;
  }
  void setColorBlue(float value)
  {
    B = value;
  }
  void setColorAlpha(float value)
  {
    A = value;
  }

  void setColorRed(int value)
  {
    R = double(value) / 255.0;;
  }
  void setColorGreen(int value)
  {
    G = double(value) / 255.0;;
  }
  void setColorBlue(int value)
  {
    B = double(value) / 255.0;;
  }
  void setColorAlpha(int value)
  {
    A = double(value) / 255.0;
  }

  RGBAColor& setColorRed(RGBAColor value)
  {
    R = value.R;
    return *this;
  }
  RGBAColor& setColorGreen(RGBAColor value)
  {
    G = value.G;
    return *this;
  }
  RGBAColor& setColorBlue(RGBAColor value)
  {
    B = value.B;
    return *this;
  }
  RGBAColor& setColorAlpha(RGBAColor value)
  {
    A = value.A;
    return *this;
  }

  void setColorRed(RGBAColor *value)
  {
    R = value->R;
  }
  void setColorGreen(RGBAColor *value)
  {
    G = value->G;
  }
  void setColorBlue(RGBAColor *value)
  {
    B = value->B;
  }
  void setColorAlpha(RGBAColor *value)
  {
    A = value->A;
  }

  RGBAColor& clamp(double min = 0.0, double max = 1.0)
  {
    if (R < min) { R = min; }
    if (G < min) { G = min; }
    if (B < min) { B = min; }
    if (A < min) { A = min; }

    if (R > max) { R = max; }
    if (G > max) { G = max; }
    if (B > max) { B = max; }
    if (A > max) { A = max; }
    return *this;
  }

  // Convert from RGBA to BGRA, or vice-versa.
  RGBAColor invert() {
    double tempr = R;
    R = B;
    B = tempr;
    return *this;
  }

  // Averages the RGB colors, doesn't touch the Alpha channel
  void equalizeRGB()
  {
    double l = R;
    l += G + B;
    l = l / 3;
    R = G = B = l;
  }

  // Averages all the channels
  void equalizeRGBA()
  {
    double l = R;
    l += G + B + A;
    l = l / 4;
    R = G = B = A = l;
  }

  RGBIntColor toInts() {
    RGBIntColor colors;
    int color = (int)(R * 255);
    colors.R = color;
    color = (int)(G * 255);
    colors.G = color;
    color = (int)(B * 255);
    colors.B = color;
    color = (int)(A * 255);
    colors.A = color;

    return colors;
  }

  QString toString()
  {
    QString s = QString("%1,%2,%3,%4").arg(R).arg(G).arg(B).arg(A);
    return s;
  }

  static RGBAColor fromString(QString input)
  {
    RGBAColor c;
    QStringList sl = input.split(",");
    c.R = sl.at(0).toDouble();
    c.G = sl.at(1).toDouble();
    c.B = sl.at(2).toDouble();
    c.A = sl.at(3).toDouble();
    return c;
  }

  RGBAColor& operator= (const RGBAColor &v) {
    R = v.R;
    G = v.G;
    B = v.B;
    A = v.A;
    return *this;
  }

  RGBAColor operator+(RGBAColor &other) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R += other.R;
    a.G += other.G;
    a.B += other.B;
    a.A += other.A;
    return a;
  }
  RGBAColor operator-(RGBAColor &other) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R -= other.R;
    a.G -= other.G;
    a.B -= other.B;
    a.A -= other.A;
    return a;
  }
  RGBAColor operator*(RGBAColor &other) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R *= other.R;
    a.G *= other.G;
    a.B *= other.B;
    a.A *= other.A;
    return a;
  }
  RGBAColor operator/(RGBAColor &other) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R /= other.R;
    a.G /= other.G;
    a.B /= other.B;
    a.A /= other.A;
    return a;
  }
  void operator+=(const RGBAColor &other)
  {
    R += other.R;
    G += other.G;
    B += other.B;
    A += other.A;
  }
  void operator-=(const RGBAColor &other)
  {
    R -= other.R;
    G -= other.G;
    B -= other.B;
    A -= other.A;
  }
  void operator*=(const RGBAColor &other)
  {
    R *= other.R;
    G *= other.G;
    B *= other.B;
    A *= other.A;
  }
  void operator/=(const RGBAColor &other)
  {
    R /= other.R;
    G /= other.G;
    B /= other.B;
    A /= other.A;
  }
  RGBAColor operator*(float multiplier) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R *= multiplier;
    a.G *= multiplier;
    a.B *= multiplier;
    a.A *= multiplier;
    return a;
  }
  RGBAColor operator*(double multiplier) const
  {
    RGBAColor a = RGBAColor(R, G, B, A);
    a.R *= multiplier;
    a.G *= multiplier;
    a.B *= multiplier;
    a.A *= multiplier;
    return a;
  }
  void operator*=(float multiplier)
  {
    R *= multiplier;
    G *= multiplier;
    B *= multiplier;
    A *= multiplier;
  }
  void operator*=(double multiplier)
  {
    R *= multiplier;
    G *= multiplier;
    B *= multiplier;
    A *= multiplier;
  }

  bool operator ==(const RGBAColor& second) const
  {
    return (R == second.R) && (G == second.G) && (B == second.B) && (A == second.A);
  }

  bool operator ==(const RGBAColor* second) const
  {
    return (R == second->R) && (G == second->G) && (B == second->B) && (A == second->A);
  }
};

class RGBArray
{
  QList<RGBAColor> colorList;
public:
  void setArray(QList<RGBAColor> newArray) { colorList = newArray; }
  QList<RGBAColor> getArray() { return colorList; }
  uint32_t getCount() { return colorList.count(); }
  RGBAColor getAt(uint32_t Id) { return colorList.at(Id); }
  void addTo(RGBAColor color) { colorList.push_back(color); }
  void addTo(QList<RGBAColor> colors) { colorList.append(colors); }
  void addTo(RGBArray colors) { colorList.append(colors.colorList); }
  void clear() { colorList.clear(); }

  RGBArray& operator= (const RGBArray &v) {
    colorList = v.colorList;
    return *this;
  }

  RGBArray operator+(RGBArray &other) const
  {
    RGBArray a;
    a.setArray(colorList);
    for (int32_t i = 0; i < a.colorList.count(); i++)
    {
      a.colorList.value(i) += other.colorList.at(i);
    }
    return a;
  }
  RGBArray operator-(RGBArray &other) const
  {
    RGBArray a;
    a.setArray(colorList);
    for (int32_t i = 0; i < a.colorList.count(); i++)
    {
      a.colorList.value(i) -= other.colorList.at(i);
    }
    return a;
  }
  RGBArray operator*(RGBArray &other) const
  {
    RGBArray a;
    a.setArray(colorList);
    for (int32_t i = 0; i < a.colorList.count(); i++)
    {
      a.colorList.value(i) *= other.colorList.at(i);
    }
    return a;
  }
  RGBArray operator/(RGBArray &other) const
  {
    RGBArray a;
    a.setArray(colorList);
    for (int32_t i = 0; i < a.colorList.count(); i++)
    {
      a.colorList.value(i) /= other.colorList.at(i);
    }
    return a;
  }

  void operator+=(const RGBArray &other)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.value(i) += other.colorList.at(i);
    }
  }
  void operator-=(const RGBArray &other)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.value(i) -= other.colorList.at(i);
    }
  }
  void operator*=(const RGBArray &other)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.value(i) *= other.colorList.at(i);
    }
  }
  void operator/=(const RGBArray &other)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.value(i) /= other.colorList.at(i);
    }
  }

  bool operator ==(const RGBArray& second) const
  {
    return (colorList.count() == second.colorList.count()) && (colorList == second.colorList);
  }

  bool operator ==(const RGBArray* second) const
  {
    return (colorList.count() == second->colorList.count()) && (colorList == second->colorList);
  }

  RGBArray& setColor(RGBArray value)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.operator[](i).setColors(value.colorList.at(i));
    }
    return *this;
  }
  RGBArray& setColorRed(RGBArray value)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.operator[](i).setColorRed(value.colorList.at(i).R);
    }
    return *this;
  }
  RGBArray& setColorGreen(RGBArray value)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.operator[](i).setColorGreen(value.colorList.at(i).G);
    }
    return *this;
  }
  RGBArray& setColorBlue(RGBArray value)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.operator[](i).setColorBlue(value.colorList.at(i).B);
    }
    return *this;
  }
  RGBArray& setColorAlpha(RGBArray value)
  {
    for (int32_t i = 0; i < colorList.count(); i++)
    {
      colorList.operator[](i).setColorAlpha(value.colorList.at(i).A);
    }
    return *this;
  }

  RGBArray& setColor(int index, RGBAColor color)
  {
    colorList.operator[](index).setColors(color);
    return *this;
  }
  RGBArray& setColorRed(int index, RGBAColor color)
  {
    colorList.operator[](index).setColorRed(color.R);
    return *this;
  }
  RGBArray& setColorGreen(int index, RGBAColor color)
  {
    colorList.operator[](index).setColorGreen(color.G);
    return *this;
  }
  RGBArray& setColorBlue(int index, RGBAColor color)
  {
    colorList.operator[](index).setColorBlue(color.B);
    return *this;
  }
  RGBArray& setColorAlpha(int index, RGBAColor color)
  {
    colorList.operator[](index).setColorAlpha(color.A);
    return *this;
  }

  RGBArray& setData(int offset, RGBArray newData)
  {
    for (uint32_t i = 0; i < newData.getCount(); i++)
    {
      colorList.operator[](offset + i).setColors(newData.getAt(i));
    }
  }
};