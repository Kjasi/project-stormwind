#include <QtCore/qjsondocument.h>
#include <QtCore/qjsonobject.h>

class JsonSetting
{
public:
  virtual void setVariables(QString data) = 0;
  virtual QString getVariables() = 0;

protected:
  QJsonObject getJsonObject(QString data)
  {
    QJsonDocument doc = QJsonDocument::fromJson(data.toUtf8());
    return doc.object();
  }

  QString getJsonString(QJsonObject object)
  {
    return QJsonDocument(object).toJson();
  }
};