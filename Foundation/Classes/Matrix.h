#pragma once
#include "Vectors.h"

class Matrix4D
{
public:
  double m[4][4];

  /* Constructors */

  Matrix4D() {}

  /* Operators */

  Vec3D operator* (Vec3D v)
  {
    Vec3D o;
    o.x = m[0][0] * v.x + m[0][1] * v.y + m[0][2] * v.z + m[0][3];
    o.y = m[1][0] * v.x + m[1][1] * v.y + m[1][2] * v.z + m[1][3];
    o.z = m[2][0] * v.x + m[2][1] * v.y + m[2][2] * v.z + m[2][3];
    return o;
  }

  Matrix4D operator* (Matrix4D p)
  {
    Matrix4D o;
    o.m[0][0] = m[0][0] * p.m[0][0] + m[0][1] * p.m[1][0] + m[0][2] * p.m[2][0] + m[0][3] * p.m[3][0];
    o.m[0][1] = m[0][0] * p.m[0][1] + m[0][1] * p.m[1][1] + m[0][2] * p.m[2][1] + m[0][3] * p.m[3][1];
    o.m[0][2] = m[0][0] * p.m[0][2] + m[0][1] * p.m[1][2] + m[0][2] * p.m[2][2] + m[0][3] * p.m[3][2];
    o.m[0][3] = m[0][0] * p.m[0][3] + m[0][1] * p.m[1][3] + m[0][2] * p.m[2][3] + m[0][3] * p.m[3][3];

    o.m[1][0] = m[1][0] * p.m[0][0] + m[1][1] * p.m[1][0] + m[1][2] * p.m[2][0] + m[1][3] * p.m[3][0];
    o.m[1][1] = m[1][0] * p.m[0][1] + m[1][1] * p.m[1][1] + m[1][2] * p.m[2][1] + m[1][3] * p.m[3][1];
    o.m[1][2] = m[1][0] * p.m[0][2] + m[1][1] * p.m[1][2] + m[1][2] * p.m[2][2] + m[1][3] * p.m[3][2];
    o.m[1][3] = m[1][0] * p.m[0][3] + m[1][1] * p.m[1][3] + m[1][2] * p.m[2][3] + m[1][3] * p.m[3][3];

    o.m[2][0] = m[2][0] * p.m[0][0] + m[2][1] * p.m[1][0] + m[2][2] * p.m[2][0] + m[2][3] * p.m[3][0];
    o.m[2][1] = m[2][0] * p.m[0][1] + m[2][1] * p.m[1][1] + m[2][2] * p.m[2][1] + m[2][3] * p.m[3][1];
    o.m[2][2] = m[2][0] * p.m[0][2] + m[2][1] * p.m[1][2] + m[2][2] * p.m[2][2] + m[2][3] * p.m[3][2];
    o.m[2][3] = m[2][0] * p.m[0][3] + m[2][1] * p.m[1][3] + m[2][2] * p.m[2][3] + m[2][3] * p.m[3][3];

    o.m[3][0] = m[3][0] * p.m[0][0] + m[3][1] * p.m[1][0] + m[3][2] * p.m[2][0] + m[3][3] * p.m[3][0];
    o.m[3][1] = m[3][0] * p.m[0][1] + m[3][1] * p.m[1][1] + m[3][2] * p.m[2][1] + m[3][3] * p.m[3][1];
    o.m[3][2] = m[3][0] * p.m[0][2] + m[3][1] * p.m[1][2] + m[3][2] * p.m[2][2] + m[3][3] * p.m[3][2];
    o.m[3][3] = m[3][0] * p.m[0][3] + m[3][1] * p.m[1][3] + m[3][2] * p.m[2][3] + m[3][3] * p.m[3][3];

    return o;
  }

  Matrix4D& operator*= (Matrix4D p)
  {
    return *this = this->operator*(p);
  }

  /* Core Functions */

  void zero()
  {
    for (size_t x = 0; x < 4; x++)
    {
      for (size_t y = 0; y < 4; y++)
      {
        m[x][y] = 0.0;
      }
    }
  }

  void unit()
  {
    zero();
    m[0][0] = m[1][1] = m[2][2] = m[3][3] = 1.0;
  }

  Matrix4D identity()
  {
    Matrix4D mi;
    mi.unit();
    return mi;
  }

  double determinant()
  {
    #define SUB(a,b) (m[2][a]*m[3][b] - m[3][a]*m[2][b])
    double d = m[0][0] * (m[1][1] * SUB(2, 3) - m[1][2] * SUB(1, 3) + m[1][3] * SUB(1, 2))
      - m[0][1] * (m[1][0] * SUB(2, 3) - m[1][2] * SUB(0, 3) + m[1][3] * SUB(0, 2))
      + m[0][2] * (m[1][0] * SUB(1, 3) - m[1][1] * SUB(0, 3) + m[1][3] * SUB(0, 1))
      - m[0][3] * (m[1][0] * SUB(1, 2) - m[1][1] * SUB(0, 2) + m[1][2] * SUB(0, 1));
    #undef SUB
    return d;
  }

  double minor(size_t x, size_t y)
  {
    double s[3][3];
    for (size_t j = 0, v = 0; j<4; j++) {
      if (j == y) continue;
      for (size_t i = 0, u = 0; i<4; i++) {
        if (i != x) {
          s[v][u++] = m[j][i];
        }
      }
      v++;
    }
    #define SUB(a,b) (s[1][a]*s[2][b] - s[2][a]*s[1][b])
    double d = s[0][0] * SUB(1, 2) - s[0][1] * SUB(0, 2) + s[0][2] * SUB(0, 1);
    #undef SUB
    return d;
  }

  Matrix4D adjoint()
  {
    Matrix4D a;
    for (size_t j = 0; j<4; j++) {
      for (size_t i = 0; i<4; i++) {
        a.m[i][j] = (((i + j) & 1) ? -1.0f : 1.0f) * minor(i, j);
      }
    }
    return a;
  }

  void invert()
  {
    Matrix4D adj = this->adjoint();
    double invdet = 1.0f / this->determinant();
    for (size_t j = 0; j<4; j++) {
      for (size_t i = 0; i<4; i++) {
        m[j][i] = adj.m[j][i] * invdet;
      }
    }
  }

  /* Data Functions */

  void translation(Vec3D trans)
  {
    unit();
    m[0][3] = trans.x;
    m[1][3] = trans.y;
    m[2][3] = trans.z;
  }

  void scale(Vec3D scale)
  {
    zero();
    m[0][0] = scale.x;
    m[1][1] = scale.y;
    m[2][2] = scale.z;
    m[3][3] = 1.0;
  }

  void rotateQuaternion(Quaternion q)
  {
    m[0][0] = 1.0f - 2.0f * q.y * q.y - 2.0f * q.z * q.z;
    m[0][1] = 2.0f * q.x * q.y + 2.0f * q.w * q.z;
    m[0][2] = 2.0f * q.x * q.z - 2.0f * q.w * q.y;
    m[1][0] = 2.0f * q.x * q.y - 2.0f * q.w * q.z;
    m[1][1] = 1.0f - 2.0f * q.x * q.x - 2.0f * q.z * q.z;
    m[1][2] = 2.0f * q.y * q.z + 2.0f * q.w * q.x;
    m[2][0] = 2.0f * q.x * q.z + 2.0f * q.w * q.y;
    m[2][1] = 2.0f * q.y * q.z - 2.0f * q.w * q.x;
    m[2][2] = 1.0f - 2.0f * q.x * q.x - 2.0f * q.y * q.y;
    m[0][3] = m[1][3] = m[2][3] = m[3][0] = m[3][1] = m[3][2] = 0;
    m[3][3] = 1.0f;
  }

  void qRotate(Quaternion q)
  {
    double x = q.x;
    double y = q.y;
    double z = q.z;
    double angle = q.w;

    unit();

    double l = sqrt(x*x + y*y + z*z);
    if (l == 0)
      return;

    x /= l;
    y /= l;
    z /= l;

    double sina = sin(angle);
    double cosa = cos(angle);

    m[0][0] = x*x * (1 - cosa) + cosa;
    m[0][1] = x*y * (1 - cosa) - z * sina;
    m[0][2] = x*z * (1 - cosa) + y * sina;

    m[1][0] = y*x * (1 - cosa) + z * sina;
    m[1][1] = y*y * (1 - cosa) + cosa;
    m[1][2] = y*z * (1 - cosa) - x * sina;

    m[2][0] = z*x * (1 - cosa) - y * sina;
    m[2][1] = z*y * (1 - cosa) + x * sina;
    m[2][2] = z*z * (1 - cosa) + cosa;
  }

  static Matrix4D eulerAnglesToRotationMatrix(Vec3D &theta)
  {
    //Matrix4D R_x = ()
  }

  void rotateEuler(Vec3D theta)
  {




  }

  void rotateEuler(const double h, const double p, const double b)
  {




    double sinh = sin(h);
    double cosh = cos(h);
    double sinp = sin(p);
    double cosp = cos(p);
    double sinb = sin(b);
    double cosb = cos(b);

    m[0][0] = cosh * cosb + sinh * sinp * sinb;
    m[1][0] = cosp * sinb;
    m[2][0] = -sinh * cosb + cosh * sinp * sinb;
    m[0][1] = -cosh * sinb + sinh * sinp * cosb;
    m[1][1] = cosp * cosb;
    m[2][1] = sinh * sinb + cosh * sinp * cosb;
    m[0][2] = sinh * cosp;
    m[1][2] = -sinp;
    m[2][2] = cosh * cosp;
  }

  /* Static Functions */

  static Matrix4D newTranslation(Vec3D tr)
  {
    Matrix4D t;
    t.translation(tr);
    return t;
  }

  static Matrix4D newScale(Vec3D sc)
  {
    Matrix4D t;
    t.scale(sc);
    return t;
  }

  static Matrix4D newRotateQuaternion(Quaternion qr)
  {
    Matrix4D t;
    t.rotateQuaternion(qr);
    return t;
  }

  static Matrix4D newRotateEuler(const double h, const double p, double b)
  {
    Matrix4D dest;
    dest.unit();
    dest.rotateEuler(h, p, b);
    return dest;
  }

  /* Get Functions */

  Vec3D getTranslation()
  {
    return Vec3D(m[0][3], m[1][3], m[2][3]);
  }

  Vec3D getScale()
  {
    return Vec3D(m[0][0], m[1][1], m[2][2]);
  }

  Quaternion getQuaternion()
  {
    Quaternion q(Vec4D(0.0));

    double trace = m[0][0] + m[1][1] + m[2][2];

    if (trace > 0) {
      double s = 0.5f / sqrt(trace + 1.0f);
      q.w = 0.25f / s;
      q.x = (m[2][1] - m[1][2]) * s;
      q.y = (m[0][2] - m[2][0]) * s;
      q.z = (m[1][0] - m[0][1]) * s;
    }
    else {
      if ((m[0][0] > m[1][1]) && (m[0][0] > m[2][2])) {
        double s = 2.0f * sqrtf(1.0f + m[0][0] - m[1][1] - m[2][2]);
        q.w = (m[2][1] - m[1][2]) / s;
        q.x = 0.25f * s;
        q.y = (m[0][1] + m[1][0]) / s;
        q.z = (m[0][2] + m[2][0]) / s;
      }
      else if (m[1][1] > m[2][2]) {
        double s = 2.0f * sqrtf(1.0f + m[1][1] - m[0][0] - m[2][2]);
        q.w = (m[0][2] - m[2][0]) / s;
        q.x = (m[0][1] + m[1][0]) / s;
        q.y = 0.25f * s;
        q.z = (m[1][2] + m[2][1]) / s;
      }
      else {
        double s = 2.0f * sqrtf(1.0f + m[2][2] - m[0][0] - m[1][1]);
        q.w = (m[1][0] - m[0][1]) / s;
        q.x = (m[0][2] + m[2][0]) / s;
        q.y = (m[1][2] + m[2][1]) / s;
        q.z = 0.25f * s;
      }
    }

    return q;
  }

  // returns yaw, pitch and roll
  Vec3D getEuler()
  {
    Vec3D y(m[0][1], m[1][1], m[2][1]);
    Vec3D z(m[0][2], m[1][2], m[2][2]);

    Vec3D euler;

    // First get RX and RY
    bool zzero[3] = {
      (fabs(z[0]) <= std::numeric_limits<double>::epsilon()),
      (fabs(z[1]) <= std::numeric_limits<double>::epsilon()),
      (fabs(z[2]) <= std::numeric_limits<double>::epsilon())
    };

    if (zzero[0] && zzero[2])
    {
      euler.x = 0;
      if (!zzero[1])
        euler.y = (z[1] < 0) ? HALFPI : -HALFPI;
      else
        euler.y = 0;
    }
    else
    {
      if (zzero[2])
        euler.x = (z[0] < 0) ? -HALFPI : HALFPI;
      else
        euler.x = atan2(z[0], z[2]);
      float hyp = sqrt(z[0] * z[0] + z[2] * z[2]);
      if (hyp <= std::numeric_limits<float>::epsilon())
        euler.y = (z[1] < 0.0) ? HALFPI : -HALFPI;
      else
        euler.y = -atan2(z[1], hyp);
    }

    // Find RZ
    Matrix4D rotEuler(newRotateEuler(euler.x, euler.y, 0));
    rotEuler.invert();
    Vec3D rot_y(rotEuler * y);
    bool rot_yzero[3] = {
      (fabs(rot_y[0]) <= std::numeric_limits<float>::epsilon()),
      (fabs(rot_y[1]) <= std::numeric_limits<float>::epsilon()),
      (fabs(rot_y[2]) <= std::numeric_limits<float>::epsilon())
    };
    if (rot_yzero[0] && rot_yzero[1])
      euler.z = 0;
    else if (rot_yzero[1])
      euler.z = (rot_y[0] < 0) ? HALFPI : -HALFPI;
    else
      euler.z = atan2(-rot_y[0], rot_y[1]);

    euler *= RADIAN64;

    LOGFILE.Debug("Matrix Euler Values: x: %f, y: %f, z: %f", euler.x, euler.y, euler.z);
    return euler;
  }
};