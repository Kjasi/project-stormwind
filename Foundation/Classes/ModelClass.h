#pragma once
#include <Foundation/Classes/ModelObject.h>
#include <Foundation/Classes/PlacementData.h>
#include <Foundation/Vulkan/VulkanModelData.hpp>

class ModelClass
{
public:
  ~ModelClass();

  // Virtual Functions

  // Builds the Model Object data from the current model. Unique to each file format.
  // Must call buildVulkanData() at the end to get display data!
  virtual void buildModelObject() = 0;

  // Builds the Collision object(s) for the current model.
  virtual void buildCollisionObject() = 0;

  // Build any and all placement data needed for subobjects, such as doodads.
  virtual void buildPlacementData() = 0;

  // Functions
  void buildVulkanData(Vec3D offset = Vec3D());	// Builds the Vulkan data from the available Model Object data.

  // Inline functions
  void setModelObject(ModelObject *obj) {
    modelObject = obj;
    buildVulkanData();
  }
  void setModelObject(ModelObject obj) {
    modelObject = &obj;
    buildVulkanData();
  }
  void addModelObject(ModelObject *obj) {
    if (modelScene == nullptr) modelScene = new ModelScene;
    modelScene->Objects.push_back(obj);
  }
  ModelScene *getModelScene() { if (modelScene == nullptr) addModelObject(getModelObject()); return modelScene; }
  ModelObject *getModelObject() { if (modelObject == nullptr) buildModelObject(); return modelObject; }
  ModelObject *getCollisionObject() { if (collisionObject == nullptr) buildCollisionObject(); return collisionObject; }
  PlacementData* getPlacementData() { if (placementData == nullptr) buildPlacementData(); return placementData; }
  VulkanSceneData* getVulkanData() { if (scene == nullptr) buildVulkanData(); return scene; }

protected:
  ModelScene *modelScene = nullptr;
  ModelObject *modelObject = nullptr;
  ModelObject *collisionObject = nullptr;
  PlacementData *placementData = nullptr;
  VulkanSceneData *scene = nullptr;
};