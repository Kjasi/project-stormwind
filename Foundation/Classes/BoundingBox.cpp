#include "BoundingBox.h"

void BoundingBox::updateMin(Vec3D in)
{
  min.takeSmaller(in);
}

void BoundingBox::updateMax(Vec3D in)
{
  max.takeLarger(in);
}

void BoundingBox::updateMinAndMax(Vec3D in)
{
  updateMin(in);
  updateMax(in);
}

void BoundingBox::updateMin(BoundingBox in)
{
  updateMin(in.min);
}

void BoundingBox::updateMax(BoundingBox in)
{
  updateMax(in.max);
}

void BoundingBox::updateMinAndMax(BoundingBox in)
{
  updateMin(in.min);
  updateMax(in.max);
}

// returns largest dimension length
double BoundingBox::getLength()
{
  double len = 0.0;

  if (abs(max.x - min.x) > len) {	len = abs(max.x - min.x); }
  if (abs(max.y - min.y) > len) { len = abs(max.y - min.y); }
  if (abs(max.z - min.z) > len) { len = abs(max.z - min.z); }

  return len;
}