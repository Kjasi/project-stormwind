#pragma once
#include <math.h>
#include <QtGui/QVector3D>
#include <Foundation/Constants.h>
#include <Foundation/LogFile.h>

// Vector 2-Float Class
struct Vec2F
{
  float x, y;

  Vec2F() : x(0.0f), y(0.0f) {};
  Vec2F(float x0, float y0) : x(x0), y(y0) {};
  Vec2F(float val) : x(val), y(val) {};
  Vec2F(const Vec2F& v) : x(v.x), y(v.y) {};

  void reset() {
    x = y = 0.0f;
  }

  Vec2F& operator= (const Vec2F &v) {
    x = v.x;
    y = v.y;
    return *this;
  }
  Vec2F operator= (const float &v) { return operator=(Vec2F(v, v)); }

  Vec2F operator+ (const Vec2F &v)
  {
    Vec2F r(x + v.x, y + v.y);
    return r;
  }
  Vec2F& operator+= (const Vec2F &v) {
    x += v.x;
    y += v.y;
    return *this;
  }
  Vec2F operator+ (const float &v) { return operator+(Vec2F(v, v)); }
  Vec2F& operator+= (const float &v) { return operator+=(Vec2F(v, v)); }

  Vec2F operator- (const Vec2F &v)
  {
    Vec2F r(x - v.x, y - v.y);
    return r;
  }
  Vec2F& operator-= (const Vec2F &v)
  {
    x -= v.x;
    y -= v.y;
    return *this;
  }
  Vec2F operator- (const float &v) { return operator-(Vec2F(v, v)); }
  Vec2F& operator-= (const float &v) { return operator-=(Vec2F(v, v)); }

  Vec2F operator* (const Vec2F &v)
  {
    Vec2F r(x*v.x, y*v.y);
    return r;
  }
  Vec2F& operator*= (const Vec2F &v)
  {
    x *= v.x;
    y *= v.y;
    return *this;
  }
  Vec2F operator* (const float &v) { return operator*(Vec2F(v, v)); }
  Vec2F& operator*= (const float &v) { return operator*=(Vec2F(v, v)); }

  Vec2F operator/ (const Vec2F &v)
  {
    Vec2F r(x / v.x, y / v.y);
    return r;
  }
  Vec2F& operator/= (const Vec2F &v)
  {
    x /= v.x;
    y /= v.y;
    return *this;
  }
  Vec2F operator/ (const float &v) { return operator/(Vec2F(v, v)); }
  Vec2F& operator/= (const float &v) { return operator/=(Vec2F(v, v)); }

  bool operator ==(const Vec2F& second) const
  {
    return (x == second.x) && (y == second.y);
  }

  bool operator ==(const Vec2F* second) const
  {
    return (x == second->x) && (y == second->y);
  }

  operator float*()
  {
    return (float*)this;
  }

  void InvertX() { x = -x; }
  void InvertY() { y = -y; }
};

// Vector 2-Double Class
struct Vec2D
{
  double x, y;

  Vec2D() : x(0.0), y(0.0) {};
  Vec2D(double x0, double y0) : x(x0), y(y0) {};
  Vec2D(float val) : x(val), y(val) {};
  Vec2D(double val) : x(val), y(val) {};
  Vec2D(const Vec2D& v) : x(v.x), y(v.y) {};
  Vec2D(const Vec2F& v) : x(v.x), y(v.y) {};

  void reset() {
    x = y = 0.0f;
  }

  Vec2D& operator= (const Vec2D &v) {
    x = v.x;
    y = v.y;
    return *this;
  }
  Vec2D operator= (const double &v) { return operator=(Vec2D(v, v)); }

  Vec2D operator+ (const Vec2D &v)
  {
    Vec2D r(x + v.x, y + v.y);
    return r;
  }
  Vec2D& operator+= (const Vec2D &v) {
    x += v.x;
    y += v.y;
    return *this;
  }
  Vec2D operator+ (const double &v) { return operator+(Vec2D(v, v)); }
  Vec2D& operator+= (const double &v) { return operator+=(Vec2D(v, v)); }

  Vec2D operator- (const Vec2D &v)
  {
    Vec2D r(x - v.x, y - v.y);
    return r;
  }
  Vec2D& operator-= (const Vec2D &v)
  {
    x -= v.x;
    y -= v.y;
    return *this;
  }
  Vec2D operator- (const double &v) { return operator-(Vec2D(v, v)); }
  Vec2D& operator-= (const double &v) { return operator-=(Vec2D(v, v)); }

  Vec2D operator* (const Vec2D &v)
  {
    Vec2D r(x*v.x, y*v.y);
    return r;
  }
  Vec2D& operator*= (const Vec2D &v)
  {
    x *= v.x;
    y *= v.y;
    return *this;
  }
  Vec2D operator* (const double &v) { return operator*(Vec2D(v, v)); }
  Vec2D& operator*= (const double &v) { return operator*=(Vec2D(v, v)); }

  Vec2D operator/ (const Vec2D &v)
  {
    Vec2D r(x / v.x, y / v.y);
    return r;
  }
  Vec2D& operator/= (const Vec2D &v)
  {
    x /= v.x;
    y /= v.y;
    return *this;
  }
  Vec2D operator/ (const double &v) { return operator/(Vec2D(v, v)); }
  Vec2D& operator/= (const double &v) { return operator/=(Vec2D(v, v)); }

  bool operator ==(const Vec2D& second) const
  {
    return (x == second.x) && (y == second.y);
  }

  bool operator ==(const Vec2D* second) const
  {
    return (x == second->x) && (y == second->y);
  }

  operator double*()
  {
    return (double*)this;
  }

  const float *toFloats() {
    float *a = new float[2];
    a[0] = (float)x;
    a[1] = (float)y;
    return a;
  }

  const Vec2F to2F() {
    Vec2F a((float)x, (float)y);
    return a;
  }

  void InvertX() { x = -x; }
  void InvertY() { y = -y; }
};

// Vector 3-Float Class
struct Vec3F
{
  float x, y, z;

  Vec3F() : x(0.0f), y(0.0f), z(0.0f) {};
  Vec3F(float x0, float y0, float z0) : x(x0), y(y0), z(z0) {};
  Vec3F(float val) : x(val), y(val), z(val) {};
  Vec3F(const Vec3F& v) : x(v.x), y(v.y), z(v.z) {};

  void reset() {
    x = y = z = 0.0f;
  }

  Vec3F& operator= (const Vec3F &iv) {
    x = iv.x;
    y = iv.y;
    z = iv.z;
    return *this;
  }
  Vec3F operator= (const float &v) { return operator=(Vec3F(v, v, v)); }

  Vec3F operator+ (const Vec3F &v)
  {
    Vec3F r(x + v.x, y + v.y, z + v.z);
    return r;
  }
  Vec3F& operator+= (const Vec3F &v) {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }
  Vec3F operator+ (const float &v) { return operator+(Vec3F(v, v, v)); }
  Vec3F& operator+= (const float &v) { return operator+=(Vec3F(v, v, v)); }

  Vec3F operator- (const Vec3F &v)
  {
    Vec3F r(x - v.x, y - v.y, z - v.z);
    return r;
  }
  Vec3F& operator-= (const Vec3F &v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }
  Vec3F operator- (const float &v) { return operator-(Vec3F(v, v, v)); }
  Vec3F& operator-= (const float &v) { return operator-=(Vec3F(v, v, v)); }

  Vec3F operator* (const Vec3F &v)
  {
    Vec3F r(x*v.x, y*v.y, z*v.z);
    return r;
  }
  Vec3F& operator*= (const Vec3F &v)
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
  }
  Vec3F operator* (const float &v) { return operator*(Vec3F(v, v, v)); }
  Vec3F& operator*= (const float &v) { return operator*=(Vec3F(v, v, v)); }

  Vec3F operator/ (const Vec3F &v)
  {
    Vec3F r(x / v.x, y / v.y, z / v.z);
    return r;
  }
  Vec3F& operator/= (const Vec3F &v)
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
  }
  Vec3F operator/ (const float &v) { return operator/(Vec3F(v, v, v)); }
  Vec3F& operator/= (const float &v) { return operator/=(Vec3F(v, v, v)); }

  bool operator ==(const Vec3F& second) const
  {
    return (x == second.x) && (y == second.y) && (z == second.z);
  }

  bool operator ==(const Vec3F* second) const
  {
    return (x == second->x) && (y == second->y) && (z == second->z);
  }

  operator float*()
  {
    return (float*)this;
  }

  void takeLarger(Vec3F other)
  {
    if (x < other.x) { x = other.x; }
    if (y < other.y) { y = other.y; }
    if (z < other.z) { z = other.z; }
  }
  void takeSmaller(Vec3F other)
  {
    if (x > other.x) { x = other.x; }
    if (y > other.y) { y = other.y; }
    if (z > other.z) { z = other.z; }
  }

  Vec3F InvertX() { x = -x; return *this;	}
  Vec3F InvertY() { y = -y; return *this;	}
  Vec3F InvertZ() { z = -z; return *this; }

  Vec3F SwapXZ()
  {
    float tmp = x;
    x = z;
    z = tmp;
    return *this;
  }

  Vec3F SwapYZ()
  {
    float tmp = y;
    y = z;
    z = tmp;
    return *this;
  }
};

// Vector 3-Double Class
struct Vec3D
{
  double x, y, z;

  Vec3D() : x(0.0), y(0.0), z(0.0) {};
  Vec3D(double x0, double y0, double z0) : x(x0), y(y0), z(z0) {};
  Vec3D(float val) : x(val), y(val), z(val) {};
  Vec3D(double val) : x(val), y(val), z(val) {};
  Vec3D(const Vec3D& v) : x(v.x), y(v.y), z(v.z) {};
  Vec3D(const Vec3F& v) : x(v.x), y(v.y), z(v.z) {};

  Vec3D& operator= (const Vec3D &v) {
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
  }
  Vec3D operator= (const double &v) { return operator=(Vec3D(v, v, v)); }
  Vec3D& operator= (const QVector3D &v) {
    x = v.x();
    y = v.y();
    z = v.z();
    return *this;
  }

  Vec3D operator+ (const Vec3D &v)
  {
    Vec3D r(x + v.x, y + v.y, z + v.z);
    return r;
  }
  Vec3D& operator+= (const Vec3D &v) {
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
  }
  Vec3D operator+ (const double &v) { return operator+(Vec3D(v, v, v)); }
  Vec3D& operator+= (const double &v) { return operator+=(Vec3D(v, v, v)); }

  Vec3D operator- (const Vec3D &v)
  {
    Vec3D r(x - v.x, y - v.y, z - v.z);
    return r;
  }
  Vec3D& operator-= (const Vec3D &v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
  }
  Vec3D operator- (const double &v) { return operator-(Vec3D(v, v, v)); }
  Vec3D& operator-= (const double &v) { return operator-=(Vec3D(v, v, v)); }

  Vec3D operator* (const Vec3D &v)
  {
    Vec3D r(x*v.x, y*v.y, z*v.z);
    return r;
  }
  Vec3D& operator*= (const Vec3D &v)
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
  }
  Vec3D operator* (const double &v) { return operator*(Vec3D(v, v, v)); }
  Vec3D& operator*= (const double &v) { return operator*=(Vec3D(v,v,v)); }

  Vec3D operator/ (const Vec3D &v)
  {
    Vec3D r(x / v.x, y / v.y, z / v.z);
    return r;
  }
  Vec3D& operator/= (const Vec3D &v)
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
  }
  Vec3D operator/ (const double &v) { return operator/(Vec3D(v, v, v)); }
  Vec3D& operator/= (const double &v) { return operator/=(Vec3D(v, v, v)); }

  bool operator ==(const Vec3D& second) const
  {
    return (x == second.x) && (y == second.y) && (z == second.z);
  }

  bool operator ==(const Vec3D* second) const
  {
    return (x == second->x) && (y == second->y) && (z == second->z);
  }

  operator double*()
  {
    return (double*)this;
  }

  const float *toFloats() {
    float *a = new float[3];
    a[0] = (float)x;
    a[1] = (float)y;
    a[2] = (float)z;
    return a;
  }

  const Vec3F to3F() {
    Vec3F a((float)x, (float)y, (float)z);
    return a;
  }

  void takeLarger(Vec3D other)
  {
    if (x < other.x) { x = other.x; }
    if (y < other.y) { y = other.y; }
    if (z < other.z) { z = other.z; }
  }
  void takeSmaller(Vec3D other)
  {
    if (x > other.x) { x = other.x; }
    if (y > other.y) { y = other.y; }
    if (z > other.z) { z = other.z; }
  }

  Vec3D InvertX() { x = -x; return *this; }
  Vec3D InvertY() { y = -y; return *this; }
  Vec3D InvertZ() { z = -z; return *this; }

  Vec3D SwapXZ()
  {
    float tmp = x;
    x = z;
    z = tmp;
    return *this;
  }

  Vec3D SwapYZ()
  {
    float tmp = y;
    y = z;
    z = tmp;
    return *this;
  }
};

struct Vec4I16
{
  int16_t x, y, z, w;

  Vec4I16(int16_t iX, int16_t iY, int16_t iZ, int16_t iW) : x(iX), y(iY), z(iZ), w(iW) {};
};

inline float i16tf(int16_t t) {
  return float(t < 0 ? t + 32768 : t - 32767) / 32767.0f; // (Short > 0 ? Short-32767 : Short+32767)/32767.0;
}
inline double i16td(int16_t t) {
  return double(t < 0 ? t + 32768 : t - 32767) / 32767.0; // (Short > 0 ? Short-32767 : Short+32767)/32767.0;
}

// Vector 4-Float Class
struct Vec4F
{
  float x, y, z, w;

  Vec4F() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {};
  Vec4F(float x0, float y0, float z0, float w0) : x(x0), y(y0), z(z0), w(w0) {};
  Vec4F(float val) : x(val), y(val), z(val), w(val) {};
  Vec4F(const Vec4F& v) : x(v.x), y(v.y), z(v.z), w(v.w) {};
  Vec4F(const Vec4I16& v) : x(i16tf(v.x)), y(i16tf(v.y)), z(i16tf(v.z)), w(i16tf(v.w)) {};

  Vec4F& operator= (const Vec4F &v) {
    x = v.x;
    y = v.y;
    z = v.z;
    w = v.w;
    return *this;
  }
  Vec4F operator= (const float &v) { return operator=(Vec4F(v, v, v, v)); }

  Vec4F operator+ (const Vec4F &v)
  {
    Vec4F r(x + v.x, y + v.y, z + v.z, w + v.w);
    return r;
  }
  Vec4F& operator+= (const Vec4F &v) {
    x += v.x;
    y += v.y;
    z += v.z;
    w += v.w;
    return *this;
  }
  Vec4F operator+ (const float &v) { return operator+(Vec4F(v, v, v, v)); }
  Vec4F& operator+= (const float &v) { return operator+=(Vec4F(v, v, v, v)); }

  Vec4F operator- (const Vec4F &v)
  {
    Vec4F r(x - v.x, y - v.y, z - v.z, w - v.w);
    return r;
  }
  Vec4F& operator-= (const Vec4F &v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    w -= v.w;
    return *this;
  }
  Vec4F operator- (const float &v) { return operator-(Vec4F(v, v, v, v)); }
  Vec4F& operator-= (const float &v) { return operator-=(Vec4F(v, v, v, v)); }

  Vec4F operator* (const Vec4F &v)
  {
    Vec4F r(x*v.x, y*v.y, z*v.z, w*v.w);
    return r;
  }
  Vec4F& operator*= (const Vec4F &v)
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    w *= v.w;
    return *this;
  }
  Vec4F operator* (const float &v) { return operator*(Vec4F(v, v, v, v)); }
  Vec4F& operator*= (const float &v) { return operator*=(Vec4F(v, v, v, v)); }

  Vec4F operator/ (const Vec4F &v)
  {
    Vec4F r(x / v.x, y / v.y, z / v.z, w / v.w);
    return r;
  }
  Vec4F& operator/= (const Vec4F &v)
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    w /= v.w;
    return *this;
  }
  Vec4F operator/ (const float &v) { return operator/(Vec4F(v, v, v, v)); }
  Vec4F& operator/= (const float &v) { return operator/=(Vec4F(v, v, v, v)); }

  bool operator ==(const Vec4F& second) const
  {
    return (x == second.x) && (y == second.y) && (z == second.z) && (w == second.w);
  }

  bool operator ==(const Vec4F* second) const
  {
    return (x == second->x) && (y == second->y) && (z == second->z) && (w == second->w);
  }

  operator float*()
  {
    return (float*)this;
  }

  void takeLarger(Vec4F other)
  {
    if (x < other.x) { x = other.x; }
    if (y < other.y) { y = other.y; }
    if (z < other.z) { z = other.z; }
    if (w < other.w) { z = other.w; }
  }
  void takeSmaller(Vec4F other)
  {
    if (x > other.x) { x = other.x; }
    if (y > other.y) { y = other.y; }
    if (z > other.z) { z = other.z; }
    if (w > other.w) { w = other.w; }
  }

  Vec4F InvertX() { x = -x; return *this; }
  Vec4F InvertY() { y = -y; return *this; }
  Vec4F InvertZ() { z = -z; return *this; }
  Vec4F InvertW() { w = -w; return *this; }

  Vec4F SwapXZ()
  {
    float tmp = x;
    x = z;
    z = tmp;
    return *this;
  }

  Vec4F SwapYZ()
  {
    float tmp = y;
    y = z;
    z = tmp;
    return *this;
  }
};

// Vector 4-Double Class
struct Vec4D
{
  double x, y, z, w;

  Vec4D() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {};
  Vec4D(double x0, double y0, double z0, double w0) : x(x0), y(y0), z(z0), w(w0) {};
  Vec4D(float val) : x(val), y(val), z(val), w(val) {};
  Vec4D(double val) : x(val), y(val), z(val), w(val) {};
  Vec4D(const Vec4D& v) : x(v.x), y(v.y), z(v.z), w(v.w) {};
  Vec4D(const Vec3D& v, const double iw) : x(v.x), y(v.y), z(v.z), w(iw) {};
  Vec4D(const Vec4I16& v) : x(i16td(v.x)), y(i16td(v.y)), z(i16td(v.z)), w(i16td(v.w)) {};

  Vec4D& operator= (const Vec4D &v) {
    x = v.x;
    y = v.y;
    z = v.z;
    w = v.w;
    return *this;
  }
  Vec4D operator= (const double &v) { return operator=(Vec4D(v, v, v, v)); }

  const Vec4D operator+ (const Vec4D &v) const
  {
    Vec4D r(x + v.x, y + v.y, z + v.z, w + v.w);
    return r;
  }
  Vec4D& operator+= (const Vec4D &v) {
    x += v.x;
    y += v.y;
    z += v.z;
    w += v.w;
    return *this;
  }
  Vec4D operator+ (const double &v) { return operator+(Vec4D(v, v, v, v)); }
  Vec4D& operator+= (const double &v) { return operator+=(Vec4D(v, v, v, v)); }

  const Vec4D operator- (const Vec4D &v) const
  {
    Vec4D r(x - v.x, y - v.y, z - v.z, w - v.w);
    return r;
  }
  Vec4D& operator-= (const Vec4D &v)
  {
    x -= v.x;
    y -= v.y;
    z -= v.z;
    w -= v.w;
    return *this;
  }
  Vec4D operator- (const double &v) { return operator-(Vec4D(v, v, v, v)); }
  Vec4D& operator-= (const double &v) { return operator-=(Vec4D(v, v, v, v)); }

  Vec4D operator* (const Vec4D &v) const
  {
    return Vec4D(x*v.x,y*v.y,z*v.z,w*v.w);
  }

  const Vec4D operator* (double d) const
  {
    Vec4D r(x*d, y*d, z*d, w*d);
    return r;
  }
  Vec4D& operator*= (const Vec4D &v)
  {
    x *= v.x;
    y *= v.y;
    z *= v.z;
    w *= v.w;
    return *this;
  }
  Vec4D operator* (const double &v) { return operator*(Vec4D(v, v, v, v)); }
  Vec4D& operator*= (const double &v) { return operator*=(Vec4D(v, v, v, v)); }

  const Vec4D operator/ (const Vec4D &v)
  {
    Vec4D r(x / v.x, y / v.y, z / v.z, w / v.w);
    return r;
  }
  Vec4D& operator/= (const Vec4D &v)
  {
    x /= v.x;
    y /= v.y;
    z /= v.z;
    w /= v.w;
    return *this;
  }
  Vec4D operator/ (const double &v) { return operator/(Vec4D(v, v, v, v)); }
  Vec4D& operator/= (const double &v) { return operator/=(Vec4D(v, v, v, v)); }

  bool operator ==(const Vec4D& second) const
  {
    return (x == second.x) && (y == second.y) && (z == second.z) && (w == second.w);
  }

  bool operator ==(const Vec4D* second) const
  {
    return (x == second->x) && (y == second->y) && (z == second->z) && (w == second->w);
  }

  double dot(const Vec4D& v)const
  {
    return (x*v.x + y*v.y + z*v.z + w*v.w);
  }

  double length() const
  {
    return sqrt(x*x + y*y + z*z + w*w);
  }

  Vec4D& normalize()
  {
    this->operator*= (1.0f / length());
    return *this;
  }

  operator double*()
  {
    return (double*)this;
  }

  const float *toFloats() {
    float *a = new float[4];
    a[0] = (float)x;
    a[1] = (float)y;
    a[2] = (float)z;
    a[3] = (float)w;
    return a;
  }

  const Vec4F to4F() {
    Vec4F a((float)x, (float)y, (float)z, (float)w);
    return a;
  }

  void takeLarger(Vec4D other)
  {
    if (x < other.x) { x = other.x; }
    if (y < other.y) { y = other.y; }
    if (z < other.z) { z = other.z; }
    if (w < other.w) { z = other.w; }
  }
  void takeSmaller(Vec4D other)
  {
    if (x > other.x) { x = other.x; }
    if (y > other.y) { y = other.y; }
    if (z > other.z) { z = other.z; }
    if (w > other.w) { w = other.w; }
  }

  Vec4D InvertX() { x = -x; return *this; }
  Vec4D InvertY() { y = -y; return *this; }
  Vec4D InvertZ() { z = -z; return *this; }
  Vec4D InvertW() { w = -w; return *this; }

  Vec4D SwapXZ()
  {
    float tmp = x;
    x = z;
    z = tmp;
    return *this;
  }

  Vec4D SwapYZ()
  {
    float tmp = y;
    y = z;
    z = tmp;
    return *this;
  }
};

class QuaternionF : public Vec4F
{
public:
  float w, x, y, z;

  QuaternionF() : x(0.0f), y(0.0f), z(0.0f), w(0.0f) {};
  QuaternionF(double x0, double y0, double z0, double w0) : x(x0), y(y0), z(z0), w(w0) {};
  QuaternionF(Vec4F v4) : x(v4.x), y(v4.y), z(v4.z), w(v4.w) {};
};

class Quaternion : public Vec4D
{
public:
  double x, y, z, w;

  Quaternion() : x(0.0), y(0.0), z(0.0), w(0.0) {};
  Quaternion(double x0, double y0, double z0, double w0) : x(x0), y(y0), z(z0), w(w0) {};
  Quaternion(Vec4F v4) : x(v4.x), y(v4.y), z(v4.z), w(v4.w) {};
  Quaternion(Vec4D v4) : x(v4.x), y(v4.y), z(v4.z), w(v4.w) {};
  Quaternion(QuaternionF v4) : x(v4.x), y(v4.y), z(v4.z), w(v4.w) {};

  static const Quaternion slerp(const float r, const Quaternion &v1, const Quaternion &v2)
  {
    // SLERP
    float dot = v1.dot(v2);

    if (fabs(dot) > 0.9995f) {
      // fall back to LERP
      return Quaternion::lerp(r, v1, v2);
    }

    float a = acosf(dot) * r;
    Quaternion q = (v2 - v1 * dot);
    q.normalize();

    return v1 * cosf(a) + q * sinf(a);
  }

  void ConvertYUpToZUp()
  {
    Quaternion a;
    a = YUpToZUp(Quaternion(x,y,z,w));
    x = a.x;
    y = a.y;
    z = a.z;
    w = a.w;
  }

  void ConvertZUpToYUp()
  {
    Quaternion a;
    a = ZUpToYUp(Quaternion(x,y,z,w));
    x = a.x;
    y = a.y;
    z = a.z;
    w = a.w;
  }

  static Quaternion YUpToZUp(Quaternion V)
  {
    return Quaternion(V * Quaternion(-0.707107, 0, 0, 0.707107));
  }
  static Quaternion ZUpToYUp(Quaternion V)
  {
    return Quaternion(V * Quaternion(0.707107, 0, 0, -0.707107));
  }

  Quaternion YUpToZUp()
  {
    return YUpToZUp(Quaternion(x, y, z, w));
  }
  Quaternion ZUpToYUp()
  {
    return ZUpToYUp(Quaternion(x, y, z, w));
  }

  static const Quaternion lerp(const float r, const Quaternion &v1, const Quaternion &v2)
  {
    return v1*(1.0f - r) + v2*r;
  }

  static Quaternion fromEuler(Vec3D inVec)
  {
    // yaw (Z), pitch (Y), roll (X)

    // Abbreviations for the various angular functions
    double cy = cos(inVec.z * 0.5);
    double sy = sin(inVec.z * 0.5);
    double cp = cos(inVec.y * 0.5);
    double sp = sin(inVec.y * 0.5);
    double cr = cos(inVec.x * 0.5);
    double sr = sin(inVec.x * 0.5);

    Quaternion q;
    q.w = (cy * cp * cr + sy * sp * sr);
    q.x = (cy * cp * sr - sy * sp * cr);
    q.y = (sy * cp * sr + cy * sp * cr);
    q.z = (sy * cp * cr - cy * sp * sr);
    return q;
  }

  // Returns Roll, Pitch, Yaw (X, Y, Z)
  Vec3D toEuler()
  {
    const float SingularityTest = z*x - w*y;
    const float YawY = 2.f*(w*z + x*y);
    const float YawX = (1.f - 2.f*((y*y) + (z*z)));

    const float SINGULARITY_THRESHOLD = 0.4999995f;
    const float RAD_TO_DEG = (180.f) / PI32;

    Vec3D RotatorFromQuat;

    if (SingularityTest < -SINGULARITY_THRESHOLD)
    {
      RotatorFromQuat.y = -90.f;
      RotatorFromQuat.z = atan2(YawY, YawX) * RAD_TO_DEG;
      RotatorFromQuat.x = Math::NormalizeAxis(-RotatorFromQuat.z - (2.f *atan2(x, w) * RAD_TO_DEG));
    }
    else if (SingularityTest > SINGULARITY_THRESHOLD)
    {
      RotatorFromQuat.y = 90.f;
      RotatorFromQuat.z = atan2(YawY, YawX) * RAD_TO_DEG;
      RotatorFromQuat.x = Math::NormalizeAxis(RotatorFromQuat.z - (2.f * atan2(x, w) * RAD_TO_DEG));
    }
    else
    {
      RotatorFromQuat.y = asin(2.f*(SingularityTest)) * RAD_TO_DEG;
      RotatorFromQuat.z = atan2(YawY, YawX) * RAD_TO_DEG;
      RotatorFromQuat.x = atan2(-2.f*(w*x + y*z), (1.f - 2.f*((x*x) + (y*y)))) * RAD_TO_DEG;
    }

    // LOGFILE.Debug("Quatnerion Euler Values: x: %f, y: %f, z: %f", RotatorFromQuat.x, RotatorFromQuat.y, RotatorFromQuat.z);
    return RotatorFromQuat;
  }
};