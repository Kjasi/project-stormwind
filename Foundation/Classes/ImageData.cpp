#include "ImageData.h"
#include "../LogFile.h"
#include "../Classes/Lerp.h"
#include <Vulkan/vulkan.h>

// Zero-based addressing
quint32 ImageData::getIndex(uint32_t w = 0, uint32_t h = 0)
{
  return (h * Width) + w;
}

RGBAColor ImageData::getColor(uint32_t w, uint32_t h)
{
  if (ColorData->getCount() == 0) return RGBAColor();

  uint32_t index = getIndex(w, h);
  if (index > ColorData->getCount())
  {
    LOGFILE.Warn("getColor error; W: %i, H: %i, index: %i, ColorCount: %i", w, h, index, ColorData->getCount());
    return ColorData->getAt(ColorData->getCount() - 1);
  }
  return ColorData->getAt(index);
}

RGBAColor ImageData::getColorRaw(uint32_t w, uint32_t h)
{
  if (ColorDataRaw->getCount() == 0) return RGBAColor();

  uint32_t index = getIndex(w, h);
  if (index > ColorDataRaw->getCount())
  {
    LOGFILE.Warn("getColorRaw error; W: %i, H: %i, index: %i, ColorCount: %i", w, h, index, ColorDataRaw->getCount());
    return ColorDataRaw->getAt(ColorDataRaw->getCount() - 1);
  }
  return ColorDataRaw->getAt(index);
}

bool ImageData::hasAlphaData()
{
  for (uint32_t i = 0; i < ColorData->getCount(); i++)
  {
    RGBAColor color = ColorData->getAt(i);
    if (color.A < 1.0f)
    {
      return true;
    }
  }

  return false;
}

uint8_t ImageData::getNumChannels()
{
  return ColorType;
}

bool ImageData::isEmpty()
{
  if (Width <= 0 || Height <= 0) return true;
  return (ColorData->getCount() <= 0 && ColorDataRaw->getCount() <= 0);
}

VkFormat ImageData::getVulkanFormat()
{
  VkFormat f;
  switch (ColorType)
  {
  case Grey:
    f = VK_FORMAT_R8_UNORM;
    break;
  case GreyAlpha:
    f = VK_FORMAT_R8G8_UNORM;
    break;
  case RGB:
    f = VK_FORMAT_R8G8B8_UNORM;
    break;
  case RGBA:
  default:
    f = VK_FORMAT_R8G8B8A8_UNORM;
    break;
  }
  return f;
}

void ImageData::makeTestTexture(uint32_t width, uint32_t height)
{
  *this = generateTestTexture(width, height);
}

void ImageData::makeSolidTexture(RGBAColor color, QString filename, uint32_t width, uint32_t height)
{
  *this = generateSolidTexture(color, filename, width, height);
}

void ImageData::makeWhiteTexture(uint32_t width, uint32_t height)
{
  *this = generateWhiteTexture(width, height);
}

void ImageData::makeBlackTexture(uint32_t width, uint32_t height)
{
  *this = generateBlackTexture(width, height);
}

ImageData ImageData::generateTestTexture(uint32_t width, uint32_t height)
{
  ImageData img = ImageData();
  img.ColorType = RGBA;
  img.sourceFilename = "Test Texture";
  img.setWidthHeight(width, height);

  RGBAColor Red = RGBAColor(1.0, 0.0, 0.0);
  RGBAColor Green = RGBAColor(0.0, 1.0, 0.0);
  RGBAColor Blue = RGBAColor(0.0, 0.0, 1.0);
  RGBAColor White = RGBAColor(1.0, 1.0, 1.0);

  for (uint32_t y = 0; y < height; y++)
  {
    double ypercent = (double)y / (double)height;
    for (uint32_t x = 0; x < width; x++)
    {
      double xpercent = (double)x / (double)width;

      RGBAColor currentX(255, 0, 255);
      double f = xpercent * 3;
      if (xpercent < (1.0 / 3.0)) {
        currentX = Interpolate::Cosine(Red, Green, f);
      }
      else if (xpercent < (2.0 / 3.0)) {
        currentX = Interpolate::Cosine(Green, Blue, f-1.0);
      }
      else {
        currentX = Interpolate::Cosine(Blue, White, f-2.0);
      }

      img.ColorData->addTo(currentX);
      img.ColorDataRaw->addTo(currentX);
    }
  }

  return img;
}

ImageData ImageData::generateSolidTexture(RGBAColor color, QString Filename, uint32_t width, uint32_t height)
{
  ImageData img = ImageData();
  img.sourceFilename = Filename;
  img.ColorType = RGBA;
  img.setWidthHeight(width, height);

  for (uint32_t i = 0; i < (height * width); i++)
  {
    img.ColorData->addTo(color);
    img.ColorDataRaw->addTo(color);
  }

  return img;
}

ImageData ImageData::generateWhiteTexture(uint32_t width, uint32_t height)
{
  return generateSolidTexture(RGBAColor(1.0, 1.0, 1.0, 1.0), "Solid White", width, height);
}

ImageData ImageData::generateBlackTexture(uint32_t width, uint32_t height)
{
  return generateSolidTexture(RGBAColor(0.0, 0.0, 0.0, 1.0), "Solid Black", width, height);
}

bool ImageData::setBlockData(ImageData *src, int offsetX, int offsetY, RGBAColorID useColor)
{
  // Fail if we'll go off the edge.
  //if ((src->Width + offsetX) > Width) return false;
  //if ((src->Height + offsetY) > Height) return false;

  for (int y = 0; y < src->Height; y++)
  {
    int dstYPixel = offsetY + y;
    if (dstYPixel >= Height) continue;
    for (int x = 0; x < src->Width; x++)
    {
      int dstXPixel = offsetX + x;
      if (dstXPixel >= Width) continue;
      int dstIndex = (dstYPixel * Width) + dstXPixel;
      int srcIndex = (y * src->Width) + x;

      RGBAColor dstColor = src->ColorData->getAt(srcIndex);
      RGBAColor dstColorRaw = src->ColorDataRaw->getAt(srcIndex);
      // LOGFILE.Debug("Dst Index: %i, src Index %i | New Color: %f %f %f", dstIndex, srcIndex, dstColor.R, dstColor.G, dstColor.B);

      switch (useColor)
      {
      case COLORID_RED:
        ColorData->setColorRed(dstIndex, dstColor);
        ColorDataRaw->setColorRed(dstIndex, dstColorRaw);
        break;
      case COLORID_GREEN:
        ColorData->setColorGreen(dstIndex, dstColor);
        ColorDataRaw->setColorGreen(dstIndex, dstColorRaw);
        break;
      case COLORID_BLUE:
        ColorData->setColorBlue(dstIndex, dstColor);
        ColorDataRaw->setColorBlue(dstIndex, dstColorRaw);
        break;
      case COLORID_ALPHA:
        ColorData->setColorAlpha(dstIndex, dstColor);
        ColorDataRaw->setColorAlpha(dstIndex, dstColorRaw);
        break;
      case COLORID_RGBONLY:
        ColorData->setColorRed(dstIndex, dstColor);
        ColorDataRaw->setColorRed(dstIndex, dstColorRaw);
        ColorData->setColorGreen(dstIndex, dstColor);
        ColorDataRaw->setColorGreen(dstIndex, dstColorRaw);
        ColorData->setColorBlue(dstIndex, dstColor);
        ColorDataRaw->setColorBlue(dstIndex, dstColorRaw);
        break;
      case COLORID_ALL:
      default:
        ColorData->setColorRed(dstIndex, dstColor);
        ColorDataRaw->setColorRed(dstIndex, dstColorRaw);
        ColorData->setColorGreen(dstIndex, dstColor);
        ColorDataRaw->setColorGreen(dstIndex, dstColorRaw);
        ColorData->setColorBlue(dstIndex, dstColor);
        ColorDataRaw->setColorBlue(dstIndex, dstColorRaw);
        ColorData->setColorAlpha(dstIndex, dstColor);
        ColorDataRaw->setColorAlpha(dstIndex, dstColorRaw);
        break;
      }
      //LOGFILE.Debug("Final Color: %f %f %f %f", ColorData->getAt(dstIndex).R, ColorData->getAt(dstIndex).G, ColorData->getAt(dstIndex).B, ColorData->getAt(dstIndex).A);
    }
  }

  return true;
}

bool ImageData::setRedChannel(ImageData * src, int offsetX, int offsetY)
{
  return setBlockData(src, offsetX, offsetY, COLORID_RED);
}

bool ImageData::setGreenChannel(ImageData * src, int offsetX, int offsetY)
{
  return setBlockData(src, offsetX, offsetY, COLORID_GREEN);
}

bool ImageData::setBlueChannel(ImageData * src, int offsetX, int offsetY)
{
  return setBlockData(src, offsetX, offsetY, COLORID_BLUE);
}

bool ImageData::setAlphaChannel(ImageData * src, int offsetX, int offsetY)
{
  return setBlockData(src, offsetX, offsetY, COLORID_ALPHA);
}

void ImageData::resizeImage(uint32_t newWidth, uint32_t newHeight)
{
  if (newWidth == Width && newHeight == Height) return;

  //LOGFILE.Debug("Resizing image from %ix%i to %ix%i", Width, Height, newWidth, newHeight);

  RGBArray NewColors;
  RGBArray NewColorsRaw;

  float x_ratio = ((float)(Width - 1)) / newWidth;
  float y_ratio = ((float)(Height - 1)) / newHeight;
  float w, h;

  // Rescale the current colors to the new height and width using Bilinear scaling (http://tech-algorithm.com/articles/bilinear-image-scaling/)
  for (size_t i = 0; i < newHeight; i++)
  {
    for (size_t j = 0; j < newWidth; j++)
    {
      int x = (int)(x_ratio * j);
      int y = (int)(y_ratio * i);
      w = (x_ratio * j) - x;
      h = (y_ratio * i) - y;

      RGBAColor A = getColor(x, y);
      RGBAColor B = getColor(x + 1, y);
      RGBAColor C = getColor(x, y + 1);
      RGBAColor D = getColor(x + 1, y + 1);

      float red = (A.R * (1 - w) * (1 - h) + B.R * (w) * (1 - h) + C.R * (h) * (1 - w) + D.R * (w * h));
      float green = (A.G * (1 - w) * (1 - h) + B.G * (w) * (1 - h) + C.G * (h) * (1 - w) + D.G * (w * h));
      float blue = (A.B * (1 - w) * (1 - h) + B.B * (w) * (1 - h) + C.B * (h) * (1 - w) + D.B * (w * h));
      float alpha = (A.A * (1 - w) * (1 - h) + B.A * (w) * (1 - h) + C.A * (h) * (1 - w) + D.A * (w * h));

      RGBAColor newColor = RGBAColor(red, green, blue, alpha);
      NewColors.addTo(newColor);
      
      if (ColorDataRaw > 0)
      {
        RGBAColor rA = getColorRaw(x, y);
        RGBAColor rB = getColorRaw(x + 1, y);
        RGBAColor rC = getColorRaw(x, y + 1);
        RGBAColor rD = getColorRaw(x + 1, y + 1);

        float rred = (rA.R * (1 - w) * (1 - h) + rB.R * (w) * (1 - h) + rC.R * (h) * (1 - w) + rD.R * (w * h));
        float rgreen = (rA.G * (1 - w) * (1 - h) + rB.G * (w) * (1 - h) + rC.G * (h) * (1 - w) + rD.G * (w * h));
        float rblue = (rA.B * (1 - w) * (1 - h) + rB.B * (w) * (1 - h) + rC.B * (h) * (1 - w) + rD.B * (w * h));
        float ralpha = (rA.A * (1 - w) * (1 - h) + rB.A * (w) * (1 - h) + rC.A * (h) * (1 - w) + rD.A * (w * h));

        RGBAColor newColorRaw = RGBAColor(rred, rgreen, rblue, ralpha);
        NewColorsRaw.addTo(newColorRaw);
      }
    }
  }

  /*
  for (size_t y = 0; y < newHeight; y++)
  {
    for (size_t x = 0; x < newWidth; x++)
    {
      double SrcX = (double)x / (double)(newWidth) * (Width - 1);
      double SrcY = (double)y / (double)(newWidth) * (Height - 1);
      int iSrcX = (int)SrcX;
      int iSrcY = (int)SrcY;

      RGBAColor Start0 = getColor(iSrcX, iSrcY);
      RGBAColor End0 = getColor(iSrcX + 1, iSrcY);
      RGBAColor Start1 = getColor(iSrcX, iSrcY + 1);
      RGBAColor End1 = getColor(iSrcX + 1, iSrcY + 1);
      lerp<RGBAColor> startColor = lerp<RGBAColor>(Start0, End0);
      lerp<RGBAColor> endColor = lerp<RGBAColor>(Start1, End1);

      RGBAColor result = lerp<RGBAColor>::doBlerp(startColor, endColor, (double)(SrcX - iSrcX), (double)(SrcY - iSrcY));
      NewColors.push_back(result);

      // Just in case ColorDataRaw gets ommited somewhere...
      if (ColorDataRaw > 0)
      {
        RGBAColor StartRaw0 = getColorRaw(iSrcX, iSrcY);
        RGBAColor EndRaw0 = getColorRaw(iSrcX + 1, iSrcY);
        RGBAColor StartRaw1 = getColorRaw(iSrcX, iSrcY + 1);
        RGBAColor EndRaw1 = getColorRaw(iSrcX + 1, iSrcY + 1);

        lerp<RGBAColor> startColorRaw = lerp<RGBAColor>(StartRaw0, EndRaw0);
        lerp<RGBAColor> endColorRaw = lerp<RGBAColor>(StartRaw1, EndRaw1);

        RGBAColor resultRaw = lerp<RGBAColor>::doBlerp(startColorRaw, endColorRaw, (double)(SrcX - iSrcX), (double)(SrcY - iSrcY));
        NewColorsRaw.push_back(resultRaw);
      }
    }
  }
  */
  //LOGFILE.Debug("Resizing data complete");

  // Final Steps
  Width = newWidth;
  Height = newHeight;
  //LOGFILE.Debug("Width & Height set");
  ColorData->clear();
  ColorData->addTo(NewColors);
  //LOGFILE.Debug("ColorData set");
  ColorDataRaw->clear();
  ColorDataRaw->addTo(NewColorsRaw);
  //LOGFILE.Debug("ColorDataRaw set");
}

std::vector<uchar> ImageData::getDataAsUCharVector(bool sixteenbit, bool bigendian)
{
  std::vector<uchar> data;
  int databyte = 4;
  
  if (ColorType == RGBAColorType::Grey)
  {
    databyte = 1;
  }
  else if (ColorType == RGBAColorType::GreyAlpha)
  {
    databyte = 2;
  }
  else if (ColorType == RGBAColorType::RGB)
  {
    databyte = 3;
  }

  if (sixteenbit == true) {
    databyte *= 2;
  }

  data.resize(Width * Height * databyte);

  for (uint32_t y = 0; y < (uint32_t)Height; y++)
  {
    for (uint32_t x = 0; x < (uint32_t)Width; x++)
    {
      uint32_t baseNum = (databyte * Width * y) + (databyte * x);
      int colorID = (Width * y) + x;
      RGBAColor color = ColorData->getAt(colorID);
      //LOGFILE.Debug("Color Count: %d, Color ID: %d, BaseNum: %d, Color Value: %f %f %f %f", ColorData->getCount(), colorID, baseNum, color.R, color.G, color.B, color.A);

      if (sixteenbit == true)
      {
        unsigned short red = (65535 * color.R);
        unsigned short green = (65535 * color.G);
        unsigned short blue = (65535 * color.B);
        unsigned short alpha = (65535 * color.A);

        unsigned char byteR1, byteR2, byteG1, byteG2, byteB1, byteB2, byteA1, byteA2;

        byteR1 = (red & 0x00FF);
        byteR2 = (red & 0xFF00) >> 8;
        if (bigendian == true)
        {
          byteR1 = (red & 0xFF00) >> 8;
          byteR2 = (red & 0x00FF);
        }

        byteG1 = (green & 0x00FF);
        byteG2 = (green & 0xFF00) >> 8; 
        if (bigendian == true)
        {
          byteG1 = (red & 0xFF00) >> 8;
          byteG2 = (red & 0x00FF);
        }

        byteB1 = (blue & 0xFF00) >> 8;
        byteB2 = (blue & 0x00FF);
        if (bigendian == true)
        {
          byteB1 = (red & 0xFF00) >> 8;
          byteB2 = (red & 0x00FF);
        }

        byteA1 = (alpha & 0x00FF);
        byteA2 = (alpha & 0xFF00) >> 8; 
        if (bigendian == true)
        {
          byteA1 = (red & 0xFF00) >> 8;
          byteA2 = (red & 0x00FF);
        }

        if (ColorType == RGBAColorType::Grey)
        {
          data[baseNum + 0] = byteR1;
          data[baseNum + 1] = byteR2;
        }
        if (ColorType == RGBAColorType::GreyAlpha)
        {
          data[baseNum + 0] = byteR1;
          data[baseNum + 1] = byteR2;
          data[baseNum + 2] = byteA1;
          data[baseNum + 3] = byteA2;
        }
        if (ColorType == RGBAColorType::RGB)
        {
          data[baseNum + 0] = byteR1;
          data[baseNum + 1] = byteR2;
          data[baseNum + 2] = byteG1;
          data[baseNum + 3] = byteG2;
          data[baseNum + 4] = byteB1;
          data[baseNum + 5] = byteB2;
        }
        if (ColorType == RGBAColorType::RGBA)
        {
          data[baseNum + 0] = byteR1;
          data[baseNum + 1] = byteR2;
          data[baseNum + 2] = byteG1;
          data[baseNum + 3] = byteG2;
          data[baseNum + 4] = byteB1;
          data[baseNum + 5] = byteB2;
          data[baseNum + 6] = byteA1;
          data[baseNum + 7] = byteA2;
        }
      }
      else {
        uint32_t red = (255 * color.R);
        uint32_t green = (255 * color.G);
        uint32_t blue = (255 * color.B);
        uint32_t alpha = (255 * color.A);

        if (ColorType == RGBAColorType::Grey)
        {
          data[baseNum + 0] = red;
        }
        if (ColorType == RGBAColorType::GreyAlpha)
        {
          data[baseNum + 0] = red;
          data[baseNum + 1] = alpha;
        }
        if (ColorType == RGBAColorType::RGB)
        {
          data[baseNum + 0] = red;
          data[baseNum + 1] = green;
          data[baseNum + 2] = blue;
        }
        if (ColorType == RGBAColorType::RGBA)
        {
          data[baseNum + 0] = red;
          data[baseNum + 1] = green;
          data[baseNum + 2] = blue;
          data[baseNum + 3] = alpha;
        }
      }
    }
  }

  return data;
}

std::vector<uint8_t> ImageData::getDataAsUInt8Vector(bool sixteenbit, bool bigendian)
{
  std::vector<uchar> vec = getDataAsUCharVector(sixteenbit, bigendian);
  if (vec.size() <= 0)
  {
    LOGFILE.Warn("getDataAsUCharVector return 0 uchars.");
    return std::vector<uint8_t>();
  }
  std::vector<uint8_t> data;
  data.resize(vec.size());
  for (uint32_t i = 0; i < vec.size(); i++)
  {
    data[i] = static_cast<uint8_t>(vec.at(i));
  }
  return data;
}

uchar* ImageData::getDataAsUCharRef(bool inverse, bool sixteenbit, bool bigendian)
{
  std::vector<uchar> vec = getDataAsUCharVector(sixteenbit, bigendian);
  if (vec.size() <= 0)
  {
    LOGFILE.Warn("getDataAsUCharVector return 0 uchars.");
    return nullptr;
  }
  uchar *data = new uchar[vec.size() + 1];

  for (uint32_t i = 0; i < vec.size(); i+=4)
  {
    //LOGFILE.Debug("Vector %i", i);
    if (inverse == true)
    {
      data[i + 2] = vec.at(i + 0);
      data[i + 1] = vec.at(i + 1);
      data[i + 0] = vec.at(i + 2);
      data[i + 3] = vec.at(i + 3);
    }
    else {
      data[i + 0] = vec.at(i + 0);
      data[i + 1] = vec.at(i + 1);
      data[i + 2] = vec.at(i + 2);
      data[i + 3] = vec.at(i + 3);
    }
  }
  
  //LOGFILE.Debug("Vec Count: %i, sizeof Data: %i, Sizeof uChar:%i", vec.size(), sizeof(data), sizeof(data[0]));
  //LOGFILE.Debug("Data 0: %i, Data 7: %i, Data 100: %i, Data %i: %i", (int)data[0], (int)data[7], (int)data[100], (vec.size() - 1), (int)data[vec.size() - 1]);

  return data;
}