#pragma once
#include "BoundingBox.h"
#include "ImageData.h"

enum ModelMeshType
{
  STATICMESH = 0,
  ANIMATEDMESH
};

template<class T>
struct AnimationKeyframe
{
  T Value;			// What the value of this keyframe should be.
  uint32_t Time;		// The millisecond of the animation that this keyframe is placed.
};

struct AnimationTrack
{
  uint32_t Length;	// How long the animation is, in milliseconds
  QString Name;		// The name for this animation.
  AnimationKeyframe<Vec3D> Translation;
  AnimationKeyframe<Quaternion> Rotation;
  AnimationKeyframe<Vec3D> Scale;
};

struct ModelPoint
{
  Vec3D Position;
  Vec2D UVPosition;
  Vec3D NormalVector;
  RGBAColor VertexColor;

  // Weights go in here?

  ModelPoint()
  {
    VertexColor = RGBAColor(1.0f, 1.0f, 1.0f);
    UVPosition = Vec2D(0.0, 0.0);
    NormalVector = Vec3D(1.0, 1.0, 1.0);
  }
  ModelPoint& operator= (const ModelPoint &v) {
    Position = v.Position;
    UVPosition = v.UVPosition;
    NormalVector = v.NormalVector;
    VertexColor = v.VertexColor;
    return *this;
  }
  bool operator ==(const ModelPoint& second) const
  {
    return (Position == second.Position) && (UVPosition == second.UVPosition) && (NormalVector == second.NormalVector) && (VertexColor == second.VertexColor);
  }

  bool operator ==(const ModelPoint* second) const
  {
    return (Position == second->Position) && (UVPosition == second->UVPosition) && (NormalVector == second->NormalVector) && (VertexColor == second->VertexColor);
  }
};

struct ModelPolygon
{
  uint64_t point0, point1, point2;
  uint32_t materialID = 0;	// Entry in ModelObject's MaterialList

  ModelPolygon(uint64_t pnt1 = 0, uint64_t pnt2 = 0, uint64_t pnt3 = 0, uint32_t matid = 0)
  {
    materialID = matid;
    point0 = pnt1;
    point1 = pnt2;
    point2 = pnt3;
  }
};

struct ModelBone
{
  uint32_t BoneID = 0;
  QString BoneName;		// Optional. If not set, BoneName = Bone_(BoneID)
  int32_t ParentBoneID = -1;

  Vec3D Position;
  Quaternion Rotation;
  Vec3D Scale = Vec3D(1.0);

  QList<ModelBone*> ChildBones;
};

struct ModelMaterial
{
  QString Name;
  int32_t ColorTextureID = -1;		// Index of ImageList & TextureList
  int32_t SpecularTextureID = -1;		// Index of ImageList & TextureList
  bool hasSpecular = false;
  bool useAlphaAsSpecular = false;
};

// Base model class that is exported by the Exporters
struct ModelMeshData
{
  QString groupName;
  QString originalFilename;
  ModelMeshType meshType = ModelMeshType::STATICMESH;
  QList<ModelPoint> pointList;
  QList<ModelPolygon> polygonList;
  BoundingBox boundingBox;
  Vec3D positionOffset;
  Vec3D rotationOffset;
};

struct ModelCamera
{
  QString Name;
  Vec3D Position;
  Vec3D Rotation;
};

struct ModelObject
{
  bool isAnimated = false;
  QString objectName = "ModelObject";
  BoundingBox boundingBox;			// Encompasses all models.
  QList<ModelMaterial> materialList;
  QList<ImageData*> ImageList;		// Load all the textures into this!
  QStringList TextureList;			// List of all the textures, with the same index as ImageList
  QList<ModelMeshData*> MeshList;		// List of all mesh data for this object. Also functions as a model pass
  QList<ModelCamera*> cameraList;		// Attached Cameras
  QList<ModelObject*> childObjects;	// Child objects of this object.
  QList<ModelBone*> Skeleton;			// A hierarchy of all bones in this model.
  QList<AnimationTrack*> Animations;	// All the animations for this model.
};

struct ModelScene
{
  QString Name;
  QList<ModelObject*> Objects;
  QList<ModelCamera*> Cameras;
};