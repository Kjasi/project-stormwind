#pragma once
#include "RGBAColor.h"
#include <QtCore/qlist.h>
#include <QtCore/qsize.h>
#include <vector>

typedef unsigned char uchar;
enum VkFormat;

class ImageData
{
private:
  int32_t Width = 0;
  int32_t Height = 0;
public:
  RGBArray *ColorData;
  RGBArray *ColorDataRaw;
  RGBAColorType ColorType;
  QString sourceFilename;
  ImageData() : Width(0), Height(0), sourceFilename(""), ColorType(RGBAColorType::RGBA), ColorData(new RGBArray), ColorDataRaw(new RGBArray) {};

  void setWidth(int32_t w) { Width = w; };
  void setHeight(int32_t h) { Height = h; };
  void setWidthHeight(int32_t w, int32_t h) { setWidth(w); setHeight(h); }
  int32_t getWidth() { return Width; }
  int32_t getHeight() { return Height; }
  bool hasAlphaData();
  uint8_t getNumChannels();
  uint32_t getDataSize() { return ColorData->getCount(); }
  uint32_t getSize() { return (ColorData->getCount() * ColorType); }
  QSize getImageSize() { return QSize(Width, Height); }
  bool setBlockData(ImageData *src, int offsetX = 0, int offsetY = 0, RGBAColorID useColor = COLORID_ALL);
  bool setRedChannel(ImageData *src, int offsetX = 0, int offsetY = 0);
  bool setGreenChannel(ImageData *src, int offsetX = 0, int offsetY = 0);
  bool setBlueChannel(ImageData *src, int offsetX = 0, int offsetY = 0);
  bool setAlphaChannel(ImageData *src, int offsetX = 0, int offsetY = 0);

  void resizeImage(uint32_t newWidth, uint32_t newHeight);
  std::vector<uchar> getDataAsUCharVector(bool sixteenbit = false, bool bigendian = false);
  std::vector<uint8_t> getDataAsUInt8Vector(bool sixteenbit = false, bool bigendian = false);
  uchar* getDataAsUCharRef(bool inverse = false, bool sixteenbit = false, bool bigendian = false);
  bool isEmpty();
  VkFormat getVulkanFormat();

  void makeTestTexture(uint32_t width = 64, uint32_t height = 64);
  void makeSolidTexture(RGBAColor color, QString filename = QString(), uint32_t width = 64, uint32_t height = 64);
  void makeWhiteTexture(uint32_t width = 64, uint32_t height = 64);
  void makeBlackTexture(uint32_t width = 64, uint32_t height = 64);
  static ImageData generateTestTexture(uint32_t width = 64, uint32_t height = 64);
  static ImageData generateSolidTexture(RGBAColor color, QString Filename = QString(), uint32_t width = 64, uint32_t height = 64);
  static ImageData generateWhiteTexture(uint32_t width = 64, uint32_t height = 64);
  static ImageData generateBlackTexture(uint32_t width = 64, uint32_t height = 64);

  bool operator ==(const ImageData& second) const
  {
    return (Width == second.Width) && (Height == second.Height) && (ColorType == second.ColorType) && (ColorData == second.ColorData) && (ColorDataRaw == second.ColorDataRaw);
  }
  bool operator ==(const ImageData* second) const
  {
    return (Width == second->Width) && (Height == second->Height) && (ColorType == second->ColorType) && (ColorData == second->ColorData) && (ColorDataRaw == second->ColorDataRaw);
  }

private:
  quint32 getIndex(uint32_t w, uint32_t h);
  RGBAColor getColor(uint32_t w, uint32_t h);
  RGBAColor getColorRaw(uint32_t w, uint32_t h);
};