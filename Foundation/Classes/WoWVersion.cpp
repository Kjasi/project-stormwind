#include <Foundation/Constants.h>
#include <Foundation/LogFile.h>
#include <QtWidgets/QMessageBox>
#include <QtCore/qdir.h>
#include <QtCore/qdebug.h>
#include "WoWVersion.h"

#if defined Q_OS_WIN
#include <Windows.h>
#endif

cWoWVersion::cWoWVersion(int major, int minor, int patch, int build)
{
  Major = major;
  Minor = minor;
  Patch = patch;
  Build = build;
  buildKnownLiveBuildsList();
  buildKnownLiveClassicBuildsList();
}

bool cWoWVersion::isUnknown()
{
  if (Major == 0 && Minor == 0 && Patch == 0 && Build == 0)
  {
    return true;
  }
  return !isSupportedBuild();
}

bool cWoWVersion::isExpansion(Expansion expansion)
{
  return (Major == expansion);
}

bool cWoWVersion::isPre(Expansion expansion)
{
  return (Major < expansion);
}

bool cWoWVersion::isPost(Expansion expansion)
{
  return (Major > expansion);
}

bool cWoWVersion::isMPQUser()
{
  return isPre(EXPANSION_WARLORDSOFDRAENOR);
}

bool cWoWVersion::isCASCUser()
{
  return isPost(EXPANSION_MISTSOFPANDARIA);
}

bool cWoWVersion::isSupportedBuild()
{
  // Remove any invalid expansions
  if (isValidExpansion() == false) return false;

  // Catch all for current expansion & classic only.
  // This will enable all builds after the last known live build, even the Test builds.
  if (isExpansion(EXPANSION_CURRENT) && Build > lastKnownLiveBuild) return true;
  if (isExpansion(EXPANSION_VANILLA) && Build > lastKnownLiveClassicBuild) return true;

  return isKnownLiveBuild();
}

bool cWoWVersion::isValidExpansion()
{
  return (Major >= EXPANSION_VANILLA && Major <= EXPANSION_CURRENT);
}

QString cWoWVersion::getString()
{
  return QString("%1.%2.%3.%4").arg(Major).arg(Minor).arg(Patch).arg(Build);
}

bool cWoWVersion::isKnownLiveBuild()
{
  return knownLiveBuilds.contains(Build) || knownLiveClassicBuilds.contains(Build);
}

Expansion cWoWVersion::getExpansion()
{
  switch (Major)
  {
  case EXPANSION_VANILLA:
    return EXPANSION_VANILLA;
    break;
  case EXPANSION_BURNINGCRUSADE:
    return EXPANSION_BURNINGCRUSADE;
    break;
  case EXPANSION_WRATHOFTHELICHKING:
    return EXPANSION_WRATHOFTHELICHKING;
    break;
  case EXPANSION_CATACLYSM:
    return EXPANSION_CATACLYSM;
    break;
  case EXPANSION_MISTSOFPANDARIA:
    return EXPANSION_MISTSOFPANDARIA;
    break;
  case EXPANSION_WARLORDSOFDRAENOR:
    return EXPANSION_WARLORDSOFDRAENOR;
    break;
  case EXPANSION_LEGION:
    return EXPANSION_LEGION;
    break;
  case EXPANSION_BATTLEFORAZEROTH:
    return EXPANSION_BATTLEFORAZEROTH;
    break;
  case EXPANSION_BETA:
    return EXPANSION_BETA;
    break;
  case EXPANSION_MAX:
  case EXPANSION_ALL:
  case EXPANSION_UNKNOWN:
  default:
    return EXPANSION_UNKNOWN;
    break;
  }
}

// Generates our Known Live Build list, and sets the lastKnownLiveBuild
// Updates from http://wowwiki.wikia.com/wiki/Public_client_builds
// Last Updated March 8th, 2019
void cWoWVersion::buildKnownLiveBuildsList()
{
  // Empty our knownLiveBuilds, so we don't end up with multiples...
  knownLiveBuilds.clear();

  // Vanilla
  knownLiveBuilds << 3980 << 3989 << 4044 << 4062 << 4125 << 4147 << 4150 << 4196 << 4211 << 4222 << 4284 << 4297 << 4341 << 4364 << 4375 << 4442 << 4499 << 4500 << 4544 << 4671 << 4695 << 4735 << 4769 << 4784
    << 4807 << 4878 << 4937 << 4983 << 4996 << 5059 << 5086 << 5195 << 5462 << 5230 << 5302 << 5428 << 5464 << 5595 << 5875 << 6005;

  // Burning Crusade
  knownLiveBuilds << 6080 << 6180 << 6299 << 6320 << 6337 << 6383 << 6403 << 6448 << 6546 << 6692 << 6729 << 6739 << 6803 << 6898 << 7272 << 7318 << 7359 << 7561 << 7741 << 7799 << 8089 << 8125 << 8278 << 8606;

  // Wrath of the Lich King
  knownLiveBuilds << 8714 << 9056 << 9183 << 9464 << 9506 << 9551 << 9767 << 9806 << 9835 << 9901 << 9947 << 10192 << 10314 << 10482 << 10505 << 10958 << 11159 << 12213 << 12340;

  // Cataclysm
  knownLiveBuilds << 13164 << 13205 << 13287 << 13329 << 13596 << 13623 << 13914 << 14007 << 14333 << 14480 << 14545 << 15005 << 15050 << 15211 << 15354 << 15595;

  // Mists of Pandaria
  knownLiveBuilds << 16016 << 16048 << 16057 << 16135 << 16309 << 16357 << 16650 << 16669 << 16683 << 16685 << 16701 << 16709 << 16716 << 16733 << 16760 << 16769 << 16826 << 16977 << 16981 << 16983 << 16992
    << 17055 << 17116 << 17128 << 17359 << 17371 << 17399 << 17538 << 17658 << 17688 << 17898 << 17930 << 17956 << 18019 << 18291 << 18414;

  // Warlords of Draenor
  knownLiveBuilds << 19027 << 19034 << 19085 << 19102 << 19103 << 19116 << 19234 << 19342 << 19678 << 19702 << 19802 << 19831 << 19865 << 20173 << 20182 << 20201 << 20216 << 20253 << 20338 << 20444 << 20490
    << 20574 << 20726 << 20779 << 20886 << 21315 << 21336 << 21348 << 21355 << 21463 << 21676 << 21742;

  // Legion
  knownLiveBuilds << 22248 << 22267 << 22280 << 22289 << 22293 << 22345 << 22396 << 22410 << 22423 << 22498 << 22522 << 22522 << 22566 << 22594 << 22624 << 22747 << 22810 << 22900 << 22908 << 22950 << 22989
    << 22995 << 22996 << 23171 << 23222 << 23360 << 23420 << 23826 << 23835 << 23836 << 23846 << 23852 << 23857 << 23877 << 23911 << 23937 << 24015 << 24330 << 24367 << 24415 << 24430 << 24461 << 24742
    << 24920 << 24931 << 25021 << 25021 << 25383 << 25442 << 25455 << 25480 << 25497 << 25549 << 25848 << 25860 << 25864 << 25875 << 25881 << 25901 << 25928 << 25937 << 25961 << 25996 << 26124 << 26365
    << 26654 << 26822 << 26899 << 26972;

  // Battle for Azeroth
  // This list will need to be expanded on periodically, and finalized once the next expansion is about to come out.
  knownLiveBuilds << 27101 << 27144 << 27165 << 27178 << 27219 << 27326 << 27355 << 27366 << 27377 << 27404 << 27547 << 27602 << 27843 << 27980 << 28153 << 28724 << 28768 << 28807 << 28822 << 28833 << 29088
    << 29139 << 29235 << 29297 << 29482 << 29600 << 29621 << 29683 << 29701 << 29718 << 29732 << 29737 << 29814 << 29869 << 29896 << 29981 << 30477 << 30706 << 30920 << 30948 << 30993 << 31229 << 31429
    << 31478;

  lastKnownLiveBuild = knownLiveBuilds.last();
}

void cWoWVersion::buildKnownLiveClassicBuildsList()
{
  // Empty our knownLiveClassicBuilds, so we don't end up with multiples...
  knownLiveClassicBuilds.clear();

  // WoW Classic
  // Not yet live, so support is not yet official.
  knownLiveClassicBuilds << 31466;  // Temp Beta build for functionality... Remove 31466 once Classic is live!

  lastKnownLiveClassicBuild = knownLiveClassicBuilds.last();
}

void cWoWVersion::buildWoWVersion(QString pathToExe, bool &show_warning)
{
  buildWoWVersion(QDir(pathToExe), show_warning);
}

void cWoWVersion::buildWoWVersion(QDir pathToExe, bool &show_warning)
{
#ifdef Q_OS_WIN
  LOGFILE.Debug("Getting WoW Version in Windows. Path: %s", qPrintable(pathToExe.absolutePath()));
  QStringList exeNames;
  QStringList knownExeNames;
  knownExeNames << "WoW.exe" << "WoW-64.exe" << "MWoW.exe" << "MWoW-x64.exe" << "_retail_/WoW.exe" << "_classic_/WoW.exe";

  for (int i = 0; i < knownExeNames.count(); i++)
  {
    QString a = knownExeNames.at(i);

    QFileInfo exeInfo(pathToExe.absoluteFilePath(a));
    if (exeInfo.exists() == true)
    {
      exeNames.push_back(a);
    }
  }

  if (exeNames.isEmpty() == true)
  {
    LOGFILE.Error("Could not find WoW.exe or WoW-64.exe in the path: %s", qPrintable(pathToExe.absolutePath()));
    QMessageBox a(QMessageBox::Icon::Critical, QObject::tr("Unable to Find WoW Application", "Title for missing World of Warcraft application"), QObject::tr("Unable to find WoW.exe or WoW-64.exe in your current World of Warcraft directory.\n\nPlease open your preferences, and make sure the WoW directory points to either of these two files.", "Message for missing Windows WoW application"), QMessageBox::Ok);
    a.exec();
    return;
  }

  QString exeName;
  int i = 0;
  while (exeName.isEmpty() && i < exeNames.count())
  {
    // first of all, GetFileVersionInfoSize
    DWORD dwHandle;
    DWORD dwLen = GetFileVersionInfoSize(pathToExe.absoluteFilePath(exeNames.at(i)).toStdString().c_str(), &dwHandle);
    // GetFileVersionInfo
    LPVOID lpData = new BYTE[dwLen];
    if (!GetFileVersionInfo(pathToExe.absoluteFilePath(exeNames.at(i)).toStdString().c_str(), dwHandle, dwLen, lpData))
    {
      LOGFILE.Error("error in GetFileVersionInfo");
      qDebug() << "error in GetFileVersionInfo";
      delete[] lpData;
      return;
    }

    // VerQueryValue
    VS_FIXEDFILEINFO *lpBuffer = NULL;
    UINT uLen;

    if (!VerQueryValue(lpData,
      QString("\\").toStdString().c_str(),
      (LPVOID*)&lpBuffer,
      &uLen))
    {
      qDebug() << "error in VerQueryValue"; delete[] lpData; return;
    }
    Major = (lpBuffer->dwFileVersionMS >> 16) & 0xffff;
    Minor = (lpBuffer->dwFileVersionMS) & 0xffff;
    Patch = (lpBuffer->dwFileVersionLS >> 16) & 0xffff;
    Build = (lpBuffer->dwFileVersionLS) & 0xffff;
    qDebug() << "WoW Version Filename:" << qPrintable(exeNames.at(i)) << "Build:" << Build;
    LOGFILE.Debug("WoW Version Filename: %s, Build %i", qPrintable(exeNames.at(i)), Build);

    if (isSupportedBuild() && isKnownLiveBuild())
    {
      exeName = exeNames.at(i);
    }
    else if(isSupportedBuild() && isKnownLiveBuild() == false)
    {
      exeName = exeNames.at(i);
      if (isExpansion(EXPANSION_VANILLA) && Build > 6005)
        LOGFILE.Warn("Selected WoW Version is not officially supported. Discovered WoW Version: %i.%i.%i, Build %i. Last Known Classic Build: %i", Major, Minor, Patch, Build, lastKnownLiveClassicBuild);
      else
        LOGFILE.Warn("Selected WoW Version is not officially supported. Discovered WoW Version: %i.%i.%i, Build %i. Last Known live Build: %i", Major, Minor, Patch, Build, lastKnownLiveBuild);
      if (show_warning == true)
      {
        QMessageBox a(QMessageBox::Icon::Warning, QObject::tr("Unknown but supported WoW Version", "Title for a popup about an unknown but supported WoW build."), QObject::tr("This WoW Version is not a known live build, but may still be supported. Incorrect results may occur.", "An unsupported build was discovered, but it won't stop the program from working."), QMessageBox::Ok);
        a.exec();
        show_warning = false;
      }
    }
    else {
      Major = Minor = Patch = Build = 0;
    }
    i++;
  }

#elif defined Q_OS_MACOS
  LOGFILE.Debug("Getting WoW Version in Mac OS. Path: %s", qPrintable(pathToExe.absolutePath()));
  assert("This operating system is not yet supported.");
#elif defined Q_OS_LINUX
  LOGFILE.Debug("Getting WoW Version in Linux. Path: %s", qPrintable(pathToExe.absolutePath()));
  assert("This operating system is not yet supported.");
#else
  assert("This operating system is not yet supported.");
#endif // Q_OS

  LOGFILE.Debug("WoW Version Results: %i.%i.%i, Build %i", Major, Minor, Patch, Build);
}