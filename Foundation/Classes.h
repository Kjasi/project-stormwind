#pragma once
#include "Classes/Vectors.h"
#include "GlobalEnums.h"

struct PointBlock
{
  Vec3D topLeft, topRight, bottomLeft, bottomRight, middle;
};

//QList<Vec2F> getBlockCoordinates(int pixels, PointBlock *point_block);
unsigned int inline getIndexfromCoord(Vec2F coord, unsigned int width)
{
  unsigned int result = (unsigned int)(coord.y * width + coord.x);

  return result;
}

//QList<Vec2F> inline getBlockCoordinates(int pixels, PointBlock *point_block)
//{
//	QList<Vec2F> list;
//	/*
//
//	How we get pixel data:
//
//	1) PixelLerpDistance = pixels + 1
//	2) PixelDivisions = 1 / PixelLerpDistance
//	3) PixelLocationX = lerpside.getPoint((PixelDivisions*(currentXPixel-1))*0.5)
//	4) PixelLocationY = lerpside.getPoint((PixelDivisions*(currentYPixel-1))*0.5)
//	5) Vec2D = Vec2D(PixelLocationX, PixelLocationY)
//
//	Vec2D will contain the Lerp coordinates for all our pixels.
//
//	*/
//
//	int PixelLerpDistance = pixels + 1;
//	float PixelDivisions = 1.0f / PixelLerpDistance;
//	for (int y = 0; y < pixels; y++)
//	{
//		for (int x = 0; x < pixels; x++)
//		{
//			lerp<float, float> axisX = point_block->lerpTop;
//			lerp<float, float> axisY = point_block->lerpLeft;
//			// Change Axis Y as we move across Axis X
//			if (x >(pixels * (1 / 2)))
//			{
//				axisY = point_block->lerpRight;
//			}
//		}
//	}
//
//
//	return list;
//}

template<class T>
T Normalize(T input, T min = 0.0, T max = 1.0)
{
  return (input - min) / (max - min);
}

template<class T>
T NormalizeToRange(T input, T min = 0.0, T max = 1.0, T RangeMin = 0.0, T RangeMax = 1.0)
{
  T value = (input - min) / (max - min);
  return (RangeMax - RangeMin)* value + RangeMin;
}

struct FileInfo
{
  SourceFileType fileType;

  // BLP
  int TextureWidth;
  int TextureHeight;
  int TextureChannelCount;
  bool TextureHasAlpha;

  QStringList TextureList;
  QStringList WMOFileList;
  QStringList ModelFileList;
};