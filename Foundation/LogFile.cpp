/*
  LogFile Class
  Written by Alexander Aberle (Kjasi / Sephiroth3D)
  Copyright �2010-2019

  This is a simple debugging class I wrote for debugging my programs. It includes both a LogFile class that works only in Debug mode,
  and an ErrorLog class that will always export a file when called. Note that even if defined, the files won't be generated unless
  data has been written to them.
*/

#include <time.h>
#include <typeinfo>
#if defined(WIN32) || defined(WIN64)
#include <Windows.h>
#endif
#include "LogFile.h"
using namespace LoggingSystem;

LogFile * LogFile::m_instance = 0;

void LogFile::WriteStream(const char* format, va_list argp)
{
  if (m_stream.is_open() == false) {
    m_stream.open(file, ofstream::out | ofstream::app);
  }
  char cbuffer[1024];
  vsnprintf(cbuffer, 1024, format, argp);
  m_stream << cbuffer << endl;
}
void LogFile::WriteStream(std::string format, va_list argp)
{
  WriteStream(format.c_str(), argp);
}

void LogFile::truncateFile()
{
  if (m_stream.is_open() == false) {
    m_stream.close();
  }
  m_stream.open(file, ofstream::out | ofstream::trunc);
  m_stream.close();
}

string LogFile::getTimeStamp()
{
  char buffer[256];
  SYSTEMTIME st;
  memset(&st, 0, sizeof(SYSTEMTIME));
  GetLocalTime(&st);

  snprintf(buffer, 256, "%d-%02d-%02d %02d:%02d:%02d.%03d | ", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

  std::string myTime = buffer;
  return myTime;
}

string LogFile::BuildString(AlertTypes level, const char* format)
{
  std::string s1(format);

  if (useTimestamp == true) {
    s1.insert(0, getTimeStamp());
  }

  switch (level) {
  case AlertDebug:
    s1.insert(0, "DEBUG | ");
    break;
  case AlertWarn:
    s1.insert(0, " WARN | ");
    break;
  case AlertError:
    s1.insert(0, "\nERROR | ");
    break;
  case AlertFatal:
    s1.insert(0, "\nFATAL | ");
    break;
  case AlertInfo:
  default:
    s1.insert(0, " INFO | ");
    break;
  }
  return s1;
}

// Log a comment, or anything else
void LogFile::Write(const char* logline, ...)
{
  va_list argList;
  va_start(argList, logline);
  WriteStream(logline, argList);
  va_end(argList);
}

// Log an Info message
void LogFile::Info(const char* logline, ...)
{
  string format = BuildString(AlertInfo, logline);
  va_list argList;
  va_start(argList, logline);
  WriteStream(format, argList);
  va_end(argList);
}

// Log a Debug message
// Debug only shows up in Debug builds!
void LogFile::Debug(const char* logline, ...)
{
  if (showDebug == false) return;
  string format = BuildString(AlertDebug, logline);
  va_list argList;
  va_start(argList, logline);
  WriteStream(format, argList);
  va_end(argList);
}

// Log a Warning message
void LogFile::Warn(const char* logline, ...)
{
  string format = BuildString(AlertWarn, logline);
  va_list argList;
  va_start(argList, logline);
  WriteStream(format, argList);
  va_end(argList);
}

// Log an Error message
void LogFile::Error(const char* logline, ...)
{
  string format = BuildString(AlertError, logline);
  //format.append("\n");
  va_list argList;
  va_start(argList, logline);
  WriteStream(format, argList);
  va_end(argList);
}

// Log an Fatal message
void LogFile::Fatal(const char* logline, ...)
{
  string format = BuildString(AlertFatal, logline);
  format.append("\n");
  va_list argList;
  va_start(argList, logline);
  WriteStream(format, argList);
  va_end(argList);
}

#ifdef QT_VERSION
void LogFile::Write(QString logline, ...) {
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(logline.toStdString(), argList);
  va_end(argList);
}

// Log an Info message
void LogFile::Info(QString logline, ...)
{
  string format = BuildString(AlertInfo, logline.toStdString().c_str());
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(format, argList);
  va_end(argList);
}

// Log a Debug message
// Debug only shows up in Debug builds!
void LogFile::Debug(QString logline, ...)
{
  if (showDebug == false) return;
  string format = BuildString(AlertDebug, logline.toStdString().c_str());
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(format, argList);
  va_end(argList);
}

// Log a Warning message
void LogFile::Warn(QString logline, ...)
{
  string format = BuildString(AlertWarn, logline.toStdString().c_str());
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(format, argList);
  va_end(argList);
}

// Log an Error message
void LogFile::Error(QString logline, ...)
{
  string format = BuildString(AlertError, logline.toStdString().c_str());
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(format, argList);
  va_end(argList);
}

// Log an Error message
void LogFile::Fatal(QString logline, ...)
{
  string format = BuildString(AlertFatal, logline.toStdString().c_str());
  va_list argList;
  va_start(argList, logline.toStdString().c_str());
  WriteStream(format, argList);
  va_end(argList);
}

#endif // QT_VERSION

LogFile::~LogFile(){
  if (m_stream.is_open() == true){
    m_stream.close();
  }
}