#pragma once
#include <list>
#include "Component.h"

namespace KJProductions
{
  template <class DataType>
  class Container : public Component
  {
  public:
    Container();
    virtual ~Container();

    bool addChild(DataType * child, DataType * previous = 0, bool afterComponent = false);
    bool removeChild(DataType * child);
    void removeAllChildren();

    template <class ChildType>
      int removeAllChildrenOfType();

    unsigned int nbChildren() const { return (unsigned int)m_children.size(); }

    int findChild(DataType * child, bool recursive = false) const;
    DataType * getChild(int index);
    const DataType * getChild(int index) const;

  private:
    std::list<DataType *> m_children;
  };

  template<class DataType>
  Container<DataType>::Container()
  {
    m_children.clear();
    setName("Container");
  }

  template<class DataType>
  Container<DataType>::~Container()
  {
    typename std::list<DataType *>::iterator l_it;
    for (l_it = m_children.begin(); l_it != m_children.end(); l_it++)
    {
      (*l_it)->unref();
    }
  }

  template<class DataType>
  bool Container<DataType>::addChild(DataType * child, DataType * previous /* = 0 */, bool afterComponent /* = false */)
  {
    // is this child already in list ?
    if (findChild(child) != -1)
    {
      return false;
    }
    // LOGFILE.Debug("Adding Child...");

    // if a component to insert after was given, we search for it before inserting
    typename std::list<DataType *>::iterator l_it = m_children.end();

    if (previous != 0)
    {
      for (l_it = m_children.begin(); l_it != m_children.end(); l_it++)
      {
        if ((*l_it) == previous)
        {
          if (afterComponent)
          {
            l_it++;
          }
          break;
        }
      }
    }

    // ok, we add it in list
    if (l_it != m_children.end())
    {
      m_children.insert(l_it, child);
    }
    else
    {
      m_children.push_back(child);
    }
    child->setParent(this);

    // for hierarchy notification
    // LOGFILE.Debug("onChildAdded...");
    onChildAdded(child);

    // for external observers notification
    child->setParent(this);
    Event * l_p_event = new Event(this, Event::CHILD_ADDED);
    notify(*l_p_event);
    delete l_p_event;

    return true;
  }

  template<class DataType>
  bool Container<DataType>::removeChild(DataType * child)
  {
    // ok, we remove this child from the list
    child->ref();
    m_children.remove(child);
    child->setParent(0);

    // for hierarchy notification
    onChildRemoved(child);

    // for external observers notification
    child->setParent(this);
    Event * l_p_event = new Event(this, Event::CHILD_REMOVED);
    notify(*l_p_event);
    delete l_p_event;

    child->unref();
    return true;
  }

  template<class DataType>
  void Container<DataType>::removeAllChildren()
  {
    while (!m_children.empty())
    {
      removeChild(*m_children.rbegin());
    }
  }

  template <class DataType> template <class ChildType>
  int Container<DataType>::removeAllChildrenOfType()
  {
    typename std::list<DataType *>::iterator l_it;
    std::list<DataType *> l_childrenToRemove;
    for (l_it = m_children.begin(); l_it != m_children.end(); l_it++)
    {
      if (dynamic_cast<ChildType *>(*l_it) != 0)
      {
        l_childrenToRemove.push_back(*l_it);
      }
    }

    int l_result = l_childrenToRemove.size();

    for (l_it = l_childrenToRemove.begin(); l_it != l_childrenToRemove.end(); l_it++)
    {
      removeChild(*l_it);
    }

    return l_result;
  }



  template<class DataType>
  int Container<DataType>::findChild(DataType * child, bool recursive /* = false */) const
  {
    typename std::list<DataType *>::const_iterator l_it = m_children.begin();
    int l_result = -1;
    for (int l_index = 0; l_it != m_children.end(); l_it++, l_index++)
    {
      if (*l_it == child)
      {
        l_result = l_index;
      }
      if (recursive)
      {
        l_result = (*l_it)->findChild(child, recursive);
        if (l_result != -1)
        {
          break;
        }
      }
    }
    return l_result;
  }

  template<class DataType>
  DataType * Container<DataType>::getChild(int index)
  {
    DataType * l_p_result = 0;
    if (index >= 0 && index < m_children.size())
    {
      size_t l_index = 0;
      typename std::list<DataType *>::iterator l_it;
      for (l_it = m_children.begin(); l_index < index; l_index++)
      {
        l_it++;
      }
      l_p_result = *l_it;
    }
    return l_p_result;
  }

  template<class DataType>
  const DataType * Container<DataType>::getChild(int index) const
  {
    const DataType * l_p_result = 0;
    if (index >= 0 && index < m_children.size())
    {
      size_t l_index = 0;
      typename std::list<DataType *>::const_iterator l_it;
      for (l_it = m_children.begin(); l_index < index; l_index++)
      {
        l_it++;
      }
      l_p_result = *l_it;
    }
    return l_p_result;
  }

}