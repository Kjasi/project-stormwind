#include <QtCore/qdir.h>
#include <QtCore/qpluginloader.h>
#include "Kernel.h"

#include "../Plugins/PluginExportModel.h"
#include "../Plugins/PluginExportImage.h"
#include "../Plugins/PluginExportPlacement.h"

using namespace ProjectSW;

Kernel::Kernel()
{
  l_PluginExportImage = new QList<PluginExportImage*>();
  l_PluginExportModel = new QList<PluginExportModel*>();
  l_PluginExportPlacement = new QList<PluginExportPlacement*>();
}

Kernel::~Kernel() {}

void Kernel::init(const QString & a_pluginDir)
{
  LOGFILE.Info("--== Loading Plugins ==--");
  QDir pluginDir(a_pluginDir);
  QStringList plugins = pluginDir.entryList(QStringList() << "*.dll", QDir::Files);
  for (size_t i = 0; i<plugins.size(); i++)
  {
    loadPlugin(pluginDir.absoluteFilePath(plugins[(int)i]));
  }

  LOGFILE.Info("--== Plugin Report ==--");
  LOGFILE.Info("Number of loaded plugins: %i", nbChildren());
  LOGFILE.Info("Number of Model Exporters: %i", nbModelExportPlugin());
  LOGFILE.Info("Number of Image Exporters: %i", nbImageExportPlugin());
  LOGFILE.Info("Number of Placement Exporters: %i", nbImageExportPlugin());
  LOGFILE.Info("--== End Plugin Loading ==--");
}

void Kernel::loadPlugin(const QString &sFileName)
{
  QString lname;
  lname.append(sFileName);
  lname = lname.mid(lname.lastIndexOf("/")+1);
  LOGFILE.Info("Loading Plugin: %s", qPrintable(lname));

  QPluginLoader m_loader(sFileName);

  if (QObject * l_p_plugin = m_loader.instance())
  {
    for (int i = 0; i < (int)nbChildren(); i++)
    {
      if (getChild(i)->id() == dynamic_cast<Plugin *>(l_p_plugin)->id())
      {
        LOGFILE.Warn("Plugin already loaded. Aborting...");
        return;
      }
    }
    addChild(dynamic_cast<Plugin *>(l_p_plugin));

  } else {
    LOGFILE.Error("Unable to load plugin file %s: %s", qPrintable(lname), qPrintable(m_loader.errorString()));
  }
}

template<class PIC>
bool Kernel::CheckPlugin(Component * child, int checkver, const char* logType, QString & log, QList<PIC*> *list)
{
  PIC *a = dynamic_cast<PIC*>(child);
  if (a->compatibleAppVersion() == checkver)
  {
    log.append(" --> ").append(logType).append(": ").append(a->name().c_str());
    list->push_back(a);
    LOGFILE.Debug("Added %s to the plugin list for %s. Total: %i", a->name().c_str(), logType, list->count());

  }
  else {
    LOGFILE.Warn("Plugin Loading Error: \"%s\" is an incompatible version. Please use a version of the plugin that is compatible.", a->name().c_str());
    return true;
  }
  return false;
};

void Kernel::onChildAdded(Component * l_p_child)
{
  bool badplugin = false;
  QString logPluginType = "Kernel: New Plugin";

  if (dynamic_cast<PluginExportImage*>(l_p_child))
  {
    badplugin = CheckPlugin<PluginExportImage>(l_p_child, PLUGINEXPORTIMAGEVERSION, "Image Export", logPluginType, l_PluginExportImage);
    if (badplugin == false)
    {
      LOGFILE.Debug("Added Image Exporter: %s", l_p_child->name().c_str());
    }
  }
  else if (dynamic_cast<PluginExportModel*>(l_p_child)) {
    badplugin = CheckPlugin<PluginExportModel>(l_p_child, PLUGINEXPORTMODELVERSION, "Model Export", logPluginType, l_PluginExportModel);
    if (badplugin == false)
    {
      LOGFILE.Debug("Added Model Exporter: %s", l_p_child->name().c_str());
    }
  }
  else if (dynamic_cast<PluginExportPlacement*>(l_p_child)) {
    badplugin = CheckPlugin<PluginExportPlacement>(l_p_child, PLUGINEXPORTPLACEMENTVERSION, "Placement Export", logPluginType, l_PluginExportPlacement);
    if (badplugin == false)
    {
      LOGFILE.Debug("Added Placement Exporter: %s", l_p_child->name().c_str());
    }
  }
  else {
    logPluginType.append(" --> Unknown Plugin Type");
  }

  if (badplugin == false)
    LOGFILE.Debug(qPrintable(logPluginType));
}

int ProjectSW::Kernel::nbModelExportPlugin()
{
  if (l_PluginExportModel == nullptr || l_PluginExportModel == NULL) return 0;
  return l_PluginExportModel->size();
}

int ProjectSW::Kernel::nbImageExportPlugin()
{
  if (l_PluginExportImage == nullptr || l_PluginExportImage == NULL) return 0;
  return l_PluginExportImage->size();
}

int ProjectSW::Kernel::nbPlacementExportPlugin()
{
  if (l_PluginExportPlacement == nullptr || l_PluginExportPlacement == NULL) return 0;
  return l_PluginExportPlacement->size();
}
