#pragma once
#include <iostream>
#include <string.h>
#include <typeinfo> // for std::bad_cast in inherited classes

#include "Observable.h"
#include "Observer.h"
#include "Visitor.h"

namespace KJProductions
{
  class Component : public Observable, public Observer {
  public:
    // Constructors 
    Component();

    // Destructors
    virtual ~Component();

    virtual bool addChild(Component *, Component * previous = 0, bool afterComponent = false);
    virtual bool removeChild(Component *);
    virtual void removeAllChildren() { }

    virtual void onChildAdded(Component *);
    virtual void onChildRemoved(Component *);

    virtual unsigned int nbChildren() const { return 0; }

    virtual int findChild(Component * /* component */, bool /* recursive */) const { return -1; }
    virtual Component * getChild(int index) { return 0; }
    virtual const Component * getChild(int index) const { return 0; }

    // visitor method
    virtual void accept(Visitor *) = 0;

    void setParent(Component *);
    virtual void onParentSet(Component *);
    const Component * parent() const { return m_p_parent; }
    Component * parent() { return m_p_parent; }

    template <class DataType>
      const DataType * firstParentOfType();

    // auto delete management
    void ref();
    void unref();

    // Name management
    void setName(const std::string & name);
    std::string name() const;
    virtual void onNameChanged();

    // misc
    void print(int l_depth = 0);

    // copy
    void copy(const Component & component, bool /* recursive*/);

  private:

    Component * m_p_parent;

    unsigned int m_refCounter;

    std::string m_name;
  };

  template <class DataType>
  const DataType * Component::firstParentOfType()
  {
    if (parent() != 0)
    {
      DataType * l_p_parent = dynamic_cast<DataType *>(parent());
      if (l_p_parent != 0)
        return l_p_parent;
      else
        return parent()->firstParentOfType<DataType>();
    }
    return 0;
  }
}