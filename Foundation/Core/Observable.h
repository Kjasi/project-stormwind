#pragma once
#include <list>
#include "Event.h"

namespace KJProductions
{
  class Observer;
}

// Namespaces used
//--------------------------------------------------------------------


// Class Declaration
//--------------------------------------------------------------------
namespace KJProductions
{
  class Observable
  {
  public:
    // Constants / Enums

    // Constructors 
    Observable();

    // Destructors
    virtual ~Observable();

    // Methods
    void attach(Observer *);

    void detach(Observer *);

  protected:
    // Constants / Enums

    // Constructors 

    // Destructors

    // Methods
    void notify(Event &);

    // Members

  private:
    // Constants / Enums

    // Constructors 

    // Destructors

    // Methods
    std::list<Observer *>::iterator observerAttached(Observer *);

    // Members
    std::list<Observer *> m_observerList;

    // friend class declarations
  };
}