#pragma once
#include "Container.h"
#include "Singleton.h"
#include "../Plugins/Plugin.h"

namespace ProjectSW
{
  class PluginExportImage;
  class PluginExportModel;
  class PluginExportPlacement;

  class Kernel : public KJProductions::Singleton<Kernel>, public KJProductions::Container<ProjectSW::Plugin>
  {
  public:
    Kernel();
    ~Kernel();

    void init(const QString & a_pluginDir);

    void loadPlugin(const QString &a_fileName);

    virtual void accept(KJProductions::Visitor *) {};

    void onChildAdded(Component *);

    int nbModelExportPlugin();
    int nbImageExportPlugin();
    int nbPlacementExportPlugin();
    QList<PluginExportImage*> *getPluginExportImageList() { return l_PluginExportImage; };
    QList<PluginExportModel*> *getPluginExportModelList() { return l_PluginExportModel; };
    QList<PluginExportPlacement*> *getPluginExportPlacementList() { return l_PluginExportPlacement; };

    template <class PIC>
    bool CheckPlugin(Component * child, int checkver, const char* logType, QString & log, QList<PIC*> *list);

  private:
    QList<PluginExportImage*> *l_PluginExportImage;
    QList<PluginExportModel*> *l_PluginExportModel;
    QList<PluginExportPlacement*> *l_PluginExportPlacement;
  };
}

#define KERNEL ProjectSW::Kernel::instance()