/*----------------------------------------------------------------------*\
| This file is part of Machinima Producer                                |
|                                                                        |
| Machinima Producer is free software: you can redistribute it and/or    |
| modify it under the terms of the GNU General Public License as         |
| published by the Free Software Foundation, either version 3 of the     |
| License, or (at your option) any later version.                        |
|                                                                        |
| Machinima Producer is distributed in the hope that it will be useful,  |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with Machinima Producer.                                         |
| If not, see <http://www.gnu.org/licenses/>.                            |
\*----------------------------------------------------------------------*/


/*
 * Event.h
 *
 *  Created on: 26 may 2012
 *   Copyright: 2012 , Machinima Producer (http://machinima-producer.org)
 */
 
#ifndef _EVENT_H_
#define _EVENT_H_

// Includes / class Declarations
//--------------------------------------------------------------------
// STL

// Qt 

// Externals

// Other libraries

// Current library
namespace KJProductions
{
  class Observable;
}

// Namespaces used
//--------------------------------------------------------------------


// Class Declaration
//--------------------------------------------------------------------
namespace KJProductions
{
class Event
{
  public :
    // Constants / Enums
    enum EventType
    {
      DESTROYED = 0xFFFFFF00,
      BEFORE_DELETED,
      NAME_CHANGED,
      CHILD_ADDED,
      CHILD_REMOVED,
      STATIC_PARAMETERS_CHANGED,
      DYNAMIC_PARAMETERS_CHANGED
    };


    // Constructors 
    Event(Observable *, EventType);

    // Destructors
    ~Event();

    // Methods
    EventType type() { return m_type; }

    Observable * sender() { return m_p_sender; }

  protected :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods

    // Members
    
  private :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods
    
    // Members
    EventType m_type;
    Observable * m_p_sender;

    // friend class declarations
};

// static members definition
#ifdef _EVENT_CPP_

#endif
}

#endif /* _EVENT_H_ */
