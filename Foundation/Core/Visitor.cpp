/*----------------------------------------------------------------------*\
| This file is part of Machinima Producer                                |
|                                                                        |
| Machinima Producer is free software: you can redistribute it and/or    |
| modify it under the terms of the GNU General Public License as         |
| published by the Free Software Foundation, either version 3 of the     |
| License, or (at your option) any later version.                        |
|                                                                        |
| Machinima Producer is distributed in the hope that it will be useful,  |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with Machinima Producer.                                         |
| If not, see <http://www.gnu.org/licenses/>.                            |
\*----------------------------------------------------------------------*/


/*
 * Visitor.cpp
 *
 *  Created on: 26 may 2012
 *   Copyright: 2012 , Machinima Producer (http://machinima-producer.org)
 */

#define _VISITOR_CPP_
#include "Visitor.h"
#undef _VISITOR_CPP_

// Includes / class Declarations
//--------------------------------------------------------------------
// STL
#include <iostream>

// Qt 

// Irrlicht

// Externals

// Other libraries

// Current library
#include "Component.h"


// Namespaces used
//--------------------------------------------------------------------
using namespace KJProductions;


// Beginning of implementation
//--------------------------------------------------------------------


// Constructors 
//--------------------------------------------------------------------
Visitor::Visitor() : m_name(""), m_componentName(""), m_searchForComponentName(true)
{

}


// Destructor
//--------------------------------------------------------------------
Visitor::~Visitor()
{

}


// Public methods
//--------------------------------------------------------------------
void Visitor::defaultAction(Component * component)
{
  // we search for component name, only if needed (we create a visitor inside a visitor => boolean is a stop in recursivity)
  /* @TODO : call class name visitor
  if(m_searchForComponentName)
  {
    Visitor * l_p_nameVisitor = component->visitor(Visitor::CLASSNAME);
    l_p_nameVisitor->m_searchForComponentName = false;
    component->accept(l_p_nameVisitor);
    m_componentName = l_p_nameVisitor->componentName();
  }
  */
  m_componentName = "";

  std::cout << m_name << " : no visitor implemented for " << ((m_componentName == "")?"this component":m_componentName) << std::endl;
}


// Protected methods
//--------------------------------------------------------------------

// Private methods
//--------------------------------------------------------------------
