#pragma once

namespace KJProductions
{
  template <typename T>
  class Singleton
  {
  public:
    static T & instance()
    {
      if (!p_sw_singleton)
      {
        p_sw_singleton = new T;
      }
      return *p_sw_singleton;
    }
  protected:
    Singleton() {}
    virtual ~Singleton() {}

  private:
    static T * p_sw_singleton;
  };

  // static members definition
  template <typename T> T * KJProductions::Singleton<T>::p_sw_singleton = NULL;

}