/*----------------------------------------------------------------------*\
| This file is part of Machinima Producer                                |
|                                                                        |
| Machinima Producer is free software: you can redistribute it and/or    |
| modify it under the terms of the GNU General Public License as         |
| published by the Free Software Foundation, either version 3 of the     |
| License, or (at your option) any later version.                        |
|                                                                        |
| Machinima Producer is distributed in the hope that it will be useful,  |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with Machinima Producer.                                         |
| If not, see <http://www.gnu.org/licenses/>.                            |
\*----------------------------------------------------------------------*/


/*
 * Visitor.h
 *
 *  Created on: 26 may 2012
 *   Copyright: 2012 , Machinima Producer (http://machinima-producer.org)
 */
 
#ifndef _VISITOR_H_
#define _VISITOR_H_

// Includes / class Declarations
//--------------------------------------------------------------------
// STL
#include <string>

// Qt 

// Irrlicht

// Externals

// Other libraries

// Current library
namespace KJProductions
{
  class Component;
}

// Namespaces used
//--------------------------------------------------------------------


// Class Declaration
//--------------------------------------------------------------------
namespace KJProductions
{
class Visitor
{
  public :
    // Constants / Enums
    enum VisitorType
    {
      CLASSNAME = 1
    };

    // Constructors 
    Visitor();

    // Destructors
    virtual ~Visitor();

    // Methods
    void setName(std::string name) { m_name = name; }
    std::string name() { return m_name; }

    virtual void defaultAction(Component *);

    std::string componentName() { return m_componentName; }

    
  protected :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods

    // Members
    std::string m_componentName;
    bool m_searchForComponentName;
    
  private :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods
    
    // Members
    std::string m_name;


    // friend class declarations
};

// static members definition
#ifdef _VISITOR_CPP_

#endif
}

#endif /* _VISITOR_H_ */
