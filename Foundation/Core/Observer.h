/*----------------------------------------------------------------------*\
| This file is part of Machinima Producer                                |
|                                                                        |
| Machinima Producer is free software: you can redistribute it and/or    |
| modify it under the terms of the GNU General Public License as         |
| published by the Free Software Foundation, either version 3 of the     |
| License, or (at your option) any later version.                        |
|                                                                        |
| Machinima Producer is distributed in the hope that it will be useful,  |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with Machinima Producer.                                         |
| If not, see <http://www.gnu.org/licenses/>.                            |
\*----------------------------------------------------------------------*/


/*
 * Observer.h
 *
 *  Created on: 26 may 2012
 *   Copyright: 2012 , Machinima Producer (http://machinima-producer.org)
 */
 
#ifndef _OBSERVER_H_
#define _OBSERVER_H_

// Includes / class Declarations
//--------------------------------------------------------------------
// STL
#include <list>
// Qt 

// Externals

// Other libraries

// Current library
namespace KJProductions
{
  class Event;
  class Observable;
}

// Namespaces used
//--------------------------------------------------------------------


// Class Declaration
//--------------------------------------------------------------------
namespace KJProductions
{
class Observer
{
  public :
    // Constants / Enums

    // Constructors 
    Observer();

    // Destructors
    virtual ~Observer();

    // Methods
    
  protected :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods
    virtual void onDestroyEvent() {}
    virtual void onEvent(Event *) {}

    // Members
    
  private :
    // Constants / Enums
  
    // Constructors 
  
    // Destructors
  
    // Methods
    void treatEvent(Event *);
    void addObservable(Observable *);
    void removeObservable(Observable *);
    
    std::list<Observable *>::iterator findObservable(Observable *);

    // Members
    std::list<Observable *> m_observableList;

    // friend class declarations
    friend class Observable;

};

// static members definition
#ifdef _OBSERVER_CPP_

#endif
}

#endif /* _OBSERVER_H_ */
