#pragma once
#include <QtCore/qsettings.h>

#define SETTINGS ProjectSettings::instance()

class ProjectSettings : public QSettings
{
private:
  static ProjectSettings *m_instance;

public:

  ProjectSettings(const QString &fileName, Format format, QObject *parent = nullptr) : QSettings(fileName, format, parent) {}

  static ProjectSettings & instance();
};