#pragma once
#include <Foundation/GlobalEnums.h>
#include <Foundation/Settings.h>
#include <Foundation/Core/Kernel.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <WoWFormats/Textures/BLP.h>

#include <QtWidgets/qfiledialog.h>

namespace PSWUtil
{
  QStringList getFilenameVariations(QString FilePath)
  {
    LOGFILE.Info("Getting Name Variations for %s", qPrintable(FilePath));
    QStringList filenameSplit = FilePath.split("\\");
    QString fileName = filenameSplit.last();
    QString fileNameSpec = QString("%1_s%2").arg(fileName.mid(0, fileName.lastIndexOf("."))).arg(fileName.mid(fileName.lastIndexOf(".")));
    QString fileNameHeight = QString("%1_h%2").arg(fileName.mid(0, fileName.lastIndexOf("."))).arg(fileName.mid(fileName.lastIndexOf(".")));
    SETTINGS.sync();
    QDir WoWDir = SETTINGS.value("wowdir").toString();
    QDir LastDir = SETTINGS.value("lastdir").toString();

    QStringList filesToCheck;

    uint32_t count = filenameSplit.count() - 1;
    for (int64_t str = count; str >= 0; str--)
    {
      // Check WoW Path
      QString filepath = WoWDir.absolutePath();
      filepath.append("/");
      for (uint32_t i = 0; i < str; i++)
      {
        filepath.append(filenameSplit.at(i));
        filepath.append("/");
      }
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileName));
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileNameSpec));
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileNameHeight));

      //LOGFILE.Debug("Pushing WoW: %s & %s", qPrintable(QString("%1%2").arg(filepath).arg(fileName)), qPrintable(QString("%1%2").arg(filepath).arg(fileNameSpec)));

      // Check Local Files
      filepath = LastDir.absolutePath();
      filepath.append("/");
      for (uint32_t i = 0; i < str; i++)
      {
        filepath.append(filenameSplit.at(i));
        filepath.append("/");
      }
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileName));
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileNameSpec));
      filesToCheck.push_back(QString("%1%2").arg(filepath).arg(fileNameHeight));

      //LOGFILE.Debug("Pushing Local: %s & %s", qPrintable(QString("%1%2").arg(filepath).arg(fileName)), qPrintable(QString("%1%2").arg(filepath).arg(fileNameSpec)));
    }

    return filesToCheck;
  }

  QString getExportFilename(QString originalFilePath, QDir rootPath = QString())
  {
    QString ofn = QString();
    SETTINGS.sync();
    if (rootPath.isEmpty() || rootPath == QString() || rootPath == QDir())
    {
      rootPath.setPath(SETTINGS.value("outputdir").toString());
    }
    QDir WoWDir = SETTINGS.value("wowdir").toString();
    ofn = rootPath.absolutePath();
    ofn.append("/");
    LOGFILE.Debug("OutputPath OpenFileName: %s", qPrintable(ofn));
    LOGFILE.Debug("originalFilePath: %s", qPrintable(originalFilePath));
    LOGFILE.Debug("WoW Dir: %s", qPrintable(WoWDir.absolutePath()));

    if (SETTINGS.value("mirrorwowpaths") == true)
    {
      QString wowPath = originalFilePath;
      wowPath.replace(WoWDir.absolutePath() + "/", "");
      wowPath = wowPath.left(wowPath.lastIndexOf("/"));
      LOGFILE.Debug("WoWPath: %s", qPrintable(wowPath));

      ofn.append(wowPath);
      LOGFILE.Debug("MirrorPath OpenFileName: %s", qPrintable(ofn));
    }
    ofn.replace("\\", "/");
    QDir().mkpath(ofn);

    QString filesName = originalFilePath.mid(originalFilePath.lastIndexOf("/"));
    LOGFILE.Debug("filesName 1: %s", qPrintable(filesName));
    filesName = filesName.left(filesName.lastIndexOf("."));
    LOGFILE.Debug("filesName 2: %s", qPrintable(filesName));
    ofn.append(filesName);

    LOGFILE.Debug("Final ofn: %s", qPrintable(ofn));

    return ofn;
  }

  QString getSaveFileError(SaveFileErrorType sfet)
  {
    QString error = "";

    switch (sfet)
    {
    case SIET_None:
      break;
    case SEIT_BadFilter:
      error = QObject::tr("Unable to find selected filter.", "SaveFileErrorType for a bad filter.");
      break;
    case SEIT_BadFilename:
      error = QObject::tr("An invalid or empty filename was used.", "SaveFileErrorType for a bad, invalid, or empty filename.");
      break;
    case SEIT_NoPlugins:
      error = QObject::tr("No plugins found at all!", "SaveFileErrorType for having no plugins at all.");
      break;
    case SEIT_NoPluginsOfType:
      error = QObject::tr("No exporting plugins of the proper type were found.", "SaveFileErrorType for having no plugins of the right type.");
      break;
    case SEIT_PluginNotFound:
      error = QObject::tr("Unable to find the plugin...", "SaveFileErrorType for missing plugin.");
      break;
    case SEIT_PluginError:
      error = QObject::tr("An error occured in the plugin itself.", "SaveFileErrorType for a plugin error.");
      break;
    case SEIT_NoData:
      error = QObject::tr("No data to export.", "SaveFileErrorType for no data.");
      break;
    default:
      error = QObject::tr("An unknown error occured...", "SaveFileErrorType for an Unknown error.");
      break;
    }

    return error;
  }

  SaveFileErrorType getImagePlugin(ProjectSW::PluginExportImage *&SavingPlugin, QString &selectedFilter, ProjectSW::ImageDestinationType ImageType = ProjectSW::ImageDestinationType::COLORALPHA)
  {
    ProjectSW::Kernel *kernal = &ProjectSW::Kernel::instance();

    if (kernal->nbChildren() <= 0)
    {
      LOGFILE.Error("No plugins found at all. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPlugins;
    }

    QList<ProjectSW::PluginExportImage*> *imgpluginList = new QList<ProjectSW::PluginExportImage*>;
    for (unsigned int i = 0; i < kernal->nbChildren(); i++)
    {
      if (dynamic_cast<ProjectSW::PluginExportImage*>(kernal->getChild(i)))
      {
        ProjectSW::PluginExportImage *plg = dynamic_cast<ProjectSW::PluginExportImage*>(kernal->getChild(i));
        imgpluginList->push_back(plg);
      }
    }

    //LOGFILE.Debug("Num Children: %i", krnl.nbChildren());
    //LOGFILE.Debug("Num Plugins Models: %i", krnl.nbModelExportPlugin());
    //LOGFILE.Debug("Num Plugins Images: %i", imgpluginList->count());

    if (imgpluginList->count() <= 0)
    {
      LOGFILE.Error("No image exporting plugins found. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPluginsOfType;
    }
    for (size_t i = 0; i < imgpluginList->count(); i++)
    {
      ProjectSW::PluginExportImage* plugin = imgpluginList->at((int)i);
      if (!plugin->getImageOutTypes().contains(ImageType)) { continue; }

      QStringList pluginext = plugin->getFileExtensions();
      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at((int)j)));
      }
      exts.append(")");

      if (selectedFilter == exts)
      {
        // Found the plugin!
        SavingPlugin = plugin;
        LOGFILE.Debug("Plugin %s found!", SavingPlugin->name().c_str());
        break;
      }
    }
    return SaveFileErrorType::SIET_None;
  }

  SaveFileErrorType getSaveFilename(QString initialName, QString &OutputName, QString FilterList, QString &selectedFilter, QWidget *parent = 0)
  {
    LOGFILE.Info("Getting Save Filename; initial: %s, filterList: %s", qPrintable(initialName), qPrintable(FilterList));
    OutputName = QFileDialog::getSaveFileName(parent, QObject::tr("Save File"), initialName, FilterList, &selectedFilter);
    SETTINGS.sync();
    QDir LastDir = SETTINGS.value("lastdir").toString();
    if (&selectedFilter == nullptr || selectedFilter == NULL || selectedFilter.isEmpty())
    {
      LOGFILE.Error("Unable to find selected filter. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilter;
    }
    if (OutputName.isEmpty() || OutputName.isNull())
    {
      LOGFILE.Error("Filename Empty. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }
    else if (OutputName == LastDir.absolutePath())
    {
      LOGFILE.Error("Filename not set. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }
    return SaveFileErrorType::SIET_None;
  }

  // Returns an empty string if the directory is not set.
  QString getExportRootDirectory(QString startDir)
  {
    QString dir = QFileDialog::getExistingDirectory(0, QObject::tr("Select Export Root Directory"), startDir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir.isNull() || dir.isEmpty())
    {
      LOGFILE.Error("Unable to find directory...");
      return QString();
    }
    return dir;
  }
  QString getExportRootDirectory(QDir startDir)
  {
    return getExportRootDirectory(startDir.absolutePath());
  }

  SaveFileErrorType SaveImage(ProjectSW::PluginExportImage *plugin, QString fileName, ImageData *imageData, ProjectSW::ImageDestinationType ImageType = ProjectSW::ImageDestinationType::COLORALPHA)
  {
    if (plugin == NULL || plugin == nullptr)
    {
      LOGFILE.Error("No plugin specified. Unable to save image.");
      return SaveFileErrorType::SEIT_PluginNotFound;
    }
    if (fileName.isNull() == true || fileName.isEmpty() == true)
    {
      LOGFILE.Error("No filename specified. Unable to save image.");
      return SaveFileErrorType::SEIT_BadFilename;
    }
    if (imageData == NULL || imageData == nullptr || imageData->isEmpty() == true)
    {
      LOGFILE.Error("No image data supplied. Unable to save image.");
      return SaveFileErrorType::SEIT_NoData;
    }
    LOGFILE.Info("Saving image: %s", qPrintable(fileName));

    int extensionIndex = 0;

    // Fix filename if BLP is still attached!
    if (fileName.contains(".blp", Qt::CaseInsensitive) == true)
    {
      QString ext = plugin->getFileExtensions().at(extensionIndex);
      fileName = fileName.mid(0, fileName.lastIndexOf(".blp", -1, Qt::CaseInsensitive) + 1);
      fileName.append(ext);
      LOGFILE.Debug("Correcting filename to: %s", qPrintable(fileName));
    }

    ProjectSW::PluginImageType saveImageType = ProjectSW::IMGTYPE_RGB;
    ProjectSW::PluginImageDepth saveImageDepth = ProjectSW::IMGDEPTH_8BIT;
    switch (ImageType)
    {
    case ProjectSW::LANDSCAPEHEIGHT:
      saveImageType = ProjectSW::IMGTYPE_HEIGHTMAP;
      saveImageDepth = ProjectSW::IMGDEPTH_32BIT;
      break;
    case ProjectSW::MASKLAYER:
      saveImageType = ProjectSW::IMGTYPE_GREY;
      break;
    case ProjectSW::COLORALPHA:
      saveImageType = ProjectSW::IMGTYPE_RGBA;
      break;
    case ProjectSW::COLOR:
    default:
      saveImageType = ProjectSW::IMGTYPE_RGB;
      break;
    }

    LOGFILE.Debug("Image Type: %i, Image Depth: %i", saveImageType, saveImageDepth);

    // Correct the max heightmap depth
    if (saveImageType == ProjectSW::IMGTYPE_HEIGHTMAP && plugin->getMaxHeightmapDepth() < saveImageDepth)
    {
      saveImageDepth = plugin->getMaxHeightmapDepth();
    }

    // Test Output
    // imageData = &ImageData::generateTestTexture();

    // change to return SaveFileErrorType::SEIT_PluginError if problem... 
    bool saved = plugin->writeImageFile(fileName, imageData, extensionIndex, saveImageType, saveImageDepth);
    if (saved == false)
    {
      LOGFILE.Error("Experienced a plugin error while saving.");
      return SaveFileErrorType::SEIT_PluginError;
    }

    LOGFILE.Info("Saving image Completed.");
    return SaveFileErrorType::SIET_None;
  }

  SaveFileErrorType SaveImage(QString fileName, QString FilterList, ImageData * imageData, ProjectSW::PluginExportImage *&SavingPlugin, ProjectSW::ImageDestinationType ImageType = ProjectSW::ImageDestinationType::COLORALPHA, QString &filePath = QString(), QWidget *parent = 0)
  {
    LOGFILE.Info("Saving image: %s...", qPrintable(fileName));
    SETTINGS.sync();
    QDir WoWDir = SETTINGS.value("wowdir").toString();
    QDir LastDir = SETTINGS.value("lastdir").toString();

    SETTINGS.sync();
    QString selectedFilter = SETTINGS.value("preferedExporter/textures").toString();
    QString fileTypeString = "";
    if (LastDir == QDir() || LastDir.isEmpty()) LastDir = WoWDir;

    switch (ImageType)
    {
    case ProjectSW::LANDSCAPEHEIGHT:
      fileTypeString = "Height";
      break;
    case ProjectSW::MASKLAYER:
      fileTypeString = "Mask";
      break;
    default:
      break;
    }

    QString extension = selectedFilter.mid(selectedFilter.indexOf(" (*.") + 4, 3);
    QString rawFileName = fileName;
    LOGFILE.Debug("rawFilename: %s, new extension: %s", qPrintable(rawFileName), qPrintable(extension));

    QString saveFileName = QString("%1_%2.%3").arg(rawFileName).arg(fileTypeString).arg(extension);
    if (fileTypeString.isEmpty())
    {
      saveFileName = QString("%1.%2").arg(rawFileName).arg(extension);
    }
    LOGFILE.Debug("saveFileName: %s", qPrintable(saveFileName));

    QString FileName;
    SaveFileErrorType filenameResults = getSaveFilename(saveFileName, FileName, FilterList, selectedFilter, parent);
    if (filenameResults != SaveFileErrorType::SIET_None)
      return filenameResults;

    if (FileName == saveFileName)
    {
      filePath = QString();
    }
    else {
      filePath = FileName.left(FileName.lastIndexOf("/"));
    }

    SaveFileErrorType pluginResult = getImagePlugin(SavingPlugin, selectedFilter, ImageType);
    if (pluginResult != SaveFileErrorType::SIET_None)
      return pluginResult;
    
    if (SavingPlugin == nullptr)
    {
      LOGFILE.Error("Unable to find selected plugin. Aborting Save...");
      return SaveFileErrorType::SEIT_PluginNotFound;
    }

    return SaveImage(SavingPlugin, FileName, imageData, ImageType);
  }

  SaveFileErrorType SaveTexture(ProjectSW::PluginExportImage* SavingPlugin, ImageData *imageData, QString SaveFilename, ProjectSW::ImageDestinationType ImageType = ProjectSW::ImageDestinationType::IMGDESTTYPE_MAX)
  {
    if (SavingPlugin == nullptr)
    {
      LOGFILE.Error("Unable to find selected plugin. Aborting Save...");
      return SaveFileErrorType::SEIT_PluginNotFound;
    }

    ProjectSW::PluginImageType saveImageType = ProjectSW::IMGTYPE_RGB;
    ProjectSW::PluginImageDepth saveImageDepth = ProjectSW::IMGDEPTH_8BIT;

    if (ImageType == ProjectSW::ImageDestinationType::IMGDESTTYPE_MAX)
    {
      if (imageData->getNumChannels() < RGBAColorType::RGB)
      {
        saveImageType = ProjectSW::IMGTYPE_RGBA;
      }
      else if (imageData->getNumChannels() < RGBAColorType::GreyAlpha)
      {
        saveImageType = ProjectSW::IMGTYPE_RGB;
      }
      else if (imageData->getNumChannels() < RGBAColorType::Grey)
      {
        saveImageType = ProjectSW::IMGTYPE_GREYALPHA;
      }
      else {
        saveImageType = ProjectSW::IMGTYPE_GREY;
      }
    }
    else {
      switch (ImageType)
      {
      case ProjectSW::LANDSCAPEHEIGHT:
        saveImageType = ProjectSW::IMGTYPE_HEIGHTMAP;
        saveImageDepth = ProjectSW::IMGDEPTH_32BIT;
        break;
      case ProjectSW::MASKLAYER:
        saveImageType = ProjectSW::IMGTYPE_GREY;
        break;
      case ProjectSW::COLORALPHA:
        saveImageType = ProjectSW::IMGTYPE_RGBA;
        break;
      case ProjectSW::COLOR:
      default:
        saveImageType = ProjectSW::IMGTYPE_RGB;
        break;
      }
    }

    LOGFILE.Debug("Image Type: %i, Image Depth: %i", saveImageType, saveImageDepth);

    // Correct the max heightmap depth
    if (saveImageType == ProjectSW::IMGTYPE_HEIGHTMAP && SavingPlugin->getMaxHeightmapDepth() < saveImageDepth)
    {
      saveImageDepth = SavingPlugin->getMaxHeightmapDepth();
    }

    LOGFILE.Info("Saving texture to file: %s", qPrintable(SaveFilename));
    bool saved = SavingPlugin->writeImageFile(SaveFilename, imageData, 0, saveImageType, saveImageDepth);
    if (saved == false)
    {
      LOGFILE.Error("Experienced a plugin error while saving.");
      return SaveFileErrorType::SEIT_PluginError;
    }

    return SaveFileErrorType::SIET_None;
  }

  SaveFileErrorType SaveTexture(QString selectedFilter, ImageData *imageData, QString SaveFilename)
  {
    ProjectSW::PluginExportImage* SavingPlugin = nullptr;

    if (KERNEL.nbChildren() <= 0)
    {
      LOGFILE.Error("No plugins found at all. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPlugins;
    }

    QList<ProjectSW::PluginExportImage*> *texturepluginList = new QList<ProjectSW::PluginExportImage*>;
    for (unsigned int i = 0; i < KERNEL.nbChildren(); i++)
    {
      if (dynamic_cast<ProjectSW::PluginExportImage*>(KERNEL.getChild(i)))
      {
        ProjectSW::PluginExportImage *plg = dynamic_cast<ProjectSW::PluginExportImage*>(KERNEL.getChild(i));
        texturepluginList->push_back(plg);
      }
    }
    if (texturepluginList->count() <= 0)
    {
      LOGFILE.Error("No mesh exporting plugins found. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPluginsOfType;
    }
    for (size_t i = 0; i < texturepluginList->count(); i++)
    {
      ProjectSW::PluginExportImage* plugin = texturepluginList->at((int)i);
      QStringList pluginext = plugin->getFileExtensions();
      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at((int)j)));
      }
      exts.append(")");

      if (selectedFilter == exts)
      {
        // Found the plugin!
        SavingPlugin = plugin;
        LOGFILE.Debug("Plugin found! Saving with %s...", SavingPlugin->name().c_str());
        break;
      }
    }
    if (!SaveFilename.endsWith(QString(".%1").arg(SavingPlugin->getFileExtensions().at(0))))
    {
      if (SaveFilename.lastIndexOf(".", -6) <= -1)
      { 
        SaveFilename = SaveFilename.mid(0, SaveFilename.lastIndexOf(".", -6)).append(QString(".%1").arg(SavingPlugin->getFileExtensions().at(0)));
      }
      else {
        SaveFilename.append(QString(".%1").arg(SavingPlugin->getFileExtensions().at(0)));
      }
    }
    return SaveTexture(SavingPlugin, imageData, SaveFilename);
  }

  SaveFileErrorType SaveMesh(ProjectSW::PluginExportModel* SavingPlugin, ModelScene *mesh_scene, QString SaveFilename, QString &pluginOptions=QString())
  {
    if (SavingPlugin == nullptr)
    {
      LOGFILE.Error("Unable to find selected plugin. Aborting Save...");
      return SaveFileErrorType::SEIT_PluginNotFound;
    }

    LOGFILE.Info("Saving mesh to file: %s", qPrintable(SaveFilename));
    bool showOptions = true;
    if (!pluginOptions.isEmpty())
    {
      showOptions = false;
      SavingPlugin->setOptionsJson(pluginOptions);
    }
    bool saved = SavingPlugin->writeFile(SaveFilename, mesh_scene, 0, showOptions);
    if (saved == false)
    {
      LOGFILE.Error("Experienced a plugin error while saving.");
      return SaveFileErrorType::SEIT_PluginError;
    }
    if (showOptions == true)
      pluginOptions = SavingPlugin->getOptionsJson();

    return SaveFileErrorType::SIET_None;
  }

  SaveFileErrorType SaveMesh(QString selectedFilter, ModelScene *mesh_scene, QString SaveFilename, QString &pluginOptions=QString())
  {
    ProjectSW::PluginExportModel* SavingPlugin = nullptr;

    if (KERNEL.nbChildren() <= 0)
    {
      LOGFILE.Error("No plugins found at all. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPlugins;
    }

    QList<ProjectSW::PluginExportModel*> *meshpluginList = new QList<ProjectSW::PluginExportModel*>;
    for (unsigned int i = 0; i < KERNEL.nbChildren(); i++)
    {
      if (dynamic_cast<ProjectSW::PluginExportModel*>(KERNEL.getChild(i)))
      {
        ProjectSW::PluginExportModel *plg = dynamic_cast<ProjectSW::PluginExportModel*>(KERNEL.getChild(i));
        meshpluginList->push_back(plg);
      }
    }
    if (meshpluginList->count() <= 0)
    {
      LOGFILE.Error("No mesh exporting plugins found. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPluginsOfType;
    }
    for (size_t i = 0; i < meshpluginList->count(); i++)
    {
      ProjectSW::PluginExportModel* plugin = meshpluginList->at((int)i);
      QStringList pluginext = plugin->getFileExtensions();
      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at((int)j)));
      }
      exts.append(")");

      if (selectedFilter == exts)
      {
        // Found the plugin!
        SavingPlugin = plugin;
        LOGFILE.Debug("Plugin found! Saving with %s...", SavingPlugin->name().c_str());
        break;
      }
    }
    QString ext = QString(".%1").arg(SavingPlugin->getFileExtensions().at(0));
    if (SaveFilename.lastIndexOf(ext) == -1)
    {
      SaveFilename.append(ext);
    }
    return SaveMesh(SavingPlugin, mesh_scene, SaveFilename, pluginOptions);
  }

  SaveFileErrorType SaveModel(QString FilterList, ModelScene * mesh_scene, QString OpenFileName, QWidget *parent = 0)
  {
    LOGFILE.Info("Saving Model...");
    SETTINGS.sync();
    QDir LastDir = SETTINGS.value("lastdir").toString();
    QString selectedFilter = SETTINGS.value("preferedExporter/models").toString();
    int selectedFilterStart = selectedFilter.indexOf(" (*.") + 4;
    int selectedFilterEnd = selectedFilter.indexOf(")", selectedFilterStart);
    QString selectedFilterExt = selectedFilter.mid(selectedFilterStart, selectedFilterEnd - selectedFilterStart);
    QString saveFileName = QString("%1.%2").arg(OpenFileName).arg(selectedFilterExt);
    LOGFILE.Info("saveFileName: %s", qPrintable(saveFileName));

    QString FileName = QFileDialog::getSaveFileName(parent, QObject::tr("Save File"), saveFileName, FilterList, &selectedFilter);
    if (selectedFilter == NULL)
    {
      LOGFILE.Error("Unable to find selected filter. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilter;
    }
    if (FileName.isEmpty() || FileName.isNull())
    {
      LOGFILE.Error("Filename Empty. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }
    else if (FileName == LastDir.absolutePath())
    {
      LOGFILE.Error("Filename not set. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }

    return SaveMesh(selectedFilter, mesh_scene, FileName);
  }

  SaveFileErrorType SavePlacement(QString FilterList, PlacementData *placement_data, QString OpenFileName, QWidget *parent = 0)
  {
    LOGFILE.Info("Saving Placement Data...");
    SETTINGS.sync();
    QDir LastDir = SETTINGS.value("lastdir").toString();
    QString selectedFilter = SETTINGS.value("preferedExporter/placement").toString();
    LOGFILE.Debug("Full FilterList: %s", qPrintable(FilterList));
    int extStart = selectedFilter.indexOf(" (*.") + 4;
    int extEnd = selectedFilter.indexOf(")", extStart);
    QString extension = selectedFilter.mid(extStart, extEnd - extStart);
    QString saveFileName = QString("%1.%2").arg(OpenFileName).arg(extension);
    QString FileName = QFileDialog::getSaveFileName(parent, QObject::tr("Save File"), saveFileName, FilterList, &selectedFilter);
    if (selectedFilter == NULL)
    {
      LOGFILE.Error("Unable to find selected filter. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilter;
    }
    if (FileName.isEmpty() || FileName.isNull())
    {
      LOGFILE.Error("Filename Empty. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }
    else if (FileName == LastDir.absolutePath())
    {
      LOGFILE.Error("Filename not set. Aborting Save...");
      return SaveFileErrorType::SEIT_BadFilename;
    }

    if (KERNEL.nbChildren() <= 0)
    {
      LOGFILE.Error("No plugins found at all. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPlugins;
    }

    ProjectSW::PluginExportPlacement* SavingPlugin = nullptr;

    QList<ProjectSW::PluginExportPlacement*> *pluginList = new QList<ProjectSW::PluginExportPlacement*>;
    for (unsigned int i = 0; i < KERNEL.nbChildren(); i++)
    {
      if (dynamic_cast<ProjectSW::PluginExportPlacement*>(KERNEL.getChild(i)))
      {
        ProjectSW::PluginExportPlacement *plg = dynamic_cast<ProjectSW::PluginExportPlacement*>(KERNEL.getChild(i));
        pluginList->push_back(plg);
      }
    }
    if (pluginList->count() <= 0)
    {
      LOGFILE.Error("No mesh exporting plugins found. Aborting Save...");
      return SaveFileErrorType::SEIT_NoPluginsOfType;
    }
    for (size_t i = 0; i < pluginList->count(); i++)
    {
      ProjectSW::PluginExportPlacement* plugin = pluginList->at((int)i);
      QStringList pluginext = plugin->getFileExtensions();
      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at((int)j)));
      }
      exts.append(")");

      if (selectedFilter == exts)
      {
        // Found the plugin!
        SavingPlugin = plugin;
        LOGFILE.Debug("Plugin found! Saving with %s...", SavingPlugin->name().c_str());
        break;
      }
    }
    if (SavingPlugin == nullptr)
    {
      LOGFILE.Error("Unable to find selected plugin. Aborting Save...");
      return SaveFileErrorType::SEIT_PluginNotFound;
    }

    LOGFILE.Info("Saving placement data to file: %s", qPrintable(FileName));
    // change to return SaveFileErrorType::SEIT_PluginError if problem... 
    bool saved = SavingPlugin->writeFile(FileName, placement_data, 0);
    if (saved == false)
    {
      LOGFILE.Error("Experienced a plugin error while saving.");
      return SaveFileErrorType::SEIT_PluginError;
    }

    return SaveFileErrorType::SIET_None;
  }

  QString getImagePluginExtensions(ProjectSW::ImageDestinationType destType)
  {
    QList<ProjectSW::PluginExportImage*> *pluginList = KERNEL.getPluginExportImageList();
    QStringList extList;
    for (size_t i = 0; i < pluginList->count(); i++)
    {
      ProjectSW::PluginExportImage* plugin = pluginList->at((int)i);
      if (!plugin->getImageOutTypes().contains(destType)) { continue; }

      QStringList pluginext = plugin->getFileExtensions();
      switch (destType)
      {
      case ProjectSW::LANDSCAPEHEIGHT:
        pluginext = plugin->getHeightmapExtensions();
        break;
      case ProjectSW::MASKLAYER:
      case ProjectSW::COLOR:
      case ProjectSW::COLORALPHA:
      default:
        pluginext = plugin->getFileExtensions();
        break;
      }

      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at((int)j)));
      }
      exts.append(")");
      extList.push_back(exts);
      LOGFILE.Debug("Filters for %s: %s", qPrintable(plugin->getPluginFilterName()), qPrintable(exts));
    }
    extList.push_back(QObject::tr("All files (*.*)", "Specifies all the files in the folder."));
    QString extensions = extList.join(";;");
    LOGFILE.Debug("ExtList Count: %i", extList.count());
    if (extList.count() <= 1)
    {
      extensions = extList.at(0);
    }
    LOGFILE.Debug("Final Save File filters: %s", qPrintable(extensions));
    return extensions;
  }

  bool exportTexture(QString fileToExport, QString destinationName = QString())
  {
    ProjectSW::PluginExportImage *imgPlugin = nullptr;
    BLPInfo *a = ReadBLPFile(fileToExport);
    if (!a)
    {
      LOGFILE.Error("Unable to open BLP file for export: %s", qPrintable(fileToExport));
      return false;
    }

    QString filter = SETTINGS.value("preferedExporter/textures").toString();

    SaveFileErrorType r = SaveTexture(filter, a->getImageData(), destinationName);
    delete a;
    if (r != SaveFileErrorType::SIET_None)
    {
      QString error = getSaveFileError(r);

      LOGFILE.Error("Unable to export %s. Error: %s", qPrintable(fileToExport), qPrintable(error));
      return false;
    }
    return true;
  }

  uint32_t exportTextures(QStringList filesToExport)
  {
    uint32_t exportCount = 0;
    ProjectSW::PluginExportImage *imgPlugin = nullptr;
    QString imgFilePath;
    for (size_t i = 0; i < filesToExport.count(); i++)
    {
      BLPInfo *a = ReadBLPFile(filesToExport.at((int)i));
      SaveFileErrorType r = SaveFileErrorType::SIET_None;

      ProjectSW::ImageDestinationType imgType = ProjectSW::ImageDestinationType::COLORALPHA;
      if (a->hasAlpha() == false)
      {
        imgType = ProjectSW::ImageDestinationType::COLOR;
      }

      // Call global image saving plugin...
      QString extList = getImagePluginExtensions(imgType);
      if (imgPlugin == nullptr)
      {
        r = SaveImage(filesToExport.at((int)i), extList, a->getImageData(), imgPlugin, imgType, imgFilePath);
        LOGFILE.Debug("Saved Image Path: %s", qPrintable(imgFilePath));
      }
      else {
        QString filename = filesToExport.at((int)i);
        if (imgFilePath.isEmpty() == false)
        {
          QString f = filesToExport.at((int)i);
          f = f.mid(f.lastIndexOf("/") + 1);
          filename = QString("%1/%2").arg(imgFilePath).arg(f);
        }

        LOGFILE.Debug("Altered Image Path: %s", qPrintable(filename));
        r = SaveImage(imgPlugin, filename, a->getImageData(), imgType);
      }
      delete a;
      if (r != SaveFileErrorType::SIET_None)
      {
        QString error = getSaveFileError(r);

        LOGFILE.Error("Unable to export %s. Error: %a", qPrintable(filesToExport.at((int)i)), qPrintable(error));
        continue;
      }
      exportCount++;
    }
    return exportCount;
  }

  template<typename pluginType>
  QString getPluginExtensions(QList<pluginType*> *pluginList)
  {
    QStringList extList;
    for (size_t i = 0; i < pluginList->count(); i++)
    {
      pluginType* plugin = pluginList->at(i);
      QStringList pluginext = plugin->getFileExtensions();

      QString exts;
      exts.append(QString("%1 (").arg(plugin->getPluginFilterName()));
      for (size_t j = 0; j < pluginext.count(); j++)
      {
        if (j > 0) {
          exts.append(" ");
        }
        exts.append(QString("*.%1").arg(pluginext.at(j)));
      }
      exts.append(")");
      extList.push_back(exts);
      LOGFILE.Debug("Filters for %s: %s", qPrintable(plugin->getPluginFilterName()), qPrintable(exts));
    }
    extList.push_back(QObject::tr("All files (*.*)", "Specifies all the files in the folder."));
    QString extensions = extList.join(";;");
    LOGFILE.Debug("ExtList Count: %i", extList.count());
    if (extList.count() <= 1)
    {
      extensions = extList.at(0);
    }
    LOGFILE.Debug("Final Save File filters: %s", qPrintable(extensions));
    return extensions;
  }
}