#include <QtCore/QCoreApplication>
#include "WMOSearch.h"

LogFile *rlog = new LogFile("WMOSearch_Results.txt");

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	WMOSearch b;
	b.CheckFiles();

	return a.exec();
}
