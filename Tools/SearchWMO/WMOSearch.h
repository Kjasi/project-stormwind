#pragma once
#include <qstring.h>
#include <qlist.h>
#include <Foundation/Systems.h>
#include <Foundation/Classes/Vectors.h>

enum LightType {
	LIGHT_OMNI,				// AKA Point Light
	LIGHT_SPOT,
	LIGHT_DIRECTIONAL,
	LIGHT_AMBIENT,
};

class WMOSearch
{
	QList<QString> WMOList;

	void GetList();
	void CheckFile(QString file);

public:
	void CheckFiles();
};

inline void flipcc(char *fcc) {
	char t;
	t = fcc[0];
	fcc[0] = fcc[3];
	fcc[3] = t;
	t = fcc[1];
	fcc[1] = fcc[2];
	fcc[2] = t;
};

struct MOLTChunk
{
	uint8 TypeOfLight;
	uint8 Type;
	uint8 UseAttenuation;
	uint8 pad;
	uint8 color[4];
	Vec3F Position;		// X, Z -Y
	float intensity;
	float attenStart;
	float attenEnd;
	float QuatX;
	float QuatY;
	float QuatZ;
	float QuatW;

	MOLTChunk() {};
};

struct WMORootHeader {
	uint32 nMaterials;
	uint32 nGroups;
	uint32 nPortals;
	uint32 nLights;
	uint32 nDoodadNames;
	uint32 nDoodads;
	uint32 nDoodadSets;
	uint8 AmbColorR;
	uint8 AmbColorG;
	uint8 AmbColorB;
	uint8 AmbColorA;
	uint32 WMOID;
	Vec3F BoundingBox1;
	Vec3F BoundingBox2;
	uint32 Flags;

	WMORootHeader() {};
};