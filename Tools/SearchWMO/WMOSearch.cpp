#include <QDirIterator>
#include "WMOSearch.h"
#include <Foundation/LogFile.h>

bool checkIsRoot(QString filename)
{
	FILE *fp;
	char str[5];
	uint32 size;

	// Open the file
	fp = fopen(qPrintable(filename), "rb");
	if (!fp) {
		return false;
	}

	// --== Check File Type ==--
	// Check to make sure this is an ADT file.
	fread(str, 1, 4, fp);
	fread(&size, 4, 1, fp);
	flipcc(str);
	if (strncmp(str, "MVER", 4) || size == 0) {
		fclose(fp);
		return false;
	}
	fread(str, 1, 4, fp); // Read version number
	fread(str, 1, 4, fp); // Read the first Chunk.
	fread(&size, 4, 1, fp);
	fclose(fp);
	flipcc(str);
	if (strncmp(str, "MOHD", 4) || size == 0) {
		return false;
	}
	if (!strncmp(str, "MOHD", 4)) {
		return true;
	}
	return false;
}

void WMOSearch::GetList()
{
	QDirIterator it(".", QStringList() << "*.wmo", QDir::Files, QDirIterator::Subdirectories);
	while (it.hasNext()) {
		it.next();
		printf("Processing File: %s\n", qPrintable(it.filePath()));
		if (checkIsRoot(it.filePath()) == true) {
			WMOList.push_back(it.filePath());
		}
	}
}

void WMOSearch::CheckFiles()
{
	GetList();
	for (size_t i = 0; i < WMOList.size(); i++)
	{
		CheckFile(WMOList[i]);
	}
}

void WMOSearch::CheckFile(QString file)
{
	char str[5];
	uint32 size;

	WMORootHeader Header;

	size_t LOmniCount = 0;
	size_t LSpotCount = 0;
	size_t LDirectCount = 0;
	size_t LAmbCount = 0;

	FILE *RootFile = fopen(qPrintable(file), "rb");
	if (!RootFile) {
		return;
	}

	//rlog->Debug("Checking File: %s", qPrintable(file));
	fseek(RootFile, 12, SEEK_SET);

	bool endoffile = false;
	while (endoffile == false) {
		fread(str, 1, 4, RootFile);		// Read the Chunk.
		fread(&size, 4, 1, RootFile);
		flipcc(str);
		str[4] = 0;

		if (size == 0)
			continue;

		// Set the next location
		fpos_t fPos;
		fgetpos(RootFile, &fPos);
		long nextPos = (long)(fPos + size);

		if (strncmp(str, "MOHD", 4) == 0) {			// Header Information
			//rlog->Debug("Reading Header...");
			fread(&Header, sizeof(WMORootHeader), 1, RootFile);
		}else if (strncmp(str, "MOLT", 4) == 0) {		// Lighting Info
			//rlog->Debug("Reading Lighting...");
			for (size_t i = 0; i < Header.nLights; i++) {
				MOLTChunk a;
				fread(&a, sizeof(MOLTChunk), 1, RootFile);
				switch (a.TypeOfLight) {
					case LIGHT_OMNI:
						LOmniCount++;
						break;
					case LIGHT_SPOT:
						LSpotCount++;
						break;
					case LIGHT_DIRECTIONAL:
						LDirectCount++;
						break;
					case LIGHT_AMBIENT:
						LAmbCount++;
						break;
					default: break;
				}
			}
		}

		// Check for end of file
		if (feof(RootFile))
			endoffile = true;

		// Go to next section
		fseek(RootFile, nextPos, SEEK_SET);
	}
	fclose(RootFile);

	if (Header.nLights > 0) {
    LOGFILE.Write("File: %s", qPrintable(file));
    LOGFILE.Write("  Light Info:\n    Total Lights: %i", Header.nLights);
    LOGFILE.Write("    Omni Lights: %i", LOmniCount);
		if (LSpotCount > 0 || LDirectCount > 0 || LAmbCount > 0) {
      LOGFILE.Write("FOUND ONE!");
      LOGFILE.Write("    Spot Lights: %i", LSpotCount);
      LOGFILE.Write("    Directional Lights: %i", LDirectCount);
      LOGFILE.Write("    Ambient Lights: %i", LAmbCount);
		}
	}
	
}
