#include <Foundation/Core/Kernel.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/Plugins/PluginExportPlacement.h>

#include "AboutPlugins.h"

using namespace ProjectSW;

WindowAboutPlugins::WindowAboutPlugins(QWidget *parent)
{
  LOGFILE.Info("Preparing About Plugins Dialog...");
  setupUi(this);
  connect(comboBoxFilterByType, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &WindowAboutPlugins::pluginFilterType_Change);
  filterType = PLUGINFILTER_ALL;

  updateList();

  LOGFILE.Info("Finished Preparing About Plugins Dialog.");
}

void WindowAboutPlugins::updateList()
{
  LOGFILE.Info("Updating List of Plugins...");

  pluginList->clear();
  pluginList->setColumnCount(3);
  pluginList->setHorizontalHeaderLabels(QStringList() << tr("Plugin Name") << tr("Type") << tr("Exports"));
  pluginList->horizontalHeader()->setResizeContentsPrecision(QHeaderView::ResizeToContents);

  size_t lnbPluginsExportImage = KERNEL.nbImageExportPlugin();
  size_t lnbPluginsExportModel = KERNEL.nbModelExportPlugin();
  size_t lnbPluginsExportPlace = KERNEL.nbPlacementExportPlugin();
  size_t numTotalPlugins = KERNEL.nbChildren();

  pluginCountTotal->setText(tr("Total Plugins: %1").arg(numTotalPlugins));
  pluginCountImageExporters->setText(tr("Texture Exporters: %1").arg(lnbPluginsExportImage));
  pluginCountModelExporters->setText(tr("Model Exporters: %1").arg(lnbPluginsExportModel));
  pluginCountPlacementExporters->setText(tr("Placement Exporters: %1").arg(lnbPluginsExportPlace));

  if (numTotalPlugins == 0) {
    LOGFILE.Warn("No plugins found. Aborting List Update.");
    return;
  }

  size_t numPlugins = 0;

  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_TEXTURE)
    numPlugins += lnbPluginsExportImage;
  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_MODEL)
    numPlugins += lnbPluginsExportModel;
  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_PLACEMENT)
    numPlugins += lnbPluginsExportPlace;

  pluginList->setRowCount(numPlugins);
  QTableWidgetItem *item;

  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_TEXTURE)
  {
    int offset = 0;
    for (size_t i = 0; i < lnbPluginsExportImage; i++)
    {
      Plugin *plug = KERNEL.getPluginExportImageList()->at(i);
      QString plugName(plug->name().c_str());
      LOGFILE.Info("Building table item for Texture Exporter %s...", qPrintable(plugName));

      QString pluginTypeText = tr("Image Exporter");
      PluginExportImage *imgPlug = nullptr;
      if (dynamic_cast<PluginExportImage *>(plug)) {
        imgPlug = dynamic_cast<PluginExportImage *>(plug);
      }
      if (imgPlug == nullptr)
      {
        LOGFILE.Error("Unable to get Texture Plugin data for listing...");
        continue;
      }

      // First row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(plugName);
      pluginList->setItem(i + offset, 0, item);

      // Second Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(pluginTypeText);
      pluginList->setItem(i + offset, 1, item);

      // Third Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      QStringList l = imgPlug->getFileExtensions();
      l.append(imgPlug->getHeightmapExtensions());
      l.removeDuplicates();
      QString text;
      for each (QString ext in l)
      {
        if (text.isEmpty() == false)
        {
          text.append(tr(", ", "Used for displaying multiple extensions used by a plugin. IE: png, gif, tga"));
        }
        text.append(ext);
      }
      item->setText(text);
      pluginList->setItem(i + offset, 2, item);
    }
  }

  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_MODEL)
  {
    int offset = 0;
    if (filterType == PLUGINFILTER_ALL) offset = lnbPluginsExportImage;
    for (size_t i = 0; i < lnbPluginsExportModel; i++)
    {
      Plugin *plug = KERNEL.getPluginExportModelList()->at(i);
      QString plugName(plug->name().c_str());
      LOGFILE.Info("Building table item for Model Exporter %s...", qPrintable(plugName));

      QString pluginTypeText = tr("Model Exporter");
      PluginExportModel *mdlPlug = nullptr;
      if (dynamic_cast<PluginExportModel *>(plug)) {
        mdlPlug = dynamic_cast<PluginExportModel *>(plug);
      }
      if (mdlPlug == nullptr)
      {
        LOGFILE.Error("Unable to get Texture Plugin data for listing...");
        continue;
      }

      // First row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(plugName);
      pluginList->setItem(i + offset, 0, item);

      // Second Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(pluginTypeText);
      pluginList->setItem(i + offset, 1, item);

      // Third Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      QStringList l = mdlPlug->getFileExtensions();
      l.removeDuplicates();
      QString text;
      for each (QString ext in l)
      {
        if (text.isEmpty() == false)
        {
          text.append(tr(", ", "Used for displaying multiple extensions used by a plugin. IE: png, gif, tga"));
        }
        text.append(ext);
      }
      item->setText(text);
      pluginList->setItem(i + offset, 2, item);
    }
  }

  if (filterType == PLUGINFILTER_ALL || filterType == PLUGINFILTER_PLACEMENT)
  {
    int offset = 0;
    if (filterType == PLUGINFILTER_ALL) offset = lnbPluginsExportImage + lnbPluginsExportModel;
    for (size_t i = 0; i < lnbPluginsExportPlace; i++)
    {
      Plugin *plug = KERNEL.getPluginExportPlacementList()->at(i);
      QString plugName(plug->name().c_str());
      LOGFILE.Info("Building table item for Placement Exporter %s...", qPrintable(plugName));

      QString pluginTypeText = tr("Placement Exporter");
      PluginExportPlacement *plcPlug = nullptr;
      if (dynamic_cast<PluginExportPlacement *>(plug)) {
        plcPlug = dynamic_cast<PluginExportPlacement *>(plug);
      }
      if (plcPlug == nullptr)
      {
        LOGFILE.Error("Unable to get Texture Plugin data for listing...");
        continue;
      }

      // First row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(plugName);
      pluginList->setItem(i + offset, 0, item);

      // Second Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      item->setText(pluginTypeText);
      pluginList->setItem(i + offset, 1, item);

      // Third Row
      item = new QTableWidgetItem;
      item->setFlags(item->flags() & ~Qt::ItemIsEditable);
      QStringList l = plcPlug->getFileExtensions();
      l.removeDuplicates();
      QString text;
      for each (QString ext in l)
      {
        if (text.isEmpty() == false)
        {
          text.append(tr(", ", "Used for displaying multiple extensions used by a plugin. IE: png, gif, tga"));
        }
        text.append(ext);
      }
      item->setText(text);
      pluginList->setItem(i + offset, 2, item);
    }
  }

  LOGFILE.Info("Completed List of Plugins Update.");
}

void WindowAboutPlugins::pluginFilterType_Change(int index)
{
  filterType = static_cast<FilterType>(index);
  updateList();
}