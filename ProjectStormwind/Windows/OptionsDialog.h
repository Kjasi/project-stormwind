#pragma once
#include <QtWidgets/qdialog.h>
#include <Foundation/Classes/ExportOptions.h>
#include "ui_OptionsDialog.h"

class WindowOptionsDialog : public QDialog, public Ui::Dialog_Options {
  Q_OBJECT

public:
  WindowOptionsDialog(QWidget *parent = 0);

private slots:
  void on_checkBox_Debug_clicked(bool value);
  void on_checkBox_DisableRenderer_clicked(bool value);
  void on_checkBox_DisableTextureLoading_clicked(bool value);
  void on_checkBox_Timestamp_clicked(bool value);
  void on_checkBox_ADTUseMiddlePoint_clicked(bool value);
  void on_checkBox_ADTBuildSingleMaterial_clicked(bool value);
  void on_checkBox_MirrorWoWPaths_clicked(bool value);
  void on_checkBox_CombineMeshes_clicked(bool value);
  void on_btn_BrowseRootDir_clicked();
  void on_btn_BrowseOutputDir_clicked();
  void on_okButton_pressed();
  void on_cancelButton_pressed();
  void on_comboBoxPreferedExporterModels_currentTextChanged(QString value);
  void on_comboBoxPreferedExporterTextures_currentTextChanged(QString value);
  void on_comboBoxPreferedExporterPlacement_currentTextChanged(QString value);

private:
  QString SettingsFile;

  QString tempRootDir;
  QString tempOutDir;
  bool tempDisableRenderer;
  bool tempDisableRenderTextures;
  bool tempMirrorWoWPaths;
  bool tempCombineMeshes;
  bool tempShowDebug;
  bool tempUseTimestamp;
  bool tempUseADTMiddle;
  bool tempADTBuildSingleMat;
  QString tempPreferedModels = QString();
  QString tempPreferedTextures = QString();
  QString tempPreferedPlacement = QString();

  QStringList exporterModels;
  QStringList exporterTextures;
  QStringList exporterPlacement;

  int getIndexFromStringList(QStringList list, QString value);
};