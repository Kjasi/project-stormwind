#pragma once
#include <QtWidgets/qdialog.h>
#include <QtWidgets/QTableView>

#include "ui_AboutPlugins.h"

class WindowAboutPlugins : public QDialog, public Ui::AboutPlugins
{
  Q_OBJECT

  enum FilterType {
    PLUGINFILTER_ALL = 0,

    PLUGINFILTER_TEXTURE,
    PLUGINFILTER_MODEL,
    PLUGINFILTER_PLACEMENT
  };

public:
  WindowAboutPlugins(QWidget *parent = 0);

private:
  void updateList();
  FilterType filterType = PLUGINFILTER_ALL;

private slots:
  void pluginFilterType_Change(int index);

};