#include "BatchExporterDialog.h"
#include <Foundation/GlobalEnums.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <QtWidgets/qmessagebox.h>
#include <QtWidgets/qfiledialog.h>
#include "Utilities/common.h"

#include <Foundation/Core/Kernel.h>

void hasHeightmapPlugins(Ui::DialogBatchExporter* ui, bool value = true)
{
  ui->label_HeightmapFormat->setEnabled(value);
  ui->checkBox_ExportADTasHeightmap->setEnabled(value);
  ui->label_HeightmapResolution->setEnabled(value);
  ui->comboBox_ADTHeightmapResolution->setEnabled(value);
  ui->checkBox_ADTUseMiddlePoint->setEnabled(value);
}

void hasImagePlugins(Ui::DialogBatchExporter* ui, bool value = true)
{
  ui->comboBox_ImageFormat->setEnabled(value);
  ui->label_TextureFormat->setEnabled(value);
  ui->checkBox_ExportM2Textures->setEnabled(value);
  ui->checkBox_ExportWMOTextures->setEnabled(value);
  ui->checkBox_ExportADTTextures->setEnabled(value);
  hasHeightmapPlugins(ui, value);
}

void hasModelPlugins(Ui::DialogBatchExporter* ui, bool value = true)
{
  ui->checkBox_ExportM2Mesh->setEnabled(value);
  ui->checkBox_ExportAllModels->setEnabled(value);
  ui->checkBox_ExportAllWorldModels->setEnabled(value);
  ui->checkBox_ExportM2MaxLOD->setEnabled(value);
  ui->checkBox_ExportM2AllLODs->setEnabled(value);
  ui->checkBox_ExportWMOMesh->setEnabled(value);
  ui->checkBox_ExportADTMesh->setEnabled(value);
  ui->label_ModelFormat->setEnabled(value);
  ui->comboBox_ModelFormat->setEnabled(value);
}

void hasPlacementPlugins(Ui::DialogBatchExporter* ui, bool value = true)
{
  ui->label_PlacementFormat->setEnabled(value);
  ui->comboBox_PlacementFormat->setEnabled(value);
  ui->checkBox_ExportWMOPlacement->setEnabled(value);
  ui->checkBox_ExportADTPlacement->setEnabled(value);
}

BatchExporterDialog::BatchExporterDialog(QWidget *parent): ui(new Ui::DialogBatchExporter)
{
  LOGFILE.Info("Preparing Batch Exporter Dialog...");
  ui->setupUi(this);

  windowQuickSelections = new WindowBatchExporterQuickSelectionDialog(this);
  windowQuickSelections->setVisible(false);

  QList<ProjectSW::PluginExportImage*> *PluginsImage = KERNEL.getPluginExportImageList();
  LOGFILE.Debug("Plugin Image Size: %i", PluginsImage->count());
  QList<ProjectSW::PluginExportModel*> *PluginsModel = KERNEL.getPluginExportModelList();
  LOGFILE.Debug("Plugin Model Size: %i", PluginsModel->count());
  QList<ProjectSW::PluginExportPlacement*> *PluginsPlacement = KERNEL.getPluginExportPlacementList();
  LOGFILE.Debug("Plugin Placement Size: %i", PluginsPlacement->count());

  if (PluginsImage->count() <= 0 && PluginsModel->count() <= 0 && PluginsPlacement->count() <= 0)
  {
    LOGFILE.Error("No plugins found at all. Unable to export... ANYTHING!");
    QMessageBox msg(QMessageBox::Icon::Critical, tr("That's not suppose to happen..."), tr("So, it turns out that there are no plugins for you to export with. As such, you can not export anything.\n\nPlease re-install Project Stormwind to at least get the default plugins."), QMessageBox::StandardButton::Ok);
    msg.exec();
    cancelOpening = true;
    return;
  }

  if (PluginsImage->count() <= 0)
  {
    hasImagePlugins(ui, false);
    LOGFILE.Warn("No Image plugins found. Unable to export Textures or Heightmaps.");
  }
  else {
    hasImagePlugins(ui, true);
    for (int i = 0; i < PluginsImage->count(); i++)
    {
      ui->comboBox_ImageFormat->addItem(PluginsImage->at(i)->getPluginFilterName());
      if (PluginsImage->at(i)->getImageOutTypes().contains(ProjectSW::LANDSCAPEHEIGHT) == true)
      {
        ui->comboBox_HeightmapFormat->addItem(PluginsImage->at(i)->getPluginFilterName());
      }
    }
    if (ui->comboBox_HeightmapFormat->count() <= 0)
    {
      LOGFILE.Warn("No Heightmap compatible plugins found. Unable to export heightmaps.");
      hasHeightmapPlugins(ui, false);
    }
    else
    {
      hasHeightmapPlugins(ui, true);
    }
  }

  if (PluginsModel->count() <= 0)
  {
    LOGFILE.Warn("No Model plugins found. Unable to export any Meshes.");
    hasModelPlugins(ui, false);
  }
  else
  {
    hasModelPlugins(ui, true);
    for (int i = 0; i < PluginsModel->count(); i++)
    {
      ui->comboBox_ModelFormat->addItem(PluginsModel->at(i)->getPluginFilterName());
    }
  }

  if (PluginsPlacement->count() <= 0)
  {
    LOGFILE.Warn("No Placement plugins found. Unable to export any placement data.");
    hasPlacementPlugins(ui, false);
  }
  else
  {
    hasPlacementPlugins(ui, true);
    for (int i = 0; i < PluginsPlacement->count(); i++)
    {
      ui->comboBox_PlacementFormat->addItem(PluginsPlacement->at(i)->getPluginFilterName());
    }
  }

  if (ui->label_TextureFormat->isEnabled() == false && ui->label_ModelFormat->isEnabled() == false)
  {
    ui->checkBox_ADTBuildSingleMaterial->setEnabled(false);
  }
  else {
    ui->checkBox_ADTBuildSingleMaterial->setEnabled(true);
  }

  ui->comboBox_ADTHeightmapResolution->addItem(tr("160x160 (25%)"), ADTResolution::ADTRES_025);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("320x320 (50%)"), ADTResolution::ADTRES_05);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("640x640 (100%)"), ADTResolution::ADTRES_ORIGINAL);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("1280x1280 (200%)"), ADTResolution::ADTRES_2X);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("2560x2560 (400%)"), ADTResolution::ADTRES_4X);

  ui->comboBox_ADTHeightmapResolution->addItem(tr("256x256 (3D/Cryengine)"), ADTResolution::ADTRES_CRYLUM_256);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("512x512 (3D/Cryengine)"), ADTResolution::ADTRES_CRYLUM_512);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("1024x1024 (3D/Cryengine)"), ADTResolution::ADTRES_CRYLUM_1024);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("2048x2048 (3D/Cryengine)"), ADTResolution::ADTRES_CRYLUM_2048);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("4096x4096 (3D/Cryengine)"), ADTResolution::ADTRES_CRYLUM_4096);

  ui->comboBox_ADTHeightmapResolution->addItem(tr("253x253 (Unreal Engine)"), ADTResolution::ADTRES_UE4_253);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("505x505 (Unreal Engine)"), ADTResolution::ADTRES_UE4_505);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("1009x1009 (Unreal Engine)"), ADTResolution::ADTRES_UE4_1009);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("2017x2017 (Unreal Engine)"), ADTResolution::ADTRES_UE4_2017);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("4033x4033 (Unreal Engine)"), ADTResolution::ADTRES_UE4_4033);

  ui->comboBox_ADTHeightmapResolution->addItem(tr("257x257 (Unity)"), ADTResolution::ADTRES_UNITY_257);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("513x513 (Unity)"), ADTResolution::ADTRES_UNITY_513);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("1025x1025 (Unity)"), ADTResolution::ADTRES_UNITY_1025);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("2049x2049 (Unity)"), ADTResolution::ADTRES_UNITY_2049);
  ui->comboBox_ADTHeightmapResolution->addItem(tr("4097x4097 (Unity)"), ADTResolution::ADTRES_UNITY_4097);

  int index = ADTOutputResolution;
  ui->comboBox_ADTHeightmapResolution->setCurrentIndex(index);

  loadSettings();

  checkWoWVersion();
  updateUI();
}

// Save Settings, then start processing!
void BatchExporterDialog::on_Button_Start_pressed()
{
  LOGFILE.Debug("Start button pressed");
  saveSettings();
  startProcessing = true;
}

// Save settings, but don't start.
void BatchExporterDialog::on_Button_Close_pressed()
{
  LOGFILE.Debug("Close button pressed");
  saveSettings();
}

// Don't save settings, don't start
void BatchExporterDialog::on_Button_Cancel_pressed()
{
  LOGFILE.Debug("Cancel button pressed");
}

void BatchExporterDialog::on_button_BrowseWoWRootDir_pressed()
{
  LOGFILE.Debug("Browse WoW Dir button pressed");
  QString dir = QFileDialog::getExistingDirectory(this, tr("Choose a World of Warcraft Root directory..."), ui->lineEdit_WoWRootDir->text(), QFileDialog::Option::ShowDirsOnly);
  if (dir.isEmpty())
    return;
  ui->lineEdit_WoWRootDir->setText(dir);
  exportVariables.wowDir = QDir(dir);
  checkWoWVersion();
  updateUI();
}

void BatchExporterDialog::on_button_BrowseInputDir_pressed()
{
  LOGFILE.Debug("Browse Input Dir button pressed");
  QString dir = QFileDialog::getExistingDirectory(this, tr("Choose an input directory..."), ui->lineEdit_InputDir->text(), QFileDialog::Option::ShowDirsOnly);
  if (dir.isEmpty())
    return;
  ui->lineEdit_InputDir->setText(dir);
  exportVariables.inputDir = QDir(dir);
  updateUI();
}

void BatchExporterDialog::on_button_BrowseOutputDir_pressed()
{
  LOGFILE.Debug("Browse Output Dir button pressed");
  QString dir = QFileDialog::getExistingDirectory(this, tr("Choose an output directory..."), ui->lineEdit_OutputDir->text(), QFileDialog::Option::ShowDirsOnly);
  if (dir.isEmpty())
    return;
  ui->lineEdit_OutputDir->setText(dir);
  exportVariables.outputDir = QDir(dir);
  updateUI();
}

void BatchExporterDialog::on_button_ShowQuickSelections_pressed()
{
  if (windowQuickSelections->isVisible() == true)
  {
    windowQuickSelections->setVisible(false);
  }
  else {
    windowQuickSelections->setVisible(true);
  }
  updateUI();
}

void BatchExporterDialog::on_checkBox_OverwriteExistingFiles_clicked(bool value)
{
  LOGFILE.Debug("Overwrite Exiting Files: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.overwriteExistingFiles = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportAllModels_clicked(bool value)
{
  LOGFILE.Debug("Export All Models: %s", (value == true ? "True":"False"));
  exportVariables.exportOptions.exportAllM2s = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportAllWorldModels_clicked(bool value)
{
  LOGFILE.Debug("Export All World Models: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.exportAllWMOs = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportAllTerrain_clicked(bool value)
{
  LOGFILE.Debug("Export All Terrains: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.exportAllADTs = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportAllTextures_clicked(bool value)
{
  LOGFILE.Debug("Export all Textures: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.exportAllTextures = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ContinueOnError_clicked(bool value)
{
  LOGFILE.Debug("Continue on Error: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.continueOnError = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_CombineMeshes_clicked(bool value)
{
  LOGFILE.Debug("Combine Meshes: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.combineMeshes = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_MirrorWoWDirs_clicked(bool value)
{
  LOGFILE.Debug("Mirror WoW Dirs: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.mirrorWoWFolders = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_IncludeSubdirectories_clicked(bool value)
{
  LOGFILE.Debug("Include Subdirectories: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.includeSubdirectories = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportM2Mesh_clicked(bool value)
{
  LOGFILE.Debug("Export M2 Mesh: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.M2ExportMesh = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportM2AllLODs_clicked(bool value)
{
  LOGFILE.Debug("Export All M2 LODs: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.M2ExportAllLODs = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportM2MaxLOD_clicked(bool value)
{
  LOGFILE.Debug("Export M2 Max LOD: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.M2HighestRes = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportM2Textures_clicked(bool value)
{
  LOGFILE.Debug("Export M2 Textures: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.M2ExportTextures = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportWMOMesh_clicked(bool value)
{
  LOGFILE.Debug("Export WMO Mesh: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.WMOExportMesh = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportWMOTextures_clicked(bool value)
{
  LOGFILE.Debug("Export WMO Textures: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.WMOExportTextures = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportWMOSubmodels_clicked(bool value)
{
  LOGFILE.Debug("Export WMO Sub-models: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.WMOExportSubmodels = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportWMOPlacement_clicked(bool value)
{
  LOGFILE.Debug("Export WMO Placement: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.WMOExportPlacement = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportADTMesh_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Mesh: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportMesh = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportADTasHeightmap_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Heightmap: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportHeightmap = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ADTUseMiddlePoint_clicked(bool value)
{
  LOGFILE.Debug("ADT Use middle point: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTHeightmapUseMiddlepoint = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ADTBuildSingleMaterial_clicked(bool value)
{
  LOGFILE.Debug("Build ADT Single Material: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTSingleMaterial = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ADTBakeTexture_clicked(bool value)
{
  LOGFILE.Debug("Bake ADT Textures: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTBakeSingleTextures = value;
  updateUI();
}

void BatchExporterDialog::on_comboBox_ImageFormat_currentIndexChanged(int value)
{
  LOGFILE.Debug("Image Format Changed: %i - %s", value, qPrintable(ui->comboBox_ImageFormat->itemText(value)));
  exportVariables.exportOptions.TextureFormat = value;
  updateUI();
}

void BatchExporterDialog::on_comboBox_ModelFormat_currentIndexChanged(int value)
{
  LOGFILE.Debug("Model Format Changed: %i - %s", value, qPrintable(ui->comboBox_ModelFormat->itemText(value)));
  exportVariables.exportOptions.ModelFormat = value;
  updateUI();
}

void BatchExporterDialog::on_comboBox_PlacementFormat_currentIndexChanged(int value)
{
  LOGFILE.Debug("Placement Format Changed: %i - %s", value, qPrintable(ui->comboBox_PlacementFormat->itemText(value)));
  exportVariables.exportOptions.PlacementFormat = value;
  updateUI();
}

void BatchExporterDialog::on_comboBox_HeightmapFormat_currentIndexChanged(int value)
{
  LOGFILE.Debug("Heightmap Format Changed: %i - %s", value, qPrintable(ui->comboBox_HeightmapFormat->itemText(value)));
  exportVariables.exportOptions.ADTHeightmapFormat = value;
  updateUI();
}

void BatchExporterDialog::on_comboBox_ADTHeightmapResolution_currentIndexChanged(int value)
{
  LOGFILE.Debug("Heightmap Resolution Changed: %i - %s", value, qPrintable(ui->comboBox_ADTHeightmapResolution->itemText(value)));
  exportVariables.exportOptions.ADTHeightmapResolution = value;
  updateUI();
}

void BatchExporterDialog::on_lineEdit_WoWRootDir_textEdited(QString value)
{
  exportVariables.wowDir = QDir(value);
}

void BatchExporterDialog::on_lineEdit_InputDir_textEdited(QString value)
{
  exportVariables.inputDir = QDir(value);
}

void BatchExporterDialog::on_lineEdit_OutputDir_textEdited(QString value)
{
  exportVariables.outputDir = QDir(value);
}

void BatchExporterDialog::on_checkBox_ExportADTMasks_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Masks: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportMasks = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportADTTextures_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Textures: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportTextures = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportADTSubmodels_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Sub-models: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportSubmodels = value;
  updateUI();
}

void BatchExporterDialog::on_checkBox_ExportADTPlacement_clicked(bool value)
{
  LOGFILE.Debug("Export ADT Placement: %s", (value == true ? "True" : "False"));
  exportVariables.exportOptions.ADTExportPlacement = value;
  updateUI();
}

void BatchExporterDialog::updateUI()
{
  // Check Quick Selections
  if (windowQuickSelections->isVisible() == false)
  {
    ui->button_ShowQuickSelections->setChecked(false);
  }
  else {
    ui->button_ShowQuickSelections->setChecked(true);
  }

  // Check for Model options
  if (ui->checkBox_ExportAllModels->isChecked() == true || (ui->checkBox_ExportWMOSubmodels->isChecked() == true && ui->checkBox_ExportAllWorldModels->isChecked() == true) || (ui->checkBox_ExportADTSubmodels->isChecked() == true && ui->checkBox_ExportAllTerrain->isChecked() == true))
  {
    ui->groupBox_Models->setEnabled(true);
  }
  else {
    ui->groupBox_Models->setEnabled(false);
  }

  // Check for World Model options
  if (ui->checkBox_ExportAllWorldModels->isChecked() == true || (ui->checkBox_ExportADTSubmodels->isChecked() == true && ui->checkBox_ExportAllTerrain->isChecked() == true))
  {
    ui->groupBox_WorldModels->setEnabled(true);
  }
  else {
    ui->groupBox_WorldModels->setEnabled(false);
  }

  // Check for World Model options
  if (ui->checkBox_ExportAllTerrain->isChecked() == true)
  {
    ui->groupBox_Terrain->setEnabled(true);
  }
  else {
    ui->groupBox_Terrain->setEnabled(false);
  }

  if (ui->checkBox_ExportAllModels->isChecked() == false && ui->checkBox_ExportAllWorldModels->isChecked() == false && ui->checkBox_ExportAllTerrain->isChecked() == false && ui->checkBox_ExportAllTextures->isChecked() == false)
  {
    ui->Button_Start->setEnabled(false);
  }
  else {
    if (supportedBuild == true)
    {
      ui->Button_Start->setEnabled(true);
    }
    else {
      ui->Button_Start->setEnabled(false);
    }
  }
}

void BatchExporterDialog::loadSettings()
{
  exportVariables.exportOptions.setVariables(SETTINGS.value("BatchExporterOptions", ExportVariables().getVariables()).toString());

  // Update UI
  if (exportVariables.wowDir == QDir() || exportVariables.wowDir.isEmpty())
  {
    exportVariables.wowDir = WoWDir.absolutePath();
  }
  if (exportVariables.inputDir == QDir() || exportVariables.inputDir.isEmpty())
  {
    exportVariables.inputDir = WoWDir.absolutePath();
  }
  if (exportVariables.outputDir == QDir() || exportVariables.outputDir.isEmpty())
  {
    exportVariables.outputDir = OutputDir.absolutePath();
  }
  ui->lineEdit_WoWRootDir->setText(exportVariables.wowDir.absolutePath());
  ui->lineEdit_InputDir->setText(exportVariables.inputDir.absolutePath());
  ui->lineEdit_OutputDir->setText(exportVariables.outputDir.absolutePath());

  ui->checkBox_ExportAllTextures->setChecked(exportVariables.exportOptions.exportAllTextures);
  ui->checkBox_ExportAllModels->setChecked(exportVariables.exportOptions.exportAllM2s);
  ui->checkBox_ExportAllWorldModels->setChecked(exportVariables.exportOptions.exportAllWMOs);
  ui->checkBox_ExportAllTerrain->setChecked(exportVariables.exportOptions.exportAllADTs);

  ui->checkBox_ContinueOnError->setChecked(exportVariables.exportOptions.continueOnError);
  ui->checkBox_MirrorWoWDirs->setChecked(exportVariables.exportOptions.mirrorWoWFolders);
  ui->checkBox_IncludeSubdirectories->setChecked(exportVariables.exportOptions.includeSubdirectories);
  ui->checkBox_CombineMeshes->setChecked(exportVariables.exportOptions.combineMeshes);
  ui->checkBox_OverwriteExistingFiles->setChecked(exportVariables.exportOptions.overwriteExistingFiles);

  ui->comboBox_ModelFormat->setCurrentIndex(exportVariables.exportOptions.ModelFormat);
  ui->comboBox_ImageFormat->setCurrentIndex(exportVariables.exportOptions.TextureFormat);
  ui->comboBox_PlacementFormat->setCurrentIndex(exportVariables.exportOptions.PlacementFormat);
  ui->comboBox_HeightmapFormat->setCurrentIndex(exportVariables.exportOptions.ADTHeightmapFormat);
  ui->comboBox_ADTHeightmapResolution->setCurrentIndex(exportVariables.exportOptions.ADTHeightmapResolution);

  ui->checkBox_ADTUseMiddlePoint->setChecked(exportVariables.exportOptions.ADTHeightmapUseMiddlepoint);
  ui->checkBox_ADTBakeTexture->setChecked(exportVariables.exportOptions.ADTBakeSingleTextures);
  ui->checkBox_ADTBuildSingleMaterial->setChecked(exportVariables.exportOptions.ADTSingleMaterial);
  ui->checkBox_ExportADTasHeightmap->setChecked(exportVariables.exportOptions.ADTExportHeightmap);
  ui->checkBox_ExportADTMasks->setChecked(exportVariables.exportOptions.ADTExportMasks);
  ui->checkBox_ExportADTMesh->setChecked(exportVariables.exportOptions.ADTExportMesh);
  ui->checkBox_ExportADTPlacement->setChecked(exportVariables.exportOptions.ADTExportPlacement);
  ui->checkBox_ExportADTSubmodels->setChecked(exportVariables.exportOptions.ADTExportSubmodels);
  ui->checkBox_ExportADTTextures->setChecked(exportVariables.exportOptions.ADTExportTextures);

  ui->checkBox_ExportWMOMesh->setChecked(exportVariables.exportOptions.WMOExportMesh);
  ui->checkBox_ExportWMOPlacement->setChecked(exportVariables.exportOptions.WMOExportPlacement);
  ui->checkBox_ExportWMOSubmodels->setChecked(exportVariables.exportOptions.WMOExportSubmodels);
  ui->checkBox_ExportWMOTextures->setChecked(exportVariables.exportOptions.WMOExportTextures);

  ui->checkBox_ExportM2Mesh->setChecked(exportVariables.exportOptions.M2ExportMesh);
  ui->checkBox_ExportM2Textures->setChecked(exportVariables.exportOptions.M2ExportTextures);
  ui->checkBox_ExportM2MaxLOD->setChecked(exportVariables.exportOptions.M2HighestRes);
  ui->checkBox_ExportM2AllLODs->setChecked(exportVariables.exportOptions.M2ExportAllLODs);
}

void BatchExporterDialog::saveSettings()
{
  SETTINGS.setValue("BatchExporterOptions", exportVariables.exportOptions.getVariables());
  SETTINGS.sync();
}

void BatchExporterDialog::checkWoWVersion()
{
  findWoWApp(ui->lineEdit_WoWRootDir->text());
  cWoWVersion wow = getWoWVersion(exportVariables.wowAppLocation, showUnsupportedBuildWarning);
  if (wow.isBlank())
  {
    supportedBuild = false;
    ui->label_DetectedWoWVersion->setText(tr("Unable to find WoW.exe"));
    ui->label_VersionSourceFile->hide();
    ui->label_VersionSourceFile_Location->setText("");
  }
  else if (wow.isKnownLiveBuild() == false)
  {
    supportedBuild = false;
    ui->label_DetectedWoWVersion->setText(tr("Unsupported build detected: %s").arg(wow.getString()));
    ui->label_VersionSourceFile->hide();
    ui->label_VersionSourceFile_Location->setText("");
  }
  else {
    supportedBuild = true;
    ui->label_DetectedWoWVersion->setText(wow.getString());
    ui->label_VersionSourceFile->show();
    ui->label_VersionSourceFile_Location->setText(exportVariables.wowAppFilePath);
  }
}

void BatchExporterDialog::findWoWApp(QDir dir)
{
  LOGFILE.Info("Attemping to find WoW application...");
  if (dir.isEmpty())
  {
    qCritical("No directory specified. Can't find WoW.");
    LOGFILE.Error("No directory specified. Can't find WoW.");
    exportVariables.wowAppLocation.clear();
    return;
  }
  if (dir.exists() == false)
  {
    qCritical("Specified directory doesn't exist. Can't find WoW.");
    LOGFILE.Error("Specified directory doesn't exist. Can't find WoW.");
    exportVariables.wowAppLocation.clear();
    return;
  }

  QStringList exeNames;
#if defined Q_OS_WIN
  exeNames << "WoW.exe" << "WoW-64.exe";
#elif defined Q_OS_LINUX
  assert("This operating system is not yet supported.");
#elif defined Q_OS_MAC
  assert("This operating system is not yet supported.");
#else
  assert("This operating system is not yet supported.");
#endif

  bool breakloop = false;
  while (dir.isRoot() != true)
  {
    for (uint32_t i = 0; i < exeNames.count(); i++)
    {
      QString a = exeNames.at(i);
      LOGFILE.Debug("Checking for WoW application in path: %s", qPrintable(dir.absolutePath()));

      QFileInfo exeInfo(dir.absoluteFilePath(a));
      if (exeInfo.exists() == true)
      {
        LOGFILE.Info("Found WoW application at %s", qPrintable(dir.absolutePath()));
        exportVariables.wowAppLocation = dir.absolutePath();
        exportVariables.wowAppFilename = a;
        exportVariables.wowAppFilePath = dir.absoluteFilePath(a);
        return;
      }
    }
    dir.cdUp();
  }

  // If we've reached this point, WoW isn't in this file path...
  exportVariables.wowAppLocation.clear();
}
