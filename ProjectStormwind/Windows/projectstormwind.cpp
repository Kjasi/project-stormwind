// Project Includes
#include "projectstormwind.h"
#include "OptionsDialog.h"
#include "AboutPlugins.h"
#include "BatchExporterDialog.h"
#include "Tools/ADTHeightScanner.h"
#include "Tools/M2VersionScanner.h"
#include "Tools/BatchExporter.h"

// Solution Includes
#include <Foundation/saveFiles.hpp>
#include <Foundation/Classes/Vectors.h>
#include <Foundation/Classes/ImageData.h>
#include <WoWFormats/Landscape/ADT.h>
#include <WoWFormats/WorldModels/WMO.h>
#include <WoWFormats/Models/M2.h>
#include <WoWFormats/Textures/BLP.h>

// Qt Includes
#include <QtCore/qsettings.h>
#include <QtCore/qprocess.h>
#include <QtGui/qdesktopservices.h>
#include <QtWidgets/qactiongroup.h>
#include <QtWidgets/qbuttongroup.h>
#include <QtWidgets/qmessagebox.h>
#include <QXmlStreamWriter>
#include <QRegExp>
#include <qloggingcategory.h>
using namespace ProjectSW;

ADT *adt = nullptr;
BLPInfo *blp = nullptr;
WMO *wmo = nullptr;
M2 *m2 = nullptr;

ADTResolution ADTOutputResolution = ADTRES_ORIGINAL;

ProjectStormwind::ProjectStormwind(QVulkanInstance *vulkan_instance, QWidget *parent)
  : QMainWindow(parent), VulkanInst(vulkan_instance)
{
  LOGFILE.Info("Initializing Window...");
  ui.setupUi(this);
  loadSettings();

  FileType = SOURCE_NONE;

#ifdef _DEBUG
  QLoggingCategory::setFilterRules("qt.vulkan.debug=true");
#else
  QLoggingCategory::setFilterRules("qt.vulkan.debug=false");
#endif // _DEBUG

  fileDetailsWidget = new widgetFileDetails(this);
  instancesWidget = new widgetInstances(this);

  FileInfo fi;
  fi.fileType = SourceFileType::SOURCE_NONE;
  fileDetailsWidget->setFileInfo(fi);

  ui.dockWidgetInstances->setWidget(instancesWidget);
  ui.dockWidgetFileDetails->setWidget(fileDetailsWidget);
  ui.dockWidgetInstances->hide();
  ui.dockWidgetFileDetails->hide();

  VkWindow = new VulkanWindow();
  VkWindow->setVulkanInstance(VulkanInst);
  VkWindow->setDefaultClearColor(BackgroundColor);
  ui.VkWidget->layout()->addWidget(QWidget::createWindowContainer(VkWindow));

  ADTResolutionGroup = new QActionGroup(this);
  ADTResolutionGroup->addAction(ui.actionADTRes0_25x);
  ADTResolutionGroup->addAction(ui.actionADTRes0_5x);
  ADTResolutionGroup->addAction(ui.actionADTResOriginal);
  ADTResolutionGroup->addAction(ui.actionADTRes2x);
  ADTResolutionGroup->addAction(ui.actionADTRes4x);
  ADTResolutionGroup->addAction(ui.actionADTResUE4253x253);
  ADTResolutionGroup->addAction(ui.actionADTResUE4505x505);
  ADTResolutionGroup->addAction(ui.actionADTResUE41009x1009);
  ADTResolutionGroup->addAction(ui.actionADTResUE42017x2017);
  ADTResolutionGroup->addAction(ui.actionADTResUE44033x4033);
  ADTResolutionGroup->addAction(ui.actionADTResUnity257x257);
  ADTResolutionGroup->addAction(ui.actionADTResUnity513x513);
  ADTResolutionGroup->addAction(ui.actionADTResUnity1025x1025);
  ADTResolutionGroup->addAction(ui.actionADTResUnity2049x2049);
  ADTResolutionGroup->addAction(ui.actionADTResUnity4097x4097);
  ADTResolutionGroup->addAction(ui.actionADTResCryLum256x256);
  ADTResolutionGroup->addAction(ui.actionADTResCryLum512x512);
  ADTResolutionGroup->addAction(ui.actionADTResCryLum1024x1024);
  ADTResolutionGroup->addAction(ui.actionADTResCryLum2048x2048);
  ADTResolutionGroup->addAction(ui.actionADTResCryLum4096x4096);
  ADTResolutionGroup->setExclusive(true);

  viewStyleGroup = new QButtonGroup(this);
  viewStyleGroup->addButton(ui.viewButton_Wireframe);
  viewStyleGroup->addButton(ui.viewButton_Shaded);
  viewStyleGroup->addButton(ui.viewButton_WireframeShaded);
  viewStyleGroup->addButton(ui.viewButton_Textured);
  viewStyleGroup->addButton(ui.viewButton_TexturedShaded);
  viewStyleGroup->addButton(ui.viewButton_WireframeTextured);
  viewStyleGroup->setExclusive(true);

  viewColorGroup = new QButtonGroup(this);
  viewColorGroup->addButton(ui.viewButton_ColorAlpha);
  viewColorGroup->addButton(ui.viewButton_Color);
  viewColorGroup->addButton(ui.viewButton_Alpha);
  viewColorGroup->addButton(ui.viewButton_Heightmap);
  viewColorGroup->addButton(ui.viewButton_ADTLayerMasks);
  viewColorGroup->setExclusive(true);

  // Display Mode
  connect(ui.viewButton_Wireframe, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_Wireframe);
  connect(ui.viewButton_Shaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_Solid);
  connect(ui.viewButton_WireframeShaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_WireframeOnSolid);
  connect(ui.viewButton_Textured, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_Textured);
  connect(ui.viewButton_TexturedShaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_TexturedShaded);
  connect(ui.viewButton_WireframeTextured, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_WireframeOnTextured);

  // Enable/Disable Image/Texture Controls
  connect(ui.viewButton_Wireframe, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_DisableImageControls);
  connect(ui.viewButton_Shaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_DisableImageControls);
  connect(ui.viewButton_WireframeShaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_DisableImageControls);
  connect(ui.viewButton_Textured, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_EnableImageControls);
  connect(ui.viewButton_TexturedShaded, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_EnableImageControls);
  connect(ui.viewButton_WireframeTextured, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_EnableImageControls);

  // Texture Controls & culling
  connect(ui.viewButton_Cull, &QAbstractButton::clicked, this, &ProjectStormwind::toggleCulling);
  connect(ui.viewButton_ColorAlpha, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_RGBA);
  connect(ui.viewButton_Color, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_RGB);
  connect(ui.viewButton_Alpha, &QAbstractButton::clicked, this, &ProjectStormwind::changeDisplay_Alpha);

  QString title = tr("%1, Version %2", "First, the title, then the version number.").arg(PROJECTSTORMWIND_TITLE).arg(PROJECTSTORMWIND_VERSION);
  setWindowTitle(title);

  UpdateMenu();
}

ProjectStormwind::~ProjectStormwind()
{
  saveSettings();
  VkWindow->deleteLater();
  fileDetailsWidget->deleteLater();
  instancesWidget->deleteLater();
  LOGFILE.Info("Program Closed.");
}

void ProjectStormwind::changeDisplay_DisableImageControls()
{
  ui.viewButton_ColorAlpha->setEnabled(false);
  ui.viewButton_Color->setEnabled(false);
  ui.viewButton_Alpha->setEnabled(false);
}

void ProjectStormwind::changeDisplay_EnableImageControls()
{
  ui.viewButton_ColorAlpha->setEnabled(true);
  ui.viewButton_Color->setEnabled(true);
  ui.viewButton_Alpha->setEnabled(true);
}

void ProjectStormwind::OpenFile(QString FileName)
{
  LOGFILE.Info("Opening File: %s", qPrintable(FileName));
  OpenFileName = FileName.right(FileName.lastIndexOf(QDir::separator())).left(FileName.lastIndexOf("."));

  QString ext = FileName.mid(FileName.lastIndexOf(".")+1).toLower();
  LOGFILE.Debug("Extension: %s", qPrintable(ext));

  // Check to see if this is a supported extention.
  if (ext != "adt" && ext != "wmo" && ext != "m2" && ext != "mdx" && ext != "blp" && ext != "wdl")
  {
    qCritical(qPrintable(QString("Extension is of an unknown type: %s").arg(ext)));
    LOGFILE.Error("Extension is of an unknown type: %s", qPrintable(ext));
    return;
  }
  if (CheckFile(ext, FileName) == false) {
    qCritical("File is of an unknown type.");
    LOGFILE.Error("File is of an unknown type.");
    return;
  }

  currentWoW = getWoWVersion(WoWDir, showUnsupportedBuildWarning);
  if (currentWoW.isBlank() == true)
  {
    qCritical("Unable to determine current WoW version.");
    LOGFILE.Error("Unable to determine current WoW version. Results may be unpredictable!");
    QMessageBox a(QMessageBox::Icon::Critical, QObject::tr("Unable to determine Current WoW version", "A simple error message for currentWoW being blank"), QObject::tr("Unable to determine the current WoW version.\n\nPlease open your preferences, and make sure the WoW Root directory is pointed to the right directory."), QMessageBox::Ok);
    a.exec();
    return;
  }
  LOGFILE.Info("Current WoW version: %s", qPrintable(currentWoW.getString()));
  WoWVersion = currentWoW;

  // At this point, I'm confident that the file is exactly what we think it is.
  // Any further checks are to ensure that a m2 file was not renamed as a wmo or blp.

  if (ext == "m2" || ext == "mdx") {
    LOGFILE.Debug("Getting file as M2...\n");
    m2 = new M2(FileName, currentWoW);
    if (m2 == nullptr || m2->isLoaded() == false) { ui.label_CurrentFileName->setText(tr("None")); return; }
    FileType = SOURCE_M2;
    FileInfo fi;
    fi.fileType = FileType;
    fi.TextureList = m2->getTextureList();
    fileDetailsWidget->setFileInfo(fi);
    if (disableRenderer == false) VkWindow->setSceneData(m2->getVulkanData());
  } else if (ext == "adt") {
    LOGFILE.Debug("Getting file as ADT...\n");
    adt = new ADT(FileName, ADTBuildSingleMat);
    if (adt == nullptr || adt->isLoaded() == false) { ui.label_CurrentFileName->setText(tr("None")); return; }
    FileType = SOURCE_ADT;
    FileName = adt->getCoreName();
    FileInfo fi;
    fi.fileType = FileType;
    fi.TextureList = adt->getTextureList();
    fi.WMOFileList = adt->getWMOModelList();
    fi.ModelFileList = adt->getDoodadModelList();
    fileDetailsWidget->setFileInfo(fi);
    // Load ADT into Vulkan
    if (disableRenderer == false) VkWindow->setSceneData(adt->getVulkanData());
  } else if (ext == "wdl") {
    LOGFILE.Debug("Getting file as WDL...\n");
    FileType = SOURCE_WDL;
    FileInfo fi;
    fi.fileType = FileType;
    fileDetailsWidget->setFileInfo(fi);
  } else if (ext == "blp") {
    LOGFILE.Debug("Getting file \"%s\" as BLP...\n", qPrintable(FileName));
    FileType = SOURCE_BLP;
    blp = ReadBLPFile(FileName);
    FileInfo fi;
    fi.fileType = FileType;
    fi.TextureHasAlpha = blp->hasAlpha();
    fi.TextureWidth = blp->Header.ResX;
    fi.TextureHeight = blp->Header.ResY;
    fi.TextureChannelCount = -1;
    fileDetailsWidget->setFileInfo(fi);
    VkWindow->generateQuad();
    DisplayBLPImage();
  }else if (ext == "wmo") {
    LOGFILE.Debug("Getting file as WMO...\n");
    wmo = new WMO(FileName);
    if (wmo == nullptr || wmo->isLoaded() == false) { ui.label_CurrentFileName->setText(tr("None")); return; }
    FileType = SOURCE_WMO;
    FileName = wmo->getRootFileName();
    FileInfo fi;
    fi.fileType = FileType;
    fi.TextureList = wmo->getTextureList();
    fi.ModelFileList = wmo->getDoodadList();
    fileDetailsWidget->setFileInfo(fi);
    if (disableRenderer == false) VkWindow->setSceneData(wmo->getVulkanData());
  } else {
    ui.label_CurrentFileName->setText(tr("None"));
    LOGFILE.Error("File type is not supported.\n");
    FileType = SOURCE_NONE;
    return;
  }
  if (FileType == SOURCE_BLP)
  {
    DisplayBLPImage();
  }
  else {
    HideBLPImage();
  }

  ui.label_CurrentFileName->setText(FileName);
  addToRecentFiles(FileName);
  LastDir = FileName.mid(0, FileName.lastIndexOf("/"));

  UpdateMenu();
}

bool ProjectStormwind::CheckFile(QString Extension, QString FileName)
{
  FILE *fp = fopen(qPrintable(FileName), "rb");

  fseek(fp, 0, SEEK_SET);
  char str[5];

  fread(str, 1, 4, fp);
  str[4] = 0;
  fclose(fp);

  LOGFILE.Debug("File data format check: %s",str);

  if ((strncmp(str, "REVM", 4) == 0) || (strncmp(str, "MD20", 4) == 0) || (strncmp(str, "MD21", 4) == 0) || (strncmp(str, "BLP2", 4) == 0)) {
    return true;
  }

  LOGFILE.Warn("File failed data format check.");
  return false;
}

void ProjectStormwind::loadSettings()
{
  LOGFILE.Info("Loading Settings...");
  SETTINGS.sync();
  WoWDir = SETTINGS.value("wowdir", QDir(QApplication::applicationFilePath()).absolutePath()).toString();
  OutputDir = SETTINGS.value("outputdir", QDir(QApplication::applicationFilePath()).absolutePath()).toString();
  LastDir = SETTINGS.value("lastdir", QDir(QApplication::applicationFilePath()).absolutePath()).toString();
  MirrorWoWPaths = SETTINGS.value("mirrorwowpaths", true).toBool();
  disableRenderer = SETTINGS.value("disablerenderer", false).toBool();
  disableTextures = SETTINGS.value("disablerendertextures", false).toBool();
  LOGFILE.setShowDebug(SETTINGS.value("logging/showdebug", false).toBool());
  LOGFILE.setUseTimestamp(SETTINGS.value("logging/usetimestamp", true).toBool());
  ADTOutputResolution = static_cast<ADTResolution>(SETTINGS.value("adt/outputresolution", (int)ADTResolution::ADTRES_ORIGINAL).toInt());
  ADTUseMiddlePoint = SETTINGS.value("adt/usemiddlepoint", false).toBool();
  ADTBuildSingleMat = SETTINGS.value("adt/buildsinglematerial", false).toBool();
  PreferedExporter_Models = SETTINGS.value("preferedExporter/models", QString()).toString();
  PreferedExporter_Textures = SETTINGS.value("preferedExporter/textures", QString()).toString();
  PreferedExporter_Placement = SETTINGS.value("preferedExporter/placement", QString()).toString();
  QString color = SETTINGS.value("bgcolor", RGBAColor(89, 105, 122).toString()).toString();
  RGBAColor c = RGBAColor::fromString(color);
  //LOGFILE.Debug("Loaded BG color: %s", qPrintable(c.toString()));
  BackgroundColor->setColors(c);
}

void ProjectStormwind::saveSettings()
{
  SETTINGS.setValue("wowdir", WoWDir.absolutePath());
  SETTINGS.setValue("outputdir", OutputDir.absolutePath());
  SETTINGS.setValue("lastdir", LastDir.absolutePath());
  SETTINGS.setValue("mirrorwowpaths", MirrorWoWPaths);
  SETTINGS.setValue("disablerenderer", disableRenderer);
  SETTINGS.setValue("disablerendertextures", disableTextures);
  SETTINGS.setValue("logging/showdebug", LOGFILE.getShowDebug());
  SETTINGS.setValue("logging/usetimestamp", LOGFILE.getUseTimestamp());
  SETTINGS.setValue("adt/outputresolution", (int)ADTOutputResolution);
  SETTINGS.setValue("adt/usemiddlepoint", ADTUseMiddlePoint);
  SETTINGS.setValue("adt/buildsinglematerial", ADTBuildSingleMat);
  SETTINGS.setValue("preferedExporter/models", PreferedExporter_Models);
  SETTINGS.setValue("preferedExporter/textures", PreferedExporter_Textures);
  SETTINGS.setValue("preferedExporter/placement", PreferedExporter_Placement);
  SETTINGS.setValue("bgcolor", BackgroundColor->toString());

  //LOGFILE.Debug("Saved BG color: %s", qPrintable(BackgroundColor->toString()));
  SETTINGS.sync();
}

void ProjectStormwind::ClearMenu()
{
  ui.menuExport->clear();
  ui.menuExport->setDisabled(true);
  ui.menuADTs->menuAction()->setVisible(false);
  ui.viewButton_Heightmap->setVisible(false);
  ui.viewButton_ADTLayerMasks->setVisible(false);
  ui.menuRecentFiles->clear();
}

void ProjectStormwind::UpdateMenu()
{
  LOGFILE.Info("Updating Menu...");

  // Non-Content-based Updates
  ui.currentFileWidget->setVisible(ui.actionShowCurrentFile->isChecked());
  ui.viewControlsWidget->setVisible(ui.actionShowViewControls->isChecked());
  ui.dockWidgetInstances->setVisible(ui.actionShowInstances->isChecked());
  ui.dockWidgetFileDetails->setVisible(ui.actionShowFileDetails->isChecked());

  // Update ADT Resolution Menu

  switch (ADTOutputResolution)
  {
  case ADTResolution::ADTRES_025:
    ui.actionADTRes0_25x->setChecked(true);
    break;
  case ADTResolution::ADTRES_05:
    ui.actionADTRes0_5x->setChecked(true);
    break;
  case ADTResolution::ADTRES_2X:
    ui.actionADTRes2x->setChecked(true);
    break;
  case ADTResolution::ADTRES_4X:
    ui.actionADTRes4x->setChecked(true);
    break;
  case ADTResolution::ADTRES_UE4_253:
    ui.actionADTResUE4253x253->setChecked(true);
    break;
  case ADTResolution::ADTRES_UE4_505:
    ui.actionADTResUE4505x505->setChecked(true);
    break;
  case ADTResolution::ADTRES_UE4_1009:
    ui.actionADTResUE41009x1009->setChecked(true);
    break;
  case ADTResolution::ADTRES_UE4_2017:
    ui.actionADTResUE42017x2017->setChecked(true);
    break;
  case ADTResolution::ADTRES_UNITY_257:
    ui.actionADTResUnity257x257->setChecked(true);
    break;
  case ADTResolution::ADTRES_UNITY_513:
    ui.actionADTResUnity513x513->setChecked(true);
    break;
  case ADTResolution::ADTRES_UNITY_1025:
    ui.actionADTResUnity1025x1025->setChecked(true);
    break;
  case ADTResolution::ADTRES_UNITY_2049:
    ui.actionADTResUnity2049x2049->setChecked(true);
    break;
  case ADTResolution::ADTRES_CRYLUM_256:
    ui.actionADTResCryLum256x256->setChecked(true);
    break;
  case ADTResolution::ADTRES_CRYLUM_512:
    ui.actionADTResCryLum512x512->setChecked(true);
    break;
  case ADTResolution::ADTRES_CRYLUM_1024:
    ui.actionADTResCryLum1024x1024->setChecked(true);
    break;
  case ADTResolution::ADTRES_CRYLUM_2048:
    ui.actionADTResCryLum2048x2048->setChecked(true);
    break;
  case ADTResolution::ADTRES_CRYLUM_4096:
    ui.actionADTResCryLum4096x4096->setChecked(true);
    break;
  case ADTResolution::ADTRES_ORIGINAL:
  default:
    ui.actionADTResOriginal->setChecked(true);
    break;
  }
  ClearMenu();

  // Update Recent Menu
  SETTINGS.beginGroup("RecentFiles");
  QStringList recentFileKeys = SETTINGS.allKeys();

  ui.menuRecentFiles->setDisabled(false);
  if (recentFileKeys.count() <= 0)
  {
    LOGFILE.Debug("No keys found. Disabling Recent menu.");
    ui.menuRecentFiles->setDisabled(true);
  }
  LOGFILE.Debug("RecentFileKeys Count: %i", recentFileKeys.count());

  for (int i = 0; i < recentFileKeys.count(); i++)
  {
    QString recentFileName = SETTINGS.value(recentFileKeys.at(i)).toString();
    LOGFILE.Debug("Adding %s to Recent List", qPrintable(recentFileName));
    QAction *action = new QAction(QString("%1 %2").arg(i+1).arg(recentFileName), this);
    action->setData(recentFileName);
    ui.menuRecentFiles->addAction(action);
    connect(action, &QAction::triggered, this, &ProjectStormwind::openRecentFile);
  }
  SETTINGS.endGroup();

  // Content-based Updates
  if (FileType == SOURCE_NONE)
  {
    return;
  }
  ui.menuExport->setDisabled(false);

  // Common Export Options & Control Visibility
  if (FileType != SOURCE_BLP) {
    QAction *actExAll = new QAction(tr("Export &All..."), this);
    actExAll->setStatusTip(tr("Export everything using the default exporters."));
    ui.menuExport->addAction(actExAll);
    ui.menuExport->addSeparator();
    QAction *actExMesh = new QAction(tr("&Export Mesh..."), this);
    actExMesh->setStatusTip(tr("Export current object as a mesh."));
    ui.menuExport->addAction(actExMesh);
    QAction *actExTexture = new QAction(tr("Export &Textures..."), this);
    actExTexture->setStatusTip(tr("Export the current object's textures."));
    QAction *actExPlace = new QAction(tr("Export &Placement..."), this);
    actExPlace->setStatusTip(tr("Export sub-object placements."));
    QAction *actExSubModels = new QAction(tr("Export &Submodels..."), this);
    actExSubModels->setStatusTip(tr("Export models referenced by the current model."));
    if (FileType == SOURCE_WDL) {
      ui.menuExport->addAction(actExPlace);
      ui.menuExport->addSeparator();
      ui.menuExport->addAction(actExSubModels);
      ui.viewButton_Heightmap->setVisible(true);
      connect(actExAll, &QAction::triggered, this, &ProjectStormwind::ExportWDLAll);
      connect(actExMesh, &QAction::triggered, this, &ProjectStormwind::ExportWDLMesh);
      connect(actExPlace, &QAction::triggered, this, &ProjectStormwind::ExportWDLPlacement);
      connect(actExSubModels, &QAction::triggered, this, &ProjectStormwind::ExportWDLSubmeshes);
    }
    else if (FileType == SOURCE_WMO) {
      ui.menuExport->addAction(actExTexture);
      ui.menuExport->addAction(actExPlace);
      ui.menuExport->addSeparator();
      ui.menuExport->addAction(actExSubModels);
      connect(actExAll, &QAction::triggered, this, &ProjectStormwind::ExportWMOAll);
      connect(actExMesh, &QAction::triggered, this, &ProjectStormwind::ExportWMOMesh);
      connect(actExPlace, &QAction::triggered, this, &ProjectStormwind::ExportWMOPlacement);
      connect(actExSubModels, &QAction::triggered, this, &ProjectStormwind::ExportWMOSubmeshes);
      connect(actExTexture, &QAction::triggered, this, &ProjectStormwind::ExportWMOTextures);
    }
    else if (FileType == SOURCE_M2) {
      ui.menuExport->addAction(actExTexture);
      connect(actExAll, &QAction::triggered, this, &ProjectStormwind::ExportM2All);
      connect(actExMesh, &QAction::triggered, this, &ProjectStormwind::ExportM2Mesh);
      connect(actExTexture, &QAction::triggered, this, &ProjectStormwind::ExportM2Textures);
    }
    else if (FileType == SOURCE_ADT) {
      ui.menuExport->addAction(actExTexture);
      ui.menuExport->addAction(actExPlace);
      ui.menuExport->addSeparator();
      ui.menuExport->addAction(actExSubModels);
      ui.menuADTs->menuAction()->setVisible(true);
      ui.viewButton_Heightmap->setVisible(true);
      ui.viewButton_ADTLayerMasks->setVisible(true);
      connect(actExAll, &QAction::triggered, this, &ProjectStormwind::ExportADTAll);
      connect(actExMesh, &QAction::triggered, this, &ProjectStormwind::ExportADTMesh);
      connect(actExPlace, &QAction::triggered, this, &ProjectStormwind::ExportADTPlacement);
      connect(actExSubModels, &QAction::triggered, this, &ProjectStormwind::ExportADTSubmeshes);
      connect(actExTexture, &QAction::triggered, this, &ProjectStormwind::ExportADTTextures);
    }
    else {
      LOGFILE.Error("No Exporter setup yet for FileType %i.", FileType);
      QMessageBox a(QMessageBox::Warning, tr("Invalid Export"), tr("Unable to export file. Could not find the proper export for the FileType."), QMessageBox::Ok);
      a.exec();
    }
  }

  // Additional Export Options
  if (FileType == SOURCE_BLP) {
    QAction *actExImg = new QAction(tr("&Export Image..."), this);
    actExImg->setStatusTip(tr("Export current BLP as an image."));
    ui.menuExport->addAction(actExImg);
    connect(actExImg, &QAction::triggered, this, &ProjectStormwind::ExportBLP);
  }

  if (FileType == SOURCE_ADT || FileType == SOURCE_WDL)
  {
    QAction *actSeparator = new QAction();
    actSeparator->setSeparator(true);
    ui.menuExport->addAction(actSeparator);

    QAction *actExHeight = new QAction(tr("&Heightmap Data..."),this);
    actExHeight->setStatusTip(tr("Export the Heightmap data as an image."));
    ui.menuExport->addAction(actExHeight);
    if (FileType == SOURCE_WDL){
      connect(actExHeight, &QAction::triggered, this, &ProjectStormwind::ExportWDLHeightmap);
    }
    else {
      connect(actExHeight, &QAction::triggered, this, &ProjectStormwind::ExportADTHeightmap);
    }

    if (FileType == SOURCE_ADT)
    {
      QAction *actExBaked = new QAction(tr("&Baked Texture..."), this);
      actExBaked->setStatusTip(tr("Export a baked texture that covers the entire terrain."));
      ui.menuExport->addAction(actExBaked);
      connect(actExBaked, &QAction::triggered, this, &ProjectStormwind::ExportADTBakedTexture);

      QAction *actExMasks = new QAction(tr("&Mask Layers..."), this);
      actExMasks->setStatusTip(tr("Export the chunk masks as images."));
      ui.menuExport->addAction(actExMasks);
      connect(actExMasks, &QAction::triggered, this, &ProjectStormwind::ExportADTMasks);

      QAction *actExLiquid = new QAction(tr("&Liquid Mesh..."), this);
      actExLiquid->setStatusTip(tr("Export liquid chunks as a mesh."));
      ui.menuExport->addAction(actExLiquid);
      connect(actExLiquid, &QAction::triggered, this, &ProjectStormwind::ExportADTLiquids);
    }

    ui.menuExport->addSeparator();
  }

  // Dynamic, plugin-based Mesh actions...
  if (FileType == SOURCE_ADT || FileType == SOURCE_M2 || FileType == SOURCE_WDL || FileType == SOURCE_WMO) {
    //ui.menuExport->setDisabled(false);
  }

  LOGFILE.Info("Finished Menu Update.");
}

void ProjectStormwind::RescaleHeightmap(ImageData* imageData)
{
  if (ADTOutputResolution == ADTRES_ORIGINAL) { return; }

  LOGFILE.Info("Starting Rescale of Image Data...");

  int w = imageData->getWidth();
  int h = imageData->getHeight();
  int resW = w;
  int resH = h;
  switch (ADTOutputResolution)
  {
  case ADTResolution::ADTRES_025:
    resW = w / 4;
    resH = h / 4;
    break;
  case ADTResolution::ADTRES_05:
    resW = w / 2;
    resH = h / 2;
    break;
  case ADTResolution::ADTRES_2X:
    resW = w * 2;
    resH = h * 2;
    break;
  case ADTResolution::ADTRES_4X:
    resW = w * 4;
    resH = h * 4;
    break;
  case ADTResolution::ADTRES_UE4_253:
    resW = resH = 253;
    break;
  case ADTResolution::ADTRES_UE4_505:
    resW = resH = 505;
    break;
  case ADTResolution::ADTRES_UE4_1009:
    resW = resH = 1009;
    break;
  case ADTResolution::ADTRES_UE4_2017:
    resW = resH = 2017;
    break;
  case ADTResolution::ADTRES_UE4_4033:
    resW = resH = 4033;
    break;
  case ADTResolution::ADTRES_UNITY_257:
    resW = resH = 257;
    break;
  case ADTResolution::ADTRES_UNITY_513:
    resW = resH = 513;
    break;
  case ADTResolution::ADTRES_UNITY_1025:
    resW = resH = 1025;
    break;
  case ADTResolution::ADTRES_UNITY_2049:
    resW = resH = 2049;
    break;
  case ADTResolution::ADTRES_UNITY_4097:
    resW = resH = 4097;
    break;
  case ADTResolution::ADTRES_CRYLUM_256:
    resW = resH = 256;
    break;
  case ADTResolution::ADTRES_CRYLUM_512:
    resW = resH = 512;
    break;
  case ADTResolution::ADTRES_CRYLUM_1024:
    resW = resH = 1024;
    break;
  case ADTResolution::ADTRES_CRYLUM_2048:
    resW = resH = 2048;
    break;
  case ADTResolution::ADTRES_CRYLUM_4096:
    resW = resH = 4096;
    break;
  case ADTResolution::ADTRES_ORIGINAL:
  case ADTResolution::ADTRES_MAX:
  default:
    resW = resH = 640;
    break;
  }

  if (w == resW && h == resH)
  {
    return;
  }
  imageData->resizeImage(resW, resH);
}

void ProjectStormwind::addToRecentFiles(QString filename)
{
  int maxRecent = 10;

  SETTINGS.beginGroup("RecentFiles");
  QStringList list = SETTINGS.allKeys();
  QStringList values;
  for (int i = 0; i < list.count(); i++)
  {
    values.push_back(SETTINGS.value(list.at(i)).toString());
  }
  while (values.contains(filename))
  {
    values.takeAt(values.lastIndexOf(filename));
  }

  values.prepend(filename);
  for (int i = 0; i < values.count() && i < maxRecent; i++)
  {
    SETTINGS.setValue(QString("recent%1").arg(i), values.at(i));
  }
  SETTINGS.endGroup();
  SETTINGS.sync();
  UpdateMenu();
}

void ProjectStormwind::removeFromRecentFiles(QString filename)
{
  int maxRecent = 10;

  LOGFILE.Debug("Removing %s from recent list...", qPrintable(filename));
  SETTINGS.beginGroup("RecentFiles");
  QStringList list = SETTINGS.allKeys();
  QStringList values;
  for (int i = 0; i < list.count(); i++)
  {
    values.push_back(SETTINGS.value(list.at(i)).toString());
  }
  while (values.contains(filename))
  {
    values.takeAt(values.lastIndexOf(filename));
  }
  LOGFILE.Debug("Initial List Count: %i | Updated List: %i", list.count(), values.count());

  SETTINGS.clear();

  for (int i = 0; i < values.count(); i++)
  {
    SETTINGS.setValue(QString("recent%1").arg(i), values.at(i));
  }
  SETTINGS.endGroup();
  SETTINGS.sync();
  UpdateMenu();
}

void ProjectStormwind::DisplayBLPImage()
{
  VkWindow->generateQuad(blp->getImageData(), blp->Header.ResX/blp->Header.ResY);
}

void ProjectStormwind::HideBLPImage()
{}

void ProjectStormwind::ExportBLP()
{
  ImageData *img = blp->getImageData();
  ImageDestinationType ImgType = ImageDestinationType::COLOR;
  if ((blp->hasAlpha() == true) || (img->hasAlphaData() == true))
  {
    ImgType = ImageDestinationType::COLORALPHA;
  }

  QString extList = PSWUtil::getImagePluginExtensions(ImgType);

  PluginExportImage *imgplug;
  PSWUtil::SaveImage(OpenFileName, extList, img, imgplug, ImgType, QString(), this);
}

void ProjectStormwind::ExportADTAll()
{
  ExportADTMesh();
  ExportADTTextures();
  ExportADTPlacement();
  ExportADTSubmeshes();
  ExportADTLiquids();
  ExportADTMasks();
  //ExportADTHeightmap();
  //ExportADTBakedTexture();
}

void ProjectStormwind::ExportADTMesh()
{
  if (adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Mesh Export.");
    return;
  }
  LOGFILE.Info("Outputting ADT as a mesh...");

  if (adt->isObjectBuilt() == false)
  {
    adt->buildModelObject();
  }

  ModelScene *scene = adt->getModelScene();

  OpenFileName = PSWUtil::getExportFilename(adt->getCoreName());

  QString extList = PSWUtil::getPluginExtensions<PluginExportModel>(KERNEL.getPluginExportModelList());

  bool r = PSWUtil::SaveModel(extList, scene, OpenFileName, this);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the ADT mesh."), QMessageBox::StandardButton::Ok);
    a.exec();
    QDir().rmpath(OpenFileName);
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting ADT Model."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportADTPlacement()
{
  if (adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Placement Export.");
    return;
  }
  LOGFILE.Info("Outputting ADT's Placement Info...");

  PlacementData *placements = adt->getPlacementData();

  if (placements->getNumObjects() <= 0 && placements->getNumCameras() <= 0 && placements->getNumLights() <= 0 && placements->getNumSounds() <= 0)
  {
    LOGFILE.Error("ADT has no sub-objects. Aborting Placement Export.");
    return;
  }
  LOGFILE.Debug("ADT has Placement Items...");

  OpenFileName = PSWUtil::getExportFilename(adt->getCoreName());
  LOGFILE.Debug("Gathered OpenFileName: %s", qPrintable(OpenFileName));

  QString extList = PSWUtil::getPluginExtensions<PluginExportPlacement>(KERNEL.getPluginExportPlacementList());
  LOGFILE.Debug("ExtList: %s", qPrintable(extList));

  SaveFileErrorType r = PSWUtil::SavePlacement(extList, placements, OpenFileName, this);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the ADT placement data."), QMessageBox::StandardButton::Ok);
    a.exec();
    QDir().rmpath(OpenFileName);
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting ADT Placement Data."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportADTHeightmap()
{
  if (adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Height Export.");
    return;
  }
  LOGFILE.Info("Outputting ADT as a Heightmap...");

  if (adt->isHeightmapBuilt() == false || ADTOutputResolution != adt->getHeightmapResolution())
  {
    adt->buildHeightMap(ADTOutputResolution, ADTUseMiddlePoint);
  }
  ImageData *img = adt->getHeightmap();

  // Re-Scale for output
  if (ADTOutputResolution != ADTRES_ORIGINAL) { RescaleHeightmap(img); }

  // Call global image saving plugin...
  QString extList = PSWUtil::getImagePluginExtensions(ImageDestinationType::LANDSCAPEHEIGHT);
  PluginExportImage *imgplug;
  SaveFileErrorType r = PSWUtil::SaveImage(OpenFileName, extList, img, imgplug, ImageDestinationType::LANDSCAPEHEIGHT, QString(), this);

  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the ADT heightmap."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting ADT heightmap."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportADTSubmeshes()
{
  if (adt == nullptr || adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Submesh Export.");
    return;
  }
}

void ProjectStormwind::ExportADTTextures()
{
  if (adt == nullptr || adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Texture Export.");
    return;
  }
  LOGFILE.Info("Outputting ADT's Textures...");

  QStringList textureList = adt->getTextureList();
  if (textureList.count() <= 0)
  {
    LOGFILE.Error("No textures found in the loaded ADT file. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("No textures found in the loaded ADT file. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QString rootPath = PSWUtil::getExportRootDirectory(OutputDir);
  LOGFILE.Debug("RootPath: %s", qPrintable(rootPath));
  if (rootPath.isNull() || rootPath.isEmpty())
  {
    LOGFILE.Warn("RootPath is empty. There was an error, it's an invalid path, or the user aborted. Cancelling Export...");
    return;
  }

  QStringList filesToExport;

  for (uint32_t i = 0; i < textureList.count(); i++)
  {
    QStringList filesToCheck = PSWUtil::getFilenameVariations(textureList.at(i));

    for (uint32_t t = 0; t < filesToCheck.count(); t++)
    {
      QFileInfo fi(filesToCheck.at(t));
      if (fi.exists() == true)
      {
        filesToExport.push_back(filesToCheck.at(t));
      }
    }
  }

  if (filesToExport.count() <= 0)
  {
    LOGFILE.Error("Could not find the files for any of the textures specified by the ADT. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("Could not find the files for any of the textures specified by the ADT. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  LOGFILE.Debug("%i texture files to export:", filesToExport.count());
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {
    LOGFILE.Debug("\t%s", qPrintable(filesToExport.at(i)));
  }

  // Get and Export the textures
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {

    QString texture = filesToExport.at(i);
    LOGFILE.Debug("Texture: %s", qPrintable(texture));
    texture.replace("\\", "/");
    texture.replace(".BLP", ".blp");
    QString textureFile = WoWDir.absoluteFilePath(texture);
    LOGFILE.Debug("Texture File: %s", qPrintable(textureFile));
    QString exportTextureName = PSWUtil::getExportFilename(textureFile, QDir(rootPath));
    LOGFILE.Debug("exportTextureName: %s", qPrintable(exportTextureName));

    BLPInfo blpData = *ReadBLPFile(textureFile);
    SaveFileErrorType error = PSWUtil::SaveTexture(PreferedExporter_Textures, blpData.getImageData(), exportTextureName);

    if (error != SaveFileErrorType::SIET_None)
    {
      QString err = PSWUtil::getSaveFileError(error);
      LOGFILE.Error(qPrintable(err));
      QMessageBox a(QMessageBox::Icon::Critical, tr("ADT Texture Export Error"), err, QMessageBox::StandardButton::Ok);
      a.exec();
    }
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting ADT textures."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportADTBakedTexture()
{
  if (adt == nullptr || adt->isLoaded() == false) {
    QString errorString = tr("No ADT file loaded. Aborting Baked Texture Export.");
    LOGFILE.Error(qPrintable(errorString));
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), qPrintable(errorString), QMessageBox::StandardButton::Ok);
    return;
  }
  LOGFILE.Info("Outputting ADT baked map texture...");

  QString imageName = PSWUtil::getExportFilename(QString("%1_mapTexture").arg(OpenFileName));

  QString extList = PSWUtil::getImagePluginExtensions(ImageDestinationType::COLORALPHA);

  SETTINGS.sync();
  QString selectedFilter = SETTINGS.value("preferedExporter/textures").toString();
  QString resultName;

  PSWUtil::getSaveFilename(imageName, resultName, extList, selectedFilter, this);

  QString rname = resultName.mid(0, resultName.lastIndexOf("."));
  QString ext = resultName.mid(resultName.lastIndexOf("."));
  LOGFILE.Debug("rname: %s, Ext: %s", qPrintable(rname), qPrintable(ext));

  PluginExportImage *SavingPlugin = 0;
  SaveFileErrorType pluginResult = PSWUtil::getImagePlugin(SavingPlugin, selectedFilter, ImageDestinationType::COLORALPHA);
  if (pluginResult != SaveFileErrorType::SIET_None)
  {
    QString errorString = tr("An Error occured while getting the image plugin: %s");
    LOGFILE.Error(qPrintable(errorString), qPrintable(PSWUtil::getSaveFileError(pluginResult)));
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), qPrintable(errorString.arg(PSWUtil::getSaveFileError(pluginResult))), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }
  if (SavingPlugin == nullptr)
  {
    QString errorString = tr("Unable to find selected plugin. Aborting Save...");
    LOGFILE.Error(qPrintable(errorString));
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), qPrintable(errorString), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  LOGFILE.Debug("Checking Map's Existance & Building it...");
  if (!adt->getMapTexture())
    adt->buildMapTileTexture();

  LOGFILE.Debug("Checking Map's Existance (Again)...");
  if (!adt->getMapTexture())
  {
    QString errorString = tr("Unable to build ADT Map Texture. Aborting Baked Texture Export.");
    LOGFILE.Error(qPrintable(errorString));
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), qPrintable(errorString), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }
  LOGFILE.Debug("Getting Map...");
  ImageData* mapTexture = adt->getMapTexture();
  LOGFILE.Debug("Saving Map...");
  SaveFileErrorType r = PSWUtil::SaveImage(SavingPlugin, resultName, mapTexture);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the ADT Map Texture."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting ADT Map Texture."), QMessageBox::StandardButton::Ok);
  a.exec();
}

// Export Texture Layer Masks, plus Hole visibility
void ProjectStormwind::ExportADTMasks()
{
  if (adt == nullptr || adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Mask Export.");
    return;
  }

  LOGFILE.Info("Outputting ADT's layer mask data...");

  QString imageName = PSWUtil::getExportFilename(QString("%1_masks").arg(OpenFileName));
  QList<ImageData> imgs = adt->getLayerMasks();

  QString extList = PSWUtil::getImagePluginExtensions(ImageDestinationType::MASKLAYER);

  SETTINGS.sync();
  QString selectedFilter = SETTINGS.value("preferedExporter/textures").toString();
  QString resultName;

  PSWUtil::getSaveFilename(imageName, resultName, extList, selectedFilter, this);

  QString rname = resultName.mid(0, resultName.lastIndexOf("."));
  QString ext = resultName.mid(resultName.lastIndexOf("."));
  LOGFILE.Debug("rname: %s, Ext: %s", qPrintable(rname), qPrintable(ext));

  PluginExportImage *SavingPlugin = 0;
  SaveFileErrorType pluginResult = PSWUtil::getImagePlugin(SavingPlugin, selectedFilter, ImageDestinationType::MASKLAYER);
  if (pluginResult != SaveFileErrorType::SIET_None)
  {
    LOGFILE.Error("An Error occured while getting the image plugin: %s", qPrintable(PSWUtil::getSaveFileError(pluginResult)));
    return;
  }
  if (SavingPlugin == nullptr)
  {
    LOGFILE.Error("Unable to find selected plugin. Aborting Save...");
    return;
  }

  int exported = 0;

  // Export RGB mask
  {
    ImageData *img = adt->getLayerMaskRGB();
    SaveFileErrorType r = PSWUtil::SaveImage(SavingPlugin, resultName, img);

    if (r != SaveFileErrorType::SIET_None)
    {
      QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the ADT Masks."), QMessageBox::StandardButton::Ok);
      a.exec();
      return;
    }
    exported++;
  }

  // Export the individual masks
  for (int i = 1; i < imgs.count(); i++)
  {
    ImageData img = imgs.at(i);

    // Scale for output
    // if (ADTOutputResolution != ADTRES_ORIGINAL) { RescaleHeightmap(img); }

    // Make Filename
    QString maskimageName = QString("%1_%2%3").arg(rname).arg(i).arg(ext);
    LOGFILE.Debug("Final mask name: %s", qPrintable(maskimageName));

    SaveFileErrorType filenameResult = PSWUtil::SaveImage(SavingPlugin, maskimageName, &img, ImageDestinationType::MASKLAYER);

    if (filenameResult != SaveFileErrorType::SIET_None)
    {
      QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), QString(tr("An error occured while exporting the ADT Masks: %s", "%s is the error string.")).arg(PSWUtil::getSaveFileError(filenameResult)), QMessageBox::StandardButton::Ok);
      a.exec();
      return;
    }
    exported++;
  }

  if (exported > 1)
  {
    QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting all ADT Masks."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }
  else if (exported > 0)
  {
    QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Combine masked exported. Individual masks not exported..."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }
  else {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("We were unable to export any ADT masks..."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }
}

void ProjectStormwind::ExportADTLiquids()
{
  if (adt->isLoaded() == false) {
    LOGFILE.Error("No ADT file loaded. Aborting Liquid Mesh Export.");
    return;
  }
  LOGFILE.Info("Outputting ADT's liquid as a mesh...");
}

void ProjectStormwind::ExportWDLAll()
{
  ExportWDLMesh();
  ExportWDLPlacement();
  ExportWDLHeightmap();
  ExportWDLSubmeshes();
}

void ProjectStormwind::ExportWDLMesh()
{

}

void ProjectStormwind::ExportWDLPlacement()
{
}

void ProjectStormwind::ExportWDLHeightmap()
{
  LOGFILE.Info("Outputting WDL as Heightmap data...");

  QString extList = PSWUtil::getImagePluginExtensions(ImageDestinationType::LANDSCAPEHEIGHT);

  // Gather WDL heightmap data as an image
  ImageData *img = new ImageData();

  // Original Resolution is 2050x2050. 17 points, plus 16 inbetween points, times 64 blocks, minus overlapping points. (64 blocks - 2 (a point on each end))
  // Each chunk (equal to each ADT file) is 17 + 16 points (33) so a chunk would be 33x33 points to equal an ADT file.


  // Scale for output
  if (ADTOutputResolution != ADTRES_ORIGINAL) { RescaleHeightmap(img); }


  // Call global image saving plugin...
  PluginExportImage *imgplug;
  SaveFileErrorType r = PSWUtil::SaveImage(OpenFileName, extList, img, imgplug, ImageDestinationType::LANDSCAPEHEIGHT, QString(), this);

  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the WDL heightmap."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting WDL heightmap."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportWDLSubmeshes()
{
}

void ProjectStormwind::ExportWMOAll()
{
  ExportWMOMesh();
  ExportWMOTextures();
  ExportWMOPlacement();
  ExportWMOSubmeshes();
}

void ProjectStormwind::ExportWMOMesh()
{
  if (wmo->isLoaded() == false) {
    LOGFILE.Error("No WMO file is loaded. Aborting Mesh Export.");
    return;
  }
  LOGFILE.Info("Outputting WMO as a mesh...");

  ModelScene *scene = wmo->getModelScene();
  if (scene->Objects.count() <= 0)
  {
    LOGFILE.Error("WMO scene data is empty. Aborting Mesh Export.");
    return;
  }

  OpenFileName = PSWUtil::getExportFilename(wmo->getRootFileName());
  LOGFILE.Info("OpenFileName: %s", qPrintable(OpenFileName));

  QString extList = PSWUtil::getPluginExtensions<PluginExportModel>(KERNEL.getPluginExportModelList());

  SaveFileErrorType r = PSWUtil::SaveModel(extList, scene, OpenFileName, this);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the WMO model."), QMessageBox::StandardButton::Ok);
    a.exec();
    QDir().rmpath(OpenFileName);
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting WMO Model."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportWMOPlacement()
{
  if (wmo->isLoaded() == false) {
    LOGFILE.Error("No WMO file is loaded. Aborting Placement Export.");
    return;
  }
  LOGFILE.Info("Outputting WMO Placement data...");

  PlacementData *placements = wmo->getPlacementData();

  if (placements->getNumObjects() <= 0 && placements->getNumCameras() <= 0 && placements->getNumLights() <= 0 && placements->getNumSounds() <= 0)
  {
    LOGFILE.Error("WMO has no sub-objects. Aborting Placement Export.");
    return;
  }
  LOGFILE.Debug("WMO has Placement Items...");

  OpenFileName = PSWUtil::getExportFilename(wmo->getRootFileName());
  LOGFILE.Debug("Gathered OpenFileName: %s", qPrintable(OpenFileName));

  QString extList = PSWUtil::getPluginExtensions<PluginExportPlacement>(KERNEL.getPluginExportPlacementList());
  LOGFILE.Debug("ExtList: %s", qPrintable(extList));

  SaveFileErrorType r = PSWUtil::SavePlacement(extList, placements, OpenFileName, this);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the WMO placement data."), QMessageBox::StandardButton::Ok);
    a.exec();
    QDir().rmpath(OpenFileName);
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting WMO Placement Data."), QMessageBox::StandardButton::Ok);
  a.exec();
}

// Exports all submeshes and textures for WMOs
void ProjectStormwind::ExportWMOSubmeshes()
{
  if (wmo == nullptr || wmo->isLoaded() == false) {
    LOGFILE.Error("No WMO file loaded. Aborting Submesh Export.");
    return;
  }
  LOGFILE.Info("Outputting WMO's Submeshes & Textures...");

  QString rootPath = PSWUtil::getExportRootDirectory(OutputDir);
  LOGFILE.Debug("RootPath: %s", qPrintable(rootPath));
  if (rootPath.isNull() || rootPath.isEmpty())
  {
    LOGFILE.Warn("RootPath is empty. There was an error, it's an invalid path, or the user aborted. Cancelling Export...");
    return;
  }

  QString pluginoptions;
  for (int i = 0; i < wmo->getDoodadList().count(); i++)
  {
    LOGFILE.Debug("Processing SubMesh: %s", qPrintable(wmo->getDoodadList().at(i)));
    QString doodadFile = WoWDir.absoluteFilePath(wmo->getDoodadList().at(i));
    doodadFile.replace("\\", "/");
    doodadFile.replace(".mdx", ".m2", Qt::CaseInsensitive);
    doodadFile.replace(".mdl", ".m2", Qt::CaseInsensitive);
    LOGFILE.Debug("doodadFile: %s", qPrintable(doodadFile));
    QString exportName = PSWUtil::getExportFilename(doodadFile, QDir(rootPath));
    LOGFILE.Debug("exportName: %s", qPrintable(exportName));

    // Get and Export the doodad's model
    M2 doodadModel = M2(doodadFile, currentWoW, true);
    PSWUtil::SaveMesh(PreferedExporter_Models, doodadModel.getModelScene(), exportName, pluginoptions);

    // Get and Export the doodad's textures
    for (int t = 0; t < doodadModel.getTextureList().count(); t++)
    {
      QString texture = doodadModel.getTextureList().at(t);
      LOGFILE.Debug("Texture: %s", qPrintable(texture));
      texture.replace("\\", "/");
      texture.replace(".BLP", ".blp");
      QString textureFile = WoWDir.absoluteFilePath(texture);
      LOGFILE.Debug("Texture File: %s", qPrintable(textureFile));
      QString exportTextureName = PSWUtil::getExportFilename(textureFile, QDir(rootPath));
      LOGFILE.Debug("exportTextureName: %s", qPrintable(exportTextureName));

      BLPInfo blpData = *ReadBLPFile(textureFile);
      PSWUtil::SaveTexture(PreferedExporter_Textures, blpData.getImageData(), exportTextureName);
    }
  }

  LOGFILE.Info("Submesh Exporting complete!");
  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Submesh Exporting is now complete!"), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportWMOTextures()
{
  if (wmo == nullptr || wmo->isLoaded() == false) {
    LOGFILE.Error("No WMO file loaded. Aborting Texture Export.");
    return;
  }
  LOGFILE.Info("Outputting WMO's Textures...");

  QStringList textureList = wmo->getTextureList();
  if (textureList.count() <= 0)
  {
    LOGFILE.Error("No textures found in the loaded WMO file. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("No textures found in the loaded WMO file. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QString rootPath = PSWUtil::getExportRootDirectory(OutputDir);
  LOGFILE.Debug("RootPath: %s", qPrintable(rootPath));
  if (rootPath.isNull() || rootPath.isEmpty())
  {
    LOGFILE.Warn("RootPath is empty. There was an error, it's an invalid path, or the user aborted. Cancelling Export...");
    return;
  }

  QStringList filesToExport;

  for (uint32_t i = 0; i < textureList.count(); i++)
  {
    QStringList filesToCheck = PSWUtil::getFilenameVariations(textureList.at(i));

    for (uint32_t t = 0; t < filesToCheck.count(); t++)
    {
      QFileInfo fi(filesToCheck.at(t));
      if (fi.exists() == true)
      {
        filesToExport.push_back(filesToCheck.at(t));
      }
    }
  }

  if (filesToExport.count() <= 0)
  {
    LOGFILE.Error("Could not find the files for any of the textures specified by the WMO. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("Could not find the files for any of the textures specified by the WMO. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  LOGFILE.Debug("%i texture files to export:", filesToExport.count());
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {
    LOGFILE.Debug("\t%s", qPrintable(filesToExport.at(i)));
  }

  // Get and Export the textures
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {

    QString texture = filesToExport.at(i);
    LOGFILE.Debug("Texture: %s", qPrintable(texture));
    texture.replace("\\", "/");
    texture.replace(".BLP", ".blp");
    QString textureFile = WoWDir.absoluteFilePath(texture);
    LOGFILE.Debug("Texture File: %s", qPrintable(textureFile));
    QString exportTextureName = PSWUtil::getExportFilename(textureFile, QDir(rootPath));
    LOGFILE.Debug("exportTextureName: %s", qPrintable(exportTextureName));

    BLPInfo blpData = *ReadBLPFile(textureFile);
    SaveFileErrorType error = PSWUtil::SaveTexture(PreferedExporter_Textures, blpData.getImageData(), exportTextureName);

    if (error != SaveFileErrorType::SIET_None)
    {
      QString err = PSWUtil::getSaveFileError(error);
      LOGFILE.Error(qPrintable(err));
      QMessageBox a(QMessageBox::Icon::Critical, tr("WMO Texture Export Error"), err, QMessageBox::StandardButton::Ok);
      a.exec();
    }
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting WMO textures."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportM2All()
{
  ExportM2Mesh();
  ExportM2Textures();
}

void ProjectStormwind::ExportM2Mesh()
{
  if (m2->isLoaded() == false) {
    LOGFILE.Error("No M2 file is loaded. Aborting Mesh Export.");
    return;
  }
  LOGFILE.Info("Outputting M2 as a mesh...");

  ModelScene *scene = m2->getModelScene();
  if (scene->Objects.count() <= 0)
  {
    LOGFILE.Error("M2 model data is empty. Aborting Mesh Export.");
    return;
  }

  OpenFileName = PSWUtil::getExportFilename(m2->getModelName());
  QString extList = PSWUtil::getPluginExtensions<PluginExportModel>(KERNEL.getPluginExportModelList());

  bool r = PSWUtil::SaveModel(extList, scene, OpenFileName, this);
  if (r != SaveFileErrorType::SIET_None)
  {
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("An error occured while exporting the M2 model."), QMessageBox::StandardButton::Ok);
    a.exec();
    QDir().rmpath(OpenFileName);
    return;
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting M2 Model."), QMessageBox::StandardButton::Ok);
  a.exec();
}

void ProjectStormwind::ExportM2Textures()
{
  if (m2 == nullptr || m2->isLoaded() == false) {
    LOGFILE.Error("No M2 file loaded. Aborting Texture Export.");
    return;
  }
  LOGFILE.Info("Outputting M2's Textures...");

  QStringList textureList = m2->getTextureList();
  if (textureList.count() <= 0)
  {
    LOGFILE.Error("No textures found in the loaded M2 file. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("No textures found in the loaded M2 file. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  QString rootPath = PSWUtil::getExportRootDirectory(OutputDir);
  LOGFILE.Debug("RootPath: %s", qPrintable(rootPath));
  if (rootPath.isNull() || rootPath.isEmpty())
  {
    LOGFILE.Warn("RootPath is empty. There was an error, it's an invalid path, or the user aborted. Cancelling Export...");
    return;
  }

  QStringList filesToExport;

  for (uint32_t i = 0; i < textureList.count(); i++)
  {
    QStringList filesToCheck = PSWUtil::getFilenameVariations(textureList.at(i));

    for (uint32_t t = 0; t < filesToCheck.count(); t++)
    {
      QFileInfo fi(filesToCheck.at(t));
      if (fi.exists() == true)
      {
        filesToExport.push_back(filesToCheck.at(t));
      }
    }
  }

  if (filesToExport.count() <= 0)
  {
    LOGFILE.Error("Could not find the files for any of the textures specified by the M2. Aborting Texture Export.");
    QMessageBox a(QMessageBox::Icon::Critical, tr("Export Error"), tr("Could not find the files for any of the textures specified by the M2. Aborting Texture Export."), QMessageBox::StandardButton::Ok);
    a.exec();
    return;
  }

  LOGFILE.Debug("%i texture files to export:", filesToExport.count());
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {
    LOGFILE.Debug("\t%s", qPrintable(filesToExport.at(i)));
  }

  // Get and Export the textures
  for (uint32_t i = 0; i < filesToExport.count(); i++)
  {

    QString texture = filesToExport.at(i);
    LOGFILE.Debug("Texture: %s", qPrintable(texture));
    texture.replace("\\", "/");
    texture.replace(".BLP", ".blp");
    QString textureFile = WoWDir.absoluteFilePath(texture);
    LOGFILE.Debug("Texture File: %s", qPrintable(textureFile));
    QString exportTextureName = PSWUtil::getExportFilename(textureFile, QDir(rootPath));
    LOGFILE.Debug("exportTextureName: %s", qPrintable(exportTextureName));

    BLPInfo blpData = *ReadBLPFile(textureFile);
    SaveFileErrorType error = PSWUtil::SaveTexture(PreferedExporter_Textures, blpData.getImageData(), exportTextureName);

    if (error != SaveFileErrorType::SIET_None)
    {
      QString err = PSWUtil::getSaveFileError(error);
      LOGFILE.Error(qPrintable(err));
      QMessageBox a(QMessageBox::Icon::Critical, tr("WMO Texture Export Error"), err, QMessageBox::StandardButton::Ok);
      a.exec();
    }
  }

  QMessageBox a(QMessageBox::Icon::Information, tr("Export Complete"), tr("Finished exporting M2 textures."), QMessageBox::StandardButton::Ok);
  a.exec();
}

int ProjectStormwind::UnrealEnginePrecision(double value)
{
  return QString::number((int)value).length() + 6;
}

int ProjectStormwind::UnrealEnginePrecision(float value)
{
  return UnrealEnginePrecision((double)value);
}

void ProjectStormwind::updateDisplayType(vulkanDisplayType type)
{
  VkWindow->changeDisplay(type);
}

void ProjectStormwind::updateDisplayColorMode(colorMode mode)
{
  VkWindow->changeColorMode(mode);
}

void ProjectStormwind::changeDisplay_ADTMasks()
{
  // Build Single image for ADT file, based on Mask data
}

void ProjectStormwind::openRecentFile()
{
  QAction *action = qobject_cast<QAction *>(sender());
  if (action)
  {
    LOGFILE.Debug("Clearing existing open files...");
    if (FileType != SOURCE_NONE)
    {
      adt = NULL;
      blp = NULL;
      wmo = NULL;
      m2 = NULL;
    }
    QString recentfile = action->data().toString();
    if (QFileInfo(recentfile).exists() == false)
    {
      LOGFILE.Error("Recent File Not Found.");
      QMessageBox::critical(this, tr("Recent File Not Found"), tr("Could not find the selected recent file.\nMaybe it was moved or deleted?"), QMessageBox::StandardButton::Close);
      removeFromRecentFiles(recentfile);
      return;
    }

    OpenFile(recentfile);
  }
}

void ProjectStormwind::on_actionOpen_triggered()
{
  LOGFILE.Info("Opening File...");
  if (LastDir.isEmpty() == true) LastDir = WoWDir;
  QString FileName = QFileDialog::getOpenFileName(this, tr("Open File"), LastDir.absoluteFilePath("./"), tr("All WoW Files (*.adt *.wmo *.m2 *.mdx *.blp *.wdl);;Images (*.blp);;Models (*.m2 *.mdx);;Terrain (*.adt *.wdl);;World Model Objects (*.wmo)", "All the file formats Project Stormwind can open. May change as support is added."));

  // Exit if...
  // The string is empty
  if (FileName.isEmpty() || FileName.isNull()) {
    LOGFILE.Error("No file selected.");
    return;
  }
  LastDir = FileName.mid(0, FileName.lastIndexOf(QDir::separator()));
  LOGFILE.Debug("Open File LastDir: %s", qPrintable(LastDir.absolutePath()));

  LOGFILE.Info("Selected Filename: %s", qPrintable(FileName));
  // No Extension Found
  if (FileName.lastIndexOf(".") < 0) {
    LOGFILE.Error("File has no extension. Unable to determine inital filetype.");
    return;
  }

  LOGFILE.Debug("Clearing existing open files...");
  if (FileType != SOURCE_NONE)
  {
    adt = NULL;
    blp = NULL;
    wmo = NULL;
    m2 = NULL;
  }

  OpenFile(FileName);
}

// Open the log file using the default viewer...
void ProjectStormwind::on_actionOpen_Log_File_triggered()
{
  QProcess *openProgram = new QProcess();
  QString file(QDir().absoluteFilePath(LOG_FILENAME));

  QMessageBox err;
  err.setIcon(QMessageBox::Critical);

  if (QFile(file).exists() == false) {
    LOGFILE.Error("Amazingly, this log file was not found!");		// Yes, I know logging this is redunant and possibly stupid, but you never know...
    //updateStatusBar(tr("Error: Log file not found. Please check for it's existance."));
    err.setWindowTitle(tr("Error: Log file not found!"));
    err.setText(tr("Could not find the %1 file.\n\nCheck your permissions, as you might be running Project Stormwind in a location that does not allow for the generation of files.").arg(LOG_FILENAME));
    err.exec();
    return;
  }

  QDesktopServices::openUrl(QUrl("file:///" + file));
  LOGFILE.Info("Opening Log file...");
}

void ProjectStormwind::on_actionPreferences_triggered()
{
  QDir lwd = WoWDir;
  options = new WindowOptionsDialog(this);
  options->exec();
  loadSettings();
  if (lwd != WoWDir)
  {
    showUnsupportedBuildWarning = true;
  }
  if (adt != NULL)
  {
    adt->setSingleMaterial(ADTBuildSingleMat);
  }
  VkWindow->setDefaultClearColor(BackgroundColor);
}

void ProjectStormwind::on_actionBatch_Export_triggered()
{
  batchExporterDlg = new BatchExporterDialog(this);
  if (batchExporterDlg->cancelOpening == false)
  {
    batchExporterDlg->exec();
    if (batchExporterDlg->startProcessing == true)
    {
      LOGFILE.Debug("Start Processing the Batch Exporter!");
      batchExportVariables = batchExporterDlg->getVariables();
      BatchExporter *batchExporter = new BatchExporter(batchExportVariables, currentWoW);

      batchExporter->StartProcess();
    }
  }
}

void ProjectStormwind::on_actionM2_Version_Scanner_triggered()
{
  LOGFILE.Info("Opening Directory...");
  if (LastDir == QString()) LastDir = WoWDir;
  QString FileName = QFileDialog::getExistingDirectory(this, tr("Open Directory"), LastDir.absolutePath());
  LastDir = FileName;

  LOGFILE.Debug("M2 Scanner LastDir: %s", qPrintable(LastDir.absolutePath()));

  if (LastDir.isEmpty() || !LastDir.exists()) {
    LOGFILE.Error("No directory selected.");
    return;
  }

  M2VersionScanner *m2vs = new M2VersionScanner;
  m2vs->Scan(LastDir);
}

void ProjectStormwind::on_actionADT_Height_Scanner_triggered()
{
  ADTFolderMin = 999999999999999999999999.0;
  ADTFolderMax = -999999999999999999999999.0;
  ADTScale = Vec3D(1.0, 1.0, 1.0);

  LOGFILE.Info("Opening Directory...");
  if (LastDir == QString()) LastDir = WoWDir;
  QString FileName = QFileDialog::getOpenFileName(this, tr("Open Directory"), LastDir.absolutePath(), tr("Terrain (*.adt)"));
  LastDir = FileName.left(FileName.lastIndexOf('/'));

  LOGFILE.Debug("ADT Scanner LastDir: %s", qPrintable(LastDir.absolutePath()));

  if (LastDir.isEmpty() || !LastDir.exists()) {
    LOGFILE.Error("No directory selected.");
    return;
  }

  QString fname = FileName.mid(FileName.lastIndexOf('/') + 1);
  //LOGFILE.Debug("Looking for files to filter: \"%s\"", qPrintable(fname));
  QRegularExpression adtcap("(.+)_\\d+_\\d+\\.adt", QRegularExpression::DotMatchesEverythingOption);
  //LOGFILE.Debug("ADTCap isValid: %s | Pattern: %s", (adtcap.isValid() == true ? "True" : "False"), qPrintable(adtcap.pattern()));
  QRegularExpressionMatch match = adtcap.match(fname);
  QString filter = "";
  if (match.hasMatch() == true)
  {
    LOGFILE.Debug("Match found in \"%s\"", qPrintable(match.captured(0)));
    filter = match.captured(1);
  }
  LOGFILE.Debug("Filter Setting: \"%s\"", qPrintable(filter));
  ADTHeightScanner *adths = new ADTHeightScanner();
  adths->Scan(LastDir, filter);
  ADTFolderMin = adths->min;
  ADTFolderMax = adths->max;
}

void ProjectStormwind::on_actionAbout_Plugins_triggered()
{
  aboutplugins = new WindowAboutPlugins(this);
  aboutplugins->show();
}