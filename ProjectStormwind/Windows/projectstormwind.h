#ifndef PROJECTSTORMWIND_H
#define PROJECTSTORMWIND_H

// Vulkan Includes
#include <Vulkan/vulkan.hpp>	// MUST be before any QVulkan includes!
#include <QtGui/qvulkaninstance.h>

// Project Includes
#include "Utilities/common.h"
#include "Vulkan/VulkanWidget.h"
#include "ui_MainWindow.h"
#include "Windows/BatchExporterDialog.h"

// Dockable Widgets
#include "widgetFileDetails.h"
#include "widgetInstances.h"

// Solution Includes
#include <Foundation/Classes.h>
#include <Foundation/GlobalEnums.h>

// Qt Includes
#include <QtWidgets/QMainWindow>
#include <QtCore/qdir.h>

extern ADTResolution ADTOutputResolution;

class ProjectStormwind : public QMainWindow
{
  Q_OBJECT

public:
  ProjectStormwind(QVulkanInstance *vulkan_instance, QWidget *parent = 0);
  ~ProjectStormwind();

private slots:
  // Main Menu
  void on_actionOpen_triggered();
  void on_actionOpen_Log_File_triggered();
  void on_actionADTResOriginal_triggered() { ADTOutputResolution = ADTRES_ORIGINAL; }
  void on_actionADTRes0_25x_triggered() { ADTOutputResolution = ADTRES_025; }
  void on_actionADTRes0_5x_triggered() { ADTOutputResolution = ADTRES_05; }
  void on_actionADTRes2x_triggered() { ADTOutputResolution = ADTRES_2X; }
  void on_actionADTRes4x_triggered() { ADTOutputResolution = ADTRES_4X; }
  void on_actionADTResUE4253x253_triggered() { ADTOutputResolution = ADTRES_UE4_253; }
  void on_actionADTResUE4505x505_triggered() { ADTOutputResolution = ADTRES_UE4_505; }
  void on_actionADTResUE41009x1009_triggered() { ADTOutputResolution = ADTRES_UE4_1009; }
  void on_actionADTResUE42017x2017_triggered() { ADTOutputResolution = ADTRES_UE4_2017; }
  void on_actionADTResUE44033x4033_triggered() { ADTOutputResolution = ADTRES_UE4_4033; }
  void on_actionADTResUnity257x257_triggered() { ADTOutputResolution = ADTRES_UNITY_257; }
  void on_actionADTResUnity513x513_triggered() { ADTOutputResolution = ADTRES_UNITY_513; }
  void on_actionADTResUnity1025x1025_triggered() { ADTOutputResolution = ADTRES_UNITY_1025; }
  void on_actionADTResUnity2049x2049_triggered() { ADTOutputResolution = ADTRES_UNITY_2049; }
  void on_actionADTResUnity4097x4097_triggered() { ADTOutputResolution = ADTRES_UNITY_4097; }
  void on_actionADTResCryLum256x256_triggered() { ADTOutputResolution = ADTRES_CRYLUM_256; }
  void on_actionADTResCryLum512x512_triggered() { ADTOutputResolution = ADTRES_CRYLUM_512; }
  void on_actionADTResCryLum1024x1024_triggered() { ADTOutputResolution = ADTRES_CRYLUM_1024; }
  void on_actionADTResCryLum2048x2048_triggered() { ADTOutputResolution = ADTRES_CRYLUM_2048; }
  void on_actionADTResCryLum4096x4096_triggered() { ADTOutputResolution = ADTRES_CRYLUM_4096; }
  void on_actionShowCurrentFile_triggered() { UpdateMenu(); }
  void on_actionShowViewControls_triggered() { UpdateMenu(); }
  void on_actionShowFileDetails_triggered() { UpdateMenu(); }
  void on_actionShowInstances_triggered() { UpdateMenu(); }

  void on_actionM2_Version_Scanner_triggered();
  void on_actionADT_Height_Scanner_triggered();
  void on_actionBatch_Export_triggered();
  void on_actionPreferences_triggered();
  void on_actionAbout_Plugins_triggered();

  // Viewport Buttons
  void changeDisplay_Solid() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_SOLID); }
  void changeDisplay_Textured() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_TEXTURED); }
  void changeDisplay_TexturedShaded() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_TEXTURESOLID); }
  void changeDisplay_Wireframe() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_WIREFRAME); }
  void changeDisplay_WireframeOnSolid() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_WIREFRAMEONSOLID); }
  void changeDisplay_WireframeOnTextured() { updateDisplayType(vulkanDisplayType::DISPLAYTYPE_WIREFRAMEONTEXTURE); }
  void changeDisplay_DisableImageControls();
  void changeDisplay_EnableImageControls();
  void changeDisplay_RGBA() { updateDisplayColorMode(colorMode::COLORMODE_FULL); }
  void changeDisplay_RGB() { updateDisplayColorMode(colorMode::COLORMODE_COLOR); }
  void changeDisplay_Alpha() { updateDisplayColorMode(colorMode::COLORMODE_ALPHA); }
  void changeDisplay_Heightmap() {}
  void changeDisplay_ADTMasks();

  void openRecentFile();

  // Misc Functions
  void toggleCulling() { VkWindow->toggleCulling(); }
  
private:
  QVulkanInstance *VulkanInst;

private:
  friend class MapBlock;

  // Dockable Widgets
  widgetFileDetails *fileDetailsWidget;
  widgetInstances *instancesWidget;

  SourceFileType FileType;
  QActionGroup *ADTResolutionGroup;
  QButtonGroup *viewStyleGroup;
  QButtonGroup *viewColorGroup;
  QString OpenFileName;
  bool disableRenderer;
  bool disableTextures;
  bool ADTUseMiddlePoint;
  bool ADTBuildSingleMat;
  bool MirrorWoWPaths;
  QString PreferedExporter_Models;
  QString PreferedExporter_Textures;
  QString PreferedExporter_Placement;
  RGBAColor *BackgroundColor = new RGBAColor(89, 105, 122);
  VulkanWindow* VkWindow;

  ExportVariables batchExportVariables;

  Ui::MainWindow ui;

  void OpenFile(QString FileName);
  bool CheckFile(QString Extension, QString FileName);
  void RescaleHeightmap(ImageData* imageData);
  void addToRecentFiles(QString filename);
  void removeFromRecentFiles(QString filename);

  QString SettingsFile;

  void loadSettings();
  void saveSettings();
  void ClearMenu();
  void UpdateMenu();
  void DisplayBLPImage();
  void HideBLPImage();
  void ExportBLP();
  void ExportADTAll();
  void ExportADTMesh();
  void ExportADTPlacement();
  void ExportADTHeightmap();
  void ExportADTSubmeshes();
  void ExportADTTextures();
  void ExportADTBakedTexture();
  void ExportADTMasks();
  void ExportADTLiquids();
  void ExportWDLAll();
  void ExportWDLMesh();
  void ExportWDLPlacement();
  void ExportWDLHeightmap();
  void ExportWDLSubmeshes();
  void ExportWMOAll();
  void ExportWMOMesh();
  void ExportWMOPlacement();
  void ExportWMOSubmeshes();
  void ExportWMOTextures();
  void ExportM2All();
  void ExportM2Mesh();
  void ExportM2Textures();

  int UnrealEnginePrecision(double value);
  int UnrealEnginePrecision(float value);
  void updateDisplayType(vulkanDisplayType type);
  void updateDisplayColorMode(colorMode mode);

  QDialog *options, *aboutplugins;
  BatchExporterDialog *batchExporterDlg;

  cWoWVersion currentWoW;
};

#endif // PROJECTSTORMWIND_H
