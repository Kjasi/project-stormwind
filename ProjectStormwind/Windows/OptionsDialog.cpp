#include "OptionsDialog.h"
#include <Foundation/Settings.h>
#include <Foundation/Core/Kernel.h>
#include <Foundation/LogFile.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/Plugins/PluginExportPlacement.h>
#include <QtWidgets/qfiledialog.h>

using namespace ProjectSW;

WindowOptionsDialog::WindowOptionsDialog(QWidget * parent) :QDialog(parent)
{
  LOGFILE.Info("Preparing Options Dialog...");
  setupUi(this);

  tempRootDir = SETTINGS.value("wowdir", QApplication::applicationDirPath()).toString();
  tempOutDir = SETTINGS.value("outputdir", QApplication::applicationDirPath()).toString();
  tempMirrorWoWPaths = SETTINGS.value("mirrorwowpaths", true).toBool();
  tempDisableRenderer = SETTINGS.value("disablerenderer", false).toBool();
  tempDisableRenderTextures = SETTINGS.value("disablerendertextures", false).toBool();
  tempCombineMeshes = SETTINGS.value("combinemeshes", false).toBool();
#ifdef _DEBUG
  tempShowDebug = SETTINGS.value("logging/showdebug", true).toBool();
#else
  tempShowDebug = SETTINGS.value("logging/showdebug", false).toBool();
#endif // _DEBUG
  tempUseTimestamp = SETTINGS.value("logging/usetimestamp", true).toBool();
  tempUseADTMiddle = SETTINGS.value("adt/usemiddlepoint", true).toBool();
  tempADTBuildSingleMat = SETTINGS.value("adt/buildsinglematerial", false).toBool();
  tempPreferedModels = SETTINGS.value("preferedExporter/models", QString()).toString();
  tempPreferedTextures = SETTINGS.value("preferedExporter/textures", QString()).toString();
  tempPreferedPlacement = SETTINGS.value("preferedExporter/placement", QString()).toString();
  //LOGFILE.Debug("Loading Prefered Model Exporter: %s", qPrintable(tempPreferedModels));
  //LOGFILE.Debug("Loading Prefered Texture Exporter: %s", qPrintable(tempPreferedTextures));
  //LOGFILE.Debug("Loading Prefered Placement Exporter: %s", qPrintable(tempPreferedPlacement));

  LOGFILE.Debug("Settings Loaded...");

  // Fill out the combo-boxes
  exporterModels.clear();
  exporterTextures.clear();
  exporterPlacement.clear();

  for (int p = 0; p < KERNEL.nbChildren(); p++)
  {
    Plugin *plug = KERNEL.getChild(p);
    QString plugName(plug->name().c_str());

    QString exts;
    exts.append(QString("%1 (").arg(plug->getPluginFilterName()));
    for (size_t j = 0; j < plug->getFileExtensions().count(); j++)
    {
      if (j > 0) {
        exts.append(" ");
      }
      exts.append(QString("*.%1").arg(plug->getFileExtensions().at(j)));
    }
    exts.append(")");


    if (dynamic_cast<PluginExportImage *>(plug)) {
      exporterTextures.push_back(exts);
    }
    else if (dynamic_cast<PluginExportModel *>(plug)) {
      exporterModels.push_back(exts);
    }
    else if (dynamic_cast<PluginExportPlacement *>(plug)) {
      exporterPlacement.push_back(exts);
    }
  }

  int modelIndex = getIndexFromStringList(exporterModels, tempPreferedModels);
  int textureIndex = getIndexFromStringList(exporterTextures, tempPreferedTextures);
  int placementIndex = getIndexFromStringList(exporterPlacement, tempPreferedPlacement);

  comboBoxPreferedExporterModels->clear();
  comboBoxPreferedExporterTextures->clear();
  comboBoxPreferedExporterPlacement->clear();

  for (int i = 0; i < exporterModels.count(); i++)
  {
    comboBoxPreferedExporterModels->addItem(exporterModels.at(i));
  }
  for (int i = 0; i < exporterTextures.count(); i++)
  {
    comboBoxPreferedExporterTextures->addItem(exporterTextures.at(i));
  }
  for (int i = 0; i < exporterPlacement.count(); i++)
  {
    comboBoxPreferedExporterPlacement->addItem(exporterPlacement.at(i));
  }

  //LOGFILE.Debug("Model Index: %i", modelIndex);
  //LOGFILE.Debug("Texture Index: %i", textureIndex);
  //LOGFILE.Debug("Placement Index: %i", placementIndex);

  RootDirPath->setText(tempRootDir);
  OutputDirPath->setText(tempOutDir);
  checkBox_DisableRenderer->setChecked(tempDisableRenderer);
  checkBox_DisableTextureLoading->setChecked(tempDisableRenderTextures);
  checkBox_MirrorWoWPaths->setChecked(tempMirrorWoWPaths);
  checkBox_CombineMeshes->setChecked(tempCombineMeshes);
  checkBox_Debug->setChecked(tempShowDebug);
  checkBox_Timestamp->setChecked(tempUseTimestamp);
  checkBox_ADTUseMiddlePoint->setChecked(tempUseADTMiddle);
  checkBox_ADTBuildSingleMaterial->setChecked(tempADTBuildSingleMat);
  
  comboBoxPreferedExporterModels->setCurrentIndex(modelIndex);
  comboBoxPreferedExporterTextures->setCurrentIndex(textureIndex);
  comboBoxPreferedExporterPlacement->setCurrentIndex(placementIndex);

  //LOGFILE.Debug("End Combo-Box Model Text: %s, Index: %i", qPrintable(comboBoxPreferedExporterModels->currentText()), comboBoxPreferedExporterModels->currentIndex());
  //LOGFILE.Debug("End Combo-Box Texture Text: %s, Index: %i", qPrintable(comboBoxPreferedExporterTextures->currentText()), comboBoxPreferedExporterTextures->currentIndex());
  //LOGFILE.Debug("End Combo-Box Placement Text: %s, Index: %i", qPrintable(comboBoxPreferedExporterPlacement->currentText()), comboBoxPreferedExporterPlacement->currentIndex());

  LOGFILE.Info("Finished Preparing Options Dialog.");
}

void WindowOptionsDialog::on_checkBox_Debug_clicked(bool value)
{
  tempShowDebug = value;
}

void WindowOptionsDialog::on_checkBox_DisableRenderer_clicked(bool value)
{
  tempDisableRenderer = value;
}

void WindowOptionsDialog::on_checkBox_DisableTextureLoading_clicked(bool value)
{
  tempDisableRenderTextures = value;
}

void WindowOptionsDialog::on_checkBox_Timestamp_clicked(bool value)
{
  tempUseTimestamp = value;
}

void WindowOptionsDialog::on_checkBox_ADTUseMiddlePoint_clicked(bool value)
{
  tempUseADTMiddle = value;
}

void WindowOptionsDialog::on_checkBox_ADTBuildSingleMaterial_clicked(bool value)
{
  tempADTBuildSingleMat = value;
}

void WindowOptionsDialog::on_checkBox_MirrorWoWPaths_clicked(bool value)
{
  tempMirrorWoWPaths = value;
}

void WindowOptionsDialog::on_checkBox_CombineMeshes_clicked(bool value)
{
  tempCombineMeshes = value;
}

void WindowOptionsDialog::on_btn_BrowseRootDir_clicked()
{
  LOGFILE.Info("Browsing for Root WoW Directory...");

  QString dir = QFileDialog::getExistingDirectory(this, tr("Select WoW Directory"), tempRootDir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
  if (dir.isNull() || dir.isEmpty())
  {
    LOGFILE.Warn("Unable to find directory...");
    return;
  }
  tempRootDir = dir;
  RootDirPath->setText(dir);
}

void WindowOptionsDialog::on_btn_BrowseOutputDir_clicked()
{
  LOGFILE.Info("Browsing for Output Directory...");

  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Output Directory"), tempRootDir, QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
  if (dir.isNull() || dir.isEmpty())
  {
    LOGFILE.Warn("Unable to find directory...");
    return;
  }
  tempOutDir = dir;
  OutputDirPath->setText(dir);
}

void WindowOptionsDialog::on_okButton_pressed()
{
  LOGFILE.Info("Saving variable changes...");
  tempRootDir = RootDirPath->text();
  tempOutDir = OutputDirPath->text();
  SETTINGS.setValue("wowdir", tempRootDir);
  SETTINGS.setValue("outputdir", tempOutDir);

  LOGFILE.Debug("Set logfile's Show Debug value to %s", (tempShowDebug == true ? "true" : "false"));
  LOGFILE.setShowDebug(tempShowDebug);
  LOGFILE.Debug("Set logfile's Use Timestamp value to %s", (tempUseTimestamp == true ? "true" : "false"));
  LOGFILE.setUseTimestamp(tempUseTimestamp);
  LOGFILE.Debug("Updating saved variables...");
  SETTINGS.setValue("mirrorwowpaths", tempMirrorWoWPaths);
  SETTINGS.setValue("disablerenderer", tempDisableRenderer);
  SETTINGS.setValue("disablerendertextures", tempDisableRenderTextures);
  SETTINGS.setValue("combinemeshes", tempCombineMeshes);
  SETTINGS.setValue("logging/showdebug", tempShowDebug);
  SETTINGS.setValue("logging/usetimestamp", tempUseTimestamp);
  SETTINGS.setValue("adt/usemiddlepoint", tempUseADTMiddle);
  SETTINGS.setValue("adt/buildsinglematerial", tempADTBuildSingleMat);
  SETTINGS.setValue("preferedExporter/models", tempPreferedModels);
  SETTINGS.setValue("preferedExporter/textures", tempPreferedTextures);
  SETTINGS.setValue("preferedExporter/placement", tempPreferedPlacement);
  //LOGFILE.Debug("Saving Prefered Model Exporter: %s", qPrintable(tempPreferedModels));
  //LOGFILE.Debug("Saving Prefered Texture Exporter: %s", qPrintable(tempPreferedTextures));
  //LOGFILE.Debug("Saving Prefered Placement Exporter: %s", qPrintable(tempPreferedPlacement));
  SETTINGS.sync();
  LOGFILE.Info("Variables saved!");
}

void WindowOptionsDialog::on_cancelButton_pressed()
{
  LOGFILE.Info("Canceling variable changes.");
}

void WindowOptionsDialog::on_comboBoxPreferedExporterModels_currentTextChanged(QString value)
{
  tempPreferedModels = value;
}

void WindowOptionsDialog::on_comboBoxPreferedExporterTextures_currentTextChanged(QString value)
{
  tempPreferedTextures = value;
}

void WindowOptionsDialog::on_comboBoxPreferedExporterPlacement_currentTextChanged(QString value)
{
  tempPreferedPlacement = value;
}

// Get the Index of a string from a QStringList. -1 means not found.
int WindowOptionsDialog::getIndexFromStringList(QStringList list, QString value)
{
  if (value.isEmpty()) return -1;

  for (int i = 0; i < list.count(); i++)
  {
    //LOGFILE.Debug("List Item: \"%s\" VS \"%s\"", qPrintable(list.at(i)), qPrintable(value));
    if (list.at(i) == value)
    {
      //LOGFILE.Debug("Match Found at %i!", i);
      return i;
    }
  }

  return -1;
}
