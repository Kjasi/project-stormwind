#include "BatchExporterQuickSelections.h"
#include "Utilities/common.h"
#include "BatchExporterDialog.h"

WindowBatchExporterQuickSelectionDialog::WindowBatchExporterQuickSelectionDialog(BatchExporterDialog *parent): parentWindow(parent), ui(new Ui::QuickSelectionsDialog)
{
  LOGFILE.Info("Preparing Batch Exporter Quick Selections Dialog...");
  ui->setupUi(this);

  updateUI();
}

// Save settings, but don't start.
void WindowBatchExporterQuickSelectionDialog::on_Button_Close_pressed()
{
  LOGFILE.Debug("Close button pressed");
  saveSettings();
  parentWindow->updateUI();
}


void WindowBatchExporterQuickSelectionDialog::updateUI()
{
  
}

void WindowBatchExporterQuickSelectionDialog::loadSettings()
{
  exportVariables.setVariables(SETTINGS.value("BatchExporterQuickSelections", ExportVariables().getVariables()).toString());
}

void WindowBatchExporterQuickSelectionDialog::saveSettings()
{
  SETTINGS.setValue("BatchExporterQuickSelections", exportVariables.getVariables());
  SETTINGS.sync();
}