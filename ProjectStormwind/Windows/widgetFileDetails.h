#pragma once
#include <Foundation/GlobalEnums.h>
#include <Foundation/Classes.h>
#include "ui_widgetFileDetails.h"

class widgetFileDetails : public QWidget
{
  Q_OBJECT
public:
  widgetFileDetails(QWidget *parent = 0);
  ~widgetFileDetails() {};

  void retranslate() { ui.retranslateUi(this); };

  void updateDisplay();

  void setFileInfo(FileInfo file_info);

private:
  Ui::widgetDetails ui;
  FileInfo fileInfo;
};