#pragma once
#include <QtWidgets/qdialog.h>
#include <Foundation/Classes/ExportOptions.h>
#include "BatchExporterQuickSelections.h"
#include "ui_BatchExportDialog.h"

class BatchExporterDialog : public QDialog
{
  Q_OBJECT

public:
  BatchExporterDialog(QWidget *parent = 0);
  bool cancelOpening = false;
  bool startProcessing = false;

  ExportVariables getVariables() { return exportVariables; }

  void updateUI();

private slots:
  void on_Button_Start_pressed();
  void on_Button_Close_pressed();
  void on_Button_Cancel_pressed();
  void on_button_BrowseWoWRootDir_pressed();
  void on_button_BrowseInputDir_pressed();
  void on_button_BrowseOutputDir_pressed();
  void on_button_ShowQuickSelections_pressed();
  void on_checkBox_OverwriteExistingFiles_clicked(bool value);
  void on_checkBox_ExportAllModels_clicked(bool value);
  void on_checkBox_ExportAllWorldModels_clicked(bool value);
  void on_checkBox_ExportAllTerrain_clicked(bool value);
  void on_checkBox_ExportAllTextures_clicked(bool value);
  void on_checkBox_ContinueOnError_clicked(bool value);
  void on_checkBox_CombineMeshes_clicked(bool value);
  void on_checkBox_MirrorWoWDirs_clicked(bool value);
  void on_checkBox_IncludeSubdirectories_clicked(bool value);
  void on_checkBox_ExportM2Mesh_clicked(bool value);
  void on_checkBox_ExportM2AllLODs_clicked(bool value);
  void on_checkBox_ExportM2MaxLOD_clicked(bool value);
  void on_checkBox_ExportM2Textures_clicked(bool value);
  void on_checkBox_ExportWMOMesh_clicked(bool value);
  void on_checkBox_ExportWMOTextures_clicked(bool value);
  void on_checkBox_ExportWMOSubmodels_clicked(bool value);
  void on_checkBox_ExportWMOPlacement_clicked(bool value);
  void on_checkBox_ExportADTMesh_clicked(bool value);
  void on_checkBox_ExportADTasHeightmap_clicked(bool value);
  void on_checkBox_ExportADTTextures_clicked(bool value);
  void on_checkBox_ExportADTSubmodels_clicked(bool value);
  void on_checkBox_ExportADTPlacement_clicked(bool value);
  void on_checkBox_ExportADTMasks_clicked(bool value);
  void on_checkBox_ADTUseMiddlePoint_clicked(bool value);
  void on_checkBox_ADTBuildSingleMaterial_clicked(bool value);
  void on_checkBox_ADTBakeTexture_clicked(bool value);
  void on_comboBox_ImageFormat_currentIndexChanged(int value);
  void on_comboBox_ModelFormat_currentIndexChanged(int value);
  void on_comboBox_PlacementFormat_currentIndexChanged(int value);
  void on_comboBox_HeightmapFormat_currentIndexChanged(int value);
  void on_comboBox_ADTHeightmapResolution_currentIndexChanged(int value);
  void on_lineEdit_WoWRootDir_textEdited(QString value);
  void on_lineEdit_InputDir_textEdited(QString value);
  void on_lineEdit_OutputDir_textEdited(QString value);
  

private:
  Ui::DialogBatchExporter *ui;
  WindowBatchExporterQuickSelectionDialog *windowQuickSelections;
  bool supportedBuild = false;

  ExportVariables exportVariables;

  void loadSettings();
  void saveSettings();
  void checkWoWVersion();

  // Find WoW, relative to the specified path
  void findWoWApp(QString dir) { findWoWApp(QDir(dir)); };
  void findWoWApp(QDir dir);
};
