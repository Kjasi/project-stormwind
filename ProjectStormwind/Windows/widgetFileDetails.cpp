#include "widgetFileDetails.h"

widgetFileDetails::widgetFileDetails(QWidget *parent)
  : QWidget(parent)
{
  qInfo("Initializing File Details widget...");
  ui.setupUi(this);
}

void widgetFileDetails::updateDisplay()
{
  ui.groupBoxWMOList->hide();
  ui.groupBoxModelList->hide();
  ui.groupBoxTextureList->hide();
  ui.groupBoxTextureInfo->hide();

  switch (fileInfo.fileType)
  {
  case SourceFileType::SOURCE_ADT:
    ui.FileTypeText->setText(tr("ADT Terrain File"));
    break;
  case SourceFileType::SOURCE_M2:
    ui.FileTypeText->setText(tr("M2 Model File"));
    break;
  case SourceFileType::SOURCE_WMO:
    ui.FileTypeText->setText(tr("World Model File"));
    break;
  case SourceFileType::SOURCE_BLP:
    ui.FileTypeText->setText(tr("BLP Image File"));
    break;
  case SourceFileType::SOURCE_WDL:
    ui.FileTypeText->setText(tr("WDL Terrain File"));
    break;
  case SourceFileType::SOURCE_NONE:
    ui.FileTypeText->setText(tr("No File"));
    break;
  default:
    ui.FileTypeText->setText(tr("Unknown file type"));
    break;
  }

  if (fileInfo.fileType == SOURCE_BLP)
  {
    ui.groupBoxTextureInfo->show();

    // Populate Texture Info
    ui.textureInfoHeight->setValue(fileInfo.TextureHeight);
    ui.textureInfoWidth->setValue(fileInfo.TextureWidth);
    ui.textureInfoChannels->setValue(fileInfo.TextureChannelCount);
    ui.labelTextureHasAlpha->setText(fileInfo.TextureHasAlpha ? "True" : "False");
  }
  else
  {
    if (fileInfo.fileType == SOURCE_M2 || fileInfo.fileType == SOURCE_WMO || fileInfo.fileType == SOURCE_ADT)
    {
      ui.groupBoxTextureList->show();

      // Populate Texture List
      ui.tableTextures->clear();

      ui.labelCountTextures->setText(QString::number(fileInfo.TextureList.count()));
      for (size_t i = 0; i < fileInfo.TextureList.count(); i++)
      {
        ui.tableTextures->addItem(fileInfo.TextureList.at(i));
      }
    }
    if (fileInfo.fileType == SOURCE_WMO || fileInfo.fileType == SOURCE_ADT || fileInfo.fileType == SOURCE_WDL)
    {
      ui.groupBoxModelList->show();

      // Populate Model List
      ui.tableDoodadModels->clear();

      ui.labelCountDoodads->setText(QString::number(fileInfo.ModelFileList.count()));
      for (size_t i = 0; i < fileInfo.ModelFileList.count(); i++)
      {
        ui.tableDoodadModels->addItem(fileInfo.ModelFileList.at(i));
      }
    }
    if (fileInfo.fileType == SOURCE_ADT || fileInfo.fileType == SOURCE_WDL)
    {
      ui.groupBoxWMOList->show();

      // Populate WMO List
      ui.tableWorldModels->clear();

      ui.labelCountWMOs->setText(QString::number(fileInfo.WMOFileList.count()));
      for (size_t i = 0; i < fileInfo.WMOFileList.count(); i++)
      {
        ui.tableWorldModels->addItem(fileInfo.WMOFileList.at(i));
      }
    }
  }
}

void widgetFileDetails::setFileInfo(FileInfo file_info)
{
  fileInfo = file_info;
  updateDisplay();
}