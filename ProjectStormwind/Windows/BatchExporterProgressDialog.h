#pragma once
#include <QtWidgets/qdialog.h>
#include "ui_BatchExportProgressWindow.h"

class BatchExporterProgressDialog : public QDialog
{
  Q_OBJECT

public:
  BatchExporterProgressDialog(QWidget *parent = 0);

public slots:
  void setProgressValue(int value);
  void setProgressMax(int value);
  void setCurrentFilename(QString filename);
  void updateData(QString filename, int value);

signals:
  void abortExport();

private:
  Ui::BatchExportProgressWindow *ui;

  QString currentFilenameThread0;
  QString currentFilenameThread1;
  QString currentFilenameThread2;
  QString currentFilenameThread3;
  QString currentFilenameThread4;
  QString currentFilenameThread5;
  QString currentFilenameThread6;
  QString currentFilenameThread7;
  int currentFiles = 0;
  int maxFiles = 0;

  void updateUI();

  void on_btn_AbortExport_pressed();
};