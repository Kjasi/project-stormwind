#include "BatchExporterProgressDialog.h"
#include <Foundation/LogFile.h>

BatchExporterProgressDialog::BatchExporterProgressDialog(QWidget *parent)
  : ui(new Ui::BatchExportProgressWindow)
{
  ui->setupUi(this);

  ui->progressBar->setValue(0);
}

void BatchExporterProgressDialog::setProgressValue(int value)
{
  currentFiles = value;
  updateUI();
}

void BatchExporterProgressDialog::setProgressMax(int value)
{
  maxFiles = value;
  ui->progressBar->setMaximum(maxFiles);
  updateUI();
}

void BatchExporterProgressDialog::setCurrentFilename(QString filename)
{
  currentFilenameThread0 = filename;
  updateUI();
}

void BatchExporterProgressDialog::updateData(QString filename, int value)
{
  currentFilenameThread0 = filename;
  currentFiles = value;
  updateUI();
}

void BatchExporterProgressDialog::updateUI()
{
  ui->progressBar->setValue(currentFiles);
  ui->currentFileNameThread0->setText(tr("File: %1").arg(currentFilenameThread0));
  ui->currentFileNameThread1->setText(tr("File: %1").arg(currentFilenameThread1));
  ui->currentFileNameThread2->setText(tr("File: %1").arg(currentFilenameThread2));
  ui->currentFileNameThread3->setText(tr("File: %1").arg(currentFilenameThread3));
  ui->currentFileNameThread4->setText(tr("File: %1").arg(currentFilenameThread4));
  ui->currentFileNameThread5->setText(tr("File: %1").arg(currentFilenameThread5));
  ui->currentFileNameThread6->setText(tr("File: %1").arg(currentFilenameThread6));
  ui->currentFileNameThread7->setText(tr("File: %1").arg(currentFilenameThread7));
  ui->currentFileNameThread1->hide();
  ui->currentFileNameThread2->hide();
  ui->currentFileNameThread3->hide();
  ui->currentFileNameThread4->hide();
  ui->currentFileNameThread5->hide();
  ui->currentFileNameThread6->hide();
  ui->currentFileNameThread7->hide();

  if (currentFilenameThread1.isEmpty() == false)
    ui->currentFileNameThread1->show();
  if (currentFilenameThread2.isEmpty() == false)
    ui->currentFileNameThread2->show();
  if (currentFilenameThread3.isEmpty() == false)
    ui->currentFileNameThread3->show();
  if (currentFilenameThread4.isEmpty() == false)
    ui->currentFileNameThread4->show();
  if (currentFilenameThread5.isEmpty() == false)
    ui->currentFileNameThread5->show();
  if (currentFilenameThread6.isEmpty() == false)
    ui->currentFileNameThread6->show();
  if (currentFilenameThread7.isEmpty() == false)
    ui->currentFileNameThread7->show();

  ui->fileCounter->setText(QString("%1/%2").arg(currentFiles).arg(tr("%n File(s)", "The Maximum number of files to be processed.", maxFiles)));
}

void BatchExporterProgressDialog::on_btn_AbortExport_pressed()
{
  LOGFILE.Debug("Abort Button Pressed. Sending Batch Abort signal...");
  emit(abortExport());
  QApplication::processEvents();
}
