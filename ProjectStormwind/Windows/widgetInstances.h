#pragma once
#include "ui_widgetInstances.h"

class widgetInstances : public QWidget
{
  Q_OBJECT
public:
  widgetInstances(QWidget *parent = 0);
  ~widgetInstances() {};

  void retranslate() { ui.retranslateUi(this); };

private:
  Ui::widgetInstances ui;
};