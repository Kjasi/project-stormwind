#pragma once
#include <QtWidgets/qdialog.h>
#include "ui_BatchExportQuickSelections.h"
#include <Foundation/Classes/ExportOptions.h>

class BatchExporterDialog;

class WindowBatchExporterQuickSelectionDialog : public QDialog
{
  Q_OBJECT

public:
  WindowBatchExporterQuickSelectionDialog(BatchExporterDialog *parent = 0);
  bool cancelOpening = false;
  bool startProcessing = false;

  ExportVariables getVariables() { return exportVariables; }

private slots:
  void on_Button_Close_pressed();

private:
  BatchExporterDialog *parentWindow;
  Ui::QuickSelectionsDialog *ui;
  bool supportedBuild = false;

  ExportVariables exportVariables;

  void updateUI();
  void loadSettings();
  void saveSettings();
};
