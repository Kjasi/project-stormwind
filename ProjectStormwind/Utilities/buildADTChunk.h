#pragma once
#include <qobject.h>
#include <qrunnable.h>
#include <Foundation/Classes/ImageData.h>
#include <WoWFormats/Landscape/MapChunk.h>

class MapChunk;

Q_DECLARE_METATYPE(ImageData);

class buildMapImageChunk : public QObject, public QRunnable
{
  Q_OBJECT
public:
  buildMapImageChunk(MapChunk chunk, QList<ImageData*> textureList, uint32_t blockSizePixels = 256);
  ~buildMapImageChunk() {
    delete p_chunk;
  };

  void setData(MapChunk *chunk, QList<ImageData*> textureList, uint32_t blockSizePixels = 256);

  void run() override;

signals:
  void chunkComplete(ImageData results, unsigned int offsetX, unsigned int offsetY);

private:
  MapChunk *p_chunk = 0;
  QList<ImageData*> p_textureList;
  uint32_t p_blockSizePixels;
  uint32_t p_blockX;
  uint32_t p_blockY;
};

class constructMapImage : public QObject
{
  Q_OBJECT
public:
  constructMapImage(uint32_t imageSize);
  ~constructMapImage() {
    delete MasterImage;
  }

  ImageData* getImage() { return MasterImage; }

public slots:
  void chunkComplete(ImageData img, unsigned int offsetX, unsigned int offsetY);

private:
  ImageData* MasterImage;
};