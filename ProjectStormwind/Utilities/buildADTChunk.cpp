#include "buildADTChunk.h"
#include <Foundation/LogFile.h>
#include <WoWFormats/Landscape/MapChunk.h>

buildMapImageChunk::buildMapImageChunk(MapChunk chunk, QList<ImageData*> textureList, uint32_t blockSizePixels)
{
  setData(&chunk, textureList, blockSizePixels);
}

void buildMapImageChunk::setData(MapChunk *chunk, QList<ImageData*> textureList, uint32_t blockSizePixels)
{
  p_chunk = chunk;
  p_textureList = textureList;
  p_blockSizePixels = blockSizePixels;
}

void buildMapImageChunk::run()
{
  return;
  if (!p_chunk || p_chunk == 0)
  {
    LOGFILE.Error("Unable to get chunk data! Cancelling chunk build...");
    return;
  }
  if (p_textureList.count() <= 0)
  {
    LOGFILE.Error("No textures found! Cancelling chunk build...");
    return;
  }
  QList<ImageData> *maskLayers = p_chunk->getMaskLayers();
  if (maskLayers->count() <= 0)
  {
    LOGFILE.Error("No Mask Layers found! Cancelling chunk build...");
    return;
  }
  ImageData chunkImage = ImageData::generateBlackTexture(p_blockSizePixels, p_blockSizePixels);
  QList<MCLY_Data> *layers = p_chunk->MCLYChunk->getLayers();

  //LOGFILE.Debug("Block %i, Layers Count: %i, ", ((p_chunk->Header->IndexY * 16) + p_chunk->Header->IndexX), layers->count());

  // assume all source images are 256x256, and need to tile 4 times!
  int sourceSize = 256;
  double scale = p_blockSizePixels / sourceSize;
  double maskScale = 64.0 / sourceSize;		// Mask layers are 64 pixels wide per chunk

  for (uint32_t pixelY = 0; pixelY < sourceSize; pixelY++)
  {
    uint32_t pixelYOffset = pixelY * sourceSize;
    uint32_t chunkY = pixelYOffset * scale;
    uint32_t maskY = pixelY * maskScale;
    for (uint32_t pixelX = 0; pixelX < sourceSize; pixelX++)
    {
      uint32_t blockPixelPosition = chunkY + (pixelX * scale);
      uint32_t pixelStart = pixelYOffset + pixelX;
      uint32_t maskX = (pixelX * maskScale);
      uint32_t maskPixelStart = (maskY * 256) + maskX;

      RGBAColor pixel;

      for (size_t i = 0; i < layers->count(); i++)
      {
        ImageData sourceImage = *p_textureList.at(layers->at(i).textureID);

        // If we don't use alphas, grab the pixel, then skip the rest
        if (!layers->at(i).flags.use_alpha_map)
        {
          pixel = sourceImage.ColorData->getAt(pixelStart);
          continue;
        }

        RGBAColor srcPixel = sourceImage.ColorData->getAt(pixelStart);

        // Get the mask
        ImageData maskImage = maskLayers->at(i);
        maskImage.resizeImage(p_blockSizePixels, p_blockSizePixels);
        
        double maskValue = maskImage.ColorData->getAt(pixelStart).R;

        //if (p_chunk->Header->IndexX == 0 && p_chunk->Header->IndexY == 0)
          //LOGFILE.Debug("Block X/Y: 0, Pixel X: %i, Pixel Y: %i, Layer: %i, Mask Value: %.3f, MaskPixelStart: %i", pixelX, pixelY, i, maskValue, maskPixelStart);

        pixel *= (1.0 - maskValue);
        srcPixel *= maskValue;
        pixel += srcPixel;
      }

      // Save final pixel
      chunkImage.ColorData->setColor(blockPixelPosition, pixel);
      chunkImage.ColorDataRaw->setColor(blockPixelPosition, pixel);
    }
  }

  emit chunkComplete(chunkImage, (p_chunk->Header->IndexX * p_blockSizePixels), (p_chunk->Header->IndexY * p_blockSizePixels));
}

constructMapImage::constructMapImage(uint32_t imageSize)
{
  //LOGFILE.Debug("Building Map Image...");
  if (MasterImage != nullptr || MasterImage != NULL)
  {
    delete MasterImage;
  }
  MasterImage = new ImageData;
  MasterImage->makeBlackTexture(imageSize, imageSize);
}

void constructMapImage::chunkComplete(ImageData img, unsigned int offsetX, unsigned int offsetY)
{
  LOGFILE.Debug("Setting Block Data, offset X: %i, Y: %i...", offsetX, offsetY);
  MasterImage->setBlockData(&img, offsetX, offsetY);
}
