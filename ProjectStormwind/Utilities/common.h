#pragma once
#include <Foundation/Settings.h>
#include <Foundation/LogFile.h>
#include <Foundation/Functions.h>
#include <Foundation/Classes/Vectors.h>
#include <Foundation/Classes/WoWVersion.h>
#include <ProjectStormwind/Defines.h>
#include <QtCore/qdir.h>

extern bool showUnsupportedBuildWarning;
extern double ADTFolderMin;
extern double ADTFolderMax;
extern Vec3D ADTScale;
extern QDir WoWDir;
extern QDir OutputDir;
extern QDir LastDir;
extern cWoWVersion WoWVersion;

inline cWoWVersion getWoWVersion(QDir pathToExe, bool &show_warning) { cWoWVersion w; w.buildWoWVersion(pathToExe, show_warning); return w; }
inline cWoWVersion getWoWVersion(QString pathToExe, bool &show_warning) { cWoWVersion w; w.buildWoWVersion(pathToExe, show_warning); return w; }