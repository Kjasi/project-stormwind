#include "common.h"
#include <Foundation/Classes/Vectors.h>
#include <qfileinfo.h>
#include <qbuffer.h>

bool showUnsupportedBuildWarning = true;
double ADTFolderMin = 999999999999999999999999.0;
double ADTFolderMax = -999999999999999999999999.0;
Vec3D ADTScale = Vec3D(1.0, 1.0, 1.0);
QDir WoWDir;
QDir OutputDir;
QDir LastDir;
cWoWVersion WoWVersion;

template <> float getQBANumber<float>(QByteArray d)
{
  float r = 0.0f;
  flipba(&d);
  uint32_t num;
  sscanf(d.toHex(), "%x", &num);
  r = *((float*)&num);
  //LOGFILE.Info("getQBANumber float | d hex: %s | r: %f", qPrintable(d.toHex()), r);
  return static_cast<float>(r);
}
template <> double getQBANumber<double>(QByteArray d)
{
  double r = 0.0;
  flipba(&d);
  uint32_t num;
  sscanf(d.toHex(), "%x", &num);
  r = *((float*)&num);
  //LOGFILE.Info("getQBANumber double | d hex: %s | r: %lf", qPrintable(d.toHex()), r);
  return static_cast<double>(r);
}