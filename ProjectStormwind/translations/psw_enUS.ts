<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US" sourcelanguage="en_US">
<context>
    <name>ADTHeightScanner</name>
    <message>
        <location filename="../Tools/ADTHeightScanner.cpp" line="104"/>
        <source>Error Getting ADT Heights</source>
        <comment>Error Messagebox Title</comment>
        <translation>Error Getting ADT Heights</translation>
    </message>
    <message>
        <location filename="../Tools/ADTHeightScanner.cpp" line="105"/>
        <source>We were unable to find any ADT Heights.</source>
        <comment>Error Message</comment>
        <translation>We were unable to find any ADT Heights.</translation>
    </message>
    <message>
        <location filename="../Tools/ADTHeightScanner.cpp" line="109"/>
        <source>ADT Height Range: %1 to %2</source>
        <comment>Specifies the Height.</comment>
        <translation>ADT Height Range: %1 to %2</translation>
    </message>
    <message>
        <location filename="../Tools/ADTHeightScanner.cpp" line="113"/>
        <source>ADT Height Result</source>
        <comment>Result Title</comment>
        <translation>ADT Height Result</translation>
    </message>
</context>
<context>
    <name>AboutPlugins</name>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="14"/>
        <source>About Plugins</source>
        <translation>About Plugins</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="22"/>
        <source>Filter by Plug-in Type</source>
        <translation>Filter by Plug-in Type</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="33"/>
        <source>All Plug-ins</source>
        <translation>All Plug-ins</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="38"/>
        <source>Texture Plug-ins</source>
        <translation>Texture Plug-ins</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="43"/>
        <source>Model Plug-ins</source>
        <translation>Model Plug-ins</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="48"/>
        <source>Placement Plug-ins</source>
        <translation>Placement Plug-ins</translation>
    </message>
    <message>
        <location filename="../Forms/AboutPlugins.ui" line="124"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>BatchExportProgressWindow</name>
    <message>
        <location filename="../Forms/BatchExportProgressWindow.ui" line="20"/>
        <source>Batch Export Progress</source>
        <translation>Batch Export Progress</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportProgressWindow.ui" line="201"/>
        <source>Abort</source>
        <translation>Abort</translation>
    </message>
</context>
<context>
    <name>BatchExporter</name>
    <message>
        <location filename="../Tools/BatchExporter.cpp" line="212"/>
        <source>Error exporting file</source>
        <translation>Error exporting file</translation>
    </message>
    <message>
        <location filename="../Tools/BatchExporter.cpp" line="212"/>
        <source>There was an unexpected error while exporting. Check log.txt for details.</source>
        <translation>There was an unexpected error while exporting. Check log.txt for details.</translation>
    </message>
</context>
<context>
    <name>BatchExporterDialog</name>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="70"/>
        <source>That&apos;s not suppose to happen...</source>
        <translation>That&apos;s not suppose to happen...</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="70"/>
        <source>So, it turns out that there are no plugins for you to export with. As such, you can not export anything.

Please re-install Project Stormwind to at least get the default plugins.</source>
        <translation>So, it turns out that there are no plugins for you to export with. As such, you can not export anything.

Please re-install Project Stormwind to at least get the default plugins.</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="138"/>
        <source>160x160 (25%)</source>
        <translation>160x160 (25%)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="139"/>
        <source>320x320 (50%)</source>
        <translation>320x320 (50%)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="140"/>
        <source>640x640 (100%)</source>
        <translation>640x640 (100%)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="141"/>
        <source>1280x1280 (200%)</source>
        <translation>1280x1280 (200%)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="142"/>
        <source>2560x2560 (400%)</source>
        <translation>2560x2560 (400%)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="144"/>
        <source>256x256 (3D/Cryengine)</source>
        <translation>256x256 (3D/Cryengine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="145"/>
        <source>512x512 (3D/Cryengine)</source>
        <translation>512x512 (3D/Cryengine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="146"/>
        <source>1024x1024 (3D/Cryengine)</source>
        <translation>1024x1024 (3D/Cryengine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="147"/>
        <source>2048x2048 (3D/Cryengine)</source>
        <translation>2048x2048 (3D/Cryengine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="148"/>
        <source>4096x4096 (3D/Cryengine)</source>
        <translation>4096x4096 (3D/Cryengine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="150"/>
        <source>253x253 (Unreal Engine)</source>
        <translation>253x253 (Unreal Engine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="151"/>
        <source>505x505 (Unreal Engine)</source>
        <translation>505x505 (Unreal Engine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="152"/>
        <source>1009x1009 (Unreal Engine)</source>
        <translation>1009x1009 (Unreal Engine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="153"/>
        <source>2017x2017 (Unreal Engine)</source>
        <translation>2017x2017 (Unreal Engine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="154"/>
        <source>4033x4033 (Unreal Engine)</source>
        <translation>4033x4033 (Unreal Engine)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="156"/>
        <source>257x257 (Unity)</source>
        <translation>257x257 (Unity)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="157"/>
        <source>513x513 (Unity)</source>
        <translation>513x513 (Unity)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="158"/>
        <source>1025x1025 (Unity)</source>
        <translation>1025x1025 (Unity)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="159"/>
        <source>2049x2049 (Unity)</source>
        <translation>2049x2049 (Unity)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="160"/>
        <source>4097x4097 (Unity)</source>
        <translation>4097x4097 (Unity)</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="195"/>
        <source>Choose a World of Warcraft Root directory...</source>
        <translation>Choose a World of Warcraft Root directory...</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="207"/>
        <source>Choose an input directory...</source>
        <translation>Choose an input directory...</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="218"/>
        <source>Choose an output directory...</source>
        <translation>Choose an output directory...</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="595"/>
        <source>Unable to find WoW.exe</source>
        <translation>Unable to find WoW.exe</translation>
    </message>
    <message>
        <location filename="../Windows/BatchExporterDialog.cpp" line="602"/>
        <source>Unsupported build detected: %s</source>
        <translation>Unsupported build detected: %s</translation>
    </message>
</context>
<context>
    <name>BatchExporterProgressDialog</name>
    <message>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="41"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="42"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="43"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="44"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="45"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="46"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="47"/>
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="48"/>
        <source>File: %1</source>
        <translation>File: %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../Windows/BatchExporterProgressDialog.cpp" line="72"/>
        <source>%n File(s)</source>
        <comment>The Maximum number of files to be processed.</comment>
        <translation>
            <numerusform>%n File</numerusform>
            <numerusform>%n Files</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogBatchExporter</name>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="17"/>
        <source>Batch Exporter</source>
        <translation>Batch Exporter</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="31"/>
        <location filename="../Forms/BatchExportDialog.ui" line="44"/>
        <location filename="../Forms/BatchExportDialog.ui" line="61"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The source directory. If it&apos;s a WoW game directory, all available files will be exported. If it&apos;s a directory with extracted game files, only the found files will be exported.&lt;/p&gt;&lt;p&gt;Please note that game directories are a much better idea, as it will contain ALL the data needed for acurate extraction. Results may vary with pre-extracted files.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The source directory. If it&apos;s a WoW game directory, all available files will be exported. If it&apos;s a directory with extracted game files, only the found files will be exported.&lt;/p&gt;&lt;p&gt;Please note that game directories are a much better idea, as it will contain ALL the data needed for acurate extraction. Results may vary with pre-extracted files.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="47"/>
        <location filename="../Forms/BatchExportDialog.ui" line="84"/>
        <location filename="../Forms/BatchExportDialog.ui" line="107"/>
        <source>Browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="54"/>
        <location filename="../Forms/BatchExportDialog.ui" line="68"/>
        <location filename="../Forms/BatchExportDialog.ui" line="81"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Where to saved the converted files.&lt;/p&gt;&lt;p&gt;If Mirror Folders is on, this will become the root directory that all files are saved under. If not, all files will be dumped here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Where to saved the converted files.&lt;/p&gt;&lt;p&gt;If Mirror Folders is on, this will become the root directory that all files are saved under. If not, all files will be dumped here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="159"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="166"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="173"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="182"/>
        <source>Export Options</source>
        <translation>Export Options</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="212"/>
        <location filename="../Forms/BatchExportDialog.ui" line="289"/>
        <location filename="../Forms/BatchExportDialog.ui" line="383"/>
        <source>Exports a mesh for this model</source>
        <translation>Exports a mesh for this model</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="248"/>
        <source>Export all the textures used by this model.</source>
        <translation>Export all the textures used by this model.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="251"/>
        <location filename="../Forms/BatchExportDialog.ui" line="305"/>
        <location filename="../Forms/BatchExportDialog.ui" line="399"/>
        <source>Export Textures</source>
        <translation>Export Textures</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="445"/>
        <source>Process children models that are a part of this model. Child WMOs will only have their used doodad set exported.</source>
        <translation>Process children models that are a part of this model. Child WMOs will only have their used doodad set exported.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="235"/>
        <source>Exports the version of the model with the most polygons.</source>
        <translation>Exports the version of the model with the most polygons.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="238"/>
        <source>Highest LOD Mesh</source>
        <translation>Highest LOD Mesh</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="277"/>
        <source>Disable to stop the Batch Exporter from processing any WMO files.</source>
        <translation>Disable to stop the Batch Exporter from processing any WMO files.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="302"/>
        <location filename="../Forms/BatchExportDialog.ui" line="396"/>
        <source>Export all the textures used by this model, and any exported child models.</source>
        <translation>Export all the textures used by this model, and any exported child models.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="318"/>
        <location filename="../Forms/BatchExportDialog.ui" line="448"/>
        <source>Export Sub-models</source>
        <translation>Export Sub-models</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="328"/>
        <location filename="../Forms/BatchExportDialog.ui" line="458"/>
        <source>Exports the placement information for submodels.</source>
        <translation>Exports the placement information for submodels.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="331"/>
        <location filename="../Forms/BatchExportDialog.ui" line="461"/>
        <source>Export Placements</source>
        <translation>Export Placements</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="586"/>
        <source>Global</source>
        <translation>Global</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="660"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="678"/>
        <source>Export all textures (BLPs) found in the input directory.</source>
        <translation>Export all textures (BLPs) found in the input directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="691"/>
        <source>Export all Models (M2) files from the Input Directory.</source>
        <translation>Export all Models (M2) files from the Input Directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="704"/>
        <source>Export all World Models (WMO) files, and all selected sub-models, from the Input Directory.</source>
        <translation>Export all World Models (WMO) files, and all selected sub-models, from the Input Directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="717"/>
        <source>Export all Terrain (ADT) files, and all selected sub-models, from the Input Directory.</source>
        <translation>Export all Terrain (ADT) files, and all selected sub-models, from the Input Directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="816"/>
        <source>File Formats</source>
        <translation>File Formats</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="834"/>
        <source>Texture Format:</source>
        <translation>Texture Format:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="844"/>
        <source>The format to export placement info as.</source>
        <translation>The format to export placement info as.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="878"/>
        <source>The file format to export textures as.</source>
        <translation>The file format to export textures as.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="851"/>
        <source>Model Format:</source>
        <translation>Model Format:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="861"/>
        <source>The format to export models as.</source>
        <translation>The format to export models as.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="868"/>
        <source>Placement Format:</source>
        <translation>Placement Format:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="681"/>
        <source>Export All Textures</source>
        <translation>Export All Textures</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="694"/>
        <source>Export All Models</source>
        <translation>Export All Models</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="707"/>
        <source>Export All World Models</source>
        <translation>Export All World Models</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="720"/>
        <source>Export All Terrain</source>
        <translation>Export All Terrain</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="751"/>
        <source>World of Warcraft Info</source>
        <translation>World of Warcraft Info</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="215"/>
        <location filename="../Forms/BatchExportDialog.ui" line="292"/>
        <location filename="../Forms/BatchExportDialog.ui" line="386"/>
        <source>Export Mesh</source>
        <translation>Export Mesh</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="71"/>
        <source>Output Root Directory</source>
        <translation>Output Root Directory</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="200"/>
        <source>Disable to stop the Batch Exporter from processing any M2 models.</source>
        <translation>Disable to stop the Batch Exporter from processing any M2 models.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="203"/>
        <source>Model Options</source>
        <translation>Model Options</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="280"/>
        <source>World Model Options</source>
        <translation>World Model Options</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="315"/>
        <source>Process children models that are a part of this model.</source>
        <translation>Process children models that are a part of this model.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="604"/>
        <source>Continue on Error</source>
        <translation>Continue on Error</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="611"/>
        <source>This treats the Output Directory like the root WoW folder, putting the exported files into the same folder structure as they are found in the Input Directory.</source>
        <translation>This treats the Output Directory like the root WoW folder, putting the exported files into the same folder structure as they are found in the Input Directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="614"/>
        <source>Mirror Input Folders</source>
        <translation>Mirror Input Folders</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="357"/>
        <source>Disable to stop Batch Exporter from processing any ADT files.</source>
        <translation>Disable to stop Batch Exporter from processing any ADT files.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="34"/>
        <source>WoW Root Directory</source>
        <translation>WoW Root Directory</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="91"/>
        <source>The Root Directory you want to start exporting from. If a WoW Root Directory, will export every file possible. Otherwise, files to be exported will be limited to this directory.</source>
        <translation>The Root Directory you want to start exporting from. If a WoW Root Directory, will export every file possible. Otherwise, files to be exported will be limited to this directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="94"/>
        <source>Input Directory</source>
        <translation>Input Directory</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="133"/>
        <source>Quick Selections</source>
        <translation>Quick Selections</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="225"/>
        <source>Extract All LOD meshes</source>
        <translation>Extract All LOD meshes</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="228"/>
        <source>All LOD Meshes</source>
        <translation>All LOD Meshes</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="360"/>
        <source>Terrain Options</source>
        <translation>Terrain Options</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="409"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, ADT files will only have 1 single material created that covers the entire model. If not checked, each of the 64 chunks will have it&apos;s own individual material.&lt;/p&gt;&lt;p&gt;This is useful for gaming materials that can split up the chunks on it&apos;s own, or large baked textures that will cover the entire model.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, ADT files will only have 1 single material created that covers the entire model. If not checked, each of the 64 chunks will have it&apos;s own individual material.&lt;/p&gt;&lt;p&gt;This is useful for gaming materials that can split up the chunks on it&apos;s own, or large baked textures that will cover the entire model.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="412"/>
        <source>Build Single Material</source>
        <translation>Build Single Material</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="422"/>
        <source>Bakes a single, large texture for this terrain. If Export Masks is enabled, then each of those mask layers will also be a single texture.</source>
        <translation>Bakes a single, large texture for this terrain. If Export Masks is enabled, then each of those mask layers will also be a single texture.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="425"/>
        <source>Bake Single Textures</source>
        <translation>Bake Single Textures</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="432"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exports the masks used for each layer of the terrain.&lt;/p&gt;&lt;p&gt;If Bake Single Textures is enabled, produces a single file per layer. (Up to 4)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Exports the masks used for each layer of the terrain.&lt;/p&gt;&lt;p&gt;If Bake Single Textures is enabled, produces a single file per layer. (Up to 4)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="488"/>
        <source>Exports a heightmap of the terrain as well as the mesh.</source>
        <translation>Exports a heightmap of the terrain as well as the mesh.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="491"/>
        <source>Export Heightmap</source>
        <translation>Export Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="501"/>
        <source>Heightmap Format:</source>
        <translation>Heightmap Format:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="514"/>
        <source>Heightmap Resolution:</source>
        <translation>Heightmap Resolution:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="531"/>
        <source>Use the middle point in Heightmap generation</source>
        <translation>Use the middle point in Heightmap generation</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="534"/>
        <source>Use Middlepoint</source>
        <translation>Use Middlepoint</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="601"/>
        <source>Continue exporting, even if we encounter an error.</source>
        <translation>Continue exporting, even if we encounter an error.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="624"/>
        <source>If we should also export the subdirectories of the Input Directory.</source>
        <translation>If we should also export the subdirectories of the Input Directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="627"/>
        <source>Include Subdirectories</source>
        <translation>Include Subdirectories</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="637"/>
        <source>If we should combine meshes into a single object. When unchecked, this will create multiple sub-objects when exporting, making it easier to distinguish different parts of the model from each other.</source>
        <translation>If we should combine meshes into a single object. When unchecked, this will create multiple sub-objects when exporting, making it easier to distinguish different parts of the model from each other.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="640"/>
        <source>Combine Meshes</source>
        <translation>Combine Meshes</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="647"/>
        <source>If we should overwrite any existing files in the Output directory.</source>
        <translation>If we should overwrite any existing files in the Output directory.</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="650"/>
        <source>Overwrite Existing Files</source>
        <translation>Overwrite Existing Files</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="765"/>
        <source>Version:</source>
        <translation>Version:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="788"/>
        <source>Source File:</source>
        <translation>Source File:</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportDialog.ui" line="435"/>
        <source>Export Masks</source>
        <translation>Export Masks</translation>
    </message>
</context>
<context>
    <name>Dialog_Options</name>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="23"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="178"/>
        <source>Disables loading of textures in the renderer.</source>
        <translation>Disables loading of textures in the renderer.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="306"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="313"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="341"/>
        <source>Export Options</source>
        <translation>Export Options</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="379"/>
        <source>Mirrors the WoW Directory Path in the Output Folder. Disabling this will dump all the files into a single sub-folder.</source>
        <translation>Mirrors the WoW Directory Path in the Output Folder. Disabling this will dump all the files into a single sub-folder.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="382"/>
        <source>Mirror WoW Paths</source>
        <translation>Mirror WoW Paths</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="347"/>
        <source>ADT Options</source>
        <translation>ADT Options</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="423"/>
        <source>The path to the root WoW folder. This is often where the wow.exe would be located.</source>
        <translation>The path to the root WoW folder. This is often where the wow.exe would be located.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="437"/>
        <location filename="../Forms/OptionsDialog.ui" line="454"/>
        <source>Browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="430"/>
        <source>Root WoW Directory</source>
        <translation>Root WoW Directory</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="353"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, ADT files will only have 1 single material created that covers the entire model. If not checked, each of the 64 chunks will have it&apos;s own individual material.&lt;/p&gt;&lt;p&gt;This is useful for gaming materials that can split up the chunks on it&apos;s own, or large baked textures that will cover the entire model.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If checked, ADT files will only have 1 single material created that covers the entire model. If not checked, each of the 64 chunks will have it&apos;s own individual material.&lt;/p&gt;&lt;p&gt;This is useful for gaming materials that can split up the chunks on it&apos;s own, or large baked textures that will cover the entire model.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="356"/>
        <source>Build Single Material</source>
        <translation>Build Single Material</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="213"/>
        <source>Allows debug messages to show up in the log. Useful for determining possible problems in the software.</source>
        <translation>Allows debug messages to show up in the log. Useful for determining possible problems in the software.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="41"/>
        <source>The default exporter for each type of item.</source>
        <translation>The default exporter for each type of item.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="44"/>
        <source>Prefered Exporters</source>
        <translation>Prefered Exporters</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="68"/>
        <source>Textures</source>
        <translation>Textures</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="103"/>
        <source>Placement</source>
        <translation>Placement</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="130"/>
        <source>Display Options</source>
        <translation>Display Options</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="181"/>
        <source>Disable Texture Loading</source>
        <translation>Disable Texture Loading</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="188"/>
        <source>Disables the rendering system, enabling for faster loading of models. NOTE: Won&apos;t display any graphics!</source>
        <translation>Disables the rendering system, enabling for faster loading of models. NOTE: Won&apos;t display any graphics!</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="191"/>
        <source>Disable Model Rendering</source>
        <translation>Disable Model Rendering</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="207"/>
        <source>Logging</source>
        <translation>Logging</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="216"/>
        <source>Show Debug messages in the Log.</source>
        <translation>Show Debug messages in the Log.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="219"/>
        <source>Show Debug Messages</source>
        <translation>Show Debug Messages</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="226"/>
        <source>Shows a timestamp in the log. Useful for seeing how long a process takes.</source>
        <translation>Shows a timestamp in the log. Useful for seeing how long a process takes.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="229"/>
        <source>Show Timestamps in the log.</source>
        <translation>Show Timestamps in the log.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="232"/>
        <source>Use Timestamps</source>
        <translation>Use Timestamps</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="138"/>
        <location filename="../Forms/OptionsDialog.ui" line="160"/>
        <source>Background Color of the viewfinder.</source>
        <translation>Background Color of the viewfinder.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="141"/>
        <source>Background Color</source>
        <translation>Background Color</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="408"/>
        <source>If we should combine meshes into a single object. When unchecked, this will create multiple sub-objects when exporting, making it easier to distinguish different parts of the model from each other.</source>
        <translation>If we should combine meshes into a single object. When unchecked, this will create multiple sub-objects when exporting, making it easier to distinguish different parts of the model from each other.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="411"/>
        <source>Combine Meshes</source>
        <translation>Combine Meshes</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="444"/>
        <source>Output Folder</source>
        <translation>Output Folder</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="87"/>
        <source>Models</source>
        <translation>Models</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="363"/>
        <source>Include the middle point when exporting ADT files. The middle point is used for accuracy to WoW, but can introduce a &quot;bumpiness&quot; to heightmaps.</source>
        <translation>Include the middle point when exporting ADT files. The middle point is used for accuracy to WoW, but can introduce a &quot;bumpiness&quot; to heightmaps.</translation>
    </message>
    <message>
        <location filename="../Forms/OptionsDialog.ui" line="366"/>
        <source>Use Middle Point</source>
        <translation>Use Middle Point</translation>
    </message>
</context>
<context>
    <name>M2VersionScanner</name>
    <message>
        <location filename="../Tools/M2VersionScanner.cpp" line="53"/>
        <source>Error Getting M2 Versions</source>
        <comment>Error Messagebox Title</comment>
        <translation>Error Getting M2 Versions</translation>
    </message>
    <message>
        <location filename="../Tools/M2VersionScanner.cpp" line="54"/>
        <source>We were unable to find any valid M2 versions.</source>
        <comment>Error Message</comment>
        <translation>We were unable to find any valid M2 versions.</translation>
    </message>
    <message>
        <location filename="../Tools/M2VersionScanner.cpp" line="60"/>
        <source>M2 Version: %1</source>
        <comment>Specifies the version number.</comment>
        <translation>M2 Version: %1</translation>
    </message>
    <message>
        <location filename="../Tools/M2VersionScanner.cpp" line="64"/>
        <location filename="../Tools/M2VersionScanner.cpp" line="73"/>
        <source>M2 Version Result</source>
        <comment>Result Title</comment>
        <translation>M2 Version Result</translation>
    </message>
    <message>
        <location filename="../Tools/M2VersionScanner.cpp" line="69"/>
        <source>M2 Version Range: %1 to %2</source>
        <comment>Specifies the version numbers.</comment>
        <translation>M2 Version Range: %1 to %2</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Forms/MainWindow.ui" line="599"/>
        <source>Current File:</source>
        <translation>Current File:</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="606"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="86"/>
        <location filename="../Forms/MainWindow.ui" line="89"/>
        <source>Wireframe</source>
        <translation>Wireframe</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="121"/>
        <location filename="../Forms/MainWindow.ui" line="124"/>
        <source>Solid</source>
        <translation>Solid</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="156"/>
        <location filename="../Forms/MainWindow.ui" line="159"/>
        <source>Solid Wireframe</source>
        <translation>Solid Wireframe</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="227"/>
        <location filename="../Forms/MainWindow.ui" line="230"/>
        <source>Textured</source>
        <translation>Textured</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="265"/>
        <location filename="../Forms/MainWindow.ui" line="268"/>
        <source>Textured Wireframe</source>
        <translation>Textured Wireframe</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="300"/>
        <source>Cull the back of models. Turn this off to automatically make every surface double-sided.</source>
        <translation>Cull the back of models. Turn this off to automatically make every surface double-sided.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="303"/>
        <source>Cull</source>
        <translation>Cull</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="357"/>
        <source>Show the Color with the Alpha channel enabled.</source>
        <translation>Show the Color with the Alpha channel enabled.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="360"/>
        <source>Color &amp; Alpha</source>
        <translation>Color &amp; Alpha</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="395"/>
        <source>Color Only</source>
        <translation>Color Only</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="398"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="430"/>
        <source>Alpha Only</source>
        <translation>Alpha Only</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="433"/>
        <source>Alpha</source>
        <translation>Alpha</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="481"/>
        <source>Heightmap</source>
        <translation>Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="632"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="636"/>
        <source>Export</source>
        <translation>Export</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="656"/>
        <location filename="../Forms/MainWindow.ui" line="798"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="667"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="699"/>
        <location filename="../Forms/MainWindow.ui" line="1068"/>
        <source>Unreal Engine</source>
        <translation>Unreal Engine</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="709"/>
        <location filename="../Forms/MainWindow.ui" line="1076"/>
        <source>Unity</source>
        <translation>Unity</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="719"/>
        <source>Commonly used by 3D apps, plus the CryEngine/Lumberyard game engine.</source>
        <translation>Commonly used by 3D apps, plus the CryEngine/Lumberyard game engine.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="722"/>
        <source>3D / CryEngine</source>
        <translation>3D / CryEngine</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="673"/>
        <source>Tools</source>
        <translation>Tools</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="191"/>
        <location filename="../Forms/MainWindow.ui" line="194"/>
        <source>Textured Unshaded</source>
        <translation>Textured Unshaded</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="513"/>
        <source>Layer Masks</source>
        <translation>Layer Masks</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="641"/>
        <source>Recent Files</source>
        <translation>Recent Files</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="682"/>
        <source>View</source>
        <translation>View</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="691"/>
        <source>ADTs</source>
        <translation>ADTs</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="695"/>
        <source>ADT Heightmap Output Resolution</source>
        <translation>ADT Heightmap Output Resolution</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="760"/>
        <source>File Details</source>
        <translation>File Details</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="774"/>
        <source>Instances</source>
        <translation>Instances</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="782"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="787"/>
        <source>Open...</source>
        <translation>Open...</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="790"/>
        <source>Open a specific file to preview/convert.</source>
        <translation>Open a specific file to preview/convert.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="793"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="803"/>
        <source>Help</source>
        <translation>Help</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="808"/>
        <source>Preferences</source>
        <translation>Preferences</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="811"/>
        <source>Configure the various options for this program.</source>
        <translation>Configure the various options for this program.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="814"/>
        <location filename="../Forms/MainWindow.ui" line="1154"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="819"/>
        <source>HeightMap...</source>
        <translation>HeightMap...</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="824"/>
        <source>About Plugins</source>
        <translation>About Plugins</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="829"/>
        <source>View Log File...</source>
        <translation>View Log File...</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="832"/>
        <source>Ctrl+L</source>
        <translation>Ctrl+L</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="837"/>
        <source>ADT Height Scanner</source>
        <translation>ADT Height Scanner</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="848"/>
        <source>Original (640x640)</source>
        <translation>Original (640x640)</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="851"/>
        <source>The base resolution, giving each block of polygons 5 pixels each.</source>
        <translation>The base resolution, giving each block of polygons 5 pixels each.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="859"/>
        <source>0.5x</source>
        <extracomment>Half the original resolution</extracomment>
        <translation>0.5x</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="862"/>
        <source>Half the original resolution</source>
        <translation>Half the original resolution</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="870"/>
        <source>2x</source>
        <extracomment>Twice the original resolution</extracomment>
        <translation>2x</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="873"/>
        <source>Twice the original resolution</source>
        <translation>Twice the original resolution</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="881"/>
        <source>4x</source>
        <extracomment>Four times the original resolution</extracomment>
        <translation>4x</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="884"/>
        <source>Four times the orignal resolution</source>
        <translation>Four times the orignal resolution</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="895"/>
        <source>This is the closest to the original resolution UE4 can get, but is just a bit smaller, so some data will be lost.</source>
        <translation>This is the closest to the original resolution UE4 can get, but is just a bit smaller, so some data will be lost.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="906"/>
        <source>Just under twice the resolution of ADT files, this is the closest you can get in UE4 while retaining all the original data.</source>
        <translation>Just under twice the resolution of ADT files, this is the closest you can get in UE4 while retaining all the original data.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="917"/>
        <source>An enlarged resolution, good for adding more detail.</source>
        <translation>An enlarged resolution, good for adding more detail.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="925"/>
        <location filename="../Forms/MainWindow.ui" line="928"/>
        <source>0.25x</source>
        <extracomment>One quarter the original resolution</extracomment>
        <translation>0.25x</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="931"/>
        <source>1/4th the original resolution</source>
        <translation>One Quarter the original resolution</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="942"/>
        <source>A very enlarged resolution, good for adding TONS of additional detail to.</source>
        <translation>A very enlarged resolution, good for adding TONS of additional detail to.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="953"/>
        <source>An insanely large resolution, good for adding immense detail to.</source>
        <translation>An insanely large resolution, good for adding immense detail to.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1044"/>
        <source>Normalize ADT Heightmaps</source>
        <translation>Normalize ADT Heightmaps</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1049"/>
        <source>Batch Export...</source>
        <translation>Batch Export...</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1060"/>
        <source>Generic 3D</source>
        <translation>Generic 3D</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1084"/>
        <source>Cryengine / Lumberyard</source>
        <translation>Cryengine / Lumberyard</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1089"/>
        <source>M2 Version Scanner</source>
        <translation>M2 Version Scanner</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1100"/>
        <source>Show Current File</source>
        <translation>Show Current File</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1111"/>
        <source>Show View Controls</source>
        <translation>Show View Controls</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1119"/>
        <source>Show Heightmap</source>
        <translation>Show Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1127"/>
        <source>Display Heightmap</source>
        <translation>Display Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1130"/>
        <source>Displays the generated heightmap on the displayed mesh.</source>
        <translation>Displays the generated heightmap on the displayed mesh.</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1135"/>
        <source>Check for Update</source>
        <translation>Check for Update</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1143"/>
        <source>Generated Mesh from Heightmap</source>
        <translation>Generated Mesh from Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1146"/>
        <source>Creates a high-resolution mesh from the generated Heightmap</source>
        <translation>Creates a high-resolution mesh from the generated Heightmap</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1151"/>
        <source>Save Screenshot...</source>
        <translation>Save Screenshot...</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1162"/>
        <source>Show File Details</source>
        <translation>Show File Details</translation>
    </message>
    <message>
        <location filename="../Forms/MainWindow.ui" line="1170"/>
        <source>Show Instance Info</source>
        <translation>Show Instance Info</translation>
    </message>
</context>
<context>
    <name>ProjectStormwind</name>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="200"/>
        <location filename="../Windows/projectstormwind.cpp" line="210"/>
        <location filename="../Windows/projectstormwind.cpp" line="243"/>
        <location filename="../Windows/projectstormwind.cpp" line="253"/>
        <source>None</source>
        <translation>None</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="454"/>
        <source>Export &amp;All...</source>
        <translation>Export &amp;All...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="455"/>
        <source>Export everything using the default exporters.</source>
        <translation>Export everything using the default exporters.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="458"/>
        <source>&amp;Export Mesh...</source>
        <translation>&amp;Export Mesh...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="459"/>
        <source>Export current object as a mesh.</source>
        <translation>Export current object as a mesh.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="461"/>
        <source>Export &amp;Textures...</source>
        <translation>Export &amp;Textures...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="462"/>
        <source>Export the current object&apos;s textures.</source>
        <translation>Export the current object&apos;s textures.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="463"/>
        <source>Export &amp;Placement...</source>
        <translation>Export &amp;Placement...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="464"/>
        <source>Export sub-object placements.</source>
        <translation>Export sub-object placements.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="465"/>
        <source>Export &amp;Submodels...</source>
        <translation>Export &amp;Submodels...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="466"/>
        <source>Export models referenced by the current model.</source>
        <translation>Export models referenced by the current model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="510"/>
        <source>Invalid Export</source>
        <translation>Invalid Export</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="510"/>
        <source>Unable to export file. Could not find the proper export for the FileType.</source>
        <translation>Unable to export file. Could not find the proper export for the FileType.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="517"/>
        <source>&amp;Export Image...</source>
        <translation>&amp;Export Image...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="518"/>
        <source>Export current BLP as an image.</source>
        <translation>Export current BLP as an image.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="529"/>
        <source>&amp;Heightmap Data...</source>
        <translation>&amp;Heightmap Data...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="530"/>
        <source>Export the Heightmap data as an image.</source>
        <translation>Export the Heightmap data as an image.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="546"/>
        <source>&amp;Mask Layers...</source>
        <translation>&amp;Mask Layers...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="547"/>
        <source>Export the chunk masks as images.</source>
        <translation>Export the chunk masks as images.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="551"/>
        <source>&amp;Liquid Mesh...</source>
        <translation>&amp;Liquid Mesh...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="552"/>
        <source>Export liquid chunks as a mesh.</source>
        <translation>Export liquid chunks as a mesh.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="767"/>
        <source>An error occured while exporting the ADT mesh.</source>
        <translation>An error occured while exporting the ADT mesh.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="809"/>
        <source>Finished exporting ADT Placement Data.</source>
        <translation>Finished exporting ADT Placement Data.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="929"/>
        <source>ADT Texture Export Error</source>
        <translation>ADT Texture Export Error</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="941"/>
        <source>No ADT file loaded. Aborting Baked Texture Export.</source>
        <translation>No ADT file loaded. Aborting Baked Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="966"/>
        <source>An Error occured while getting the image plugin: %s</source>
        <translation>An Error occured while getting the image plugin: %s</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="974"/>
        <source>Unable to find selected plugin. Aborting Save...</source>
        <translation>Unable to find selected plugin. Aborting Save...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="988"/>
        <source>Unable to build ADT Map Texture. Aborting Baked Texture Export.</source>
        <translation>Unable to build ADT Map Texture. Aborting Baked Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1000"/>
        <source>An error occured while exporting the ADT Map Texture.</source>
        <translation>An error occured while exporting the ADT Map Texture.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1005"/>
        <source>Finished exporting ADT Map Texture.</source>
        <translation>Finished exporting ADT Map Texture.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1079"/>
        <source>An error occured while exporting the ADT Masks: %s</source>
        <comment>%s is the error string.</comment>
        <translation>An error occured while exporting the ADT Masks: %s</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1088"/>
        <source>Finished exporting all ADT Masks.</source>
        <translation>Finished exporting all ADT Masks.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1094"/>
        <source>Combine masked exported. Individual masks not exported...</source>
        <translation>Combine masked exported. Individual masks not exported...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1234"/>
        <source>An error occured while exporting the WMO placement data.</source>
        <translation>An error occured while exporting the WMO placement data.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1295"/>
        <source>Submesh Exporting is now complete!</source>
        <translation>Submesh Exporting is now complete!</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1374"/>
        <location filename="../Windows/projectstormwind.cpp" line="1495"/>
        <source>WMO Texture Export Error</source>
        <translation>WMO Texture Export Error</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1546"/>
        <source>Recent File Not Found</source>
        <translation>Recent File Not Found</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1546"/>
        <source>Could not find the selected recent file.
Maybe it was moved or deleted?</source>
        <translation>Could not find the selected recent file.
Maybe it was moved or deleted?</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1559"/>
        <source>All WoW Files (*.adt *.wmo *.m2 *.mdx *.blp *.wdl);;Images (*.blp);;Models (*.m2 *.mdx);;Terrain (*.adt *.wdl);;World Model Objects (*.wmo)</source>
        <comment>All the file formats Project Stormwind can open. May change as support is added.</comment>
        <translation>All WoW Files (*.adt *.wmo *.m2 *.mdx *.blp *.wdl);;Images (*.blp);;Models (*.m2 *.mdx);;Terrain (*.adt *.wdl);;World Model Objects (*.wmo)</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1559"/>
        <source>Open File</source>
        <translation>Open File</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="767"/>
        <location filename="../Windows/projectstormwind.cpp" line="803"/>
        <location filename="../Windows/projectstormwind.cpp" line="837"/>
        <location filename="../Windows/projectstormwind.cpp" line="866"/>
        <location filename="../Windows/projectstormwind.cpp" line="898"/>
        <location filename="../Windows/projectstormwind.cpp" line="943"/>
        <location filename="../Windows/projectstormwind.cpp" line="968"/>
        <location filename="../Windows/projectstormwind.cpp" line="976"/>
        <location filename="../Windows/projectstormwind.cpp" line="990"/>
        <location filename="../Windows/projectstormwind.cpp" line="1000"/>
        <location filename="../Windows/projectstormwind.cpp" line="1056"/>
        <location filename="../Windows/projectstormwind.cpp" line="1079"/>
        <location filename="../Windows/projectstormwind.cpp" line="1099"/>
        <location filename="../Windows/projectstormwind.cpp" line="1154"/>
        <location filename="../Windows/projectstormwind.cpp" line="1198"/>
        <location filename="../Windows/projectstormwind.cpp" line="1234"/>
        <location filename="../Windows/projectstormwind.cpp" line="1311"/>
        <location filename="../Windows/projectstormwind.cpp" line="1343"/>
        <location filename="../Windows/projectstormwind.cpp" line="1410"/>
        <location filename="../Windows/projectstormwind.cpp" line="1432"/>
        <location filename="../Windows/projectstormwind.cpp" line="1464"/>
        <source>Export Error</source>
        <translation>Export Error</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="541"/>
        <source>&amp;Baked Texture...</source>
        <translation>&amp;Baked Texture...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="542"/>
        <source>Export a baked texture that covers the entire terrain.</source>
        <translation>Export a baked texture that covers the entire terrain.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="837"/>
        <source>An error occured while exporting the ADT heightmap.</source>
        <translation>An error occured while exporting the ADT heightmap.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="773"/>
        <location filename="../Windows/projectstormwind.cpp" line="809"/>
        <location filename="../Windows/projectstormwind.cpp" line="842"/>
        <location filename="../Windows/projectstormwind.cpp" line="934"/>
        <location filename="../Windows/projectstormwind.cpp" line="1005"/>
        <location filename="../Windows/projectstormwind.cpp" line="1088"/>
        <location filename="../Windows/projectstormwind.cpp" line="1094"/>
        <location filename="../Windows/projectstormwind.cpp" line="1159"/>
        <location filename="../Windows/projectstormwind.cpp" line="1204"/>
        <location filename="../Windows/projectstormwind.cpp" line="1240"/>
        <location filename="../Windows/projectstormwind.cpp" line="1295"/>
        <location filename="../Windows/projectstormwind.cpp" line="1379"/>
        <location filename="../Windows/projectstormwind.cpp" line="1416"/>
        <location filename="../Windows/projectstormwind.cpp" line="1500"/>
        <source>Export Complete</source>
        <translation>Export Complete</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="132"/>
        <source>%1, Version %2</source>
        <comment>First, the title, then the version number.</comment>
        <translation>%1, Version %2</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="773"/>
        <source>Finished exporting ADT Model.</source>
        <translation>Finished exporting ADT Model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="803"/>
        <source>An error occured while exporting the ADT placement data.</source>
        <translation>An error occured while exporting the ADT placement data.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="842"/>
        <source>Finished exporting ADT heightmap.</source>
        <translation>Finished exporting ADT heightmap.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="866"/>
        <source>No textures found in the loaded ADT file. Aborting Texture Export.</source>
        <translation>No textures found in the loaded ADT file. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="898"/>
        <source>Could not find the files for any of the textures specified by the ADT. Aborting Texture Export.</source>
        <translation>Could not find the files for any of the textures specified by the ADT. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="934"/>
        <source>Finished exporting ADT textures.</source>
        <translation>Finished exporting ADT textures.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1056"/>
        <source>An error occured while exporting the ADT Masks.</source>
        <translation>An error occured while exporting the ADT Masks.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1099"/>
        <source>We were unable to export any ADT masks...</source>
        <translation>We were unable to export any ADT masks...</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1154"/>
        <source>An error occured while exporting the WDL heightmap.</source>
        <translation>An error occured while exporting the WDL heightmap.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1159"/>
        <source>Finished exporting WDL heightmap.</source>
        <translation>Finished exporting WDL heightmap.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1198"/>
        <source>An error occured while exporting the WMO model.</source>
        <translation>An error occured while exporting the WMO model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1204"/>
        <source>Finished exporting WMO Model.</source>
        <translation>Finished exporting WMO Model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1240"/>
        <source>Finished exporting WMO Placement Data.</source>
        <translation>Finished exporting WMO Placement Data.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1311"/>
        <source>No textures found in the loaded WMO file. Aborting Texture Export.</source>
        <translation>No textures found in the loaded WMO file. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1343"/>
        <source>Could not find the files for any of the textures specified by the WMO. Aborting Texture Export.</source>
        <translation>Could not find the files for any of the textures specified by the WMO. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1379"/>
        <source>Finished exporting WMO textures.</source>
        <translation>Finished exporting WMO textures.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1410"/>
        <source>An error occured while exporting the M2 model.</source>
        <translation>An error occured while exporting the M2 model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1416"/>
        <source>Finished exporting M2 Model.</source>
        <translation>Finished exporting M2 Model.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1432"/>
        <source>No textures found in the loaded M2 file. Aborting Texture Export.</source>
        <translation>No textures found in the loaded M2 file. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1464"/>
        <source>Could not find the files for any of the textures specified by the M2. Aborting Texture Export.</source>
        <translation>Could not find the files for any of the textures specified by the M2. Aborting Texture Export.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1500"/>
        <source>Finished exporting M2 textures.</source>
        <translation>Finished exporting M2 textures.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1601"/>
        <source>Error: Log file not found!</source>
        <translation>Error: Log file not found!</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1602"/>
        <source>Could not find the %1 file.

Check your permissions, as you might be running Project Stormwind in a location that does not allow for the generation of files.</source>
        <translation>Could not find the %1 file.

Check your permissions, as you might be running Project Stormwind in a location that does not allow for the generation of files.</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1649"/>
        <location filename="../Windows/projectstormwind.cpp" line="1671"/>
        <source>Open Directory</source>
        <translation>Open Directory</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="1671"/>
        <source>Terrain (*.adt)</source>
        <translation>Terrain (*.adt)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="187"/>
        <source>Unable to determine Current WoW version</source>
        <comment>A simple error message for currentWoW being blank</comment>
        <translation>Unable to determine Current WoW version</translation>
    </message>
    <message>
        <location filename="../Windows/projectstormwind.cpp" line="187"/>
        <source>Unable to determine the current WoW version.

Please open your preferences, and make sure the WoW Root directory is pointed to the right directory.</source>
        <translation>Unable to determine the current WoW version.

Please open your preferences, and make sure the WoW Root directory is pointed to the right directory.</translation>
    </message>
    <message>
        <location filename="../Defines.h" line="5"/>
        <source>Project Stormwind</source>
        <translation>Project Stormwind</translation>
    </message>
</context>
<context>
    <name>QuickSelectionsDialog</name>
    <message>
        <location filename="../Forms/BatchExportQuickSelections.ui" line="17"/>
        <source>Quick Selections</source>
        <translation>Quick Selections</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportQuickSelections.ui" line="28"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportQuickSelections.ui" line="35"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../Forms/BatchExportQuickSelections.ui" line="55"/>
        <source>Close</source>
        <translation>Close</translation>
    </message>
</context>
<context>
    <name>WindowAboutPlugins</name>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="28"/>
        <source>Plugin Name</source>
        <translation>Plugin Name</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="28"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="28"/>
        <source>Exports</source>
        <translation>Exports</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="36"/>
        <source>Total Plugins: %1</source>
        <translation>Total Plugins: %1</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="37"/>
        <source>Texture Exporters: %1</source>
        <translation>Texture Exporters: %1</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="38"/>
        <source>Model Exporters: %1</source>
        <translation>Model Exporters: %1</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="39"/>
        <source>Placement Exporters: %1</source>
        <translation>Placement Exporters: %1</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="67"/>
        <source>Image Exporter</source>
        <translation>Image Exporter</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="120"/>
        <source>Model Exporter</source>
        <translation>Model Exporter</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="172"/>
        <source>Placement Exporter</source>
        <translation>Placement Exporter</translation>
    </message>
    <message>
        <location filename="../Windows/AboutPlugins.cpp" line="101"/>
        <location filename="../Windows/AboutPlugins.cpp" line="153"/>
        <location filename="../Windows/AboutPlugins.cpp" line="205"/>
        <source>, </source>
        <comment>Used for displaying multiple extensions used by a plugin. IE: png, gif, tga</comment>
        <translation>, </translation>
    </message>
</context>
<context>
    <name>WindowOptionsDialog</name>
    <message>
        <location filename="../Windows/OptionsDialog.cpp" line="164"/>
        <source>Select WoW Directory</source>
        <translation>Select WoW Directory</translation>
    </message>
    <message>
        <location filename="../Windows/OptionsDialog.cpp" line="178"/>
        <source>Select Output Directory</source>
        <translation>Select Output Directory</translation>
    </message>
</context>
<context>
    <name>widgetDetails</name>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="14"/>
        <source>File Details</source>
        <translation>File Details</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="20"/>
        <source>Texture Info</source>
        <translation>Texture Info</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="60"/>
        <location filename="../Forms/widgetFileDetails.ui" line="108"/>
        <source>px</source>
        <translation>px</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="76"/>
        <source>Channels:</source>
        <translation>Channels:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="89"/>
        <source>Width:</source>
        <translation>Width:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="124"/>
        <source>Alpha Channel:</source>
        <translation>Alpha Channel:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="137"/>
        <source>Height:</source>
        <translation>Height:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="154"/>
        <source>Textures</source>
        <translation>Textures</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="166"/>
        <location filename="../Forms/widgetFileDetails.ui" line="198"/>
        <location filename="../Forms/widgetFileDetails.ui" line="243"/>
        <source>Count:</source>
        <translation>Count:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="186"/>
        <source>World Model Objects</source>
        <translation>World Model Objects</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="224"/>
        <source>File Type:</source>
        <translation>File Type:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetFileDetails.ui" line="231"/>
        <source>Decoration Models</source>
        <translation>Decoration Models</translation>
    </message>
</context>
<context>
    <name>widgetFileDetails</name>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="20"/>
        <source>ADT Terrain File</source>
        <translation>ADT Terrain File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="23"/>
        <source>M2 Model File</source>
        <translation>M2 Model File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="26"/>
        <source>World Model File</source>
        <translation>World Model File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="29"/>
        <source>BLP Image File</source>
        <translation>BLP Image File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="32"/>
        <source>WDL Terrain File</source>
        <translation>WDL Terrain File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="35"/>
        <source>No File</source>
        <translation>No File</translation>
    </message>
    <message>
        <location filename="../Windows/widgetFileDetails.cpp" line="38"/>
        <source>Unknown file type</source>
        <translation>Unknown file type</translation>
    </message>
</context>
<context>
    <name>widgetInstances</name>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="14"/>
        <source>Instances</source>
        <translation>Instances</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="35"/>
        <source>WMO Instances</source>
        <translation>WMO Instances</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="54"/>
        <location filename="../Forms/widgetInstances.ui" line="154"/>
        <source>Name/ID</source>
        <extracomment>Either the file&apos;s name, or CASC file ID number.</extracomment>
        <translation>Name/ID</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="57"/>
        <location filename="../Forms/widgetInstances.ui" line="157"/>
        <source>Either the file&apos;s name, or CASC file ID number.</source>
        <translation>Either the file&apos;s name, or CASC file ID number.</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="62"/>
        <source>Doodad Set</source>
        <translation>Doodad Set</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="65"/>
        <source>The chosen WMO doodad set for this model.</source>
        <translation>The chosen WMO doodad set for this model.</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="70"/>
        <location filename="../Forms/widgetInstances.ui" line="162"/>
        <source>X</source>
        <extracomment>Position X</extracomment>
        <translation>X</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="73"/>
        <location filename="../Forms/widgetInstances.ui" line="165"/>
        <source>Position X</source>
        <translation>Position X</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="78"/>
        <location filename="../Forms/widgetInstances.ui" line="170"/>
        <source>Y</source>
        <extracomment>Position Y</extracomment>
        <translation>Y</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="81"/>
        <location filename="../Forms/widgetInstances.ui" line="173"/>
        <source>Position Y</source>
        <translation>Position Y</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="86"/>
        <location filename="../Forms/widgetInstances.ui" line="178"/>
        <source>Z</source>
        <extracomment>Position Z</extracomment>
        <translation>Z</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="89"/>
        <location filename="../Forms/widgetInstances.ui" line="181"/>
        <source>Position Z</source>
        <translation>Position Z</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="94"/>
        <location filename="../Forms/widgetInstances.ui" line="186"/>
        <source>Yaw</source>
        <extracomment>Rotation along the Y Axis</extracomment>
        <translation>Yaw</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="97"/>
        <location filename="../Forms/widgetInstances.ui" line="189"/>
        <source>Rotation along the Y Axis</source>
        <translation>Rotation along the Y Axis</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="102"/>
        <location filename="../Forms/widgetInstances.ui" line="194"/>
        <source>Pitch</source>
        <extracomment>Rotation along the X Axis</extracomment>
        <translation>Pitch</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="105"/>
        <location filename="../Forms/widgetInstances.ui" line="197"/>
        <source>Rotation along the X Axis</source>
        <translation>Rotation along the X Axis</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="110"/>
        <location filename="../Forms/widgetInstances.ui" line="202"/>
        <source>Bank</source>
        <extracomment>Rotation along the Z Axis</extracomment>
        <translation>Bank</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="113"/>
        <location filename="../Forms/widgetInstances.ui" line="205"/>
        <source>Rotation along the Z Axis</source>
        <translation>Rotation along the Z Axis</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="118"/>
        <location filename="../Forms/widgetInstances.ui" line="210"/>
        <source>Scale</source>
        <extracomment>The scale of the model</extracomment>
        <translation>Scale</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="121"/>
        <location filename="../Forms/widgetInstances.ui" line="213"/>
        <source>The scale of the model</source>
        <translation>The scale of the model</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="132"/>
        <source>Doodads</source>
        <translation>Doodads</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="227"/>
        <source>Doodad Set:</source>
        <translation>Doodad Set:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="251"/>
        <location filename="../Forms/widgetInstances.ui" line="365"/>
        <source>Y: </source>
        <translation>Y: </translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="273"/>
        <location filename="../Forms/widgetInstances.ui" line="387"/>
        <source>Z: </source>
        <translation>Z: </translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="289"/>
        <source>Rotation:</source>
        <translation>Rotation:</translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="308"/>
        <location filename="../Forms/widgetInstances.ui" line="343"/>
        <source>X: </source>
        <translation>X: </translation>
    </message>
    <message>
        <location filename="../Forms/widgetInstances.ui" line="324"/>
        <source>Position:</source>
        <translation>Position:</translation>
    </message>
</context>
</TS>
