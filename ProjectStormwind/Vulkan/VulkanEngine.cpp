#include "VulkanEngine.h"
#include <Foundation/LogFile.h>
#include <QtGui/QVulkanInstance>
#include <QtGui/QVulkanFunctions>
#include <QLoggingCategory>
#include <chrono>

void VulkanEngine::init()
{
  m_vkSurface = QVulkanInstance::surfaceForWindow(this);
  if (!m_vkSurface)
    qFatal("Failed to get surface for window");

  QVulkanInstance *inst = vulkanInstance();
  QVulkanFunctions *f = inst->functions();
  uint32_t gpuCount = 0;
  f->vkEnumeratePhysicalDevices(inst->vkInstance(), &gpuCount, nullptr);
  qDebug("%d physical devices", gpuCount);
  if (!gpuCount)
    qFatal("No physical devices");

  std::vector<VkPhysicalDevice> physicalDevices(gpuCount);
  VkResult err = f->vkEnumeratePhysicalDevices(inst->vkInstance(), &gpuCount, physicalDevices.data());
  if (err != VK_SUCCESS)
    qFatal("Failed to enumerate physical devices: %d", err);

  uint32_t selectedDevice = 0;
  // Code here in the future to allow users to select their GPU.
  physicalDevice = physicalDevices[selectedDevice];

  // Store properties (including limits), features and memory properties of the phyiscal device (so that examples can check against them)
  vkGetPhysicalDeviceProperties(physicalDevice, &deviceProperties);
  vkGetPhysicalDeviceFeatures(physicalDevice, &deviceFeatures);
  vkGetPhysicalDeviceMemoryProperties(physicalDevice, &deviceMemoryProperties);
  qDebug("Device name: %s Driver version: %d.%d.%d", deviceProperties.deviceName,
    VK_VERSION_MAJOR(deviceProperties.driverVersion), VK_VERSION_MINOR(deviceProperties.driverVersion),
    VK_VERSION_PATCH(deviceProperties.driverVersion));

  getEnabledFeatures();

  vulkanDevice = new vks::VulkanDevice(physicalDevice);
  VkResult res = vulkanDevice->createLogicalDevice(enabledFeatures, enabledDeviceExtensions);
  if (res != VK_SUCCESS) {
    vks::tools::exitFatal("Could not create Vulkan device: \n" + vks::tools::errorString(res), res);
    return;
  }
  device = vulkanDevice->logicalDevice;
  m_devFuncs = inst->deviceFunctions(device);

  // Get a graphics queue from the device
  vkGetDeviceQueue(device, vulkanDevice->queueFamilyIndices.graphics, 0, &m_vkGraphicsQueue);

  VkBool32 validDepthFormat = vks::tools::getSupportedDepthFormat(physicalDevice, &depthFormat);
  assert(validDepthFormat);

  // Create synchronization objects
  VkSemaphoreCreateInfo semaphoreCreateInfo = vks::initializers::semaphoreCreateInfo();
  // Create a semaphore used to synchronize image presentation
  // Ensures that the image is displayed before we start submitting new commands to the queu
  VK_CHECK_RESULT(vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &semaphores.presentComplete));
  // Create a semaphore used to synchronize command submission
  // Ensures that the image is not presented until all commands have been sumbitted and executed
  VK_CHECK_RESULT(vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &semaphores.renderComplete));
  // Create a semaphore used to synchronize command submission
  // Ensures that the image is not presented until all commands for the UI overlay have been sumbitted and executed
  // Will be inserted after the render complete semaphore if the UI overlay is enabled
  VK_CHECK_RESULT(vkCreateSemaphore(device, &semaphoreCreateInfo, nullptr, &semaphores.overlayComplete));

  // Set up submit info structure
  // Semaphores will stay the same during application lifetime
  // Command buffer submission info is set by each example
  submitInfo = vks::initializers::submitInfo();
  submitInfo.pWaitDstStageMask = &submitPipelineStages;
  submitInfo.waitSemaphoreCount = 1;
  submitInfo.pWaitSemaphores = &semaphores.presentComplete;
  submitInfo.signalSemaphoreCount = 1;
  submitInfo.pSignalSemaphores = &semaphores.renderComplete;
}

void VulkanEngine::initSwapChain()
{
  QVulkanInstance *inst = vulkanInstance();
  QVulkanFunctions *f = inst->functions();

  uint32_t queueCount = 0;
  f->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, nullptr);
  QVector<VkQueueFamilyProperties> queueFamilyProps(queueCount);
  f->vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, queueFamilyProps.data());
  int gfxQueueFamilyIdx = -1;
  int presQueueFamilyIdx = -1;
  // First look for a queue that supports both.
  for (int i = 0; i < queueFamilyProps.count(); ++i) {
    qDebug("queue family %d: flags=0x%x count=%d", i, queueFamilyProps[i].queueFlags, queueFamilyProps[i].queueCount);
    if (gfxQueueFamilyIdx == -1
      && (queueFamilyProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
      && inst->supportsPresent(physicalDevice, i, this))
      gfxQueueFamilyIdx = i;
  }
  if (gfxQueueFamilyIdx != -1) {
    presQueueFamilyIdx = gfxQueueFamilyIdx;
  }
  else {
    // Separate queues then.
    qDebug("No queue with graphics+present; trying separate queues");
    for (int i = 0; i < queueFamilyProps.count(); ++i) {
      if (gfxQueueFamilyIdx == -1 && (queueFamilyProps[i].queueFlags & VK_QUEUE_GRAPHICS_BIT))
        gfxQueueFamilyIdx = i;
      if (presQueueFamilyIdx == -1 && inst->supportsPresent(physicalDevice, i, this))
        presQueueFamilyIdx = i;
    }
  }
  if (gfxQueueFamilyIdx == -1)
    qFatal("No graphics queue family found");
  if (presQueueFamilyIdx == -1)
    qFatal("No present queue family found");

  graphicsQueueNodeIndex = gfxQueueFamilyIdx;

  VkDeviceQueueCreateInfo queueInfo[2];
  const float prio[] = { 0 };
  memset(queueInfo, 0, sizeof(queueInfo));
  queueInfo[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
  queueInfo[0].queueFamilyIndex = gfxQueueFamilyIdx;
  queueInfo[0].queueCount = 1;
  queueInfo[0].pQueuePriorities = prio;
  if (gfxQueueFamilyIdx != presQueueFamilyIdx) {
    queueInfo[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueInfo[1].queueFamilyIndex = presQueueFamilyIdx;
    queueInfo[1].queueCount = 1;
    queueInfo[1].pQueuePriorities = prio;
  }

  QVector<const char *> devLayers;
  if (inst->layers().contains("VK_LAYER_LUNARG_standard_validation"))
    devLayers.append("VK_LAYER_LUNARG_standard_validation");

  QVector<const char *> devExts;
  devExts.append("VK_KHR_swapchain");

  VkDeviceCreateInfo devInfo;
  memset(&devInfo, 0, sizeof(devInfo));
  devInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
  devInfo.queueCreateInfoCount = gfxQueueFamilyIdx == presQueueFamilyIdx ? 1 : 2;
  devInfo.pQueueCreateInfos = queueInfo;
  devInfo.enabledLayerCount = devLayers.count();
  devInfo.ppEnabledLayerNames = devLayers.constData();
  devInfo.enabledExtensionCount = devExts.count();
  devInfo.ppEnabledExtensionNames = devExts.constData();

  m_devFuncs->vkGetDeviceQueue(device, gfxQueueFamilyIdx, 0, &m_vkGraphicsQueue);
  if (gfxQueueFamilyIdx == presQueueFamilyIdx)
    m_vkPresQueue = m_vkGraphicsQueue;
  else
    m_devFuncs->vkGetDeviceQueue(device, presQueueFamilyIdx, 0, &m_vkPresQueue);

  swapChainColorFormat = VK_FORMAT_B8G8R8A8_UNORM; // may get changed later when setting up the swapchain
}

void VulkanEngine::releaseSwapChain()
{
  if (!device)
    return;

  vkDeviceWaitIdle(device);

  if (renderPass) {
    vkDestroyRenderPass(device, renderPass, nullptr);
    renderPass = VK_NULL_HANDLE;
  }

  for (uint32_t i = 0; i < frameBuffers.size(); i++)
  {
    if (frameBuffers[i])
    {
      vkDestroyFramebuffer(device, frameBuffers[i], nullptr);
      frameBuffers[i] = VK_NULL_HANDLE;
    }
  }

  if (descriptorPool)
  {
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
    descriptorPool = VK_NULL_HANDLE;
  }

  destroyCommandBuffers();

  if (cmdPool)
  {
    vkDestroyCommandPool(device, cmdPool, nullptr);
    cmdPool = VK_NULL_HANDLE;
  }

  for (uint32_t i = 0; i < swapChainBuffers.size(); i++) {
    if (swapChainBuffers[i].view != VK_NULL_HANDLE)
    {
      vkDestroyImageView(device, swapChainBuffers[i].view, nullptr);
      swapChainBuffers[i].view = VK_NULL_HANDLE;
    }
  }

  if (m_swapChain) {
    m_vkDestroySwapchainKHR(device, m_swapChain, nullptr);
    m_swapChain = VK_NULL_HANDLE;
  }

  prepared = false;
}

void VulkanEngine::windowResize()
{
  if (!prepared)
  {
    return;
  }
  prepared = false;

  // Ensure all operations on the device have been finished before destroying resources
  vkDeviceWaitIdle(device);

  releaseResources();

  // Recreate the frame buffers
  vkDestroyImageView(device, depthStencil.view, nullptr);
  vkDestroyImage(device, depthStencil.image, nullptr);
  vkFreeMemory(device, depthStencil.mem, nullptr);
  for (uint32_t i = 0; i < frameBuffers.size(); i++) {
    vkDestroyFramebuffer(device, frameBuffers[i], nullptr);
  }

  camera.updateAspectRatio((float)m_swapChainImageSize.width() / (float)m_swapChainImageSize.height());

  requestUpdate();

  windowResized();
}

void VulkanEngine::exposeEvent(QExposeEvent *)
{
  if (isExposed() && !m_inited) {
    qDebug("initializing");
    LOGFILE.Info("Initializing Vulkan...");
    m_inited = true;
    init();
    prepare();
    renderFrame();
  }

  // Release everything when unexposed - the meaning of which is platform specific.
  // Can be essential on mobile, to release resources while in background.
#if 0
  if (!isExposed() && m_inited) {
    m_inited = false;
    releaseResources();
  }
#endif
}

void VulkanEngine::resizeEvent(QResizeEvent *)
{
  // Nothing to do here - recreating the swapchain is handled in render(), in fact calling recreateSwapChain() from here leads to problems.
}

bool VulkanEngine::event(QEvent *e)
{
  switch (e->type()) {
  case QEvent::UpdateRequest:
    renderFrame();
    break;

    // Now the fun part: the swapchain must be destroyed before the surface as per
    // spec. This is not ideal for us because the surface is managed by the
    // QPlatformWindow which may be gone already when the unexpose comes, making the
    // validation layer scream. The solution is to listen to the PlatformSurface events.
  case QEvent::PlatformSurface:
    if (static_cast<QPlatformSurfaceEvent *>(e)->surfaceEventType() == QPlatformSurfaceEvent::SurfaceAboutToBeDestroyed)
      releaseResources();
    break;

  default:
    break;
  }

  return QWindow::event(e);
}

void VulkanEngine::mousePressEvent(QMouseEvent *e)
{
  if (ButtonsPressed.contains(e->button()) == true)
  {
    return;
  }
  ButtonsPressed.push_back(e->button());
  m_lastPos = e->pos();
}

void VulkanEngine::mouseReleaseEvent(QMouseEvent *e)
{
  if (ButtonsPressed.contains(e->button()) != true)
  {
    return;
  }
  ButtonsPressed.takeAt(ButtonsPressed.indexOf(e->button()));
}

void VulkanEngine::mouseMoveEvent(QMouseEvent *e)
{
  if (ButtonsPressed.count() <= 0)
    return;

  int dx = e->pos().x() - m_lastPos.x();
  int dy = e->pos().y() - m_lastPos.y();

  if (ButtonsPressed.contains(Qt::MouseButton::LeftButton))
  {
    rotation.x -= dy * 0.5f * rotationSpeed;
    rotation.y += dx * 0.5f * rotationSpeed;
    camera.rotate(glm::vec3(dy * camera.rotationSpeed, dx * camera.rotationSpeed, 0.0f));
    viewUpdated = true;
  }
  else if (ButtonsPressed.contains(Qt::MouseButton::MiddleButton))
  {
    cameraPos.x -= dx * 0.005f * moveSpeed * (zoom * 0.1);
    cameraPos.y -= dy * 0.005f * moveSpeed * (zoom * 0.1);
    camera.translate(glm::vec3(-dx * 0.01f, -dy * 0.01f, 0.0f));
    viewUpdated = true;
  }
  else if (ButtonsPressed.contains(Qt::MouseButton::RightButton))
  {
    zoom -= dx * 0.1f * zoomSpeed;
    zoom -= dy * 0.05f * zoomSpeed;
    camera.translate(glm::vec3(-0.0f, 0.0f, dy * .005f * zoomSpeed));
    viewUpdated = true;
  }

  m_lastPos = e->pos();
}

VulkanEngine::VulkanEngine()
{
  setSurfaceType(VulkanSurface);
}

VulkanEngine::~VulkanEngine()
{
  releaseResources();

  if (descriptorPool != VK_NULL_HANDLE)
  {
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
  }
  destroyCommandBuffers();
  vkDestroyRenderPass(device, renderPass, nullptr);
  for (uint32_t i = 0; i < frameBuffers.size(); i++)
  {
    vkDestroyFramebuffer(device, frameBuffers[i], nullptr);
  }

  for (auto& shaderModule : shaderModules)
  {
    vkDestroyShaderModule(device, shaderModule, nullptr);
  }

  vkDestroyImageView(device, depthStencil.view, nullptr);
  vkDestroyImage(device, depthStencil.image, nullptr);
  vkFreeMemory(device, depthStencil.mem, nullptr);

  vkDestroyPipelineCache(device, pipelineCache, nullptr);

  if (cmdPool != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(device, cmdPool, nullptr);
    cmdPool = VK_NULL_HANDLE;
  }

  vkDestroySemaphore(device, semaphores.presentComplete, nullptr);
  vkDestroySemaphore(device, semaphores.renderComplete, nullptr);
  vkDestroySemaphore(device, semaphores.overlayComplete, nullptr);
  for (auto& fence : waitFences) {
    vkDestroyFence(device, fence, nullptr);
  }

  if (device) {
    m_devFuncs->vkDestroyDevice(device, nullptr);

    // Play nice and notify QVulkanInstance that the QVulkanDeviceFunctions
    // for device needs to be invalidated.
    vulkanInstance()->resetDeviceFunctions(device);

    device = VK_NULL_HANDLE;
  }

  // LAST!!
  m_vkSurface = VK_NULL_HANDLE;
}

void VulkanEngine::viewChanged() {}

void VulkanEngine::windowResized()
{
  prepare();
}

void VulkanEngine::buildCommandBuffers() {}

void VulkanEngine::createSynchronizationPrimitives()
{
  VkFenceCreateInfo fenceCreateInfo = vks::initializers::fenceCreateInfo(VK_FENCE_CREATE_SIGNALED_BIT);
  waitFences.resize(m_swapChainBufferCount);
  for (auto& fence : waitFences) {
    VK_CHECK_RESULT(vkCreateFence(device, &fenceCreateInfo, nullptr, &fence));
  }
}

void VulkanEngine::createCommandPool()
{
  VkCommandPoolCreateInfo cmdPoolInfo = {};
  cmdPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
  cmdPoolInfo.queueFamilyIndex = graphicsQueueNodeIndex;
  cmdPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
  VK_CHECK_RESULT(vkCreateCommandPool(device, &cmdPoolInfo, nullptr, &cmdPool));
}

void VulkanEngine::setupDepthStencil()
{
  VkImageCreateInfo image = {};
  image.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
  image.pNext = NULL;
  image.imageType = VK_IMAGE_TYPE_2D;
  image.format = depthFormat;
  image.extent = { static_cast<uint32_t>(m_swapChainImageSize.width()), static_cast<uint32_t>(m_swapChainImageSize.height()), 1 };
  image.mipLevels = 1;
  image.arrayLayers = 1;
  image.samples = VK_SAMPLE_COUNT_1_BIT;
  image.tiling = VK_IMAGE_TILING_OPTIMAL;
  image.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
  image.flags = 0;

  VkMemoryAllocateInfo mem_alloc = {};
  mem_alloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
  mem_alloc.pNext = NULL;
  mem_alloc.allocationSize = 0;
  mem_alloc.memoryTypeIndex = 0;

  VkImageViewCreateInfo depthStencilView = {};
  depthStencilView.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
  depthStencilView.pNext = NULL;
  depthStencilView.viewType = VK_IMAGE_VIEW_TYPE_2D;
  depthStencilView.format = depthFormat;
  depthStencilView.flags = 0;
  depthStencilView.subresourceRange = {};
  depthStencilView.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
  depthStencilView.subresourceRange.baseMipLevel = 0;
  depthStencilView.subresourceRange.levelCount = 1;
  depthStencilView.subresourceRange.baseArrayLayer = 0;
  depthStencilView.subresourceRange.layerCount = 1;

  VkMemoryRequirements memReqs;

  VK_CHECK_RESULT(vkCreateImage(device, &image, nullptr, &depthStencil.image));
  vkGetImageMemoryRequirements(device, depthStencil.image, &memReqs);
  mem_alloc.allocationSize = memReqs.size;
  mem_alloc.memoryTypeIndex = vulkanDevice->getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
  VK_CHECK_RESULT(vkAllocateMemory(device, &mem_alloc, nullptr, &depthStencil.mem));
  VK_CHECK_RESULT(vkBindImageMemory(device, depthStencil.image, depthStencil.mem, 0));

  depthStencilView.image = depthStencil.image;
  VK_CHECK_RESULT(vkCreateImageView(device, &depthStencilView, nullptr, &depthStencil.view));
}

void VulkanEngine::setupFrameBuffer()
{
  VkImageView attachments[2];

  // Depth/Stencil attachment is the same for all frame buffers
  attachments[1] = depthStencil.view;

  VkFramebufferCreateInfo frameBufferCreateInfo = {};
  frameBufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
  frameBufferCreateInfo.pNext = NULL;
  frameBufferCreateInfo.renderPass = renderPass;
  frameBufferCreateInfo.attachmentCount = 2;
  frameBufferCreateInfo.pAttachments = attachments;
  frameBufferCreateInfo.width = m_swapChainImageSize.width();
  frameBufferCreateInfo.height = m_swapChainImageSize.height();
  frameBufferCreateInfo.layers = 1;

  // Create frame buffers for every swap chain image
  frameBuffers.resize(m_swapChainBufferCount);
  for (uint32_t i = 0; i < frameBuffers.size(); i++)
  {
    attachments[0] = swapChainBuffers[i].view;
    VK_CHECK_RESULT(vkCreateFramebuffer(device, &frameBufferCreateInfo, nullptr, &frameBuffers[i]));
  }
}

void VulkanEngine::setupRenderPass()
{
  std::array<VkAttachmentDescription, 2> attachments = {};
  // Color attachment
  attachments[0].format = swapChainColorFormat;
  attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
  attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
  attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attachments[0].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attachments[0].finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
  // Depth attachment
  attachments[1].format = depthFormat;
  attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
  attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
  attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
  attachments[1].initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkAttachmentReference colorReference = {};
  colorReference.attachment = 0;
  colorReference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

  VkAttachmentReference depthReference = {};
  depthReference.attachment = 1;
  depthReference.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

  VkSubpassDescription subpassDescription = {};
  subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
  subpassDescription.colorAttachmentCount = 1;
  subpassDescription.pColorAttachments = &colorReference;
  subpassDescription.pDepthStencilAttachment = &depthReference;
  subpassDescription.inputAttachmentCount = 0;
  subpassDescription.pInputAttachments = nullptr;
  subpassDescription.preserveAttachmentCount = 0;
  subpassDescription.pPreserveAttachments = nullptr;
  subpassDescription.pResolveAttachments = nullptr;

  // Subpass dependencies for layout transitions
  std::array<VkSubpassDependency, 2> dependencies;

  dependencies[0].srcSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[0].dstSubpass = 0;
  dependencies[0].srcStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
  dependencies[0].dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[0].srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
  dependencies[0].dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[0].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  dependencies[1].srcSubpass = 0;
  dependencies[1].dstSubpass = VK_SUBPASS_EXTERNAL;
  dependencies[1].srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  dependencies[1].dstStageMask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
  dependencies[1].srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
  dependencies[1].dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;
  dependencies[1].dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

  VkRenderPassCreateInfo renderPassInfo = {};
  renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
  renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
  renderPassInfo.pAttachments = attachments.data();
  renderPassInfo.subpassCount = 1;
  renderPassInfo.pSubpasses = &subpassDescription;
  renderPassInfo.dependencyCount = static_cast<uint32_t>(dependencies.size());
  renderPassInfo.pDependencies = dependencies.data();

  VK_CHECK_RESULT(vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass));
}

void VulkanEngine::getEnabledFeatures()
{
  // Permanent features go here. Can be overridden by derived classes

  // Fill mode non solid is required for wireframe display
  if (deviceFeatures.fillModeNonSolid) {
    enabledFeatures.fillModeNonSolid = VK_TRUE;
    // Wide lines must be present for line width > 1.0f
    if (deviceFeatures.wideLines) {
      enabledFeatures.wideLines = VK_TRUE;
    }
  };
}

void VulkanEngine::recreateSwapChain()
{
  m_swapChainImageSize = size();

  if (m_swapChainImageSize.isEmpty())
    return;

  QVulkanInstance *inst = vulkanInstance();
  QVulkanFunctions *f = inst->functions();
  m_devFuncs->vkDeviceWaitIdle(device);

  if (!m_vkCreateSwapchainKHR) {
    m_vkGetPhysicalDeviceSurfaceCapabilitiesKHR = reinterpret_cast<PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR>(
      inst->getInstanceProcAddr("vkGetPhysicalDeviceSurfaceCapabilitiesKHR"));
    m_vkGetPhysicalDeviceSurfaceFormatsKHR = reinterpret_cast<PFN_vkGetPhysicalDeviceSurfaceFormatsKHR>(
      inst->getInstanceProcAddr("vkGetPhysicalDeviceSurfaceFormatsKHR"));
    // note: device-specific functions
    m_vkCreateSwapchainKHR = reinterpret_cast<PFN_vkCreateSwapchainKHR>(f->vkGetDeviceProcAddr(device, "vkCreateSwapchainKHR"));
    m_vkDestroySwapchainKHR = reinterpret_cast<PFN_vkDestroySwapchainKHR>(f->vkGetDeviceProcAddr(device, "vkDestroySwapchainKHR"));
    m_vkGetSwapchainImagesKHR = reinterpret_cast<PFN_vkGetSwapchainImagesKHR>(f->vkGetDeviceProcAddr(device, "vkGetSwapchainImagesKHR"));
    m_vkAcquireNextImageKHR = reinterpret_cast<PFN_vkAcquireNextImageKHR>(f->vkGetDeviceProcAddr(device, "vkAcquireNextImageKHR"));
    m_vkQueuePresentKHR = reinterpret_cast<PFN_vkQueuePresentKHR>(f->vkGetDeviceProcAddr(device, "vkQueuePresentKHR"));
  }

  VkColorSpaceKHR colorSpace = VkColorSpaceKHR(0);
  uint32_t formatCount = 0;
  m_vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_vkSurface, &formatCount, nullptr);
  if (formatCount) {
    QVector<VkSurfaceFormatKHR> formats(formatCount);
    m_vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, m_vkSurface, &formatCount, formats.data());
    if (formats[0].format != VK_FORMAT_UNDEFINED) {
      swapChainColorFormat = formats[0].format;
      colorSpace = formats[0].colorSpace;
    }
  }

  VkSurfaceCapabilitiesKHR surfaceCaps;
  m_vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, m_vkSurface, &surfaceCaps);
  uint32_t reqBufferCount = SWAPCHAIN_BUFFER_COUNT;
  if (surfaceCaps.maxImageCount)
    reqBufferCount = qBound(surfaceCaps.minImageCount, reqBufferCount, surfaceCaps.maxImageCount);

  VkExtent2D bufferSize = surfaceCaps.currentExtent;
  if (bufferSize.width == uint32_t(-1))
    bufferSize.width = m_swapChainImageSize.width();
  if (bufferSize.height == uint32_t(-1))
    bufferSize.height = m_swapChainImageSize.height();

  VkSurfaceTransformFlagBitsKHR preTransform =
    (surfaceCaps.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR)
    ? VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR
    : surfaceCaps.currentTransform;

  VkCompositeAlphaFlagBitsKHR compositeAlpha =
    (surfaceCaps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR)
    ? VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR
    : VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

  VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;

  VkSwapchainKHR oldSwapChain = m_swapChain;
  VkSwapchainCreateInfoKHR swapChainInfo;
  memset(&swapChainInfo, 0, sizeof(swapChainInfo));
  swapChainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
  swapChainInfo.surface = m_vkSurface;
  swapChainInfo.minImageCount = reqBufferCount;
  swapChainInfo.imageFormat = swapChainColorFormat;
  swapChainInfo.imageColorSpace = colorSpace;
  swapChainInfo.imageExtent = bufferSize;
  swapChainInfo.imageArrayLayers = 1;
  swapChainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
  swapChainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
  swapChainInfo.preTransform = preTransform;
  swapChainInfo.compositeAlpha = compositeAlpha;
  swapChainInfo.presentMode = presentMode;
  swapChainInfo.clipped = true;
  swapChainInfo.oldSwapchain = oldSwapChain;

  qDebug("creating new swap chain of %d buffers, size %dx%d", reqBufferCount, bufferSize.width, bufferSize.height);

  VkSwapchainKHR newSwapChain;
  VkResult err = m_vkCreateSwapchainKHR(device, &swapChainInfo, nullptr, &newSwapChain);
  if (err != VK_SUCCESS)
    qFatal("Failed to create swap chain: %d", err);

  if (oldSwapChain)
    releaseSwapChain();

  m_swapChain = newSwapChain;

  m_swapChainBufferCount = 0;
  VK_CHECK_RESULT(m_vkGetSwapchainImagesKHR(device, m_swapChain, &m_swapChainBufferCount, nullptr));

  qDebug("actual swap chain buffer count: %d", m_swapChainBufferCount);
  Q_ASSERT(m_swapChainBufferCount <= SWAPCHAIN_BUFFER_COUNT);

  VkImage swapChainImages[SWAPCHAIN_BUFFER_COUNT];
  VK_CHECK_RESULT(m_vkGetSwapchainImagesKHR(device, m_swapChain, &m_swapChainBufferCount, swapChainImages));

  swapChainBuffers.resize(m_swapChainBufferCount);
  for (uint32_t i = 0; i < m_swapChainBufferCount; ++i) {
    SwapChainBuffer &image(swapChainBuffers[i]);
    image.image = swapChainImages[i];

    VkImageViewCreateInfo imgViewInfo;
    memset(&imgViewInfo, 0, sizeof(imgViewInfo));
    imgViewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imgViewInfo.image = swapChainImages[i];
    imgViewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    imgViewInfo.format = swapChainColorFormat;
    imgViewInfo.components.r = VK_COMPONENT_SWIZZLE_R;
    imgViewInfo.components.g = VK_COMPONENT_SWIZZLE_G;
    imgViewInfo.components.b = VK_COMPONENT_SWIZZLE_B;
    imgViewInfo.components.a = VK_COMPONENT_SWIZZLE_A;
    imgViewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    imgViewInfo.subresourceRange.levelCount = imgViewInfo.subresourceRange.layerCount = 1;
    VK_CHECK_RESULT(m_devFuncs->vkCreateImageView(device, &imgViewInfo, nullptr, &image.view));
  }
}

bool VulkanEngine::checkCommandBuffers()
{
  for (auto& cmdBuffer : drawCmdBuffers)
  {
    if (cmdBuffer == VK_NULL_HANDLE)
    {
      return false;
    }
  }
  return true;
}

void VulkanEngine::createCommandBuffers()
{
  // Create one command buffer for each swap chain image and reuse for rendering
  drawCmdBuffers.resize(m_swapChainBufferCount);

  VkCommandBufferAllocateInfo cmdBufAllocateInfo =
    vks::initializers::commandBufferAllocateInfo(
      cmdPool,
      VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      static_cast<uint32_t>(drawCmdBuffers.size()));

  VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &cmdBufAllocateInfo, drawCmdBuffers.data()));
}

void VulkanEngine::destroyCommandBuffers()
{
  if (cmdPool)
    vkFreeCommandBuffers(device, cmdPool, static_cast<uint32_t>(drawCmdBuffers.size()), drawCmdBuffers.data());
}

VkCommandBuffer VulkanEngine::createCommandBuffer(VkCommandBufferLevel level, bool begin)
{
  VkCommandBuffer cmdBuffer;

  VkCommandBufferAllocateInfo cmdBufAllocateInfo =
    vks::initializers::commandBufferAllocateInfo(
      cmdPool,
      level,
      1);

  VK_CHECK_RESULT(vkAllocateCommandBuffers(device, &cmdBufAllocateInfo, &cmdBuffer));

  // If requested, also start the new command buffer
  if (begin)
  {
    VkCommandBufferBeginInfo cmdBufInfo = vks::initializers::commandBufferBeginInfo();
    VK_CHECK_RESULT(vkBeginCommandBuffer(cmdBuffer, &cmdBufInfo));
  }

  return cmdBuffer;
}

void VulkanEngine::flushCommandBuffer(VkCommandBuffer commandBuffer, VkQueue queue, bool free)
{
  if (commandBuffer == VK_NULL_HANDLE)
  {
    return;
  }

  VK_CHECK_RESULT(vkEndCommandBuffer(commandBuffer));

  VkSubmitInfo submitI = {};
  submitI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
  submitI.commandBufferCount = 1;
  submitI.pCommandBuffers = &commandBuffer;

  VK_CHECK_RESULT(vkQueueSubmit(queue, 1, &submitI, VK_NULL_HANDLE));
  VK_CHECK_RESULT(vkQueueWaitIdle(queue));

  if (free)
  {
    vkFreeCommandBuffers(device, cmdPool, 1, &commandBuffer);
  }
}

void VulkanEngine::createPipelineCache()
{
  VkPipelineCacheCreateInfo pipelineCacheCreateInfo = {};
  pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
  VK_CHECK_RESULT(vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache));
}

void VulkanEngine::releaseResources()
{
  releaseSwapChain();
}

void VulkanEngine::prepare()
{
  initSwapChain();
  createCommandPool();
  recreateSwapChain();
  createCommandBuffers();
  createSynchronizationPrimitives();
  setupDepthStencil();
  setupRenderPass();
  createPipelineCache();
  setupFrameBuffer();
}

void VulkanEngine::renderFrame()
{
  if (!m_swapChain)
    return;

  if (size() != m_swapChainImageSize) {
    windowResize();
    if (!m_swapChain)
      return;
  }

  auto tStart = std::chrono::high_resolution_clock::now();
  if (viewUpdated)
  {
    viewUpdated = false;
    viewChanged();
  }

  render();
  frameCounter++;
  auto tEnd = std::chrono::high_resolution_clock::now();
  auto tDiff = std::chrono::duration<double, std::milli>(tEnd - tStart).count();
  frameTimer = (float)tDiff / 1000.0f;
  camera.update(frameTimer);
  if (camera.moving())
  {
    viewUpdated = true;
  }
  // Convert to clamped timer value
  if (!paused)
  {
    timer += timerSpeed * frameTimer;
    if (timer > 1.0)
    {
      timer -= 1.0f;
    }
  }
  fpsTimer += (float)tDiff;
  if (fpsTimer > 1000.0f)
  {
    lastFPS = static_cast<uint32_t>((float)frameCounter * (1000.0f / fpsTimer));
    fpsTimer = 0.0f;
    frameCounter = 0;
  }
}

void VulkanEngine::prepareFrame()
{
  // Acquire the next image from the swap chain
  VkResult err = m_vkAcquireNextImageKHR(device, m_swapChain, UINT64_MAX, semaphores.presentComplete, (VkFence)nullptr, &currentBuffer);
  // Recreate the swapchain if it's no longer compatible with the surface (OUT_OF_DATE) or no longer optimal for presentation (SUBOPTIMAL)
  if ((err == VK_ERROR_OUT_OF_DATE_KHR) || (err == VK_SUBOPTIMAL_KHR)) {
    windowResize();
  }
  else {
    VK_CHECK_RESULT(err);
  }
}

void VulkanEngine::submitFrame()
{
  bool submitOverlay = false; // settings.overlay && UIOverlay->visible;

  /*
  if (submitOverlay) {
    // Wait for color attachment output to finish before rendering the text overlay
    VkPipelineStageFlags stageFlags = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    submitInfo.pWaitDstStageMask = &stageFlags;

    // Set semaphores
    // Wait for render complete semaphore
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &semaphores.renderComplete;
    // Signal ready with UI overlay complete semaphpre
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &semaphores.overlayComplete;

    // Submit current UI overlay command buffer
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &UIOverlay->cmdBuffers[currentBuffer];
    VK_CHECK_RESULT(vkQueueSubmit(m_vkGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

    // Reset stage mask
    submitInfo.pWaitDstStageMask = &submitPipelineStages;
    // Reset wait and signal semaphores for rendering next frame
    // Wait for swap chain presentation to finish
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &semaphores.presentComplete;
    // Signal ready with offscreen semaphore
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &semaphores.renderComplete;
  }
  */

  VkPresentInfoKHR presentInfo = {};
  presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
  presentInfo.pNext = NULL;
  presentInfo.swapchainCount = 1;
  presentInfo.pSwapchains = &m_swapChain;
  presentInfo.pImageIndices = &currentBuffer;
  // Check if a wait semaphore has been specified to wait for before presenting the image
  if ((submitOverlay ? semaphores.overlayComplete : semaphores.renderComplete) != VK_NULL_HANDLE)
  {
    presentInfo.pWaitSemaphores = &(submitOverlay ? semaphores.overlayComplete : semaphores.renderComplete);
    presentInfo.waitSemaphoreCount = 1;
  }

  VkResult err = m_vkQueuePresentKHR(m_vkGraphicsQueue, &presentInfo);

  if (!((err == VK_SUCCESS) || (err == VK_SUBOPTIMAL_KHR))) {
    if (err == VK_ERROR_OUT_OF_DATE_KHR) {
      // Swap chain is no longer compatible with the surface and needs to be recreated
      windowResize();
      return;
    }
    else {
      VK_CHECK_RESULT(err);
    }
  }

  VK_CHECK_RESULT(vkQueueWaitIdle(m_vkGraphicsQueue));
}

VkPipelineShaderStageCreateInfo VulkanEngine::loadShader(QString fileName, VkShaderStageFlagBits stage)
{
  VkPipelineShaderStageCreateInfo shaderStage = {};
  shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
  shaderStage.stage = stage;
#if defined(VK_USE_PLATFORM_ANDROID_KHR)
  shaderStage.module = vks::tools::loadShader(androidApp->activity->assetManager, fileName.toStdString().c_str(), device);
#else
  shaderStage.module = vks::tools::loadShader(fileName, device);
#endif
  shaderStage.pName = "main"; // todo : make param
  assert(shaderStage.module != VK_NULL_HANDLE);
  shaderModules.push_back(shaderStage.module);
  return shaderStage;
}