#pragma once
#include <Vulkan/vulkan.hpp>
#include <QtGui/QWindow>
#include <QtGui/qevent.h>
#include <glm/glm.hpp>
#include <Foundation/Classes/ImageData.h>
#include <Foundation/Classes/RGBAColor.h>
#include <Foundation/Classes/Vertex.h>
#include <Foundation/Vulkan/VulkanBuffer.hpp>
#include <Foundation/Vulkan/VulkanDevice.hpp>
#include <Foundation/Vulkan/Camera.hpp>
#include <Foundation/Vulkan/UniformBufferObject.h>

static const int SWAPCHAIN_BUFFER_COUNT = 2;
static const int FRAME_LAG = 2;

typedef struct _SwapChainBuffers {
  VkImage image;
  VkImageView view;
} SwapChainBuffer;

class VulkanEngine : public QWindow
{
private:
  // Variables
  bool m_inited = false;
  bool viewUpdated = false;
  float fpsTimer = 0.0f;										// fps timer (one second interval)
  int graphicsQueueNodeIndex = -1;
  uint32_t swapChainImageCount;
  QPoint m_lastPos;
  QList<Qt::MouseButton> ButtonsPressed;

  // Vulkan Variables
  VkSurfaceKHR m_vkSurface;
  VkBool32 useStaging = false;								// We prefer using staging to copy the texture data to a device local optimal image

  // Functions
  void init();
  void initSwapChain();
  void releaseSwapChain();
  void windowResize();

  // Override functions
  void exposeEvent(QExposeEvent *) override;
  void resizeEvent(QResizeEvent *) override;
  bool event(QEvent *) override;
  void mousePressEvent(QMouseEvent *) override;
  void mouseReleaseEvent(QMouseEvent *) override;
  void mouseMoveEvent(QMouseEvent *) override;

protected:
  // Structures
  /* Semaphores Structure*/
  struct {
    // Swap chain image presentation
    VkSemaphore presentComplete;
    // Command buffer submission and execution
    VkSemaphore renderComplete;
    // UI overlay submission and execution
    VkSemaphore overlayComplete;
  } semaphores;

  // Variables
  QSize m_swapChainImageSize;
  uint32_t frameCounter = 0;									// Frame counter to display fps
  uint32_t lastFPS = 0;

  // Vulkan Variables
  VkPhysicalDevice physicalDevice;
  VkPhysicalDeviceProperties deviceProperties;
  VkPhysicalDeviceFeatures deviceFeatures;					// Stores the features available on the selected physical device (for e.g. checking if a feature is available)
  VkPhysicalDeviceMemoryProperties deviceMemoryProperties;	// Stores all available memory (type) properties for the physical device
  QVulkanDeviceFunctions *m_devFuncs;
  VkQueue m_vkPresQueue;
  VkQueue m_vkGraphicsQueue;									// Handle to the device graphics queue that command buffers are submitted to
  VkCommandPool cmdPool;										// Command buffer pool
  VkDevice device;											/* @brief Logical device, application's view of the physical device (GPU) */ // todo: getter? should always point to VulkanDevice->device
  VkFormat swapChainColorFormat;
  VkFormat depthFormat;										// Depth buffer format (selected during Vulkan initialization)
  VkPhysicalDeviceFeatures enabledFeatures{};					// Set of physical device features to be enabled for this example (must be set in the derived constructor) @note By default no phyiscal device features are enabled
  /* @brief Set of device extensions to be enabled for this example (must be set in the derived constructor) */
  std::vector<const char*> enabledDeviceExtensions;
  std::vector<const char*> enabledInstanceExtensions;
  VkSwapchainKHR m_swapChain = 0;
  /* @brief Pipeline stages used to wait at for graphics queue submissions */
  VkPipelineStageFlags submitPipelineStages = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
  VkSubmitInfo submitInfo;									// Contains command buffers and semaphores to be presented to the queue
  uint32_t m_swapChainBufferCount = 0;						// Number of images that can be created at one time.
  std::vector<VkCommandBuffer> drawCmdBuffers;				// Command buffers used for rendering
  VkRenderPass renderPass = 0;								// Global render pass for frame buffer writes
  std::vector<VkFramebuffer> frameBuffers;					// List of available frame buffers (same as number of swap chain images)
  uint32_t currentBuffer = 0;									// Active frame buffer index
  std::vector<VkFence> waitFences;
  std::vector<SwapChainBuffer> swapChainBuffers;
  VkPipelineCache pipelineCache;								// Pipeline cache object
  VkDescriptorPool descriptorPool = VK_NULL_HANDLE;			// Descriptor set pool

  PFN_vkCreateSwapchainKHR m_vkCreateSwapchainKHR = nullptr;
  PFN_vkDestroySwapchainKHR m_vkDestroySwapchainKHR;
  PFN_vkGetSwapchainImagesKHR m_vkGetSwapchainImagesKHR;
  PFN_vkAcquireNextImageKHR m_vkAcquireNextImageKHR;
  PFN_vkQueuePresentKHR m_vkQueuePresentKHR;
  PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR m_vkGetPhysicalDeviceSurfaceCapabilitiesKHR;
  PFN_vkGetPhysicalDeviceSurfaceFormatsKHR m_vkGetPhysicalDeviceSurfaceFormatsKHR;
  std::vector<VkShaderModule> shaderModules;					// List of shader modules created (stored for cleanup)

  VkPipelineShaderStageCreateInfo loadShader(QString fileName, VkShaderStageFlagBits stage);

public:
  VulkanEngine();												// Creator
  virtual ~VulkanEngine();									// Destructor

  // Structures
  /* Depth Stencil */
  struct
  {
    VkImage image;
    VkDeviceMemory mem;
    VkImageView view;
  } depthStencil;

  // Variables
  bool prepared = false;
  bool paused = false;
  float zoom = 0;
  float frameTimer = 1.0f;									/* @brief Last frame time measured using a high performance timer (if available) */
  float rotationSpeed = 1.0f;									// Use to adjust mouse rotation speed
  float moveSpeed = 1.0f;										// Use to adjust mouse movement speed
  float zoomSpeed = 1.0f;										// Use to adjust mouse zoom speed
  vks::VulkanDevice *vulkanDevice;							// Encapsulated physical and logical vulkan device
  Camera camera;
  glm::vec3 rotation = glm::vec3();
  glm::vec3 cameraPos = glm::vec3();
  glm::vec3 lightPosition = glm::vec3(0.0f, 20.0f, 10.0f);

  VkClearColorValue defaultClearColor = { { 0.3490196078431372f, 0.4117647058823529f, 0.4784313725490196f, 1.0f } };	// RGBAColor(89, 105, 122)

  /* Defines a frame rate independent timer value clamped from -1.0...1.0 */
  float timer = 0.0f;											// For use in animations, rotations, etc.
  float timerSpeed = 0.25f;									// Multiplier for speeding up (or slowing down) the global timer

  // Functions
  virtual void render() = 0;									// Pure virtual render function (override in derived class)
  virtual void viewChanged();									// Called when view change occurs. Can be overriden in derived class to e.g. update uniform buffers. Containing view dependant matrices
  virtual void windowResized();								// Called when the window has been resized. Can be overriden in derived class to recreate or rebuild resources attached to the frame buffer / swapchain
  virtual void buildCommandBuffers();							// Called in case of an event where e.g. the framebuffer has to be rebuild and thus all command buffers that may reference this

  void createSynchronizationPrimitives();						// Wait fences to sync command buffer access

  void createCommandPool();									// Creates a new (graphics) command pool object storing command buffers
  virtual void setupDepthStencil();							// Setup default depth and stencil views
  virtual void setupFrameBuffer();							// Create framebuffers for all requested swap chain images. Can be overriden in derived class to setup a custom framebuffer (e.g. for MSAA)
  virtual void setupRenderPass();								// Setup a default render pass. Can be overriden in derived class to setup a custom render pass (e.g. for MSAA)
  
  virtual void getEnabledFeatures();							/* @brief (Virtual) Called after the physical device features have been read, can be used to set features to enable on the device */
  
  void recreateSwapChain();

  bool checkCommandBuffers();									// Check if command buffers are valid (!= VK_NULL_HANDLE)
  void createCommandBuffers();								// Create command buffers for drawing commands
  void destroyCommandBuffers();								// Destroy all command buffers and set their handles to VK_NULL_HANDLE. May be necessary during runtime if options are toggled 

  // Command buffer creation.
  // Creates and returns a new command buffer
  VkCommandBuffer createCommandBuffer(VkCommandBufferLevel level, bool begin);
  // End the command buffer, submit it to the queue and free (if requested)
  // Note : Waits for the queue to become idle
  void flushCommandBuffer(VkCommandBuffer commandBuffer, VkQueue queue, bool free);

  void createPipelineCache();									// Create a cache pool for rendering pipelines

  virtual void releaseResources();
  virtual void prepare();										// Prepare commonly used Vulkan functions

  void renderFrame();											// Render one frame of a render loop on platforms that sync rendering

  // Prepare the frame for workload submission
  // - Acquires the next image from the swap chain 
  // - Sets the default wait and signal semaphores
  void prepareFrame();

  // Submit the frames' workload 
  void submitFrame();
};