#include <Foundation/LogFile.h>
#include <Foundation/Functions.h>
#include "VulkanWindow.h"

#define VERTEX_BUFFER_BIND_ID 0
double timer_startf = 0.0;

VulkanWindow::VulkanWindow()
{
  zoom = -3.5f;
  rotation = glm::vec3(-10.0f, 40.0f, 0.0f);

  camera.type = Camera::firstperson;
  camera.position = cameraPos;
  camera.setRotation(rotation);
  camera.setPerspective(glm::radians(60.0f), (float)m_swapChainImageSize.width() / (float)m_swapChainImageSize.height(), 0.001f, 16384.0f);
}

VulkanWindow::~VulkanWindow()
{
  if (!device)
    return;

  m_devFuncs->vkDeviceWaitIdle(device);

  if (scene != nullptr)
  {
    delete(scene);
    scene = nullptr;
  }

  releaseResources();
}

void VulkanWindow::setDefaultClearColor(RGBAColor *color)
{
  defaultClearColor = { { (float)color->R, (float)color->G, (float)color->B, (float)color->A } };
}

void VulkanWindow::getEnabledFeatures()
{
  VulkanEngine::getEnabledFeatures();

  // Enable anisotropic filtering if supported
  if (deviceFeatures.samplerAnisotropy) {
    enabledFeatures.samplerAnisotropy = VK_TRUE;
  };
}

void VulkanWindow::setSceneData(VulkanSceneData * new_scene)
{
  if (new_scene == nullptr || new_scene == NULL)
  {
    LOGFILE.Error("New Scene is null. Unable to load!");
    qCritical("New Scene is null. Unable to load!");
    return;
  }
  if (new_scene->modelData.count() <= 0)
  {
    LOGFILE.Error("Invalid Model count! (%i) Unable to load!", new_scene->modelData.count());
    qCritical("Invalid Model count! Unable to load!");
    return;
  }

  timer_startf = getCurrentTime();
  LOGFILE.Debug("Set Scene Start Time: %f", timer_startf);

  for (uint32_t i = 0; i < new_scene->modelData.count(); i++)
  {
    VulkanModelData model = new_scene->modelData.at(i);
    if (model.meshData.count() <= 0 || (model.materials.count() <= 0 && new_scene->sharedMaterials.count() <=0))
    {
      LOGFILE.Error("Wrong Mesh or Material count! (%i & %i/%i) Unable to load!", model.meshData.count(), model.materials.count(), new_scene->sharedMaterials.count());
      qCritical("Wrong Mesh or Material count! Unable to load!");
      return;
    }
    //LOGFILE.Info("Loading new Vulkan Scene from model data...");
    //LOGFILE.Debug("	Mesh Count: %i", model.meshData.count());
    //LOGFILE.Debug("	Material Count: %i | %i", model.materials.count(), new_scene->sharedMaterials.count());
    //LOGFILE.Debug("	Texture Count:  %i | %i", model.textureList.count(), new_scene->sharedTextures.count());
  }
  
  vkDeviceWaitIdle(device);
  releaseResources();

  zoom = new_scene->zoom;
  rotation = new_scene->rotation;
  cameraPos = new_scene->cameraPos;
  zoomSpeed = new_scene->zoomScale;
  moveSpeed = new_scene->moveScale;
  rotationSpeed = new_scene->rotateScale;

  scene = new VulkanScene(vulkanDevice, m_vkGraphicsQueue);
  scene->loadData(new_scene);

  prepare();
}

void VulkanWindow::generateQuad(ImageData* texture, float aspect)
{
  vkDeviceWaitIdle(device);
  releaseResources();

  zoom = -3.5f;
  rotation = glm::vec3();
  cameraPos = glm::vec3();
  zoomSpeed = 1.0f;
  moveSpeed = 1.0f;
  rotationSpeed = 1.0f;

  scene = new VulkanScene(vulkanDevice, m_vkGraphicsQueue);
  scene->generateSquare(texture, aspect);

  prepare();
}

void VulkanWindow::setupVertexDescriptions()
{
  // Binding description
  vertices.bindingDescriptions.resize(1);
  vertices.bindingDescriptions[0] = Vertex::getBindingDescription();

  // Attribute descriptions
  // Describes memory layout and shader positions
  vertices.attributeDescriptions = Vertex::getAttributeDescriptions();

  vertices.inputState = vks::initializers::pipelineVertexInputStateCreateInfo();
  vertices.inputState.vertexBindingDescriptionCount = static_cast<uint32_t>(vertices.bindingDescriptions.size());
  vertices.inputState.pVertexBindingDescriptions = vertices.bindingDescriptions.data();
  vertices.inputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertices.attributeDescriptions.size());
  vertices.inputState.pVertexAttributeDescriptions = vertices.attributeDescriptions.data();
}


void VulkanWindow::preparePipelines()
{
  VkPipelineInputAssemblyStateCreateInfo inputAssemblyState =
    vks::initializers::pipelineInputAssemblyStateCreateInfo(
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
      0,
      VK_FALSE);

  VkPipelineRasterizationStateCreateInfo rasterizationState =
    vks::initializers::pipelineRasterizationStateCreateInfo(
      VK_POLYGON_MODE_FILL,
      VK_CULL_MODE_NONE,
      VK_FRONT_FACE_CLOCKWISE,
      0);

  VkPipelineColorBlendAttachmentState blendAttachmentState =
    vks::initializers::pipelineColorBlendAttachmentState(
      0xf,
      VK_FALSE);

  VkPipelineColorBlendStateCreateInfo colorBlendState =
    vks::initializers::pipelineColorBlendStateCreateInfo(
      1,
      &blendAttachmentState);

  VkPipelineDepthStencilStateCreateInfo depthStencilState =
    vks::initializers::pipelineDepthStencilStateCreateInfo(
      VK_TRUE,
      VK_TRUE,
      VK_COMPARE_OP_LESS_OR_EQUAL);

  VkPipelineViewportStateCreateInfo viewportState =
    vks::initializers::pipelineViewportStateCreateInfo(1, 1, 0);

  VkPipelineMultisampleStateCreateInfo multisampleState =
    vks::initializers::pipelineMultisampleStateCreateInfo(
      VK_SAMPLE_COUNT_1_BIT,
      0);

  std::vector<VkDynamicState> dynamicStateEnables = {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_SCISSOR,
    VK_DYNAMIC_STATE_LINE_WIDTH,
  };
  VkPipelineDynamicStateCreateInfo dynamicState =
    vks::initializers::pipelineDynamicStateCreateInfo(
      dynamicStateEnables.data(),
      static_cast<uint32_t>(dynamicStateEnables.size()),
      0);

  VkGraphicsPipelineCreateInfo pipelineCreateInfo =
    vks::initializers::pipelineCreateInfo(scene->pipelineLayout, renderPass, 0);

  // Load shaders
  std::array<VkPipelineShaderStageCreateInfo, 2> shaderStages;

  pipelineCreateInfo.pVertexInputState = &vertices.inputState;
  pipelineCreateInfo.pInputAssemblyState = &inputAssemblyState;
  pipelineCreateInfo.pRasterizationState = &rasterizationState;
  pipelineCreateInfo.pColorBlendState = &colorBlendState;
  pipelineCreateInfo.pMultisampleState = &multisampleState;
  pipelineCreateInfo.pViewportState = &viewportState;
  pipelineCreateInfo.pDepthStencilState = &depthStencilState;
  pipelineCreateInfo.pDynamicState = &dynamicState;
  pipelineCreateInfo.stageCount = shaderStages.size();
  pipelineCreateInfo.pStages = shaderStages.data();

  // Shared vertex bindings and attributes used by all pipelines

  // Binding description
  std::vector<VkVertexInputBindingDescription> vertexInputBindings = {
    vks::initializers::vertexInputBindingDescription(VERTEX_BUFFER_BIND_ID, vertexLayout.stride(), VK_VERTEX_INPUT_RATE_VERTEX),
  };

  // Attribute descriptions
  std::vector<VkVertexInputAttributeDescription> vertexInputAttributes = {
    vks::initializers::vertexInputAttributeDescription(VERTEX_BUFFER_BIND_ID, 0, VK_FORMAT_R32G32B32_SFLOAT, 0),					// Location 0: Position
    vks::initializers::vertexInputAttributeDescription(VERTEX_BUFFER_BIND_ID, 1, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 3),	// Location 1: Color
    vks::initializers::vertexInputAttributeDescription(VERTEX_BUFFER_BIND_ID, 2, VK_FORMAT_R32G32_SFLOAT, sizeof(float) * 6),		// Location 2: Texture coordinates
    vks::initializers::vertexInputAttributeDescription(VERTEX_BUFFER_BIND_ID, 3, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 8),	// Location 3: Normal
    vks::initializers::vertexInputAttributeDescription(VERTEX_BUFFER_BIND_ID, 4, VK_FORMAT_R32G32B32_SFLOAT, sizeof(float) * 11),	// Location 4: WireColor
  };

  VkPipelineVertexInputStateCreateInfo vertexInputState = vks::initializers::pipelineVertexInputStateCreateInfo();
  vertexInputState.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexInputBindings.size());
  vertexInputState.pVertexBindingDescriptions = vertexInputBindings.data();
  vertexInputState.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexInputAttributes.size());
  vertexInputState.pVertexAttributeDescriptions = vertexInputAttributes.data();

  pipelineCreateInfo.pVertexInputState = &vertexInputState;

  // Create the graphics pipeline state objects

  // We are using this pipeline as the base for the other pipelines (derivatives)
  // Pipeline derivatives can be used for pipelines that share most of their state
  // Depending on the implementation this may result in better performance for pipeline 
  // switchting and faster creation time
  pipelineCreateInfo.flags = VK_PIPELINE_CREATE_ALLOW_DERIVATIVES_BIT;
  
  // Phong Shader
  shaderStages[0] = loadShader(QStringLiteral(":/Shaders/solid.vert"), VK_SHADER_STAGE_VERTEX_BIT);
  shaderStages[1] = loadShader(QStringLiteral(":/Shaders/solid.frag"), VK_SHADER_STAGE_FRAGMENT_BIT);
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.solid));

  // All pipelines created after the base pipeline will be derivatives
  pipelineCreateInfo.flags = VK_PIPELINE_CREATE_DERIVATIVE_BIT;
  // Base pipeline will be our first created pipeline
  pipelineCreateInfo.basePipelineHandle = scene->pipelines.solid;
  // It's only allowed to either use a handle or index for the base pipeline
  // As we use the handle, we must set the index to -1 (see section 9.5 of the specification)
  pipelineCreateInfo.basePipelineIndex = -1;
  
  rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.solidCull));

  rasterizationState.cullMode = VK_CULL_MODE_NONE;
  shaderStages[1] = loadShader(QStringLiteral(":/Shaders/textured.frag"), VK_SHADER_STAGE_FRAGMENT_BIT);
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.textured));

  rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.texturedCull));

  rasterizationState.cullMode = VK_CULL_MODE_NONE;
  shaderStages[1] = loadShader(QStringLiteral(":/Shaders/texturedsolid.frag"), VK_SHADER_STAGE_FRAGMENT_BIT);
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.texturedSolid));

  rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.texturedSolidCull));

  rasterizationState.cullMode = VK_CULL_MODE_NONE;
  blendAttachmentState.blendEnable = VK_TRUE;
  blendAttachmentState.colorBlendOp = VK_BLEND_OP_ADD;
  blendAttachmentState.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
  blendAttachmentState.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.textureBlend));

  rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
  VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.textureBlendCull));

  // Pipeline for wire frame rendering
  // Non solid rendering is not a mandatory Vulkan feature
  if (deviceFeatures.fillModeNonSolid)
  {
    blendAttachmentState.blendEnable = VK_FALSE;
    rasterizationState.cullMode = VK_CULL_MODE_NONE;
    rasterizationState.polygonMode = VK_POLYGON_MODE_LINE;
    rasterizationState.lineWidth = 1.0f;
    shaderStages[0] = loadShader(QStringLiteral(":/Shaders/wireframe.vert"), VK_SHADER_STAGE_VERTEX_BIT);
    shaderStages[1] = loadShader(QStringLiteral(":/Shaders/wireframe.frag"), VK_SHADER_STAGE_FRAGMENT_BIT);
    VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.wireframe));

    rasterizationState.cullMode = VK_CULL_MODE_BACK_BIT;
    VK_CHECK_RESULT(vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &scene->pipelines.wireframeCull));
  }
}

void VulkanWindow::updateUniformBuffers()
{
  //qDebug("Updating Uniform Buffer: Zoom: %f, X: %f, Y: %f, Z: %f, rX: %f, rY: %f, rZ: %f", zoom, cameraPos.x, cameraPos.y, cameraPos.z, rotation.x, rotation.y, rotation.z);
  //LOGFILE.Debug("Updating Uniform Buffer: Zoom: %f, X: %f, Y: %f, Z: %f, rX: %f, rY: %f, rZ: %f", zoom, cameraPos.x, cameraPos.y, cameraPos.z, rotation.x, rotation.y, rotation.z);
  
  // Vertex shader
  scene->uboVS.Projection = glm::perspective(glm::radians(60.0f), (float)m_swapChainImageSize.width() / (float)m_swapChainImageSize.height(), 0.001f, 16384.0f);
  glm::mat4 viewMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, zoom));
  scene->uboVS.modelView = viewMatrix * glm::translate(glm::mat4(1.0f), cameraPos);
  scene->uboVS.modelView = glm::rotate(scene->uboVS.modelView, glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
  scene->uboVS.modelView = glm::rotate(scene->uboVS.modelView, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
  scene->uboVS.modelView = glm::rotate(scene->uboVS.modelView, glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));

  scene->uboVS.LightPosition = lightPosition;
  scene->uboVS.viewPos = glm::vec4(0.0f, 0.0f, -zoom, 0.0f);
  memcpy(scene->uniformBuffers.VS.mapped, &scene->uboVS, sizeof(scene->uboVS));
  
  // Fragment Shader
  scene->uboFG.ColorMode = static_cast<int>(VulkanColorMode);
  scene->uboFG.AlphaClip = VulkanAlphaClip;
  memcpy(scene->uniformBuffers.FG.mapped, &scene->uboFG, sizeof(scene->uboFG));
}

void VulkanWindow::viewChanged()
{
  updateUniformBuffers();
}

void VulkanWindow::changeDisplay(vulkanDisplayType type)
{
  VulkanDisplayType = type;
  vkDeviceWaitIdle(device);
  buildCommandBuffers();
  render();
}

void VulkanWindow::changeColorMode(colorMode mode)
{
  VulkanColorMode = mode;
  vkDeviceWaitIdle(device);
  updateUniformBuffers();
  buildCommandBuffers();
  render();
}

void VulkanWindow::toggleCulling()
{
  enableCulling = !enableCulling;
  vkDeviceWaitIdle(device);
  buildCommandBuffers();
  render();
}

void VulkanWindow::prepare()
{
  double timer_prepareStartf = getCurrentTime();
  LOGFILE.Debug("Prepare Start: %f", timer_prepareStartf);
  VulkanEngine::prepare();
  setupVertexDescriptions();

  double timer_endf = getCurrentTime();
  LOGFILE.Debug("Post VertexDescriptions: %f", timer_endf);
  LOGFILE.Debug("Seconds since Prepare Start: %.3f", (timer_endf - timer_prepareStartf));
  LOGFILE.Debug("Seconds since Scene Start:   %.3f", (timer_endf - timer_startf));

  if (scene == nullptr || scene == NULL)
  {
    qDebug("Empty Scene Detected!");
    scene = new VulkanScene(vulkanDevice, m_vkGraphicsQueue);
    scene->generateSquare();
  }
  {
    VkCommandBuffer copyCmd = VulkanEngine::createCommandBuffer(VK_COMMAND_BUFFER_LEVEL_PRIMARY, false);
    scene->load(copyCmd);
    vkFreeCommandBuffers(device, cmdPool, 1, &copyCmd);
    updateUniformBuffers();
  }
  timer_endf = getCurrentTime();
  LOGFILE.Debug("Post Scene-Load, Pre-Pipelines: %f", timer_endf);
  LOGFILE.Debug("Seconds since Prepare Start: %.3f", (timer_endf - timer_prepareStartf));
  LOGFILE.Debug("Seconds since Scene Start:   %.3f", (timer_endf - timer_startf));

  preparePipelines();

  timer_endf = getCurrentTime();
  LOGFILE.Debug("Post-Pipelines, PreBuild: %f", timer_endf);
  LOGFILE.Debug("Seconds since Prepare Start: %.3f", (timer_endf - timer_prepareStartf));
  LOGFILE.Debug("Seconds since Scene Start: %.3f", (timer_endf - timer_startf));

  buildCommandBuffers();

  prepared = true;
}

void VulkanWindow::releaseResources()
{
  VulkanEngine::releaseResources();

  if (pipelineCache != VK_NULL_HANDLE)
  {
    vkDestroyPipelineCache(device, pipelineCache, nullptr);
    pipelineCache = VK_NULL_HANDLE;
  }

  if (cmdPool != VK_NULL_HANDLE)
  {
    vkDestroyCommandPool(device, cmdPool, nullptr);
    cmdPool = VK_NULL_HANDLE;
  }
}

void VulkanWindow::render()
{
  if (!prepared)
    return;
  draw();
}

void VulkanWindow::draw()
{
  VulkanEngine::prepareFrame();

  // Command buffer to be sumitted to the queue
  submitInfo.commandBufferCount = 1;
  submitInfo.pCommandBuffers = &drawCmdBuffers[currentBuffer];

  // Submit to queue
  VK_CHECK_RESULT(vkQueueSubmit(m_vkGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

  // we do not currently handle the case when the present queue is separate
  Q_ASSERT(m_vkGraphicsQueue == m_vkPresQueue);

  VulkanEngine::submitFrame();

  vulkanInstance()->presentQueued(this);
  frameCounter = (frameCounter + 1) % FRAME_LAG;
  requestUpdate();
}

void VulkanWindow::buildCommandBuffers()
{
  VkCommandBufferBeginInfo cmdBufInfo = vks::initializers::commandBufferBeginInfo();

  VkClearValue clearValues[2];
  clearValues[0].color = defaultClearColor;
  clearValues[1].depthStencil = { 1.0f, 0 };

  VkRenderPassBeginInfo renderPassBeginInfo;
  memset(&renderPassBeginInfo, 0, sizeof(renderPassBeginInfo));
  renderPassBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
  renderPassBeginInfo.renderPass = renderPass;
  renderPassBeginInfo.renderArea.extent.width = m_swapChainImageSize.width();
  renderPassBeginInfo.renderArea.extent.height = m_swapChainImageSize.height();
  renderPassBeginInfo.clearValueCount = 2;
  renderPassBeginInfo.pClearValues = clearValues;

  double timer_middlef = getCurrentTime();
  LOGFILE.Debug("Build Command Buffers (Predraw): %f", timer_middlef);
  LOGFILE.Debug("Seconds since Add Scene: %.3f", (timer_middlef-timer_startf));

  for (int32_t i = 0; i < drawCmdBuffers.size(); ++i)
  {
    // Set target frame buffer
    renderPassBeginInfo.framebuffer = frameBuffers[i];

    VK_CHECK_RESULT(vkBeginCommandBuffer(drawCmdBuffers[i], &cmdBufInfo));

    vkCmdBeginRenderPass(drawCmdBuffers[i], &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    VkViewport viewport = vks::initializers::viewport((float)m_swapChainImageSize.width(), (float)m_swapChainImageSize.height(), 0.0f, 1.0f);
    vkCmdSetViewport(drawCmdBuffers[i], 0, 1, &viewport);

    VkRect2D scissor = vks::initializers::rect2D(m_swapChainImageSize.width(), m_swapChainImageSize.height(), 0, 0);
    vkCmdSetScissor(drawCmdBuffers[i], 0, 1, &scissor);

    scene->render(drawCmdBuffers[i], VulkanDisplayType, enableCulling);

    vkCmdEndRenderPass(drawCmdBuffers[i]);

    VK_CHECK_RESULT(vkEndCommandBuffer(drawCmdBuffers[i]));
  }

  double timer_endf = getCurrentTime();
  LOGFILE.Debug("Post-Draw: %f", timer_endf);
  LOGFILE.Debug("Seconds since Add Scene: %.3f", (timer_endf - timer_startf));
  LOGFILE.Debug("Seconds since Pre-Draw:  %.3f", (timer_endf - timer_middlef));
}
