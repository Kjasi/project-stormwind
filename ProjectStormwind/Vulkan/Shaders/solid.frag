#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 fragUvCoord;
layout(location = 3) in vec3 fragViewVec;
layout(location = 4) in vec3 fragLightVec;

layout(location = 0) out vec4 outColor;

void main() {
  vec3 N = normalize(fragNormal);
  vec3 L = normalize(fragLightVec);

  vec3 diffuse = (max(dot(N, L),0.0) * vec3(0.85, 0.85, 0.85));

  outColor = vec4(diffuse, 1.0);
}