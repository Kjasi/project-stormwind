#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std140, set = 0, binding = 1) uniform UniformBufferObject
{
  int ColorMode;
  float AlphaClip;
} ubo;


layout(set = 1, binding = 0) uniform sampler2D samplerColor;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 fragUvCoord;
layout(location = 3) in vec3 fragViewVec;
layout(location = 4) in vec3 fragLightVec;

layout(push_constant) uniform Material 
{
  vec4 ambientColor;
  vec4 diffuseColor;
  vec4 specularColor;
  float opacity;
} material;

layout(location = 0) out vec4 outColor;

void main() {
  vec4 tex = texture(samplerColor, fragUvCoord, 0.0f);
  // vec4 spectex = texture(sampler2D(textures[imageIndex], samp), fragUvCoord, 0.0f);
  vec3 color = (vec3(tex.r, tex.g, tex.b) * fragColor);

  if (ubo.ColorMode == 2)
  {
    color = vec3(tex.a, tex.a, tex.a);
  }

  vec3 N = normalize(fragNormal);
  vec3 V = normalize(fragViewVec);
  vec3 L = normalize(fragLightVec);
  vec3 R = reflect(-L, N);

  vec3 diffuse = (max(dot(N, L),0.0) * material.diffuseColor.rgb);
  vec3 ambient = material.ambientColor.rgb + diffuse;
  vec3 specular = pow(max(dot(R, V), 0.0), 16.0) * material.specularColor.rgb;

  //if (hasSpecularMap == true)
  //	specular = (pow(max(dot(R, V), 0.0), 128.0) * vec3(1.35)) * spectex.r;
  

  if (ubo.ColorMode == 0)
  {
    if (tex.a < ubo.AlphaClip)
      discard;
  }
  
  outColor = vec4(ambient * color.rgb + specular, material.opacity);
}