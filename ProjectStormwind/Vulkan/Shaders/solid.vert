#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std140, set = 0, binding = 0) uniform UniformBufferObject
{
  mat4 Projection;
  mat4 modelView;
  vec4 viewPos;
  vec3 LightPosition;
} ubo;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inUVcoord;
layout(location = 3) in vec3 inNormal;
layout(location = 4) in vec3 inWireColor;

layout(location = 0) out vec3 fragColor;
layout(location = 1) out vec3 fragNormal;
layout(location = 2) out vec2 fragUVCoord;
layout(location = 3) out vec3 fragViewVec;
layout(location = 4) out vec3 fragLightVec;

out gl_PerVertex {
  vec4 gl_Position;
};

void main() {
  // Constants
  fragColor = inColor;
  fragUVCoord = inUVcoord;

  // Vertex Out
  gl_Position = ubo.Projection * ubo.modelView * vec4(inPosition.xyz, 1.0);

  // Fragment Shader Variables
  vec4 worldPos = ubo.modelView * vec4(inPosition, 1.0);
  vec3 lightPos = mat3(ubo.modelView) * ubo.LightPosition.xyz;

  // Fragment Shader Outputs
  fragNormal = mat3(ubo.modelView) * inNormal;
  fragLightVec = lightPos - worldPos.xyz;
  fragViewVec = -worldPos.xyz;
}