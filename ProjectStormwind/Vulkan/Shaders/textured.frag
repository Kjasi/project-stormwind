#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(std140, set = 0, binding = 1) uniform UniformBufferObject
{
  int ColorMode;
  float AlphaClip;
} ubo;


layout(set = 1, binding = 0) uniform sampler2D samplerColor;

layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec2 fragUvCoord;
layout(location = 3) in vec3 fragViewVec;
layout(location = 4) in vec3 fragLightVec;

layout(push_constant) uniform Material 
{
  vec4 ambientColor;
  vec4 diffuseColor;
  vec4 specularColor;
  float opacity;
} material;

layout(location = 0) out vec4 outColor;

void main() {
  vec4 tex = texture(samplerColor, fragUvCoord, 0.0f);
  vec3 color = (vec3(tex.r, tex.g, tex.b) * fragColor);

  if (ubo.ColorMode == 2)
  {
    color = vec3(tex.a, tex.a, tex.a);
  }

  if (ubo.ColorMode == 0)
  {
    if (tex.a < ubo.AlphaClip)
      discard;
  }
  
  outColor = vec4(color.rgb, material.opacity);
  // outColor = tex;

  //outColor = vec4(fragColor, 1.0);			// Test Color
  //outColor = vec4(N, 1.0);					// Test Normals
  //outColor = vec4(fragUvCoord, 0.0, 1.0);	// Test UVs

  // float val = dot(N, V);
  // outColor = vec4(val, val, val, 1.0);	// Test Surface
}