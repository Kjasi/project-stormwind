Shaders\glslangValidator.exe -V -o Shaders\solid.vert.spv Shaders\solid.vert
Shaders\glslangValidator.exe -V -o Shaders\solid.frag.spv Shaders\solid.frag
Shaders\glslangValidator.exe -V -o Shaders\textured.frag.spv Shaders\textured.frag
Shaders\glslangValidator.exe -V -o Shaders\texturedsolid.frag.spv Shaders\texturedsolid.frag
Shaders\glslangValidator.exe -V -o Shaders\wireframe.vert.spv Shaders\wireframe.vert
Shaders\glslangValidator.exe -V -o Shaders\wireframe.frag.spv Shaders\wireframe.frag