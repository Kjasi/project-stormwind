#pragma once
#include "VulkanEngine.h"
#include <Foundation/Vulkan/VulkanScene.h>

enum colorMode
{
  COLORMODE_FULL = 0,
  COLORMODE_COLOR,
  COLORMODE_ALPHA,
};

namespace vks
{
  /** @brief Vertex layout components */
  typedef enum Component {
    VERTEX_COMPONENT_POSITION = 0x0,
    VERTEX_COMPONENT_NORMAL = 0x1,
    VERTEX_COMPONENT_COLOR = 0x2,
    VERTEX_COMPONENT_UV = 0x3,
    VERTEX_COMPONENT_TANGENT = 0x4,
    VERTEX_COMPONENT_BITANGENT = 0x5,
    VERTEX_COMPONENT_DUMMY_FLOAT = 0x6,
    VERTEX_COMPONENT_DUMMY_VEC4 = 0x7
  } Component;

  struct VertexLayout {
  public:
    /** @brief Components used to generate vertices from */
    std::vector<Component> components;

    VertexLayout(std::vector<Component> components)
    {
      this->components = std::move(components);
    }

    uint32_t stride()
    {
      uint32_t res = 0;
      for (auto& component : components)
      {
        switch (component)
        {
        case VERTEX_COMPONENT_UV:
          res += 2 * sizeof(float);
          break;
        case VERTEX_COMPONENT_DUMMY_FLOAT:
          res += sizeof(float);
          break;
        case VERTEX_COMPONENT_DUMMY_VEC4:
          res += 4 * sizeof(float);
          break;
        default:
          // All components except the ones listed above are made up of 3 floats
          res += 3 * sizeof(float);
        }
      }
      return res;
    }
  };
}

class VulkanWindow : public VulkanEngine
{
public:
  VulkanWindow();
  ~VulkanWindow();

  // Vertex layout for the models
  vks::VertexLayout vertexLayout = vks::VertexLayout({
    vks::VERTEX_COMPONENT_POSITION,
    vks::VERTEX_COMPONENT_COLOR,
    vks::VERTEX_COMPONENT_UV,
    vks::VERTEX_COMPONENT_NORMAL,
    vks::VERTEX_COMPONENT_COLOR,
  });

  VulkanScene *scene = nullptr;

  struct {
    VkPipelineVertexInputStateCreateInfo inputState;
    std::vector<VkVertexInputBindingDescription> bindingDescriptions;
    std::vector<VkVertexInputAttributeDescription> attributeDescriptions;
  } vertices;

  bool imageLoaded = false;
  void setDefaultClearColor(RGBAColor *color);
  virtual void getEnabledFeatures();				// Enable physical device features required for this example
  void setSceneData(VulkanSceneData* new_scene);
  void generateQuad(ImageData* texture = nullptr, float aspect = 1.0);
  void setupVertexDescriptions();
  void preparePipelines();
  void updateUniformBuffers();
  void viewChanged();
  void changeDisplay(vulkanDisplayType type);
  void changeColorMode(colorMode mode);
  void toggleCulling();
  void setAlphaClip(float clipping = 0.333333333f) { VulkanAlphaClip = clipping; };

private:
  void prepare();
  void releaseResources();
  void render();
  void draw();
  void buildCommandBuffers();

  bool enableCulling = true;

  vulkanDisplayType VulkanDisplayType = DISPLAYTYPE_TEXTURESOLID;
  colorMode VulkanColorMode = colorMode::COLORMODE_FULL;
  float VulkanAlphaClip = 0.333333333f;
};