#pragma once
#include <QtWidgets/qwidget.h>
#include "VulkanWindow.h"

class VulkanWidget : public QWidget
{
public:
  explicit VulkanWidget(VulkanWindow *w, QWidget *parent = 0);

private:
  VulkanWindow *m_window;
};