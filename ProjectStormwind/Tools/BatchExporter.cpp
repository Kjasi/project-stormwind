#include "BatchExporter.h"
#include <Foundation/Core/Kernel.h>
#include <Foundation/Plugins/PluginExportModel.h>
#include <Foundation/Plugins/PluginExportImage.h>
#include <QtCore/qdiriterator.h>
#include <QtWidgets/qmessagebox.h>
using namespace ProjectSW;

BatchExporter::BatchExporter(ExportVariables export_variables, cWoWVersion wow_version)
  : variables(export_variables), wowVersion(wow_version)
{
  progress = new BatchExporterProgressDialog;

  connect(progress, &BatchExporterProgressDialog::abortExport, this, &BatchExporter::cancelBatch);
  connect(progress, &BatchExporterProgressDialog::reject, this, &BatchExporter::cancelBatch);
  connect(progress, &BatchExporterProgressDialog::hide, this, &BatchExporter::cancelBatch);

  connect(this, &BatchExporter::sendCurrent, progress, &BatchExporterProgressDialog::setProgressValue);
  connect(this, &BatchExporter::sendMax, progress, &BatchExporterProgressDialog::setProgressMax);
  connect(this, &BatchExporter::sendFilename, progress, &BatchExporterProgressDialog::setCurrentFilename);
  connect(this, &BatchExporter::sendData, progress, &BatchExporterProgressDialog::updateData);
  connect(&this->FutureWatcher, &QFutureWatcher<void>::finished, progress, &BatchExporterProgressDialog::accept);
}

void BatchExporter::StartProcess()
{
  cancel = false;
  LOGFILE.Info("Starting Batch Export...");
  LOGFILE.Debug("Exporing with following variables:");
  LOGFILE.Debug("	WoW Dir:	%s", qPrintable(variables.wowDir.absolutePath()));
  LOGFILE.Debug("	Input Dir:	%s", qPrintable(variables.inputDir.absolutePath()));
  LOGFILE.Debug("	Output Dir: %s", qPrintable(variables.outputDir.absolutePath()));
  LOGFILE.Debug("Global Options:");
  LOGFILE.Debug("	All M2:					%i", variables.exportOptions.exportAllM2s);
  LOGFILE.Debug("	All WMOs:				%i", variables.exportOptions.exportAllWMOs);
  LOGFILE.Debug("	All ADTs:				%i", variables.exportOptions.exportAllADTs);
  LOGFILE.Debug("	All Textures:			%i", variables.exportOptions.exportAllTextures);
  LOGFILE.Debug("	Mirror Folders:			%i", variables.exportOptions.mirrorWoWFolders);
  LOGFILE.Debug("	Include Subdirectories:	%i", variables.exportOptions.includeSubdirectories);
  LOGFILE.Debug("	Overwrite Existing:		%i", variables.exportOptions.overwriteExistingFiles);
  LOGFILE.Debug("ADTs:");
  LOGFILE.Debug("	ADT Mesh:				%i", variables.exportOptions.ADTExportMesh);
  LOGFILE.Debug("	ADT Masks:				%i", variables.exportOptions.ADTExportMasks);
  LOGFILE.Debug("	ADT Heightmap:			%i", variables.exportOptions.ADTExportHeightmap);
  LOGFILE.Debug("	ADT Textures:			%i", variables.exportOptions.ADTExportTextures);
  LOGFILE.Debug("	ADT Submodels:			%i", variables.exportOptions.ADTExportSubmodels);
  LOGFILE.Debug("	ADT Placement:			%i", variables.exportOptions.ADTExportPlacement);
  LOGFILE.Debug("	ADT Single Mat:			%i", variables.exportOptions.ADTSingleMaterial);
  LOGFILE.Debug("	ADT Bake Single:		%i", variables.exportOptions.ADTBakeSingleTextures);
  LOGFILE.Debug("	ADT Use Middle:			%i", variables.exportOptions.ADTHeightmapUseMiddlepoint);
  LOGFILE.Debug("	ADT Heightap Res:		%i", variables.exportOptions.ADTHeightmapResolution);
  LOGFILE.Debug("WMOs:");
  LOGFILE.Debug("	WMO Mesh:				%i", variables.exportOptions.WMOExportMesh);
  LOGFILE.Debug("	WMO Textures:			%i", variables.exportOptions.WMOExportTextures);
  LOGFILE.Debug("	WMO Submodels:			%i", variables.exportOptions.WMOExportSubmodels);
  LOGFILE.Debug("	WMO Placement:			%i", variables.exportOptions.WMOExportPlacement);
  LOGFILE.Debug("M2s:");
  LOGFILE.Debug("	M2 Mesh:				%i", variables.exportOptions.M2ExportMesh);
  LOGFILE.Debug("	M2 Textures:			%i", variables.exportOptions.M2ExportTextures);
  LOGFILE.Debug("	M2 Highest LOD:			%i", variables.exportOptions.M2HighestRes);
  LOGFILE.Debug("	M2 Export All LODs:		%i", variables.exportOptions.M2ExportAllLODs);

  QStringList SearchList;
  QStringList fileList;
  
  if (variables.exportOptions.exportAllADTs)
    SearchList << "*_[0-9][0-9]_[0-9][0-9].adt";
  if (variables.exportOptions.exportAllWMOs)
    SearchList << "*.wmo";
  if (variables.exportOptions.exportAllM2s)
    SearchList << "*.m2";
  if (variables.exportOptions.exportAllTextures)
    SearchList << "*.blp";

  if (SearchList.isEmpty() == true)
  {
    LOGFILE.Error("No file search parameters entered. Aborting batch export...");
    return;
  }

  if (QFile::exists(variables.inputDir.absolutePath()) == false)
  {
    qCritical("Input Folder does not exist. Aborting batch export...");
    LOGFILE.Error("Input Folder does not exist. Aborting batch export...");
    return;
  }
  QDirIterator::IteratorFlag flags = QDirIterator::IteratorFlag::NoIteratorFlags;
  if (variables.exportOptions.includeSubdirectories == true)
    flags = QDirIterator::IteratorFlag::Subdirectories;

  QDirIterator dirIt(variables.inputDir.absolutePath(), SearchList, QDir::Filter::NoDotAndDotDot | QDir::Filter::Files, flags);

  if (!dirIt.hasNext())
  {
    qCritical("No files found to export...");
    LOGFILE.Error("No files found to export...");
    return;
  }

  // Create Progress Window
  progress->setWindowModality(Qt::WindowModal);
  progress->show();
  progress->setCurrentFilename("Looking for Files...");
  QApplication::processEvents();

  // Search for files to export
  uint32_t countADTs = 0;
  uint32_t countWMOs = 0;
  uint32_t countM2s = 0;
  uint32_t countBLPs = 0;

  while (dirIt.hasNext())
  {
    if (cancel == true) return;
    dirIt.next();
    QString path = dirIt.filePath().mid(variables.inputDir.absolutePath().length() + 1);
    fileList.push_back(path);
    QString filename = dirIt.fileName();
    QFileInfo fileInfo(filename);
    if (fileInfo.exists() == false) continue;
    if (fileInfo.size() <= 0) continue;

    if ((filename.contains(".adt", Qt::CaseInsensitive)) && (variables.exportOptions.exportAllADTs == true))
      countADTs++;
    else if ((filename.contains(".wmo", Qt::CaseInsensitive)) && (variables.exportOptions.exportAllWMOs == true || (variables.exportOptions.exportAllADTs == true && variables.exportOptions.ADTExportSubmodels == true)))
      countWMOs++;
    else if ((filename.contains(".m2", Qt::CaseInsensitive)) && (variables.exportOptions.exportAllM2s == true || (variables.exportOptions.exportAllADTs == true && variables.exportOptions.ADTExportSubmodels == true) || (variables.exportOptions.exportAllWMOs == true && variables.exportOptions.WMOExportSubmodels == true)))
      countM2s++;
    else if ((filename.contains(".blp", Qt::CaseInsensitive)) && (variables.exportOptions.exportAllTextures == true || (variables.exportOptions.exportAllADTs == true && variables.exportOptions.ADTExportTextures == true) || (variables.exportOptions.exportAllWMOs == true && variables.exportOptions.WMOExportTextures == true) || (variables.exportOptions.exportAllM2s == true && variables.exportOptions.M2ExportTextures == true)))
      countBLPs++;
  }
  progress->setProgressMax(countADTs + countWMOs + countM2s + countBLPs);
  QApplication::processEvents();

  uint32_t stepCount = 0;

  if (variables.exportOptions.exportAllADTs == true)
  {
    int stepMult = 1;

    // Mesh
    if (variables.exportOptions.ADTExportMesh)
      stepMult++;

    // Heightmap
    if (variables.exportOptions.ADTExportHeightmap)
      stepMult++;

    // Masks
    if (variables.exportOptions.ADTExportMasks)
      stepMult++;

    // SubModels
    if (variables.exportOptions.ADTExportSubmodels)
      stepMult++;

    // Textures
    if (variables.exportOptions.ADTExportSubmodels)
      stepMult++;

    // Placement
    if (variables.exportOptions.ADTExportSubmodels)
      stepMult++;

    stepCount += (countADTs*stepMult);
  }

  if (variables.exportOptions.exportAllWMOs == true)
  {
    int stepMult = 1;
    stepCount += (countWMOs*stepMult);
  }

  if (variables.exportOptions.exportAllM2s == true)
  {
    int stepMult = 1;
    stepCount += (countM2s*stepMult);
  }

  if (variables.exportOptions.exportAllTextures == true)
  {
    int stepMult = 2;
    stepCount += (countBLPs*stepMult);
  }

  emit(sendMax(stepCount));
  QApplication::processEvents();

  // Build progress bar window

  for (size_t i = 0; i < fileList.count();i++)
  {
    if (cancel == true) return;
    QString filename = fileList.at(i);
    QFileInfo fileInfo(filename);
    if (fileInfo.exists() == false) continue;
    if (fileInfo.size() <= 0) continue;
    emit(sendData(filename, currentStep));
    QApplication::processEvents();

    bool fileExported = exportFile(filename);

    if (fileExported == false)
    {
      if (variables.exportOptions.continueOnError == true)
      {
        LOGFILE.Error("Unable to export file: %s. Continuing batch export...", qPrintable(filename));
        continue;
      }
      else {
        LOGFILE.Error("Unable to export file: %s. Aborting batch export.", qPrintable(filename));
        QMessageBox::critical(nullptr, tr("Error exporting file"), tr("There was an unexpected error while exporting. Check log.txt for details."), QMessageBox::Ok);
        progress->hide();
        return;
      }
    }
  }
  progress->hide();
  LOGFILE.Info("Processing complete!");
}

void BatchExporter::cancelBatch()
{
  LOGFILE.Debug("Aborted! Setting Cancel to True...");
  cancel = true;
  progress->accept();
}

bool BatchExporter::ExportM2(M2 * model, QString filename)
{
  if (variables.exportOptions.M2ExportMesh == true)
  {
    if (model->isObjectBuilt() == false)
      model->buildModelObject();
  }

  return true;
}

bool BatchExporter::ExportWMO(WMO * model, QString filename)
{
  if (variables.exportOptions.WMOExportMesh == true)
  {
    if (model->isObjectBuilt() == false)
      model->buildModelObject();
  }

  return true;
}

bool BatchExporter::ExportADT(ADT * model, QString filename)
{
  if (variables.exportOptions.ADTExportMesh == true)
  {
    if (model->isObjectBuilt() == false)
      model->buildModelObject();
    bool a = exportModelObject(model->getModelScene(), filename);
    if (a == false)
    {
      return false;
    }
    iterateStep();
  }
  if (cancel == true) return true;

  // Heightmap
  if (variables.exportOptions.ADTExportHeightmap)
  {
    iterateStep();
  }
  if (cancel == true) return true;

  // Masks
  if (variables.exportOptions.ADTExportMasks)
  {
    iterateStep();
  }
  if (cancel == true) return true;

  // SubModels
  if (variables.exportOptions.ADTExportSubmodels)
  {
    iterateStep();
  }
  if (cancel == true) return true;

  // Textures
  if (variables.exportOptions.ADTExportSubmodels)
  {
    iterateStep();
  }
  if (cancel == true) return true;

  // Placement
  if (variables.exportOptions.ADTExportSubmodels)
  {
    iterateStep();
  }
  return true;
}

bool BatchExporter::ExportBLP(BLPInfo * blp, QString FileName)
{
  if (blp == NULL)
  {
    LOGFILE.Warn("BLP was empty. Aborting Save...");
    return false;
  }
  if (cancel == true) return true;
  PluginExportImage* SavingPlugin = nullptr;
  Kernel krnl = Kernel::instance();
  QList<ProjectSW::PluginExportImage*> *imgpluginList = KERNEL.getPluginExportImageList();
  if (variables.exportOptions.TextureFormat >= imgpluginList->count() || variables.exportOptions.TextureFormat < 0)
  {
    LOGFILE.Warn("Selected plugin was an invalid selection. Aborting Save...");
    return false;
  }
  SavingPlugin = imgpluginList->at(variables.exportOptions.TextureFormat);
  if (SavingPlugin == nullptr)
  {
    LOGFILE.Warn("Unable to find selected plugin. Aborting Save...");
    return false;
  }
  if (FileName.isEmpty())
  {
    LOGFILE.Warn("No filename specified. Aborting Save...");
    return false;
  }

  variables.outputDir.mkpath(FileName.mid(0, FileName.lastIndexOf("/")));
  FileName = FileName.mid(0, FileName.lastIndexOf(".") + 1);
  FileName.append(SavingPlugin->getFileExtensions().at(0));

  if (variables.exportOptions.overwriteExistingFiles == false && QFileInfo(FileName).exists() == true)
  {
    LOGFILE.Warn("File %s already exists, and Overwrite is False. Skipping...", qPrintable(FileName));
    return true;
  }
  if (cancel == true) return true;

  LOGFILE.Info("Saving image to file: %s", qPrintable(FileName));
  // change to return SaveFileErrorType::SEIT_PluginError if problem... 
  bool saved = SavingPlugin->writeImageFile(FileName, blp->getImageData(), 0, ProjectSW::IMGTYPE_RGBA);
  LOGFILE.Info("Saving image to file successful? %s", (saved == true ? "true" : "false"));
  return saved;
}

bool BatchExporter::exportModelObject(ModelScene * Scene, QString FileName)
{
  PluginExportModel* SavingPlugin = nullptr;
  Kernel krnl = Kernel::instance();
  QList<ProjectSW::PluginExportModel*> *meshpluginList = KERNEL.getPluginExportModelList();
  if (variables.exportOptions.ModelFormat >= meshpluginList->count() || variables.exportOptions.ModelFormat < 0)
  {
    LOGFILE.Warn("Selected plugin was an invalid selection. Aborting Save...");
    return false;
  }
  SavingPlugin = meshpluginList->at(variables.exportOptions.ModelFormat);
  if (SavingPlugin == nullptr)
  {
    LOGFILE.Warn("Unable to find selected plugin. Aborting Save...");
    return false;
  }
  if (FileName.isEmpty())
  {
    LOGFILE.Warn("No filename specified. Aborting Save...");
    return false;
  }

  FileName = FileName.mid(0, FileName.lastIndexOf(".") + 1);
  FileName.append(SavingPlugin->getFileExtensions().at(0));

  LOGFILE.Info("Saving mesh to file: %s", qPrintable(FileName));
  // change to return SaveFileErrorType::SEIT_PluginError if problem... 
  bool saved = SavingPlugin->writeFile(FileName, Scene, 0, true);
  return saved;
}

bool BatchExporter::exportFile(QString filename)
{
  LOGFILE.Debug("Processing Filename: %s", qPrintable(filename));
  QString inputFileName = variables.inputDir.absoluteFilePath(filename);
  //LOGFILE.Debug("Input Filename: %s", qPrintable(inputFileName));

  QString folderIndexString = QString("%1/").arg(variables.wowDir.absolutePath());
  //LOGFILE.Debug("folderIndexString: %s", qPrintable(folderIndexString));
  QString inputFileNameTruc = inputFileName.mid(inputFileName.lastIndexOf(folderIndexString)+folderIndexString.length());
  //LOGFILE.Debug("Tructated Input Filename: %s", qPrintable(inputFileNameTruc));
  QString FileName = variables.outputDir.absoluteFilePath(inputFileNameTruc);
  
  LOGFILE.Debug("Initial Filename: %s", qPrintable(FileName));
  if (variables.exportOptions.mirrorWoWFolders == false)
  {
    QString srcPath = filename.mid(filename.lastIndexOf("/"));
    LOGFILE.Debug("src pathname: %s", qPrintable(srcPath));
    FileName = variables.outputDir.absoluteFilePath(srcPath);
  }
  LOGFILE.Debug("Final Filename: %s", qPrintable(FileName));

  // ADTs
  if (variables.exportOptions.exportAllADTs == true && filename.toLower().contains(".adt"))
  {
    LOGFILE.Debug("Detected ADT: %s", qPrintable(filename));
    ADT *mesh = new ADT(variables.inputDir.absoluteFilePath(filename), variables.exportOptions.ADTSingleMaterial, true);
    iterateStep();
    if (mesh->isLoaded() == false)
    {
      LOGFILE.Error("Unable to load ADT: %s.", qPrintable(filename));
      return false;
    }

    if (variables.outputDir.exists(FileName.mid(0, FileName.lastIndexOf("/"))) == false)
      variables.outputDir.mkpath(FileName.mid(0, FileName.lastIndexOf("/")));
    bool success = ExportADT(mesh, FileName);
    delete mesh;
    if (success == false)
    {
      LOGFILE.Error("Unable to export ADT: %s.", qPrintable(FileName));
      return false;
    }
  }
  QApplication::processEvents();

  if (cancel == true) return false;
  // WMOs
  //if (variables.exportOptions.exportAllADTs == true && dirIt.fileName().toLower().contains(".wmo"))
  //{
  //	LOGFILE.Debug("Detected WMO: %s", qPrintable(filename));
  //	WMO *mesh = new WMO(filename);
  //	ExportWMO(mesh);
  //}
  QApplication::processEvents();

  if (cancel == true) return false;
  // M2s
  //if (variables.exportOptions.exportAllADTs == true && dirIt.fileName().toLower().contains(".m2"))
  //{
  //	LOGFILE.Debug("Detected M2: %s", qPrintable(filename));
  //	M2 *mesh = new M2(filename, wowVersion, true);
  //	ExportM2(mesh);
  //}
  QApplication::processEvents();

  if (cancel == true) return false;
  // BLPs
  if (variables.exportOptions.exportAllTextures == true && filename.toLower().contains(".blp"))
  {
    LOGFILE.Debug("Detected BLP: %s", qPrintable(inputFileName));
    BLPInfo *blp = ReadBLPFile(inputFileName);
    iterateStep();
    QApplication::processEvents();
    if (blp == NULL)
    {
      LOGFILE.Error("Loaded BLP is empty.");
      return false;
    }
    if (variables.outputDir.exists(FileName.mid(0, FileName.lastIndexOf("/"))) == false)
      variables.outputDir.mkpath(FileName.mid(0, FileName.lastIndexOf("/")));
    bool success = ExportBLP(blp, FileName);
    iterateStep();
    QApplication::processEvents();
    delete blp;
    if (success == false)
    {
      LOGFILE.Error("Unable to save BLP file.");
      return false;
    }
  }

  return true;
}
