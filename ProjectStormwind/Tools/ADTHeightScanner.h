#pragma once
#include <qlist.h>
#include <qdir.h>
#include <QCoreApplication>

class ADT;

class ADTHeightScanner
{
  Q_DECLARE_TR_FUNCTIONS(ADTHeightScanner)
public:
  double min = 999999999999999.0;
  double max = -999999999999999.0;

  void setFolder(QDir dir);
  void setFolder(QString dir);
  void setFilterPrefix(QString filter_prefix);

  void Scan();
  void Scan(QDir dir, QString filter_prefix = "");
  void Scan(QString dir, QString filter_prefix = "");

private:
  QList<ADT> ADTList;
  QDir CurrentScanFolder = NULL;
  QString FilterPrefix = "";

  void ScanMapBlock(ADT *adt);
};