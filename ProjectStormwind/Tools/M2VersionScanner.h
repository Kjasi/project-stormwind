#pragma once
#include <qdir.h>
#include <WoWFormats/Models/M2.h>
#include <QCoreApplication>

class M2VersionScanner
{
  Q_DECLARE_TR_FUNCTIONS(M2VersionScanner)
public:
  M2VersionScanner() {};
  ~M2VersionScanner() {};

  void setFolder(QDir dir);
  void setFolder(QString dir);

  void Scan(QString scan_dir, bool recursive = true);
  void Scan(QDir scan_dir, bool recursive = true);
  void Scan(bool recursive = true);

private:
  int min = 9999999999;
  int max = -1;
  QDir ScanFolder;

  void ScanM2File(M2* m2file);
  QStringList getM2List(QDir scan_dir, bool recursive = true);
};