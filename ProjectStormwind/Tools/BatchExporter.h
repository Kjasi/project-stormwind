#pragma once
#include <Foundation/Classes/ExportOptions.h>
#include <WoWFormats/Textures/BLP.h>
#include <WoWFormats/Models/M2.h>
#include <WoWFormats/WorldModels/WMO.h>
#include <WoWFormats/Landscape/ADT.h>
#include "BatchExporterProgressDialog.h"
#include <qobject.h>
#include <qfuturewatcher.h>

class BatchExporter : public QObject
{
  Q_OBJECT
public:
  BatchExporter(ExportVariables export_variables, cWoWVersion wow_version);
  ~BatchExporter() { progress->hide(); delete progress; };

  void StartProcess();

public slots:
  void cancelBatch();

signals:
  void sendMax(int value);
  void sendCurrent(int value);
  void sendFilename(QString value);
  void sendData(QString value, int current);

private:
  ExportVariables variables;
  cWoWVersion wowVersion;
  uint32_t currentStep = 0;
  QFutureWatcher<void> FutureWatcher;

  bool cancel = false;

  bool ExportM2(M2 *model, QString filename);
  bool ExportWMO(WMO *model, QString filename);
  bool ExportADT(ADT *model, QString filename);
  bool ExportBLP(BLPInfo *model, QString filename);
  bool exportModelObject(ModelScene *Scene, QString FileName);

  bool exportFile(QString filename);

  void iterateStep() {
    currentStep++;
    emit(sendCurrent(currentStep));
    progress->update();
  }

  BatchExporterProgressDialog *progress;
};