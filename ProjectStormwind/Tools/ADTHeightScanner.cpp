#include "ADTHeightScanner.h"
#include <WoWFormats/Landscape/ADT.h>
#include <QRegExp>
#include <QtWidgets/qmessagebox.h>

void ADTHeightScanner::setFolder(QString dir)
{
  setFolder(QDir(dir));
}

void ADTHeightScanner::setFilterPrefix(QString filter_prefix)
{
  FilterPrefix = filter_prefix;
}

void ADTHeightScanner::setFolder(QDir dir)
{
  CurrentScanFolder = dir;
}

void ADTHeightScanner::Scan(QString dir, QString filter_prefix)
{
  setFilterPrefix(filter_prefix);
  setFolder(dir);
  Scan();
}

void ADTHeightScanner::Scan(QDir dir, QString filter_prefix)
{
  setFilterPrefix(filter_prefix);
  setFolder(dir);
  Scan();
}

void ADTHeightScanner::Scan()
{
  LOGFILE.Info("Scanning folder for ADT files: %s", qPrintable(CurrentScanFolder.absolutePath()));
  if (CurrentScanFolder.absolutePath().isEmpty())
  {
    LOGFILE.Error("Current Scan Folder is not set. Aborting Scan.");
    return;
  }
  if (CurrentScanFolder.isReadable() == false)
  {
    LOGFILE.Error("Current Scan Folder is not readable. Aborting Scan.");
    return;
  }

  // Scan for ADT files, and put them into the list
  QStringList filterList;

  if (FilterPrefix.isNull() || FilterPrefix.isEmpty())
  {
    filterList.push_back("*.adt");
  }
  else {
    filterList.push_back(QString("%1*.adt").arg(FilterPrefix));
  }

  QStringList dirFileList = CurrentScanFolder.entryList(filterList, QDir::Files, QDir::SortFlag::Name);
  QStringList adtFileList;

  LOGFILE.Debug("dirFileList size: %i", dirFileList.count());

  for (size_t i = 0; i < dirFileList.count(); i++)
  {
    QString filename = dirFileList.at(i);
    //LOGFILE.Debug("Filename: %s", toCStr(filename));

    // Checking for other files
    QString FilenameNoExt = filename.left(filename.lastIndexOf('.'));
    //LOGFILE.Debug("No Ext Filename: %s", toCStr(FilenameNoExt));

    QRegExp re("(_obj\\d+)|(_tex\\d+)|(_lod)", Qt::CaseInsensitive);
    if (re.indexIn(filename) == -1)
    {
      QString fullFilename = CurrentScanFolder.absoluteFilePath(filename);
      LOGFILE.Debug("Checking for file: %s", qPrintable(fullFilename));
      QFile fpo(fullFilename);
      if (fpo.open(QIODevice::ReadOnly))
      {
        LOGFILE.Info("Found file: %s", qPrintable(fullFilename));
        adtFileList.push_back(fullFilename);
        fpo.close();
      }
      else {
        LOGFILE.Warn("Unable to open file: %s Reason: %s", qPrintable(fullFilename), qPrintable(fpo.errorString()));
      }
    }
  }

  // Scan each ADT file to update our data
  for (size_t i = 0; i < adtFileList.count(); i++)
  {
    ADT *mapFile = new ADT(adtFileList.at(i));
    ScanMapBlock(mapFile);
  }

  if (max <= -999999999999999.0)
  {
    LOGFILE.Error("Unable to get ADT Heights.");
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setWindowTitle(tr("Error Getting ADT Heights", "Error Messagebox Title"));
    msgBox.setText(tr("We were unable to find any ADT Heights.", "Error Message"));
    msgBox.exec();
  }
  else {
    QString result = QString(tr("ADT Height Range: %1 to %2", "Specifies the Height.")).arg(min).arg(max);
    LOGFILE.Info(qPrintable(result));
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowTitle(tr("ADT Height Result", "Result Title"));
    msgBox.setText(result);
    msgBox.exec();
  }

}

void ADTHeightScanner::ScanMapBlock(ADT *adt)
{
  if (adt->PointLowest < min)
  {
    min = adt->PointLowest;
  }
  if (adt->PointHighest > max)
  {
    max = adt->PointHighest;
  }
}