#include "M2VersionScanner.h"
#include "Foundation/LogFile.h"
#include <QtWidgets/qmessagebox.h>

QStringList scannedFolders;

void M2VersionScanner::setFolder(QDir dir)
{
  ScanFolder = dir;
}

void M2VersionScanner::setFolder(QString dir)
{
  setFolder(QDir(dir));
}

void M2VersionScanner::Scan(QString scan_dir, bool recursive)
{
  setFolder(scan_dir);
  Scan(recursive);
}

void M2VersionScanner::Scan(QDir scan_dir, bool recursive)
{
  setFolder(scan_dir);
  Scan(recursive);
}

void M2VersionScanner::Scan(bool recursive)
{
  LOGFILE.Info("M2 Scanner starting with Directory: %s", qPrintable(ScanFolder.absolutePath()));
  scannedFolders.clear();
  QStringList m2FileList = getM2List(ScanFolder, recursive);

  if (m2FileList.count() <= 0)
  {
    LOGFILE.Error("M2 list was empty. Unable to get versions.");
    return;
  }

  // Scan each M2 file to update our data
  for (size_t i = 0; i < m2FileList.count(); i++)
  {
    M2 *m2File = new M2(m2FileList.at(i), true, true);
    ScanM2File(m2File);
  }

  if (max == -1)
  {
    LOGFILE.Error("Unable to get M2 versions.");
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Critical);
    msgBox.setWindowTitle(tr("Error Getting M2 Versions", "Error Messagebox Title"));
    msgBox.setText(tr("We were unable to find any valid M2 versions.", "Error Message"));
    msgBox.exec();
    return;
  }
  if (min == max)
  {
    QString result = QString(tr("M2 Version: %1", "Specifies the version number.")).arg(min);
    LOGFILE.Info(qPrintable(result));
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowTitle(tr("M2 Version Result", "Result Title"));
    msgBox.setText(result);
    msgBox.exec();
  }
  else {
    QString result = QString(tr("M2 Version Range: %1 to %2", "Specifies the version numbers.")).arg(min).arg(max);
    LOGFILE.Info(qPrintable(result));
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Information);
    msgBox.setWindowTitle(tr("M2 Version Result", "Result Title"));
    msgBox.setText(result);
    msgBox.exec();
  }
}

void M2VersionScanner::ScanM2File(M2* m2file)
{
  // Silently fail if bad version number
  if (m2file->modelVersion <= 0 || m2file->modelVersion >= 99999)
  {
    return;
  }

  if (m2file->modelVersion < min)
  {
    min = m2file->modelVersion;
  }
  if (m2file->modelVersion > max)
  {
    max = m2file->modelVersion;
  }
}

QStringList M2VersionScanner::getM2List(QDir scan_dir, bool recursive)
{
  if (scannedFolders.contains(scan_dir.absolutePath()) == true) return QStringList();

  //LOGFILE.Info("Scanning folder for M2 files: %s", qPrintable(scan_dir.absolutePath()));
  if (scan_dir.absolutePath().isEmpty())
  {
    LOGFILE.Error("Current Scan Folder is not set. Aborting Scan.");
    return QStringList();
  }
  if (scan_dir.isReadable() == false)
  {
    LOGFILE.Error("Current Scan Folder is not readable. Aborting Scan.");
    return QStringList();
  }

  QStringList filterList;
  filterList.push_back("*.m2");

  QStringList fileList = scan_dir.entryList(filterList, QDir::Files, QDir::SortFlag::Name);
  QStringList dirList = scan_dir.entryList(QDir::Dirs, QDir::SortFlag::Name);
  dirList.removeFirst();	// remove .
  dirList.removeFirst();	// remove ..
  QStringList m2FileList;

  //LOGFILE.Debug("fileList size: %i", fileList.count());
  //LOGFILE.Debug("dirList size: %i | %s", dirList.count(), qPrintable(dirList.join(", ")));

  for (size_t i = 0; i < fileList.count(); i++)
  {
    QString filename = fileList.at(i);
    //LOGFILE.Debug("Filename: %s", qPrintable(filename));

    QString fullFilename = scan_dir.absoluteFilePath(filename);
    //LOGFILE.Debug("Checking for file: %s", qPrintable(fullFilename));
    QFile fpo(fullFilename);
    if (fpo.open(QIODevice::ReadOnly))
    {
      //LOGFILE.Info("Found file: %s", qPrintable(fullFilename));
      m2FileList.push_back(fullFilename);
      fpo.close();
    }
    else {
      LOGFILE.Warn("Unable to open file: %s Reason: %s", qPrintable(fullFilename), qPrintable(fpo.errorString()));
    }
  }

  scannedFolders.append(scan_dir.absolutePath());

  if (recursive == false || dirList.count() <= 0)
  {
    return m2FileList;
  }

  for (size_t i = 0; i < dirList.count(); i++)
  {
    QString dstr = scan_dir.absolutePath();
    dstr.append(QDir::separator());
    dstr.append(dirList.at(i));
    QDir dir = dstr;
    m2FileList << getM2List(dir, recursive);
  }

  return m2FileList;
}
