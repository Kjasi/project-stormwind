#include <QtWidgets/QApplication>
#include <Foundation/Core/Kernel.h>
#include "projectstormwind.h"

int main(int argc, char *argv[])
{
  LOGFILE.setFile(LOG_FILENAME);
  LOGFILE.truncateFile();
  LOGFILE.Info("Starting Project Stormwind, Version %s", PROJECTSTORMWIND_VERSION);
  QApplication a(argc, argv);

  // Initalize our globals
  KERNEL;
  KERNEL.init(PLUGIN_PATH);

  QVulkanInstance inst;
#ifndef Q_OS_ANDROID
  inst.setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");
#else
  inst.setLayers(QByteArrayList()
    << "VK_LAYER_GOOGLE_threading"
    << "VK_LAYER_LUNARG_parameter_validation"
    << "VK_LAYER_LUNARG_object_tracker"
    << "VK_LAYER_LUNARG_core_validation"
    << "VK_LAYER_LUNARG_image"
    << "VK_LAYER_LUNARG_swapchain"
    << "VK_LAYER_GOOGLE_unique_objects");
#endif

  if (!inst.create()) {
    qFatal("Failed to create Vulkan instance: %d", inst.errorCode());
    LOGFILE.Error("Failed to create Vulkan instance: %d", inst.errorCode());
  }

  QStringList locales;
  locales << "enUS";

  LOGFILE.Info("Installing localizations...");

  for (int l = 0; l < locales.count(); l++)
  {
    QString locale = locales.at(l);
    QTranslator *translator = new QTranslator();
    bool loaded = translator->load(":/Translations/foundation_" + locale + ".qm");
    
    if (loaded == false || translator->isEmpty() == true)
    {
      LOGFILE.Warn("Unable to load locale \"%s\" for Project Stormwind's Foundation.", qPrintable(locale));
      continue;
    }
    a.installTranslator(translator);
    loaded = false;

    translator = new QTranslator();
    loaded = translator->load(":/Translations/psw_" + locale + ".qm");

    if (loaded == false || translator->isEmpty() == true)
    {
      LOGFILE.Warn("Unable to load locale \"%s\" for Project Stormwind.", qPrintable(locale));
      continue;
    }
    a.installTranslator(translator);
  }

  for (int i = 0; i < KERNEL.nbChildren(); i++)
  {
    ProjectSW::Plugin *piface = KERNEL.getChild(i);
    LOGFILE.Debug("Adding Locales for plugin \"%s\"...", qPrintable(piface->getPluginName()));
    for (int l = 0; l < locales.count(); l++)
    {
      QString locale = locales.at(l);

      QTranslator *t = piface->pluginTranslator(locale);
      if (t == nullptr || t->isEmpty() == true)
      {
        LOGFILE.Warn("Unable to load the Plugin \"%s\"'s locale \"%s\"...", qPrintable(piface->getPluginName()), qPrintable(locale));
        continue;
      }
      a.installTranslator(t);
    }
  }


  ProjectStormwind w(&inst);
  w.show();
  return a.exec();
}