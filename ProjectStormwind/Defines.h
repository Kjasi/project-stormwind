#pragma once
#include <Foundation/Constants.h>

// Project Version
#define PROJECTSTORMWIND_TITLE QObject::tr("Project Stormwind")
#define PROJECTSTORMWIND_VERSION "0.0.10 Alpha"

// Program defines
#define LOG_FILENAME	"log.txt"
#define PLUGIN_PATH		"./plugins/"	// Relative to the application